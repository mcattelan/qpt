'use strict';
app.directive("pharmacoVigilanceExportModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=?pharmacoVigilanceExportModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/pharmacoVigilance-export-filter-modal.html',
					controller: function($scope, $http, RESTURL, MessageService, FormUtilService) {
						$scope.init = function() {
							$scope.errors = {};
							$scope.detail = {};
						}
						
						$scope.generateExport = function() {
							$scope.errors = $scope.validate();
							if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
								var allRequest = {
										search : { 
											filters : {
												'DATE_FROM' : $scope.fromDate,
												'DATE_TO' : $scope.toDate
											} 
										},
										xlsExportTypes : ['RICONCILIAZIONE_SAFETY']
								};
								$http.post(RESTURL.STORED_EXPORT+'/' + 'generate', allRequest, {responseType:'arraybuffer'}).success(function (response) {
									MessageService.showSuccess('L\'Export richiesto è in generazion, a breve sarà; disponibile', '');
									$scope.modal.close();
								}).error(function(data, status, headers, config) {
									MessageService.showError('Generazione xls non riuscita', '');
								});
							}
						}

						$scope.validate = function() {
							var errors = {};
							var form = {
									formProperties : [
										{ id: 'fromDate', value : $scope.fromDate, type: 'string', required: true },
										{ id: 'toDate', value : $scope.toDate, type: 'string', required: true }
										]
							};
							errors = FormUtilService.validateForm(form);
							return errors;
						}
						
						$scope.init();
					}
				});
				
				scope.close = function() {
					scope.modal.close();
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("phuturemedModal", function($modal, $ocLazyLoad, $state, $rootScope, $filter) {
	return {
		restrict: 'A',
		scope : {
			id : "=phuturemedModal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
			activitiForm: '=?activitiForm',
		},
		link: function (scope, element, attrs) {
		
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/phuturemed/phuturemed.html',
					controller: 'PhuturemedController',
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/directives/crm-phuturemed.js'] );
									}
							)
						}
					}
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});

app.directive("phuturemedStates", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			phuturemedId: "=?phuturemedStates",
			patientId: "=?patientId",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/phuturemed/state/phuturemed-states.html',
		link: function(scope, element, attrs) {
			
			scope.constants = angular.copy($rootScope.constants);
			
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-phuturemed-state.js']).then( function() {
				var controller = $controller('PhuturemedStateController', { $scope: scope });
			});
		}
	}
});

app.directive("hcpDashboard", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			hcpId: "="
		},
		templateUrl: 'tpl/app/directive/dashboard/dashboard_HCP.html',
		link: function(scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['ui.select','js/controllers/crm-stats.js']).then( function() {
				scope.searchFilters = {
						filters: {
							'HCP_ID': scope.hcpId 
						}
				}
				scope.filters = scope.searchFilters.filters;
				var controller = $controller('StatsController', { $scope: scope });
			});
		}
	}
});

app.directive("diaryJournal", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?diaryJournal",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/patient/journal.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['js/controllers/crm-diary.js']).then( function() {
				var controller = $controller('DiaryController', { $scope: scope });
			});
		}
	}
});

app.directive("startUserServiceModalCustom", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id: "=startUserServiceModalCustom",
			userId: "=userId",
			recursiveDelayTime: "=?recursiveDelayTime",
			recursiveDelayTimeUnit: "=?recursiveDelayTimeUnit",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.constants = angular.copy($rootScope.constants);
				scope.popup = true;
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/user-service/user-service-start.html',
					controller: function($scope, $rootScope, ServiceConfService, UserServiceConfService, MessageService) {
						
						$scope.init = function() {
							$scope.errors = {};
							$scope.detail = {};
							$scope.detail.startDate = {};
							$scope.detail.recursiveDelayTime = {};
							$scope.detail.recursiveDelayTimeUnit = {};
							
							ServiceConfService.getService({ id:$scope.id }, function(result) {
								$scope.detail = result.data;
								$scope.fieldPermissions = result.fieldPermissions;
							}, function(error) {
								$scope.detail = {};
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});
						}	
						
						$scope.startUserServiceManuallyCustom = function(serviceId) {
							$scope.errors = {};
							if($scope.detail.startDate == undefined) {
								$scope.errors['detail.startDate'] =  "E' necessario inserire una data di avvio del servizio";
							}
							
							if(angular.isDefined($scope.detail.code) && $scope.detail.code == 'QUIPERTE_DRUG_PICK_UP_AND_DELIVERY') {
								if($scope.detail.recursiveDelayTime == undefined) {
									$scope.errors['detail.recursiveDelayTime'] = "E' necessario impostare la frequenza di consegna del farmaco";
								} else if($scope.detail.recursiveDelayTime <= 0) {
									$scope.errors['detail.recursiveDelayTime'] = "E' necessario impostare un valore per la frequenza di consegna del farmaco maggiore di zero";
								}
								if($scope.detail.recursiveDelayTimeUnit == undefined) {
									$scope.errors['detail.recursiveDelayTimeUnit'] = "E' necessario impostare l'unità di misura per la frequenza di consegna del farmaco";
								}
							}
							
							if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
								var request = {
										start: $scope.detail.startDate,
										recursiveDelayTime: $scope.detail.recursiveDelayTime,
										recursiveDelayTimeUnit: $scope.detail.recursiveDelayTimeUnit
								};

								UserServiceConfService.startUserServiceManuallyCustom({ userId : $scope.userId, serviceId: serviceId}, request, function(result) {
									MessageService.showSuccess('Servizio avviato con successo');
									$state.reload();

								}, function(error) {
									if (error.status == 404) {
										$state.go('access.not-found');
									} else {
										MessageService.showError('Errore in fase di salvataggio', error.message !== undefined ? error.message : 'Errore imprevisto');
									}
								});
								$scope.close();
							} else {
								MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
							}
						}
						
						$scope.init();
					},
				});
				
				scope.close = function(value) {
					scope.modal.close(value);
					if(scope.reload) {
						$state.go($state.current, {}, { reload: true });
					}
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
				
			});
		}
	};
});

app.directive("phuturemedList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = ['ui.select','angularFileUpload','smart-table', 'js/controllers/directives/crm-phuturemed.js'];
		$scope.templateUrl = 'tpl/app/directive/phuturemed/phuturemed-list.html';
		$scope.controllerName = 'PhuturemedListCtrl';
		$scope.modalSize = 'xl';
	} });
});

app.directive("quiperteDiary", function($ocLazyLoad, $state, $rootScope, $controller) {
	return {
		restrict: 'A',
		scope : {
			id : "=?quiperteDiary",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss",
		},
		templateUrl: 'tpl/app/directive/patient/quiperte-diary.html',
		link: function (scope, element, attrs) {
			scope.constants = angular.copy($rootScope.constants);
			$ocLazyLoad.load(['js/controllers/crm-diary.js']).then( function() {
				var controller = $controller('DiaryControllerCustom', { $scope: scope });
			});
		}
	}
});

app.directive("quiperteAdherence", function(StatsService) {
	return {
		restrict: 'A',
		scope : {
			filters: "=",
			refresh: "=?",
			options: "=?",
		},
		templateUrl: 'tpl/app/directive/chart/quiperte-adherence.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.renderer = false;
				$scope.id = Date.now();
				$scope.search(angular.copy($scope.filters));
			}
			
			$scope.search = function(filters) {
				$scope.renderer = false;
				$('#spinner_' + $scope.chartType + "_" + $scope.id).show();
				$('#pie_' + $scope.chartType + "_" + $scope.id).hide();
				$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
				
				delete $scope.message;
				$scope.datas = [];
				var searchFilters = {
					'chartTypes': ['QUIPERTE_PATIENTS_ADHERENCE'],
					'filters': filters
				};
				StatsService.search({}, searchFilters, function(result) {
					$scope.refresh = false;
					$scope.datas = [];
					if (result.data[0].series[0].values) {
						for(var x = 0; x < result.data[0].series.length; x++) {
							$scope.datas.push(result.data[0].series[x]);
						}
					}
					if($scope.datas && $scope.datas.length > 0) {
						
					}
					$('#pie_' + $scope.chartType + "_" + $scope.id).show();
					$('#spinner_' + $scope.chartType + "_" + $scope.id).hide();
					$('#nodata_' + $scope.chartType + "_" + $scope.id).hide();
					$scope.renderer = true;
					
				},function(error) {
					console.log(error)
				});
			}
			
			$scope.$watch("refresh", function(newValue, oldValue) {
				if ($scope.refresh == true) {
					$scope.search(angular.copy($scope.filters));
				}
			}, true);
			
		},
		link: function(scope) {
			scope.init();
		}
	}
});

app.directive("hcpCustomPatientTherapies", function($rootScope, $ocLazyLoad, $controller) {
	return {
		restrict: 'A',
		scope: {
			patientId: '=hcpCustomPatientTherapies',
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		templateUrl: 'tpl/app/directive/patient/hcp-custom-patient-therapies.html',
		link: function(scope, element, attrs) {
			
			scope.constants = angular.copy($rootScope.constants);
			
			$ocLazyLoad.load(['ui.select','smart-table','js/controllers/directives/crm-hcp-custom-patient-therapies.js']).then( function() {
				var controller = $controller('HcpCustomPatientTherapiesController', { $scope: scope });
			});
		}
	}
});