'use strict';

app.directive("formatDate", ['$filter', 'DATE_TIME_FORMATS', function($filter, DATE_TIME_FORMATS) {
	return {
		require: 'ngModel',
		scope: {
			options: '=formatDate',
			format: '@'
		},
		restrict: 'A',
		link: function (scope, element, attrs, ngModel) {
			
			moment.locale('it');
			var format = (scope.format==undefined || scope.format==null || scope.format=='') ? DATE_TIME_FORMATS.DATE : scope.format;
			var timezone = 'Europe/Rome';

			var pickerOptions = {
					format: format,
					defaultDate: undefined,
					useCurrent: false,
					ignoreReadonly: true,
					showClear: true,
					showClose: true,
					showTodayButton: true,
					locale: 'it'
			}
			if(scope.options !== undefined && angular.isObject(scope.options)) {
				delete scope.options.format;
				angular.extend(pickerOptions, scope.options);
			}

			var dt = element.closest('div');
			dt.datetimepicker(pickerOptions);
			dt.on("dp.show", function(e) {
				if(element.is(':disabled')) {
					dt.data("DateTimePicker").hide();
				}
				else {
					if(ngModel !== undefined && ngModel != null && ngModel.$modelValue !== undefined && ngModel.$modelValue != null) {
						var defaultDate = new Date(ngModel.$modelValue);
						dt.data("DateTimePicker").date(defaultDate);
						dt.data("DateTimePicker").date(defaultDate);
					}
				}
			});
			dt.on("dp.change", function(e) {
				if(dt.data("DateTimePicker") && dt.data("DateTimePicker").date() !== undefined && dt.data("DateTimePicker").date() != null) {
					ngModel.$setViewValue(dt.data("DateTimePicker").date().utc().valueOf());
				}
				else {
					ngModel.$setViewValue(null);
				}
			});

			ngModel.$parsers.push(function(data) {
				var timestampUTC = undefined;
				if(data !== undefined && data != null && data != '') {
					if(angular.isDate(data)) {
						timestampUTC = data.getTime();
					}
					else if(angular.isNumber(data)) {
						// Considero che sia già in UTC
						timestampUTC = data;
					}
					else {
						var momentDate = moment(data, format).tz(timezone);
						timestampUTC = momentDate.valueOf();
					}
				}
				return timestampUTC;
			});

			ngModel.$formatters.push(function(data) {
				var view = $filter('formatDate')(data);
				return view;
			});
		}
	};
}]);


app.directive('formatDateTime', ['$filter', 'DATE_TIME_FORMATS', function($filter, DATE_TIME_FORMATS) {
	return {
		require: 'ngModel',
		scope: {
			options: '=formatDateTime',
			defaultTime: '='
		},
		restrict: 'A',
		link: function (scope, element, attrs, ngModel) {
			moment.locale('it');
			var format = DATE_TIME_FORMATS.DATETIME;
			var timezone = 'Europe/Rome';

			var pickerOptions = {
					format: format,
					defaultDate: undefined,
					useCurrent: false,
					ignoreReadonly: true,
					showClear: true,
					showClose: true,
					showTodayButton: true,
					locale: 'it'
			}
			if(scope.options !== undefined && angular.isObject(scope.options)) {
				delete scope.options.format;
				angular.copy(scope.options, pickerOptions);
			}

			var prevEmpty = true;
			var timeOpen = false;
			var dt = element.closest('div');
			dt.datetimepicker(pickerOptions);

			dt.on("dp.show", function(e) {
				if(element.is(':disabled')) {
					dt.data("DateTimePicker").hide();
				}
				else {
					if(ngModel !== undefined && ngModel != null && ngModel.$modelValue !== undefined && ngModel.$modelValue != null) {
						prevEmpty = false;
						var defaultDate = new Date(ngModel.$modelValue);
						dt.data("DateTimePicker").date(defaultDate);
						dt.data("DateTimePicker").date(defaultDate); // Lasciare, altrimenti il giorno non viene evidenziato
					}
					dt.find('a[data-action="today"]').click(function() {
						prevEmpty = false;
					});
					dt.find('a[data-action="togglePicker"]').click(function() {
						timeOpen = !timeOpen;
						if(timeOpen) {
							prevEmpty = false;
						}
						else {
							prevEmpty = true;
							if(ngModel !== undefined && ngModel != null && ngModel.$modelValue !== undefined && ngModel.$modelValue != null) {
								prevEmpty = false;
							}
						}
					});
				}
			});

			dt.on("dp.change", function(e) {
				if(dt.data("DateTimePicker") && dt.data("DateTimePicker").date() !== undefined && dt.data("DateTimePicker").date() != null) {
					if(prevEmpty) {
						var defaultDate = new Date(dt.data("DateTimePicker").date().valueOf());
						var defaultHours = 9;
						var defaultMinutes = 0;
						var defaultSeconds = 0;
						var defaultMilliseconds = 0;
						if(scope.defaultTime) {
							defaultHours = scope.defaultTime.defaultHours != undefined ? scope.defaultTime.defaultHours : 9;
							defaultMinutes = scope.defaultTime.defaultMinutes != undefined ? scope.defaultTime.defaultMinutes : 0;
							defaultSeconds = scope.defaultTime.defaultSeconds != undefined ? scope.defaultTime.defaultSeconds : 0;
							defaultMilliseconds = scope.defaultTime.defaultMilliseconds != undefined ? scope.defaultTime.defaultMilliseconds : 0;
						}
						defaultDate.setHours(defaultHours);
						defaultDate.setMinutes(defaultMinutes);
						defaultDate.setSeconds(defaultSeconds);
						defaultDate.setMilliseconds(defaultMilliseconds);
						dt.data("DateTimePicker").date(defaultDate);
					}
					else {
						var defaultDate = new Date(dt.data("DateTimePicker").date().valueOf());
						dt.data("DateTimePicker").date(defaultDate);
					}
					ngModel.$setViewValue(dt.data("DateTimePicker").date().utc().valueOf());
					prevEmpty = false;
				}
				else {
					ngModel.$setViewValue(null);
					prevEmpty = true;
				}
			});

			ngModel.$parsers.push(function(data) {
				var timestampUTC = undefined;
				if(data !== undefined && data != null && data != '') {
					if(angular.isDate(data)) {
//						timestampUTC = data.getTime() + (data.getTimezoneOffset() * 60000);
						timestampUTC = data.getTime();
					}
					else if(angular.isNumber(data)) {
						// Considero che sia già in UTC
						timestampUTC = data;
					}
					else {
						var momentDate = moment(data, format).tz(timezone);
						timestampUTC = momentDate.valueOf();
					}
				}
				return timestampUTC;
			});

			ngModel.$formatters.push(function(data) {
				var view = $filter('formatDateTime')(data);
				return view;
			});
		}
	};
}]);

app.directive("formatTime", ['$filter', function($filter) {
	return {
		require: 'ngModel',
		scope: {
			options: '=formatTime'
		},
		restrict: 'A',
		link: function (scope, element, attrs, ngModel) {
			moment.locale('it');
			var format = 'HH:mm';
			var timezone = 'Europe/Rome';

			var pickerOptions = {
					format: format,
					useCurrent: false,
					defaultDate: undefined,
					ignoreReadonly: true,
					showClear: true,
					showClose: true,
					locale: 'it'

			}
			if(scope.options !== undefined && angular.isObject(scope.options)) {
				delete scope.options.format;
				angular.copy(scope.options, pickerOptions);
			}

			var dt = element.closest('div');
			dt.datetimepicker(pickerOptions);
			dt.on("dp.show", function(e) {
				if(element.is(':disabled')) {
					dt.data("DateTimePicker").hide();
				}
				else {
					if(ngModel !== undefined && ngModel != null && ngModel.$modelValue !== undefined && ngModel.$modelValue != null) {
						var defaultDate = new Date();
						defaultDate.setHours(0);
						defaultDate.setMinutes(0);
						defaultDate.setSeconds(0);
						defaultDate.setMilliseconds(0);
						defaultDate = new Date(defaultDate.getTime() + ngModel.$modelValue*60000);
						dt.data("DateTimePicker").date(defaultDate);
					}
					else {
						if(pickerOptions.defaultDate === undefined || pickerOptions.defaultDate == null) {
							var defaultDate = new Date();
							defaultDate.setHours(9);
							defaultDate.setMinutes(0);
							defaultDate.setSeconds(0);
							defaultDate.setMilliseconds(0);
							dt.data("DateTimePicker").date(defaultDate);
						}
					}
				}
			});
			dt.on("dp.change", function(e) {
				if(dt.data("DateTimePicker") && dt.data("DateTimePicker").date() !== undefined && dt.data("DateTimePicker").date() != null) {
					ngModel.$setViewValue(dt.data("DateTimePicker").date());
				} else {
					ngModel.$setViewValue(null);
				}
			});

			ngModel.$parsers.push(function(data) {
				var minutesFromMidnight = undefined;
				if(data !== undefined && data != null && data != '') {
					if(angular.isDate(data)) {
						var midnight = new Date(data.getTime());
						midnight.setHours(0);
						midnight.setMinutes(0);
						midnight.setSeconds(0);
						midnight.setMilliseconds(0);
						data.setSeconds(0);
						data.setMilliseconds(0);
//						timestampUTC = date.getTime() + (date.getTimezoneOffset() * 60000);
						minutesFromMidnight = (data.getTime() - midnight.getTime()) / 1000 / 60;
					}
					else if(angular.isNumber(data)) {
						// Considero che sia già in UTC
						minutesFromMidnight = data;
					}
					else if(angular.isObject(data) && data._isAMomentObject) {
						var midnight = new Date(data.valueOf());
						midnight.setHours(0);
						midnight.setMinutes(0);
						midnight.setSeconds(0);
						midnight.setMilliseconds(0);

						var dateMinutes = new Date(data.valueOf());
						dateMinutes.setSeconds(0);
						dateMinutes.setMilliseconds(0);
//						timestampUTC = date.getTime() + (date.getTimezoneOffset() * 60000);
						minutesFromMidnight = (dateMinutes.getTime() - midnight.getTime()) / 1000 / 60;
					}
					else {
						if(!isNaN(data)) {
							minutesFromMidnight = parseInt(data, 10);
						}
						else {
							minutesFromMidnight = null;
						}
					}
				}
				return minutesFromMidnight;
			});

			ngModel.$formatters.push(function(data) {
				var view = $filter('formatTime')(data);
				return view;
			});
		}
	};
}]);

app.directive('bindHtmlCompile', ['$compile', function ($compile) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			scope.$watch(function () {
				return scope.$eval(attrs.bindHtmlCompile);
			}, function (value) {
				// Incase value is a TrustedValueHolderType, sometimes it
				// needs to be explicitly called into a string in order to
				// get the HTML string.
				element.html(value && value.toString());
				// If scope is provided use it, otherwise use parent scope
				var compileScope = scope;
				if (attrs.bindHtmlScope) {
					compileScope = scope.$eval(attrs.bindHtmlScope);
				}
				$compile(element.contents())(compileScope);
			});
		}
	};
}]);

app.directive("compileContent", ['$compile', function($compile) {
	return {
		restrict: 'A',
		scope : {},
		link: function (scope, element, attrs, ngModel) {
			$compile(element.contents())(scope);
		}
	};
}]);

app.directive('avJson', function () {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, element, attrs, ngModel) {
			function into(input) {
				return input;
			}
			function out(data) {
				if(data !== undefined && data != null) {
					if(angular.isString(data)) {
						return JSON.stringify(JSON.parse(data), undefined, '\t');
					}
					else {
						return JSON.stringify(data, undefined, '\t');
					}
				}
				else {
					return data;
				}
			}
			ngModel.$parsers.push(into);
			ngModel.$formatters.push(out);

			element.on('blur', function () {
				try {
					var formatted = out(ngModel.$modelValue);
					ngModel.$setViewValue(formatted);
					element.val(formatted);
					element.change();
				}
				catch(e) {

				}
			});
		}
	};
});

app.directive('integer', function(){
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ngModel){
			ngModel.$parsers.unshift(function(viewValue){
				var newValue = '';
				if(viewValue != null && viewValue != '') {
					newValue = ngModel.$modelValue;
					if(isNaN(viewValue)) {
						ngModel.$setViewValue(newValue);
					}
					else {
						newValue = parseInt(viewValue, 10);
					}
					element.val(newValue);
					element.change();
				}
				return newValue;
			});
		}
	};
});

app.directive('float', function(){
	return {
		require: 'ngModel',
		link: function(scope, element, attr, ngModel){
			ngModel.$parsers.unshift(function(viewValue){
				var newValue = ngModel.$modelValue;
				if(isNaN(viewValue)) {
					ngModel.$setViewValue(newValue);
					element.val(newValue);
					element.change();
				}
				else {
					newValue = parseFloat(viewValue);
				}
				return newValue;
			});
		}
	};
});

app.directive('checkList', function() {
	return {
		scope: {
			list: '=checkList',
			onlyOne: '@onlyOne',
			value: '@'
		},
		link: function(scope, elem, attrs) {
			var handler = function(setup) {
				scope.onlyOne = scope.onlyOne || false;
				
				if(scope.onlyOne) {
					scope.list = scope.list || undefined;
					var checked = elem.prop('checked');
					var index = ''+scope.list === ''+scope.value;
					if (checked && !index) {
						if (setup) {
							elem.prop('checked', false);
						}
						else {
							scope.list = scope.value;
						}
					} else if (!checked && index) {
						if (setup) {
							elem.prop('checked', true);
						}
						else {
							scope.list = undefined;
						}
					} else if (!setup && checked && index) {
						elem.prop('checked', false);
						scope.list = undefined;
					}
				}
				else {
					scope.list = scope.list || [];
					var stringList = [];
					
					for(var k=0; k<scope.list.length; k++) {
						stringList.push(''+scope.list[k]);
					}
					scope.list = angular.copy(stringList);
					
					var checked = elem.prop('checked');
					var index = scope.list.indexOf(scope.value);
					if (checked && index == -1) {
						if (setup) {
							elem.prop('checked', false);
						}
						else {
							scope.list.push(scope.value);
						}
					} else if (!checked && index != -1) {
						if (setup) {
							elem.prop('checked', true);
						}
						else {
							scope.list.splice(index, 1);
						}
					}
				}
			};

			var setupHandler = handler.bind(null, true);
			var changeHandler = handler.bind(null, false);
			
			elem.bind('dblclick', function() {
				scope.$apply(changeHandler);
			});
			
			elem.bind('click', function() {
				scope.$apply(changeHandler);
			});

			scope.$watch('list', setupHandler, true);
		}
	};
});

app.directive('checkListObject', function() {
	return {
		scope: {
			list: '=checkListObject',
			onlyOne: '@onlyOne',
			value: '=?'
		},
		link: function(scope, elem, attrs) {
			var handler = function(setup) {
				scope.onlyOne = scope.onlyOne || false;
				
				if(scope.onlyOne) {
					scope.list = scope.list || undefined;
					var checked = elem.prop('checked');
					var index = ''+scope.list === ''+scope.value;
					if (checked && !index) {
						if (setup) {
							elem.prop('checked', false);
						}
						else {
							scope.list = scope.value;
						}
					} else if (!checked && index) {
						if (setup) {
							elem.prop('checked', true);
						}
						else {
							scope.list = undefined;
						}
					} else if (!setup && checked && index) {
						elem.prop('checked', false);
						scope.list = undefined;
					}
				}
				else {
					scope.list = scope.list || [];
					var stringList = [];
					
					for(var k=0; k<scope.list.length; k++) {
						stringList.push(scope.list[k]);
					}
					scope.list = angular.copy(stringList);
					
					var checked = elem.prop('checked');
					var index = -1;
					if(angular.isObject(scope.value) && scope.value.id) {
						for(var i=0;i<scope.list.length; i++) {
							if(scope.list[i].id === scope.value.id) {
								index = i;
								break;
							}
						}
					}
					else {
						index = scope.list.indexOf(scope.value);
					}
					if (checked && index == -1) {
						if (setup) {
							elem.prop('checked', false);
						}
						else {
							scope.list.push(scope.value);
						}
					} else if (!checked && index != -1) {
						if (setup) {
							elem.prop('checked', true);
						}
						else {
							scope.list.splice(index, 1);
						}
					}
				}
			};

			var setupHandler = handler.bind(null, true);
			var changeHandler = handler.bind(null, false);
			
			elem.bind('dblclick', function() {
				scope.$apply(changeHandler);
			});
			
			elem.bind('click', function() {
				scope.$apply(changeHandler);
			});

			scope.$watch('list', setupHandler, true);
		}
	};
});

app.directive("userModal", function($modal, $ocLazyLoad, $state, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id: "=userModal",
			userParent: "=?userParent",
			userType: "=userType",
			postFn : "=?postFn",
			postFnDismiss : "=?postFnDismiss"
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.closeButton = true;
				scope.popup = true;
				scope.constants = angular.copy($rootScope.constants);
				switch(scope.userType) {
			    case 'SUPPLIER':
			        scope.userController = "SupplierUserController";
			        scope.userControllerPath = ["js/controllers/crm-supplier-user.js"];
			        scope.userTemplate = "tpl/app/supplier/user.html";
			        scope.supplierId = scope.userParent;
			        break;
			    case 'CUSTOMER':
			    	scope.userController = "CustomerUserController";
			    	scope.userControllerPath = ["js/controllers/crm-customer-user.js"];
			    	scope.userTemplate = "tpl/app/customer/user.html";
			    	scope.customerId = scope.userParent;
			        break;
			    case 'HCP':
			    	scope.userController = "HcpController";
			    	scope.userControllerPath = ["js/controllers/crm-hcp.js"];
			    	scope.userTemplate = "tpl/app/hcp/hcp.html";
			    	break;
			    case 'PATIENT':
			    	scope.userController = "PatientController";
			    	scope.userControllerPath = ['js/controllers/crm-patient.js'] ;
			    	scope.userTemplate = "tpl/app/patient/patient.html";
			    	break;
			    case 'PATIENT_USER':
			    	scope.userController = "PatientUserController";
			    	scope.userControllerPath = ['js/controllers/crm-patient-user.js' ];
			    	scope.userTemplate = "tpl/app/patient/user.html";
			    	scope.patientId = scope.userParent;
			    	break;
			    case 'INTERNAL_STAFF':
			    	scope.userController = "InternalStaffUserController";
			    	scope.userControllerPath = ['js/controllers/crm-internal-staff-user.js'];
			    	scope.userTemplate = "tpl/app/internal-staff/user.html";
			    	break;
			    case 'LOGISTIC':
			        scope.userController = "LogisticUserController";
			        scope.userControllerPath = ["js/controllers/crm-logistic-user.js"];
			        scope.userTemplate = "tpl/app/logistic/user.html";
			        scope.logisticId = scope.userParent;
			        break;
			    default:
			        return;
				}
				
				scope.modal =  $modal.open({
					scope: scope,
					templateUrl: scope.userTemplate,
					controller: scope.userController,
					windowClass: 'largeModal',
					size: 'lg',
					resolve: {
						deps: function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( scope.userControllerPath );
								}
							)
						}
					}
				});
				
				scope.close = function(result) {
					scope.modal.close(result);
				}
				
				scope.modal.result.then(function() {
					if (scope.postFn)
						scope.postFn();
					console.log('Modal closed: ' + new Date());
				}, function () {
					if (scope.postFnDismiss)
						scope.postFnDismiss();
					console.log('Modal dismissed: ' + new Date());
				});
			});
		}
	};
});


app.directive("storedExportViewer", function($http, $modal, $sce, MessageService, StoredExportService, RESTURL, $rootScope, $cookies) {
	return {
		restrict: 'A',
		scope : {
			id : "=id",
			title : "=title",
			action : "=action",
			ownerId: "=ownerId",
			filename: "=filename",
			callbackFn: '&callbackFn'
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.h = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
				//Determina azione in base all'estensione del file
				scope.act = ('delete'!=scope.action) ? 'download' : 'delete';
				if(scope.act=='delete') {
					//Elimina documento
					var dialogTitle = "Conferma eliminazione documento";
					var dialogText = "Vuoi eliminare questo documento?";
					var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-danger','sm');
					modalInstance.result.then(
						function(confirm) {
							StoredExportService.deleteDocument({ id: scope.id }, function(result) {
								MessageService.showSuccess('Documento eliminato con successo','');
								if(scope.callbackFn!=undefined) scope.callbackFn();
							}, 
							function(error) {
								MessageService.showError('Operazione non riuscita', '');
							});
						});
				}
				else {
					StoredExportService.get({ id: scope.id }, function(result) {
						var type = result.data.type;
						var filename = result.data.fileName;
						var extension = filename.substr(1+filename.lastIndexOf('.'),filename.length-1);
										        
						$http({method: 'GET', url: RESTURL.STORED_EXPORT+'/'+scope.id+'/url',headers: { 'X-AUTH-TOKEN' : scope.h }}).success(function(response) {
							console.log('download!');
							scope.pdf = $sce.trustAsResourceUrl(response.data);
							console.log('file url: '+scope.pdf);
							var element = angular.element('<a/>');
							element.attr({
								href: response.data,
								target: '_blank',
							})[0].click();
							
						}).error(function(data, status, headers, config) {
							MessageService.showError('Download non riuscito', '');
						});				
					});

				}
			});
		}
	};
});

app.directive("entityAutocomplete", ['$rootScope','$timeout',function($rootScope,$timeout) {
	return {
		restrict: 'A',
		template: '<div style="margin: -7px -13px;" disable-input="disabled" initial-value="inputModel" angucomplete-alt id="id" override-suggestions="true" placeholder="{{placeholder}}" pause="100" title-field="{{titleField}}" selected-object="outputModel" local-data="sourceList" search-fields="{{searchField}}" input-class="{{inputClass}}"></div>',
		scope: {
			inputModel : '=',
			sourceList : '=',
			disabled: '=',
			placeholder: '@',
			titleField: '@',
			searchField: '@',
			inputClass: '@'
			
		},
		controller: function($scope) {
			
			$scope.init = function() {
				if($scope.disabled==undefined || $scope.disabled==null) $scope.disabled = false;
				if($scope.sourceList==undefined || $scope.sourceList==null) $scope.sourceList = [];
				if($scope.initialValue==undefined || $scope.initialValue==null) {
					if($scope.sourceList.length>0) $scope.initialValue = $scope.sourceList[0];
				}
				
				$scope.id = 'autocomplete_'+(new Date()).getTime();
				if($scope.placeholder==undefined || $scope.placeholder==null) $scope.placeholder='';
			
				
				if($scope.inputModel!=undefined && $scope.inputModel!=null) {
					var keys = Object.keys($scope.inputModel);
					if(keys.length>0) {
						if($scope.titleField==undefined || $scope.titleField==null) {
							$scope.titleField = keys[0];
						}
						if($scope.searchField==undefined || $scope.searchField==null) {
							$scope.searchField = keys[0];
						}
					}
				
				}
				
				
			}
			$scope.$watch('inputModel', function(nv,ov) {
				if(nv!=undefined && nv!=null && nv!=ov) {
					//if(ov==undefined || ov==null) {
					$scope.outputModel = angular.copy(nv);
					//}
				} else {
					$scope.outputModel = undefined;
					$scope.$broadcast('angucomplete-alt:clearInput');
				}
			}, true);
			
			$scope.$watch('outputModel', function(nv,ov) {
				if(nv!=undefined && nv!=null && nv!=ov) {
					if(nv.originalObject!=undefined && nv.originalObject!=null) {
						if(nv.originalObject[$scope.titleField]==undefined || nv.originalObject[$scope.titleField]==null) {
							var f =  ''+$scope.titleField;
							$scope.inputModel = { } 
							$scope.inputModel[f] = nv.originalObject;
						}
						else {
							delete nv.originalObject.group;
							$scope.inputModel = angular.copy(nv.originalObject);
						}
					}
					else {
						$scope.inputModel = angular.copy(nv);
					}
				}
				else if(nv!=ov && (nv==undefined || nv==null)){
					$scope.inputModel = undefined;
					$scope.$broadcast('angucomplete-alt:clearInput');
				}
			}, true);
			
		},
		link: function(scope, element, attrs) {
			$timeout(function() { scope.init(); },0);
			scope.$watch('sourceList', function(nv) {
				if(nv!=undefined) {
					$timeout(function() { scope.init(); },0);
				}
			});
		}
	}
}]);


/*
 * <av-calendar-availability-planner user-ids="meetingUsers" meeting-filters="filters" options="calendarOptions" meetings="plannedMeetings" availabilities="avList"/>
 * userIds: Lista id degli utenti di cui si mostrano i meetings (e di cui si pianificano le attività)
 * meeting-filters : Filtri sui meeting da mostrare(ad es. mostra solo le visite)
 * availabilities: Lista di disponibilità (i meetings saranno creabili sono entro questi slot)
 * meetings: Lista dei meetings creati (valore ritornato)
 * options: opzioni per il calendario
 * 	businessHours => Mostra solo slot dalle 7 alle 20 se true
 * 	hiddenDays => Array con giorni da nascondere [0 = domenica]
 *  allDaySlot => Mostra slot "Tutto il giorno" se true 
 *  
 * */
app.directive("calendarPlanning", ["RESTURL","$compile","$rootScope","MessageService","UserActivityService", "AvailabilityService","$filter","UserService","$timeout",
                                           "uiCalendarConfig","$modal","$ocLazyLoad","PermissionService", "UserEventService",
                                           function(RESTURL,$compile,$rootScope,MessageService,UserActivityService,AvailabilityService,$filter,UserService,$timeout,
                                        		   uiCalendarConfig,$modal,$ocLazyLoad,PermissionService, UserEventService) {
	return {
		template: '<div id="calendar-container" class="mTCalendar"><div ng-include="\'tpl/app/directive/calendar-planning/main.html\'"></div></div>',
		replace: true,
		scope : {
			userIdsList : "=?", //Lista degli id-utenti di cui vengono mostrati i meetings (se non si passa nulla è l'utente loggato). E' una stringa! id1, id2
			meetingFilters : "=?", //Filtri sui meetings da mostrare
			availabilityFilters : "=?", //Filtri sulle disponibilità da mostrare
			userEventFilters : "=?", //Filtri su bean UserEvent che raggruppa UserActivity e PersonalEvent
			options : "=?", //Opzioni
			eventTypes: "=?", // Array di tipo eventi da visualizzare: valori possibili 'meeting', 'availability' e 'userEvent'
			updateEventType: "=?", // Il tipo di evento che può essere aggiunto/modificato con selezione dal calendario:  valore possibile uno tra 'meeting', 'availability' e 'userEvent'
			meetingStart : '=?',
			meetingDuration : '=?',
			meetingIgnoreUnavailabilities : '=?',
			form : '='
		},
		
		
		
		link: function(scope, el, attrs, ngModel, $scope) {
			
			var element = el;
			
			scope.hexToRGB = function(color) {
				
				var R = scope.hexToR(color);
				var G = scope.hexToG(color);
				var B = scope.hexToB(color);
				
				return R+","+G+","+B;
			}
			
			scope.hexToR = function(h) {return parseInt((scope.cutHex(h)).substring(0,2),16)}
			scope.hexToG = function(h) {return parseInt((scope.cutHex(h)).substring(2,4),16)}
			scope.hexToB = function(h) {return parseInt((scope.cutHex(h)).substring(4,6),16)}
			scope.cutHex = function(h ){return (h.charAt(0)=="#") ? h.substring(1,7):h}
			
			
			scope.$on('refresh-availability-calendar',function(event,args) {
				if(scope.userIdsList.indexOf(args.userId)>=0) {
					scope.init();
				}
			});
			
			
			PermissionService.getAll({entityType:'USER_AVAILABILITY_CONFIG'},function(result){
            	
            	scope.availabilitiesPermissions = result.data;
            	
            	
            },function(error){
            	
            	
            });
			
			PermissionService.getAll({entityType:'PERSONAL_EVENT'},function(result){
            	scope.personalEventPermissions = result.data;
            },function(error){
            });
			//debugger;
			scope.configCalendar = function() {
				scope.uiConfig = {
					calendar:{
						height: 'auto',
						selectHelper: false,
						eventOverlap: true,
						selectConstraint:{
					      start: '00:01', 
					      end: '23:59', 
					    },
					    displayEventTime : false,
						minTime: scope.options && scope.options.businessHours ? scope.options.businessHours.minTime : undefined,
						maxTime: scope.options && scope.options.businessHours ? scope.options.businessHours.maxTime : undefined,
						selectable: true,
						unselectAuto: true,
						hiddenDays: scope.options.hiddenDays,
						views: {
							agendaDay: {
								displayEventTime : true,
								displayEventEnd: true,
								timeFormat: 'HH:mm',
								slotLabelFormat: 'HH:mm'
							},
							agendaWeek: {
								displayEventTime : true,
								displayEventEnd: true,
								timeFormat: 'HH:mm',
								columnFormat: 'ddd D/M',
								slotLabelFormat: 'HH:mm'
							},
							month: {
								displayEventTime : true,
								displayEventEnd: true,
								timeFormat: 'HH:mm'
							},
							listMonth: {
								displayEventTime : true,
								displayEventEnd: true,
								timeFormat: 'HH:mm',
								noEventsMessage: 'Nessun dato da visualizzare',
								listDayFormat: 'ddd',
								listDayAltFormat: 'DD MMMM YYYY'
							}
						},
						select: function(start, end, jsEvent, view) {
							scope.createNewEvent(start, end, jsEvent, view);
						},
						dayClick: function(date, jsEvent, view, resourceObj) {
							
							
							if(view.name == 'month') {
								//if(scope.updateEventType === 'availability') {
									var start = moment(start).hours(9).minutes(0).seconds(0).milliseconds(0).toDate();
									scope.createNewEvent(date, undefined, jsEvent, view);	
								/*}
								else {
									$(element).find('.meeting-calendar').fullCalendar('unselect');
	 								$(element).find('.meeting-calendar').fullCalendar('changeView','agendaWeek');
	 								$(element).find('.meeting-calendar').fullCalendar('gotoDate', date);
								}*/
							}
						},
						header:{
							left: 'prev',
							center: 'title',
							right: 'next'
						},
						timeFormat: 'HH:mm',
						lang: 'it',
						locale: 'it',
						allDaySlot: (scope.options.allDaySlot!=undefined) ? scope.options.allDaySlot : false,
						timezone: 'local',
						slotEventOverlap: false,
						/*
						monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
						monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Set','Ott','Nov','Dic'],
						dayNames: ['Domenica', 'Lunedi', 'Martedi', 'Mercoledi', 'Giovedi', 'Venerdi', 'Sabato'],
						dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
						*/
						eventClick: scope.eventClick,
						eventRender: function(event, element, view) {
							if(event.type=='availability') {
								if(view.name=='listMonth') {
									
									element.css('color',event.color);
									element.css('border',event.border);
									
									element.css('background-color',event.backgroundColor);
									
									var tds = element.find('td');
									tds[0].style='font-weight: bold';
									tds[1].innerHTML = '';
									
									//var dispo = (event.object.available) ? 'Disponibile ' : 'Non disponibile';
									
									//tds[2].firstChild.innerText = tds[2].firstChild.innerText;
									/*
									var startTime = moment(event.start).format('HH:mm');
									var endTime = moment(event.end).format('HH:mm');
									var html = '<tr class="fc-list-item ng-scope" style="background-color: '+event.backgroundColor+'; color: rgb(255, 255, 255);">';
									html += '<td class="fc-list-item-time fc-widget-content">'+startTime+' - '+endTime+'</td>';
									html += '<td class="fc-list-item-title fc-widget-content"><a class="check-link">'+(event.object.available ? 'Disponibile' : 'Non Disponibile')+'&nbsp;<small>'+(event.title==undefined || event.title==null ? '':event.title)+'</small></a></td>';
									html += '</tr>'
									element.replaceWith(html);	
									*/
								}
								else {
									
									//debugger;
									if(event.rendering == 'background') {
										//Rendering in background
										if(event.object.notes!=undefined && event.object.notes!='')
											element.attr('title',event.object.notes);
									}
									if(event.border !== undefined) {
								
										element.css('border', event.border);
//										element.prepend('<div class="pull-right" style="background-color:#f05050; color:#ffffff; width: 25px; height: 16px; text-align: center;"><i class="fa fa-close fw"></i></div>');
									}
									
									if(event.color !== undefined) {
										
										element.css('color',event.color);
									}
									
								}
							}
							else if(event.type == 'meeting') {
								//element.find('.fc-title').html('<strong>'+pat.lastName+' '+pat.firstName+'</strong> - '+event.name);
		 						if(view.name!='listMonth') {
									if(event.userColors!=undefined && event.userColors!=null) {
										for(var e=0; e<event.userColors.length; e++) {
											var margin = 'margin-right: 0px;';
											if(e==0) {
												margin = 'margin-right: 2px;';
											}
											if(event.userColors[e]!=undefined)
												element.prepend('<div style="background-color: '+event.userColors[e]+'; float: left; display: inline; '+margin+'">&nbsp;&nbsp;</div>');
										}
									}
		 						}
		 						else {
		 							var tds = element.find('td');
		 							console.log('---x--- tds  ---x---');
		 							console.log(tds)
		 							var html = '';
		 							if(event.userColors!=undefined && event.userColors!=null) {
										for(var e=0; e<event.userColors.length; e++) {
											if(event.userColors[e]!=undefined)
												html+='<span style="background-color: '+event.userColors[e]+';">&nbsp;&nbsp;</span>';
										}
									}
									tds[1].innerHTML=html;
									tds[2].innerText=event.completeTitle;
		 						}
							} else if(event.type == 'userEvent') {
								//element.find('.fc-title').html('<strong>'+pat.lastName+' '+pat.firstName+'</strong> - '+event.name);
		 						if(view.name!='listMonth') {
									if(event.userColors!=undefined && event.userColors!=null) {
										for(var e=0; e<event.userColors.length; e++) {
											var margin = 'margin-right: 0px;';
											if(e==0) {
												margin = 'margin-right: 2px;';
											}
											if(event.userColors[e]!=undefined)
												element.prepend('<div style="background-color: '+event.userColors[e]+'; float: left; display: inline; '+margin+'">&nbsp;&nbsp;</div>');
										}
									}
		 						}
		 						else {
		 							var tds = element.find('td');
		 							console.log('---x--- tds  ---x---');
		 							console.log(tds)
		 							var html = '';
		 							if(event.userColors!=undefined && event.userColors!=null) {
										for(var e=0; e<event.userColors.length; e++) {
											if(event.userColors[e]!=undefined)
												html+='<span style="background-color: '+event.userColors[e]+';">&nbsp;&nbsp;</span>';
										}
									}
									tds[1].innerHTML=html;
									tds[2].innerText=event.completeTitle;
		 						}
							}
			 				$compile(element)(scope);
						},
						viewRender: function(view, element) {
							if(view.name=='listMonth' && scope.updateEventType === 'availability') {
								$('#createNewButton').remove();
								//Aggiungo bottoni creazione nuova dispo e nuovo meeting
								var html = '<div class="m-t m-b m-l" id="createNewButton"><button ng-click="createNewDispoFromScratch()" class="btn btn-default btn-sm"><i class="fa fa-plus fw"></i> Crea nuova Disponibilit\u00e0</button></div>';
								element.prepend(html);
								$compile($('#createNewButton'))(scope);
								
							}
							scope.loadEvents(true);
						},
						eventAfterAllRender: scope.afterRender
					}
				};
			}
 		
			scope.checkUserAvailabilities = function(fieldValue,fieldName) {
				if('meeting_start'==fieldName) {
					if(scope.skipCheckAvailabilities === undefined || !scope.skipCheckAvailabilities) {

						var meetingStart = fieldValue !== undefined ? moment(fieldValue).toDate() : undefined;
						var meetingEnd = scope.extMeetingDuration !== undefined && meetingStart !== undefined ? moment(meetingStart).add(scope.extMeetingDuration.value,'minutes').toDate() : meetingStart;
						var meetingDuration = scope.extMeetingDuration;
						
						var unavailableUsers = scope.checkAvailabilities(meetingStart, meetingEnd);
						if(unavailableUsers !== undefined && unavailableUsers.length > 0) {
							var dialogText = "Per la data selezionata (";
							if(meetingDuration !== undefined) {
								dialogText = dialogText + $filter('formatDate')(meetingStart) + " dalle " + $filter('formatTime')(meetingStart) + " alle " + $filter('formatTime')(meetingEnd) + ") alcuni utenti non risultano disponibili: <ul>";
							}
							else {
								dialogText = dialogText + $filter('formatDateTime')(meetingStart) + ") alcuni utenti non risultano disponibili: <ul>";
							}
							for(var i=0; i<unavailableUsers.length; i++) {
								dialogText = dialogText + "<li>" + unavailableUsers[i].lastName + " " + unavailableUsers[i].firstName + "</li>";
							}
							dialogText += "</ul>Si vuole selezionare comunque questa data/ora?";
							scope.extMeetingIgnoreUnavailabilities.name = dialogText;
							scope.extMeetingIgnoreUnavailabilities.value = false;
						}
						else {
							scope.extMeetingIgnoreUnavailabilities.name = undefined;
							scope.extMeetingIgnoreUnavailabilities.value = false;
						}
					}
				}
				else {
					if(fieldValue=='') fieldValue=undefined;
					var meetingStart = moment(scope.extMeetingStart.value).toDate();
					var meetingEnd = scope.extMeetingDuration !== undefined ? moment(meetingStart).add(scope.extMeetingDuration.value,'minutes').toDate() : meetingStart;
					var meetingDuration = fieldValue;
					
					var unavailableUsers = scope.checkAvailabilities(meetingStart, meetingEnd);
					if(unavailableUsers !== undefined && unavailableUsers.length > 0) {
						var dialogText = "Per la data selezionata (";
						if(meetingDuration !== undefined) {
							dialogText = dialogText + $filter('formatDate')(meetingStart) + " dalle " + $filter('formatTime')(meetingStart) + " alle " + $filter('formatTime')(meetingEnd) + ") alcuni utenti non risultano disponibili: <ul>";
						}
						else {
							dialogText = dialogText + $filter('formatDateTime')(meetingStart) + ") alcuni utenti non risultano disponibili: <ul>";
						}
						for(var i=0; i<unavailableUsers.length; i++) {
							dialogText = dialogText + "<li>" + unavailableUsers[i].lastName + " " + unavailableUsers[i].firstName + "</li>";
						}
						dialogText += "</ul>Si vuole selezionare comunque questa data/ora?";
						scope.extMeetingIgnoreUnavailabilities.name = dialogText;
						scope.extMeetingIgnoreUnavailabilities.value = false;
					}
					else {
						scope.extMeetingIgnoreUnavailabilities.name = undefined;
						scope.extMeetingIgnoreUnavailabilities.value = false;
					}
				}
			}
			
			
			/*
			scope.$on('meeting',function(event, args) {
				if(scope.extMeetingStart != undefined) {
					scope.extMeetingStart.value = args.startTime;
				}
				if(scope.extMeetingDuration != undefined) {
					scope.extMeetingDuration.value = args.duration;
				}
			});
			*/

 			scope.init = function() {
				$(element).find('.meeting-calendar').fullCalendar( 'removeEventSources' );
				$(element).find('.meeting-calendar').fullCalendar( 'removeEvents' );
				
				scope.showAvailabilities = true;
				scope.showMeetings = true;
				scope.showUserEvents = true;
				
 				if(scope.userIdsList == undefined || scope.userIdsList == null) scope.userIdsList = $rootScope.loggedUser.id;
 				
 				if(scope.meetingFilters == undefined || scope.meetingFilters == null) scope.meetingFilters = {};
 				
 				if(scope.availabilityFilters == undefined || scope.availabilityFilters == null) scope.availabilityFilters = {};
 				
 				if(scope.userEventFilters == undefined || scope.userEventFilters == null) scope.userEventFilters = {};
 				
 				if(scope.options == undefined || scope.options == null) scope.options = {};
 				
 				if(scope.eventTypes == undefined || scope.eventTypes == null) scope.eventTypes = [ 'meeting', 'availability', 'userEvent' ];
 				
 				if(scope.updateEventType == undefined || scope.updateEventType == null || (scope.updateEventType != 'meeting' && scope.updateEventType != 'availability' && scope.updateEventType != 'userEvent')) scope.updateEventType = undefined;
 				if(scope.eventTypes.indexOf(scope.updateEventType) < 0) {
 					scope.eventTypes.push(scope.updateEventType);
 				}
 				
 				if(scope.form === undefined || scope.meetingStart === undefined) {
 					if(scope.updateEventType === 'meeting') {
 						scope.updateEventType = undefined;
 					}
 				}
 				
 				scope.withDuration = scope.meetingDuration !== undefined && scope.meetingDuration != null;
 			
 				if(scope.form !== undefined && scope.meetingStart !== undefined) {
 					scope.extMeetingStart = undefined;
 					scope.extMeetingDuration = undefined;
 					scope.extMeetingIgnoreUnavailabilities = undefined;
 					for(var i=0; i<scope.form.formProperties.length; i++) {
 						if(scope.form.formProperties[i].id == scope.meetingStart) {
 							scope.extMeetingStart = scope.form.formProperties[i];
 						}
 						if(scope.meetingDuration !== undefined && scope.form.formProperties[i].id == scope.meetingDuration) {
 							scope.extMeetingDuration = scope.form.formProperties[i];
 						}
 						if(scope.meetingIgnoreUnavailabilities !== undefined && scope.form.formProperties[i].id == scope.meetingIgnoreUnavailabilities) {
 							scope.extMeetingIgnoreUnavailabilities = scope.form.formProperties[i];
 						}
 					}
 					
 					if(scope.extMeetingStart !== undefined) {
 						
 						scope.$watch( function() {
 							return scope.extMeetingStart.value;
 						}, function(nv,ov) {
 							if(scope.extMeetingIgnoreUnavailabilities !== undefined) {
 								scope.checkUserAvailabilities(nv,'meeting_start');
 								scope.skipCheckAvailabilities = false;
 							}
 	 					},true);
 	 					
 					}
 					
 					if(scope.extMeetingStart !== undefined && scope.extMeetingDuration !== undefined) {
 						
 						scope.$watch( function() {
 							return scope.extMeetingDuration.value;
 						}, function(nv,ov) {
 							console.log('************ extMeetingDuration: ************');
 							console.log(scope.extMeetingDuration);
 							console.log('nv: ');
 							console.log(nv);
 							console.log('ov: ');
 							console.log(ov);
 							console.log('**********************************************')
 							if(nv!=undefined && scope.extMeetingIgnoreUnavailabilities !== undefined) {
 								scope.checkUserAvailabilities(nv,'meeting_duration');
 							}
 	 					},true);
 	 					
 					}
 				}
				
 				
 				scope.meetings = [];
 				
 				scope.availabilities = [];
 				
 				scope.userEvents = [];
 				
 				scope.meetingsLoaded = 0;
 				
 				scope.availabilitiesLoaded = 0;
 				
 				scope.userEventsLoaded = 0;
 				
 				scope.startMeetings = undefined;
 				scope.startAvailabilities = undefined;
 				scope.startUserEvents = undefined;
 				
 				scope.endMeetings = undefined;
 				scope.endAvailabilities = undefined;
 				scope.endUserEvents = undefined;
 				
// 				scope.options.businessHours = {
// 					minTime : "07:00:00",
// 					maxTime : "22:00:00"
// 				};
 				
 				if(scope.options.hiddenDays == undefined) {
 					scope.options.hiddenDays = [];
 				}
 				
 				scope.meetingEvents = []
 				scope.availabilityEvents = [];
 				scope.userEventsList = [];
 				scope.eventSources = [];
 				var date = new Date();
 				var d = date.getDate();
 				var m = date.getMonth();
 				var y = date.getFullYear();
 				
 				scope.overlay = undefined;
 				scope.overlayKey = undefined;
 				scope.precision = 400;
 				scope.lastClickTime = 0;
 				
 				/*MeetingService.getAllCategoryAsMap({},
 					function(response) {
 						scope.meetingCategoriesMap = response.data;
 					},
 					function(error) {
 						MessageService.showError('Problema nel recupero categorie meetings',error);
 						scope.meetingCategoriesMap = {};
 					}
 				);*/
 				
 				scope.configCalendar();
 				
 				scope.usersColors = ['#5cb85c','#428bca','#d9534f','#f0a311','#f06609','#ac73d4','	#6510a1','#999999'];
 				scope.usersColorMap = {};
 				
 				scope.userIds = scope.userIdsList.split(",");
 				scope.users = [];
 				
				UserService.search({filters: {'ID':scope.userIds}} , function(result) {
					scope.users = angular.copy(result.data);
					
					for(var u=0; u<scope.users.length; u++) {
	 					var index = (u%scope.usersColors.length);
	 					scope.users[u].calendarColor = scope.usersColors[index];
	 					scope.users[u].isSelected = true;
	 				}
					
					//console.log(scope.users)
					//debugger;
					scope.loadEvents(true);
				}, function(error) {
					MessageService.showError('Errore','Problema nel recupero informazioni utenti del calendario');
				});
				
				
 			}
 			
 			scope.toggleMeetings = function() {
 				scope.showMeetings = !scope.showMeetings;
 				if(scope.showMeetings) {
 					scope.loadMeetings(true);
 				}
 				else {
 					scope.meetings = [];
 				}
 				scope.meetingsLoaded++;
 			}
 			
 			scope.toggleAvailabilities = function() {
 				scope.showAvailabilities = !scope.showAvailabilities;
 				if(scope.showAvailabilities) {
 					scope.loadAvailabilities(true);
 				}
 				else {
 					scope.availabilities = undefined;
 				}
 				scope.availabilitiesLoaded++;
 			}
 			
 			
 			scope.loadEvents = function(forceReload) {
 				if(forceReload==undefined) forceReload = false;
				if(scope.updateEventType === 'userEvent') {
					scope.loadUserEvents(forceReload);
				}
				else {
					scope.loadMeetings(forceReload);
				}
 				scope.loadAvailabilities(forceReload);
 			}
 			
 			scope.loadMeetings = function(forceReload) {
 				var calendar = $(element).find('.meeting-calendar').fullCalendar('getView');
 				
 				var startTmp,endTmp;
 				if(calendar == undefined)
 					calendar = { start: undefined , end : undefined };
 				if(calendar.start == undefined || calendar.end == undefined) {
 					var meetingStart = undefined;
 					if(scope.meetingStart != undefined) {
						for(var k=0; k<scope.form.formProperties.length; k++) {
							if(scope.form.formProperties[k].id == scope.meetingStart) {
								meetingStart = scope.form.formProperties[k].value;
								break;
							}
						}
 					}
 					
 					if(scope.form != undefined && meetingStart!=undefined) {
 						
 						calendar.start = moment(meetingStart).startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 					calendar.end= moment(meetingStart).add(1,"months").utc().valueOf();
 	 					
 					}
 					else {
 						//Usa mese corrente
 						calendar.start= moment().startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 						calendar.end= moment().add(1,"months").utc().valueOf();
 					}
 				}
 				
 				startTmp = moment(calendar.start).hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 				endTmp = moment(calendar.end).subtract(1, "days").hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 				if(forceReload || scope.startMeetings === undefined || scope.endMeetings === undefined || startTmp < scope.startMeetings || startTmp > scope.endMeetings || endTmp < scope.startMeetings || endTmp > scope.endMeetings) {
 					scope.startMeetings = moment(calendar.start).hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 				scope.endMeetings = moment(calendar.end).subtract(1, "days").hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 				
 	 				if(scope.users==undefined || scope.users==null || scope.users.length==0 || scope.eventTypes.indexOf('meeting') < 0) {
 						scope.meetings = [];
 						scope.meetingsLoaded++;
 					}
 					else {
 						var userIds = [];
 						for(var u=0; u<scope.users.length; u++) {
 							userIds.push(scope.users[u].id);
 						}
 						scope.meetingFilters["ALL_USER_ACTIVITIES"] = userIds;
 						scope.meetingFilters['START_TIME_FROM'] = scope.startMeetings;
 						scope.meetingFilters['START_TIME_TO'] = scope.endMeetings;
 						
 						var pars = {
 							'filters': scope.meetingFilters
 						}
 						
 						UserActivityService.search({}, pars, function(result) {
 							scope.meetings = result.data;
 							scope.meetingsLoaded++;
 						},function(error) {
 							//MeetingService.showError("Errore","Problema nella visualizzazione del calendario");
 						});
 					}
 				}
			}
 			
 			scope.loadAvailabilities = function(forceReload) {
 				var calendar = $(element).find('.meeting-calendar').fullCalendar('getView');
 				var startTmp,endTmp;
 				if(calendar == undefined)
 					calendar = { start: undefined, end: undefined };
 				if(calendar.start == undefined || calendar.end==undefined) {
 					var meetingStart = undefined;
 					if(scope.meetingStart != undefined) {
						for(var k=0; k<scope.form.formProperties.length; k++) {
							if(scope.form.formProperties[k].id == scope.meetingStart) {
								meetingStart = scope.form.formProperties[k].value;
								break;
							}
						}
 					}
 					
 					if(scope.form != undefined && meetingStart!=undefined) {
 						
 						calendar.start= moment(meetingStart).startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 					calendar.end= moment(meetingStart).add(1,"months").utc().valueOf();
 	 					
 					}
 					else {
 						//Usa mese corrente
 						calendar.start= moment().startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 						calendar.end= moment().add(1,"months").utc().valueOf();
 						
 					}
 				}

 				startTmp = moment(calendar.start).hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 				endTmp = moment(calendar.end).subtract(1, "days").hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 				if(forceReload || scope.startAvailabilities === undefined || scope.endAvailabilities === undefined || startTmp < scope.startAvailabilities || startTmp > scope.endAvailabilities || endTmp < scope.startAvailabilities || endTmp > scope.endAvailabilities) {
 					scope.startAvailabilities = moment(calendar.start).hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 				scope.endAvailabilities = moment(calendar.end).subtract(1, "days").hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 				
 					if(scope.users==undefined || scope.users==null || scope.users.length==0 || scope.eventTypes.indexOf('availability') < 0) {
 						scope.availabilities = [];
 						scope.availabilitiesLoaded++;
 					}
 					else {
 						var userIds = [];
 						for(var u=0; u<scope.users.length; u++) {
 							userIds.push(scope.users[u].id);
 						}
 						
 						scope.availabilityFilters['USER_ID'] = userIds;
 						scope.availabilityFilters['DATE_FROM'] = scope.startAvailabilities;
 						scope.availabilityFilters['DATE_TO'] = scope.endAvailabilities;
 						
 						var pars = {
 							'filters': scope.availabilityFilters
 						}
 						
 						AvailabilityService.search({}, pars, function(result) {
 							scope.availabilities = result.data;
 							scope.availabilitiesLoaded++;
 						},function(error) {
 							//MessageService.showError("Errore","Problema nella visualizzazione del calendario");
 						});
 					}
 				}
 			}
 			
 			scope.loadUserEvents = function(forceReload) {
 				var calendar = $(element).find('.meeting-calendar').fullCalendar('getView');
 				
 				var startTmp, endTmp;
 				if(calendar == undefined)
 					calendar = { start: undefined , end : undefined };
 				if(calendar.start == undefined || calendar.end == undefined) {
 					var meetingStart = undefined;
 					if(scope.meetingStart != undefined) {
						for(var k=0; k<scope.form.formProperties.length; k++) {
							if(scope.form.formProperties[k].id == scope.meetingStart) {
								meetingStart = scope.form.formProperties[k].value;
								break;
							}
						}
 					}
 					
 					if(scope.form != undefined && meetingStart != undefined) {
 						calendar.start = moment(meetingStart).startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 	 					calendar.end= moment(meetingStart).add(1,"months").utc().valueOf();
 					}
 					else {
 						//Usa mese corrente
 						calendar.start= moment().startOf('month').hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 						calendar.end= moment().add(1,"months").utc().valueOf();
 					}
 				}
 				
 				startTmp = moment(calendar.start).hours(0).minutes(0).seconds(0).milliseconds(0).utc().valueOf();
 				endTmp = moment(calendar.end).subtract(1, "seconds").utc().valueOf();
 				
 				if(forceReload || scope.startUserEvents === undefined || scope.endUserEvents === undefined || startTmp < scope.startUserEvents || startTmp > scope.endUserEvents || endTmp < scope.startUserEvents || endTmp > scope.endUserEvents) {
 					scope.startUserEvents = startTmp;
 	 				scope.endUserEvents = endTmp;
 	 				
 	 				if(scope.users==undefined || scope.users==null || scope.users.length==0 || scope.eventTypes.indexOf('userEvent') < 0) {
 						scope.userEvents = [];
 						scope.userEventsLoaded++;
 					} else {
 						var dS = new Date(scope.startUserEvents);
 						var dE = new Date(scope.endUserEvents);
 						dS.setHours(dS.getUTCHours());
 						dE.setDate(dE.getUTCDate());
 						dE.setHours(dE.getUTCHours());
 						scope.userEventFilters['RELATED_USER_ID'] = $rootScope.loggedUser.id;
 						scope.userEventFilters['DATE_FROM'] = dS.getTime(); 
 						scope.userEventFilters['DATE_TO'] = dE.getTime(); 
 						var pars = {
 							'filters': scope.userEventFilters
 						}
 						UserEventService.search({}, pars, function(result) {
 							scope.userEvents = result.data;
 							scope.userEventsLoaded++;
 						}, function(error) {
 							//MeetingService.showError("Errore","Problema nella visualizzazione del calendario");
 						});
 					}
 				}
			}
				
			scope.$watch('meetingsLoaded', function(nv,ov) {
				$(element).find('.meeting-calendar').fullCalendar('removeEvents');
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.meetingEvents );
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.userEventsList);
				scope.meetingEvents = [];
				if(scope.meetings !== undefined && scope.meetings != null) {
					for(var i=0; i<scope.meetings.length; i++) {
						var minEndTime = moment(scope.meetings[i].startTime).add(30,'m');
						var meeting = {
							'type': 'meeting',
							'allDay' : false,
							'className' : ["b-l b-2x b-success"],
							'info' : scope.meetings[i].description,
							'key' : scope.meetings[i].id,
							'overlap' : false,
							'editable' : false,
							'completeTitle' : scope.meetings[i].name,
							'title' : $filter('limitTo')(scope.meetings[i].name, 35) + '...',
							'end' : scope.meetings[i].endDate,
							//'category' : scope.meetings[i].category.id != undefined ? ,
							'otherParticipants' : scope.meetings[i].otherParticipants,
							'start' : scope.meetings[i].date,
							'userColors' : [],
							'object' : scope.meetings[i],
							'allParticipants':scope.meetings[i].allParticipants
						};
						meeting['participantsLabel'] = '';
						var almostOne = false;
						for(var k=0; k<scope.meetings[i].allParticipants.length; k++) {
							var user = scope.findUser(scope.meetings[i].allParticipants[k].id);
							if(user !== undefined && user.isSelected) {
								almostOne = true;
							}
							
							meeting['participantsLabel'] += ''+ scope.meetings[i].allParticipants[k].lastName + ' ' + scope.meetings[i].allParticipants[k].firstName;
							if(k!=scope.meetings[i].allParticipants.length-1)
								meeting['participantsLabel'] += ', ';
							
							meeting['userColors'].push(user.calendarColor);
							
						}
						if(almostOne) {
							scope.meetingEvents.push(meeting);
						}
					}
				}
				
				scope.checkEventsInRange(scope.meetingEvents);
				
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.meetingEvents);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.userEventsList);
				$(element).find('.meeting-calendar').fullCalendar('render');
			},true);
			
			
			scope.$watch('availabilitiesLoaded', function(nv,ov) {
				$(element).find('.meeting-calendar').fullCalendar('removeEvents');
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.meetingEvents );
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.userEventsList);
				scope.availabilityEvents = [];
				
				//debugger;
				if(scope.availabilities !== undefined && scope.availabilities != null) {
					
					if(nv <= 1) {
						
						if(scope.extMeetingStart !== undefined) {
							scope.checkUserAvailabilities(scope.extMeetingStart.value,'meeting_start');
						}
						if(scope.extMeetingDuration !== undefined) {
							scope.checkUserAvailabilities(scope.extMeetingDuration.value,'meeting_duration');
						}
					}
					
					
					for(var i=0; i<scope.availabilities.length; i++) {
						var user = scope.findUser(scope.availabilities[i].user.id);
						if(!user.isSelected) continue;
						
						var hour = Math.floor(scope.availabilities[i].startTime / 60);
						var minutes = scope.availabilities[i].startTime % 60;
						var st = moment(scope.availabilities[i].startDate).hours(hour).minutes(minutes).seconds(0).milliseconds(0).toDate();
						
						hour = Math.floor(scope.availabilities[i].endTime / 60);
						minutes = scope.availabilities[i].endTime % 60;
						var en = moment(scope.availabilities[i].startDate).hours(hour).minutes(minutes).seconds(0).milliseconds(0).toDate();
						
						var duration = scope.availabilities[i].endTime - scope.availabilities[i].startTime;
						
						var availability = {
							'type': 'availability',
							'end' : en,
							'start' : st,
							'title' : $filter('limitTo')(scope.availabilities[i].notes, 50),
							'object' : scope.availabilities[i]
						};
						
						var calendar = $(element).find('.meeting-calendar').fullCalendar('getView');
						
						//debugger;
						
						if(scope.updateEventType === 'availability' || calendar.name=='month' || calendar.name=='listMonth' ) {
							if(availability.object.available) {
								
								availability.backgroundColor = (scope.updateEventType === 'availability') ? user.calendarColor : 'rgba('+scope.hexToRGB(user.calendarColor)+',0.5)';
								availability.color = '#FFFFFF';
								availability.border = (scope.updateEventType === 'availability') ? user.calendarColor+' solid 3px' : 'rgba('+scope.hexToRGB(user.calendarColor)+',0.2) solid 3px';
							}
							else {
								availability.backgroundColor = "#FFFFFF"//(scope.updateEventType === 'availability') ? user.calendarColor : 'rgba('+scope.hexToRGB('#d6d6d6')+',0.5)';
								availability.color = user.calendarColor;
								availability.border = (scope.updateEventType === 'availability') ? user.calendarColor+' solid 3px' : 'rgba('+scope.hexToRGB(user.calendarColor)+',0.2) solid 3px';
							}
						}
						else {
							availability.rendering = 'background';
							if(availability.object.available) {
								availability.backgroundColor = user.calendarColor;
								availability.color = '#FFFFFF';
							}
							else {
								availability.backgroundColor = '#d6d6d6';
								availability.border = user.calendarColor + ' solid 10px';
								availability.color = '#FFFFFF';
							}
						
						}
						
						scope.availabilityEvents.push(availability);
					}
				}
				
				scope.checkEventsInRange(scope.availabilityEvents);
				
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.meetingEvents);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.userEventsList);
				$(element).find('.meeting-calendar').fullCalendar('render');
			},true);
			
			
			scope.$watch('userEventsLoaded', function(nv,ov) {
				$(element).find('.meeting-calendar').fullCalendar('removeEvents');
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.meetingEvents);
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('removeEventSource', scope.userEventsList);
				scope.userEventsList = [];
				if(scope.userEvents !== undefined && scope.userEvents != null) {
					for(var i=0; i<scope.userEvents.length; i++) {
						var minEndTime = moment(scope.userEvents[i].start).add(30,'m').utc().valueOf();
						var userEvent = {
							'type': scope.userEvents[i].type == 'PERSONAL_EVENT' ? 'userEvent' : 'meeting',
							'allDay': false,
							'className': ["b-l b-2x b-success"],
							'info': scope.userEvents[i].subtitle,
							'key': scope.userEvents[i].id,
							'overlap': false,
							'editable': false,
							'completeTitle': scope.userEvents[i].title,
							'title': $filter('limitTo')(scope.userEvents[i].title, 35) + '...',
							'end': scope.userEvents[i].end < minEndTime ? minEndTime : scope.userEvents[i].end,
							'start': scope.userEvents[i].start,
							'userColors': [],
							'object': scope.userEvents[i],
							'relatedUsers': scope.userEvents[i].relatedUsers
						};
						userEvent['participantsLabel'] = '';
						var almostOne = false;
						//nella lista relatedUsers ci sono tutti gli utenti coinvolti nell'attività/evento personale. La ricerca li estrae tutti per data e qui filtro solo quelli relativi agli utenti che mi interessano
						for(var k=0; k<scope.userEvents[i].relatedUsers.length; k++) {
							var user = scope.findUser(scope.userEvents[i].relatedUsers[k].id);
							if(user !== undefined && user.isSelected) {
								almostOne = true;
							}
							
							userEvent['participantsLabel'] += ''+ scope.userEvents[i].relatedUsers[k].lastName + ' ' + scope.userEvents[i].relatedUsers[k].firstName;
							if(k!=scope.userEvents[i].relatedUsers.length-1) {
								userEvent['participantsLabel'] += ', ';
							}
							userEvent['userColors'].push(user.calendarColor);
						}
						if(almostOne) {
							scope.userEventsList.push(userEvent);
						}
					}
				}
				scope.checkEventsInRange(scope.userEventsList);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.meetingEvents);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.availabilityEvents);
				$(element).find('.meeting-calendar').fullCalendar('addEventSource', scope.userEventsList);
				$(element).find('.meeting-calendar').fullCalendar('render');
			},true);
			
			
			scope.findUser = function(userId) {
				if(userId !== undefined && scope.users !== undefined) {
					for(var i=0; i<scope.users.length; i++) {
						if(scope.users[i].id === userId) {
							return scope.users[i];
						}
					}
				}
				return {};
			}
			
			scope.toggleUser = function(user) {
				if(user !== undefined) {
					user.isSelected = !user.isSelected;
					scope.meetingsLoaded++;
					scope.availabilitiesLoaded++;
				}
			}
			
			scope.eventClick = function( event, jsEvent, view ) {
				
				if(event !== undefined) {
					if(event.type === 'userEvent' ) {
						if(event.object === undefined) {
							console.log('event object of type userEvent undefined');
						} else {
							var userEventScope = scope.$new(true);
							userEventScope.detail=event.object;
							userEventScope.id=event.object.id;
							userEventScope.constants = angular.copy($rootScope.constants);
							userEventScope.isModal = true;
							userEventScope.popup = true;
							userEventScope.modal =  $modal.open({
								scope: userEventScope,
								controller: 'PersonalEventDetailCtrl',
								size: 'lg',
								resolve: {
									deps: ['$ocLazyLoad',function($ocLazyLoad) {
										return $ocLazyLoad.load(['ui.select', 'toaster']).then(
											function() { return $ocLazyLoad.load( ['js/controllers/directives/crm-personal-event.js'] ); }
										)
									}]
								},
								templateUrl: 'tpl/app/directive/personalevent/personal-event-detail.html'
							});						
							userEventScope.modal.result.then(function() { 
								console.log('Modal closed: ' + new Date());
								scope.loadUserEvents(true);
							}, function() { 
								scope.loadUserEvents(true);
								console.log('Modal dismissed: ' + new Date());
								}
							);
						}
					} else if(event.type === 'availability' && scope.updateEventType === 'availability') {
						scope.openAvailability(event.object);
					}
					else if(event.type === 'meeting') {
						if(scope.overlayKey === undefined || scope.overlayKey !== event.key) {
							if(event.object != undefined && event.object.type != undefined && event.object.type == 'VIRTUAL_MEETING'){
								event.isVM = true;
							}
							scope.event = undefined;
							scope.overlayKey = event.key;
							scope.event = angular.copy(event);
							
							scope.overlay.removeClass('left right top').find('.arrow').removeClass('left right top pull-up');
							$('.info-active').removeClass('info-active');
							var wrap = (view.name!='listMonth') ? $(jsEvent.target).closest('.fc-event') : $(jsEvent.target).closest('.fc-list-item');
			 					
							//if(scope.compile) {
								try {
									$compile(scope.overlay)(scope);
								}
								catch(ex) {
									console.log('Error trying to compile!');
								}
								scope.compile = false;
							//}
							
							
							var cal = wrap.closest('.meeting-calendar');
							var left = wrap.offset().left - cal.offset().left;
							var right = cal.width() - (wrap.offset().left - cal.offset().left + wrap.width());
							var top = cal.height() - (wrap.offset().top - cal.offset().top + wrap.height());
							
							if( right > scope.overlay.width() ) { 
								scope.overlay.addClass('left').find('.arrow').addClass('left pull-up')
							}else if ( left > scope.overlay.width() ) {
								scope.overlay.addClass('right').find('.arrow').addClass('right pull-up');
							}else{
								scope.overlay.find('.arrow').addClass('top');
							}
							if( top < scope.overlay.height() ) { 
								scope.overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass('pull-down')
							}
								
							(wrap.find('.meeting-calendar-overlay-click').length == 0) && wrap.append( scope.overlay );
								
								
							wrap.addClass('info-active');
							if(view.name!='listMonth')
								wrap.closest('.fc-row').addClass('info-active');
							
							
							scope.overlay.fadeIn();
			 					
						}
						else {
							scope.closeTooltip();
						}
					}
				}
			}
			
			scope.closeTooltip = function() {
				scope.overlay.fadeOut();
				scope.overlayKey = undefined;
			}
			
			scope.changeView = function(view) {
				if(view !== undefined) {
					$(element).find('.meeting-calendar').fullCalendar('changeView', view);
					scope.loadEvents();
				}
			};

			scope.today = function(calendar) {
				$(element).find('.meeting-calendar').fullCalendar('gotoDate',(new Date()));
				$(element).find('.meeting-calendar').fullCalendar('changeView','agendaDay');
			};
			
			
			scope.afterRender = function(view) {
				if(angular.element('.meeting-calendar-overlay-click').length>0 && (scope.overlay==undefined || scope.overlay==null)){
					scope.overlay = angular.element('.meeting-calendar-overlay-click');
				}
			};
			
			
			scope.openAvailability = function(availability) {
				
				var availabilityScope = scope.$new(true);
				availabilityScope.users = scope.users || [];
				availabilityScope.availability = availability || {};
				
				//debugger;
				var canUpdate = false;
				if(angular.isDefined(availabilityScope.availability['id']) && scope.availabilitiesPermissions['UPDATE'] == true){
					canUpdate = true;
				}
				else if(angular.isDefined(availabilityScope.availability['id']) == false && scope.availabilitiesPermissions['INSERT'] == true){
					canUpdate = true;
				}
				
				
				if(canUpdate){
					availabilityScope.closePopup = function(result) {
						if(result) {
							availabilityScope.modal.close();
						}
						else {
							availabilityScope.modal.dismiss('cancel');
						}
					}
					
					availabilityScope.modal =  $modal.open({
						scope: availabilityScope,
						controller: 'AvailabilityController',
						size: 'lg',
						resolve: {
							deps: ['$ocLazyLoad',function($ocLazyLoad) {
								return $ocLazyLoad.load(['ui.select', 'toaster']).then(
									function() { return $ocLazyLoad.load( ['tpl/app/directive/calendar-planning/editor-popups/availability-controller.js'] ); }
								)
							}]
						},
						templateUrl: 'tpl/app/directive/calendar-planning/editor-popups/availability.html'
					});
					
					
					availabilityScope.modal.result.then(
						function() { 
							scope.loadAvailabilities(true);
						},
						function() { 
							
						}
					);
				}	
			}
			
			scope.createNewDispoFromScratch = function() {
				var newAvailability = {
					'id' : undefined,
					'startDate' : undefined,
					'startTime' : undefined,
					'endTime' : undefined,
					'available' : true,
					'recursive' : false,
					'search' : {}
				}
				scope.openAvailability(newAvailability);
			}
			
			scope.createNewEvent = function(start, end, jsEvent, view) {
				if(scope.updateEventType === undefined || start === undefined) {
					return;
				}
				else if(scope.updateEventType == 'userEvent') {
					if(scope.personalEventPermissions['INSERT']) {
						var userEventScope = scope.$new(true);
						userEventScope.constants = angular.copy($rootScope.constants);
						userEventScope.detail = undefined;
						userEventScope.defaultStartDate = start;
						userEventScope.isModal = true;
						userEventScope.popup = true;
						userEventScope.modal =  $modal.open({
							scope: userEventScope,
							controller: 'PersonalEventDetailCtrl',
							size: 'lg',
							resolve: {
								deps: ['$ocLazyLoad',function($ocLazyLoad) {
									return $ocLazyLoad.load(['ui.select', 'toaster']).then(
										function() { return $ocLazyLoad.load( ['js/controllers/directives/crm-personal-event.js'] ); }
									)
								}]
							},
							templateUrl: 'tpl/app/directive/personalevent/personal-event-detail.html'
						});						
						userEventScope.modal.result.then(function() { 
							console.log('Modal closed: ' + new Date());
							scope.loadUserEvents(true);
						}, function() { 
							scope.loadUserEvents(true);
							console.log('Modal dismissed: ' + new Date());
							}
						);
					}
				}
				else if(scope.updateEventType == 'availability') {
					// Se non viene passata anche la data di fine considero solo il giorno
					var startDate = moment(start).hours(0).minutes(0).seconds(0).milliseconds(0).toDate();
					var startTime = end != null ? (start.hours() * 60) + start.minutes() : undefined;
					var endTime = end != null ? (end.hours() * 60) + end.minutes() : undefined;
					
					var newAvailability = {
						'id' : undefined,
						'startDate' : startDate,
						'startTime' : startTime,
						'endTime' : endTime,
						'available' : true,
						'recursive' : false,
						'search' : {}
					}
					scope.openAvailability(newAvailability);
				}
				else if(scope.updateEventType == 'meeting') {
					var meetingStart = end !== undefined ? moment(start).toDate() : moment(start).hours(9).minutes(0).seconds(0).milliseconds(0).toDate();
					var meetingEnd = end !== undefined && scope.withDuration ? moment(end).toDate() : meetingStart;
					var meetingDuration = end !== undefined && scope.withDuration ? (meetingEnd - meetingStart) / 1000 / 60 : undefined;
					var unavailableUsers = scope.checkAvailabilities(meetingStart, meetingEnd);
					if(unavailableUsers !== undefined && unavailableUsers.length > 0) {
						var dialogTitle = "Utenti non disponibili";
						var dialogText = "Per la data selezionata (";
						if(meetingDuration !== undefined) {
							dialogText = dialogText + $filter('formatDate')(meetingStart) + " dalle " + $filter('formatTime')(meetingStart) + " alle " + $filter('formatTime')(meetingEnd) + ") alcuni utenti non risultano disponibili: <ul>";
						}
						else {
							dialogText = dialogText + $filter('formatDateTime')(meetingStart) + ") alcuni utenti non risultano disponibili: <ul>";
						}
						var message = '';
						for(var i=0; i<unavailableUsers.length; i++) {
							dialogText = dialogText + "<li>" + unavailableUsers[i].lastName + " " + unavailableUsers[i].firstName + "</li>"
						}
						var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info');
						$(element).find('.meeting-calendar').fullCalendar('unselect');
						modalInstance.result.then(
							function(confirm) {
								scope.skipCheckAvailabilities = true;
								scope.extMeetingStart.value = meetingStart.valueOf();
								if(scope.extMeetingDuration !== undefined) {
									scope.extMeetingDuration.value = meetingDuration;
								}
								if(scope.extMeetingIgnoreUnavailabilities !== undefined) {
									scope.extMeetingIgnoreUnavailabilities.value = true;
									scope.extMeetingIgnoreUnavailabilities.name = dialogText+"</ul>";
								}
							}
						);
					}
					else {
						scope.extMeetingStart.value = meetingStart.valueOf();
						if(scope.extMeetingDuration !== undefined) {
							scope.extMeetingDuration.value = meetingDuration;
						}
						if(scope.extMeetingIgnoreUnavailabilities !== undefined) {
							scope.extMeetingIgnoreUnavailabilities.value = false;
							scope.extMeetingIgnoreUnavailabilities.name = undefined;
						}
					}
				}
			}
			
			scope.checkAvailabilities = function(meetingStart, meetingEnd) {
				var unavailableUsers = [];
				if(meetingStart !== undefined && meetingEnd !== undefined) {
					var users = angular.copy(scope.users);
					for(var i=0; i<scope.availabilities.length; i++) {
						var av = scope.availabilities[i];
						var hour = Math.floor(av.startTime / 60);
						var minutes = av.startTime % 60;
						var avStart = moment(av.startDate).hours(hour).minutes(minutes).seconds(0).milliseconds(0).toDate();
						
						hour = Math.floor(av.endTime / 60);
						minutes = av.endTime % 60;
						var avEnd = moment(av.startDate).hours(hour).minutes(minutes).seconds(0).milliseconds(0).toDate();
						
						if(meetingStart >= avStart && meetingStart <= avEnd && meetingEnd >= avStart && meetingEnd <= avEnd) {
							// Disponibilità valida, guardo a che utente si riferisce
							var userIndex = -1;
							for(var j=0; j<users.length; j++) {
								if(users[j].id === av.user.id) {
									userIndex = j;
									break;
								}
							}
							if(userIndex >= 0) {
								users.splice(userIndex,1);
							}
						}
						if(users.length == 0) break;
					}
					var userIndex = [];
					for(var j=0; j<users.length; j++) {
						if(users[j].useAvailabilities) {
							unavailableUsers.push(users[j]);
						}
					}
				}
				return unavailableUsers;
			}
			
			scope.checkEventsInRange = function(events){
				if(scope.options.businessHours != undefined && events.length > 0){
					console.log(events);
					var defaultMin = scope.options.businessHours.minTime ? scope.options.businessHours.minTime : '00:00';
					var defaultMax = scope.options.businessHours.maxTime ? scope.options.businessHours.maxTime : '23:59';
					
					var minLimit = new Date();
					minLimit.setHours(defaultMin.split(':')[0]);
					minLimit.setMinutes(defaultMin.split(':')[1]);
					
					var maxLimit = new Date();
					maxLimit.setHours(defaultMax.split(':')[0]);
					maxLimit.setMinutes(defaultMax.split(':')[1]);
					for(var i=0; i<events.length; i++) {
						var start = new Date(events[i].start); //Data inizio evento
						
						var end;//Data fine evento
						if(events[i].end){
							end = new Date(events[i].end);
						}else{
							// se non ho la data fine allora calcolo un'ora dalla data inizio
							end = new Date(events[i].start)
							end.setHours(start.getHours() + 1);
						}
						
						//se l'orario di inizio evento viene prima del limite minimo mostrato dal calendario il limite minimo acquisirà quell'orario
						if(start.getHours() < minLimit.getHours() || (start.getHours() == minLimit.getHours() && start.getMinutes() < minLimit.getMinutes())){
							minLimit.setHours(start.getHours());
							minLimit.setMinutes(start.getMinutes());
						}
						//se l'orario di fine evento viene dopo del limite massimo mostrato dal calendario il limite massio acquisirà quell'orario
						if(end.getHours() > maxLimit.getHours() || (end.getHours() == maxLimit.getHours() && end.getMinutes() > maxLimit.getMinutes())){
							maxLimit.setHours(end.getHours());
							maxLimit.setMinutes(end.getMinutes());
						}
					}
					//quello che è impostato sul calendar
					var currentMin = $(element).find('.meeting-calendar').fullCalendar('option', 'minTime');
					var currentMax = $(element).find('.meeting-calendar').fullCalendar('option', 'maxTime');
					//quello appena calcolato in base agli eventi
					var resultMin = ('0' + minLimit.getHours()).slice(-2) + ':' + ('0' + minLimit.getMinutes()).slice(-2);
					var resultMax = ('0' + maxLimit.getHours()).slice(-2) + ':' + ('0' + maxLimit.getMinutes()).slice(-2);
					if(resultMin != currentMin || resultMax != currentMax){
						console.log(resultMin, resultMax);
						$(element).find('.meeting-calendar').fullCalendar('option', 'minTime', resultMin);
						$(element).find('.meeting-calendar').fullCalendar('option', 'maxTime', resultMax);
					}
				}
			}
			
			
			scope.init();
			
			
		}
	};
}]);


app.directive("meetingModal", ['$modal', '$ocLazyLoad', '$rootScope', function($modal, $ocLazyLoad, $rootScope) {
	return {
		restrict: 'A',
		scope : {
			id : "=meetingModal"
		},
		link: function (scope, element, attrs, ngModel) {
			scope.isModal = true;
			scope.reloadMeetings = false;
			
			scope.meetingCategoriesMap = $rootScope.meetingCategoriesMap;
			scope.meetingTypesMap = $rootScope.meetingTypesMap;
			scope.slaNonComplianceReasonsMap = $rootScope.slaNonComplianceReasonsMap;
			
			scope.openModal = function() {
				scope.displayCloseButton = true;
				var modal =  $modal.open({
					scope: scope,
					templateUrl: 'tpl/app/directive/activityConf-modal.html',
					controller: 'ProgramServiceActivityController',
					size: 'lg',
					windowClass: 'largeModal',
					resolve: {
						deps: ['$ocLazyLoad',
						       function($ocLazyLoad) {
							return $ocLazyLoad.load(['ui.select', 'toaster']).then(
									function() {
										return $ocLazyLoad.load( ['js/controllers/crm-program-service-activity.js'] );
									}
							)
						}
						]
					}
				});

				modal.result.then(
						function(success) {
							scope.refresh();
						},
						function() {
							scope.refresh();
						}
				);

				scope.refresh = function() {
					if($rootScope.reloadMeeting === undefined){
						$rootScope.reloadMeeting = 0;
					}
					$rootScope.reloadMeeting++;
				}
			}
			
			element.bind("click", function() {
				
				
				console.log(scope.meetingId)
				scope.openModal();
			});
		}
	};
}]);

app.directive("userPanel", function(UserServiceConfService, UserService, PatientService, MessageService, AvatarService, HcpService, $state, $rootScope, $modal) {
	return {
		restrict: 'EA',
		scope: {
			userId : "=",
			activateFn : "=",
			suspendFn : "=",
			terminateFn : "=",
			permissions : "=",
			startFn : "=",
			fieldPermissions : "=?",
			activationRequest: "=?"
		},
		templateUrl: 'tpl/common/user-panel.html',
		link: function(scope, element, attrs) {
			
			scope.loggedUser = $rootScope.loggedUser;
			scope.programProperties = $rootScope.programProperties;
			scope.isInternalStaff = $rootScope.isInternalStaff;
			scope.isPersonified = $rootScope.isPersonified;
			scope.isLogistic = $rootScope.isLogistic;
			scope.isPatient = $rootScope.isPatient;
			scope.isHcp = $rootScope.isHcp;
			
			scope.init = function() {
				if(angular.isDefined(scope.userId) && scope.userId != '') {
					UserService.getUser({ userId: scope.userId }, {}, function(result) {
						scope.user = result.data;
						if(scope.user.type == 'PATIENT') {
							PatientService.get({ id: scope.user.id }, {}, function(result) {
								scope.user = result.data;
							}, function(error) {
								scope.user = {};
								if(error.status == 404) {
									$state.go('access.not-found');
								};
							})
						}
						else if(scope.user.type == 'HCP') {
							HcpService.get({ id: scope.user.id }, {}, function(result) {
								scope.user = result.data;
							}, function(error) {
								scope.user = {};
								if(error.status == 404) {
									$state.go('access.not-found');
								};
							})
						}
					}, function(error) {
						scope.user = {};
						if(error.status == 404) {
							$state.go('access.not-found');
						};
					});
				}
			}
			
			scope.activate = function(request) {
				if(!scope.activateFn) return;
//				var dialogTitle='Attivazione';
//				var dialogText="Si desidera attivare "+ scope.user.lastName+" "+scope.user.firstName+" ?"
//				var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info');
//				modalInstance.result.then(
//					function(confirm) {
						scope.activateFn(request);
//				});
			}
			
			scope.suspend = function() {
				if(!scope.suspendFn) return;
				var dialogTitle='Sospensione';
				var dialogText="Si desidera sospendere "+ scope.user.lastName+" "+scope.user.firstName+" ?"
				var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info');
				modalInstance.result.then(
					function(confirm) {
						scope.suspendFn();
				});
			}
			
			scope.terminate = function() {
				if(!scope.terminateFn) return;
				var dialogTitle='Uscita dal programma';
				var dialogText="Si desidera far uscire dal programma "+ scope.user.lastName+" "+scope.user.firstName+" ?"
				var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info');
				modalInstance.result.then(
					function(confirm) {
						scope.terminateFn();
				});
			}
			
			scope.initCanStartMap = function() {
				scope.canStartMap = {};
				if(scope.manualActivities != undefined) {
					for(var i=0; i<scope.manualActivities.length; i++) {
						scope.canStartMap[scope.manualActivities[i].id] = (scope.manualActivities[i].serviceStatus == 'ACTIVE');
					}
				}
			}
			
			scope.start = function(masterActivity) {
				if(!scope.startFn && scope.canStart(masterActivity.id)) return;
				var dialogTitle='Avvio attivit\u00E0';
				var dialogText="Si desidera avviare l'attivit\u00E0 '" + masterActivity.name + "' su "+ scope.user.lastName+" "+scope.user.firstName+" ?"
				var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info');
				modalInstance.result.then(
					function(confirm) {
						scope.startFn(masterActivity);
				});
			}
			
			scope.canStart = function(masterActivity) {
				return scope.canStartMap[masterActivity.id];
			}
			
			scope.$watch('manualActivities', function(v) {
				if(v !== undefined) {
					scope.initCanStartMap();
				}
			});
			
			scope.$watch('user.status', function(v) {
				if(angular.isDefined(scope.user)) {
					if(scope.user.status && scope.startFn) {
						scope.initCanStartMap();
					}
				
					if(angular.isDefined(scope.user.id)) {
						scope.manualActivities = [];
						UserServiceConfService.getUserManualMasterActivities({ userId : scope.user.id }, function(result) {
							scope.manualActivities = result.data;
						}, function(error) {
							scope.manualActivities = [];
							if(error.status == 404) {
								$state.go('access.not-found');
							};
						});
						
						scope.manualServices = [];
						UserServiceConfService.getUserManualService({ userId: scope.user.id }, function(result) {
							scope.manualServices = result.data;
						}, function(error) {
							scope.manualServices = [];
							if(error.status == 404) {
								$state.go('access.not-found');
							};
						})
					}
				}
			},true);
			
			scope.changeAvatarInfo = function(){
				if(scope.user && scope.user.type == 'PATIENT' && scope.user.id == scope.loggedUser.id) {
					AvatarService.search({}, {}, function(result) {
						if(result.data !== undefined){
							scope.avatarList = result.data;
							scope.detail = {};
							scope.detail.nickName = scope.user.avatarNickName !== undefined ? scope.user.avatarNickName : undefined;
							scope.detail.avatarId = scope.user.avatar !== undefined && scope.user.avatar.id !== undefined ? scope.user.avatar.id : undefined;
							scope.fieldPermissions = scope.fieldPermissions;
							scope.modalInstance = $modal.open({
								scope: scope,
								templateUrl: 'tpl/app/patient/avatar-information.html',
								windowClass: 'largeModal'
							});
							
						scope.closeAvatarInformation = function(){
							scope.modalInstance.close();
						}
						
						scope.confirmAvatarInformation = function() {
							var request = scope.loadRequest();
							PatientService.updateAvatar({ id: scope.user.id }, request, function(result) {
								scope.user = result.data;
							}, function(error) {
								scope.user = {};
								if(error.status == 404) {
									$state.go('access.not-found');
								};
							})
							scope.modalInstance.close();
						}
						
						scope.changeAvatarSelection = function(row) {
							if(row.id == scope.detail.avatarId){
								scope.detail.avatarId = undefined;
							}else{
								scope.detail.avatarId = row.id;
							}
						}
						
						scope.loadRequest = function() {
							return {
								avatarId: scope.detail.avatarId,
								avatarNickName: scope.detail.nickName
							}
						}
							
						}
					},function(error) {
						
					});
				}
			}
			
			scope.init();
		}
	}
});

app.directive("addressInput", function(MessageService, $rootScope, TerritoryService) {
	return {
		restrict: 'EA',
		scope: {
			address : '=addressInput',
			defaultCountryCode : '@',
			disabled : '=?ngDisabled',
			errors : "=?errors"
		},
		controller: function($scope,$rootScope,TerritoryService,MessageService) {
			
			$scope.init = function() {
				
				$scope.countries = [];
				$scope.regions = [];
				$scope.provinces = [];
				$scope.towns = [];
				$scope.watchCountryEnabled = false;
				$scope.watchCountyEnabled = false;
				$scope.watchProvinceEnabled = false;
			}	

		},
		templateUrl: 'tpl/app/directive/address-input.html',
		link: function(scope, element, attrs) {
			
			scope.init();
			
			TerritoryService.searchCountries({}, function(result) {
				
				scope.countries = result.data;
				
				if(scope.address == undefined)
					scope.address = {}
				
				scope.$watch('address.country', function(nv,ov) {
					scope.watchCountryEnabled = true;
					
					if(nv!=undefined && scope.address!=undefined) {
						
						TerritoryService.searchRegions({},{ filters: {'COUNTRY_CODE' : nv }},function(result) {
							scope.regions = result.data;
							
							var found = false;
							for(var i=0; i<scope.regions.length; i++) {
								if(scope.address!=undefined && scope.regions[i].regionCode == scope.address.county) {
									found = true;
									break;
								}
							}
							
							if(scope.address!=undefined) {
								scope.address.county = (!found) ? undefined : scope.address.county;
								if(scope.address.county == undefined) {
									scope.address.province = undefined;
									scope.address.city = undefined;
								}
							}
						},function(error) {
							MessageService.showError('Errore','Problema nel recupero delle Regioni');
							scope.regions = [];
						});
					}
					else {
						if(scope.address == undefined) {
							scope.address = {};
						}
						else if(nv==undefined) {
							scope.address.country = scope.defaultCountryCode;
						}
						
					}
				},true);
				
				
				
				scope.$watch('address.province', function(nv,ov) {
					
					scope.watchProvinceEnabled = false;
					if(nv!=undefined && scope.address!=undefined) {
						TerritoryService.searchTowns({},{ filters: {'COUNTRY_CODE' : scope.address.country, 'REGION_CODE' : scope.address.county, 'PROVINCE_CODE' : nv }},function(result) {
							scope.towns = result.data;
							var found = false;
							for(var i=0; i<scope.towns.length; i++) {
								if(scope.address!=undefined && scope.towns[i].name == scope.address.city) {
									found = true;
									break;
								}
							}
							if(scope.address!=undefined)
								scope.address.city = (!found) ? undefined : scope.address.city;
						},function(error) {
							MessageService.showError('Errore','Problema nel recupero delle Città');
							
						});
						
						
					}
				});
				
				scope.$watch('address.county', function(nv,ov) {
					scope.watchCountyEnabled = true;
					if(nv!=undefined && scope.address!=undefined) {
						
						TerritoryService.searchProvinces({},{ filters: {'COUNTRY_CODE' : scope.address.country, 'REGION_CODE' : nv }},function(result) {
							scope.provinces = result.data;
							var found = false;
							for(var i=0; i<scope.provinces.length; i++) {
								if(scope.address!=undefined && scope.provinces[i].provinceCode == scope.address.province) {
									found = true;
									break;
								}
							}
							if(scope.address!=undefined) {
								scope.address.province = (!found) ? undefined : scope.address.province;
								if(scope.address.province == undefined) {
									scope.address.city = undefined;
								}
							}
						},function(error) {
							MessageService.showError('Errore','Problema nel recupero delle Province');
							scope.provinces = [];
						});
						
					}
				});
				
				
			}, function(error) {
				MessageService.showError('Errore','Problema nel recupero dei Paesi');
			});
			
		}
	}
});
app.directive("scoreSlider", ["$rootScope", "$timeout", function($rootScope,$timeout) { 
	return {
		restrict: 'E',
		templateUrl: 'tpl/app/directive/score-slider.html',
		transclude: true,
		scope: {
			ngValue: '=?',
			startlabel: '=?',
			endlabel: '=?',
			min: '=?',
			max: '=?',
			step: '=?',
			vertical: '=?',
			scale: '=?',
			ngDisabled: '='
			
		},
		link: function(scope,attr,element) {
			scope.slider = {};
			scope.id=(new Date()).getTime();
			
			scope.toggleSlider = function() {
				scope.slider.options.disabled = !scope.slider.options.disabled;
				if(scope.slider.options.disabled) {
					scope.slider.value = undefined;
					scope.ngValue = undefined;
					scope.statusLabel = "Rispondi";
					scope.statusIcon = "check_box";
				}
				else {
					scope.slider.value = scope.slider.options.floor;
					scope.ngValue = scope.slider.options.floor;
					scope.statusLabel = "Salta risposta";
					scope.statusIcon = "check_box_outline_blank";
				}
			}
			
			scope.init = function() {
				if(scope.ngDisabled==undefined || scope.ngDisabled==null) scope.ngDisabled = false;
				scope.statusLabel = (scope.ngValue==undefined || scope.slider.value) ? "Rispondi" : "Salta risposta";
				scope.statusIcon = "check_box_outline_blank";
				if(scope.min == undefined || scope.min == null) scope.min = 0;
				if(scope.max == undefined || scope.max == null) scope.max = 10;
				if(scope.step == undefined || scope.step == null) scope.step = 1;
				if(scope.scale == undefined || scope.scale == null) scope.scale = false;
			
				scope.slider = {
					value : (scope.ngValue == undefined || scope.ngValue == null) ?  undefined : scope.ngValue<scope.min ? scope.min : angular.copy(scope.ngValue),
					options: {
						ceil: parseFloat(scope.max),
						floor: parseFloat(scope.min),
						step: parseFloat(scope.step),
						vertical: scope.vertical,
						showTicks: scope.scale,
					    showTicksValues: (!scope.scale == false),
						precision: 1,
						showSelectionBar: true,
						enforceStep: false,
						disabled: (scope.ngDisabled || scope.ngValue==undefined),
						onChange: function() {
							scope.ngValue = (scope.slider.value==undefined) ? undefined : angular.copy(scope.slider.value);
						},
						translate: function(value) {
							if(value==undefined) return '';
						    return value;
						}
						
					}
				}
			}

			$timeout(function() { scope.init(); },100);
			
			
			scope.$watch( 'min', function(nv,ov) {
				if(nv!=undefined && nv!=null) {
					if(nv>scope.ngValue)
						scope.ngValue = nv;
		
				}
			},true);
			
			scope.$watch( 'ngDisabled', function(nv) {

				if(nv!=undefined && nv!=null && scope.slider!=undefined && scope.slider.options!=undefined && scope.slider!=null && scope.slider.options!=null) {
					if(scope.slider.value!=undefined) {
						scope.$broadcast('rzSliderForceRender');
						scope.slider.options.disabled = nv;
					}
					else {
						scope.$broadcast('rzSliderForceRender');
						scope.slider.options.disabled = true;
						scope.slider.value=undefined;
					}
				}
			},true);
		}

	}
}]);
app.directive("scoreInput", ["$rootScope", function($rootScope) {
	return {
		restrict: 'EA',
		template: '<div class="radio m-b-none"><span class="m-r" ng-if="startlabel!=undefined && startlabel!=null"><strong>{{startlabel}}</strong></span><label class="i-checks m-r" ng-repeat="val in scoreValues" style="text-align: center; margin-bottom: 5px;">'+
				  '<input type="radio" value="{{val}}" name="{{element_id}}"><i></i><br/><span>{{ val }}</span></label><span ng-if="endlabel!=undefined && endlabel!=null"><strong>{{endlabel}}</strong></span></div>',
		scope: {
			score: '=',
			startlabel: '=',
			endlabel: '='
		},
		link: function(scope,attr,element) {
			scope.element_id = 'score_'+(new Date().getTime());
			console.log('score');
			console.log(scope.score);
			scope.start = (scope.score!=undefined && scope.score!=null && scope.score.start!=undefined && scope.score.start!=null) ? scope.score.start : 0;
			scope.end = (scope.score!=undefined && scope.score!=null && scope.score.end!=undefined && scope.score.end!=null) ? scope.score.end : 10;
			scope.step = (scope.score!=undefined && scope.score!=null && scope.score.step!=undefined && scope.score.step!=null) ? scope.score.step : 1; 
			console.log('start: '+scope.start+' end:'+scope.end+' step:'+scope.step);
			scope.scoreValues = [];
			var k = parseFloat(scope.start);
			var step = parseFloat(scope.step)==0 ? 1 : parseFloat(scope.step);
			while(k<=scope.end) {
				scope.scoreValues.push(''+k);
				k = k+step;
			}
		}
	}
}]);
app.directive("taskAddress", ["$rootScope", "HcpService" ,'PatientService', 'PatientPharmacyService', 'PatientTemporaryHomeAddressService', 'PatientBloodDrawingCenterService', 'MessageService', function($rootScope, HcpService ,PatientService, PatientPharmacyService, PatientTemporaryHomeAddressService, PatientBloodDrawingCenterService, MessageService) {
	return {
		restrict: 'EA',
		templateUrl: 'tpl/app/directive/task-address.html',
		scope: {
			form: '=',
			detail: '=',
			disabled : '=?ngDisabled',
		},
		link: function(scope,attr,element) {
			scope.constants = $rootScope.constants;
			scope.loggedUser = $rootScope.loggedUser;
			
			scope.$watch('[form.locationType, form.date]', function(nv, ov) {
				if(angular.isDefined(nv)) {
					scope.onLocationChange();
				}
			}, true);
			
			scope.onLocationChange = function() {
				if(scope.form.locationType == 'MEDICAL_CENTER') {
					if(scope.form.user.type == 'HCP') {
						HcpService.get({id:scope.form.user.id}, function(result) {
							var hcp = result.data; 
							if(hcp['department'] != undefined && hcp.department['address'] != null) {
								scope.form.address = hcp.department.address;
								scope.form.pharmacy = {};
							} else {
								scope.form.address = {
									country: 'IT'
								}
							}
						}, function(error) {
							if(error.status == 404) {
								$state.go('access.not-found');
							};
						});
					} else if(scope.form.user.type == 'PATIENT') {
						PatientService.get({id:scope.form.user.id}, function(result) {
							var patient = result.data; 
							if(patient['hcp'] != undefined) {
								var hcp = patient.hcp;
								if(hcp['department'] != undefined && hcp.department['address'] != null) {
									scope.form.address = hcp.department.address;
									scope.form.pharmacy = {};
								} else {
									scope.form.address = {};
								}
							} else {
								scope.form.address = {
									country: 'IT'
								}
							}
						}, function(error) {
							if(error.status == 404) {
								$state.go('access.not-found');
							};
						});
					} else {
						scope.form.address = {};
						scope.form.pharmacy = {};
					}
				}else if(scope.form.locationType == 'BLOOD_DRAWING_CENTER'){
					scope.searchFilters = { 
							filters : { 
								'PATIENT_ID' : scope.form.user.id,
								'REFERENCE' : true
								}
					};
					PatientBloodDrawingCenterService.search({}, scope.searchFilters, function(result){
						var blooddrawingcenter = undefined;
						if(result.data.lenght>0){
							blooddrawingcenter = result.data[0].blooddrawingcenter;
						}
						if(blooddrawingcenter && blooddrawingcenter.address){
							scope.form.address = blooddrawingcenter.address;
							scope.form.bloodDrawingCenter = blooddrawingcenter;
							scope.form.pharmacy = {};
						} else{
							scope.form.address = {};
							scope.form.pharmacy = {};
							scope.form.bloodDrawingCenter = blooddrawingcenter || {};
							scope.form.address = {
									country: 'IT'
								}
							if(blooddrawingcenter) {
								MessageService.showError('Errore', 'Il centro di prelievo di riferimento non ha un indirizzo');
							}else{
								MessageService.showError('Errore', 'Il paziente non ha associato un centro di prelievo di riferimento');
							}
						}
					});
			}else if(scope.form.locationType == 'PATIENT_HOME') {
					if(scope.form.user.type == 'PATIENT'){
						scope.searchFilters = { 
								filters : { 
									'PATIENT_ID' : scope.form.user.id,
									'TEMPORARY_HOME_ADDRESS_STATUS' : 'CONFIRMED'
									}
						};
						PatientTemporaryHomeAddressService.search({}, scope.searchFilters, function(result) {
							var addresses = result.data;
							var notFound = true;
							if(addresses != undefined && addresses.length>0) {
								for(var i=0; i<addresses.length; i++) {
									var start = addresses[i].startDate;
									var end = addresses[i].endDate;
									if(scope.form.date>=start && scope.form.date<=end){
										scope.form.address = addresses[i].address;
										scope.form.patientTemporaryHomeAddress = addresses[i];
										notFound = false;
										break;
									}
								} 
							}
							if(notFound) {
								PatientService.get({id:scope.form.user.id}, function(result) {
									var patient = result.data; 
									if(patient['address'] != undefined) {
										scope.form.address = patient.address;
										scope.form.pharmacy = {};
									} else {
										scope.form.address = {
											country: 'IT'
										}
									}
								}, function(error) {
									if(error.status == 404) {
										$state.go('access.not-found');
									};
								});
							}
							scope.form.pharmacy = {};
							}, function(error) {
								if(error.status == 404) {
									$state.go('access.not-found');
								};
						});
					} else {
						scope.form.address = {};
						scope.form.pharmacy = {};
					}
				} else if(scope.form.locationType == 'PHARMACY') {
					if(scope.form.user.type == 'PATIENT') {
						scope.searchFilters = { 
								filters : { 
									'PATIENT_ID' : scope.form.user.id,
									'TEMPORARY_HOME_ADDRESS_STATUS' : 'CONFIRMED'
									}
						};
						PatientTemporaryHomeAddressService.search({}, scope.searchFilters, function(result) {
							var addresses = result.data;
							var notFound = true;
							if(addresses != undefined && addresses.length>0) {
								for(var i=0; i<addresses.length; i++) {
									var start = addresses[i].startDate;
									var end = addresses[i].endDate;
									if(scope.form.date>=start && scope.form.date<=end) {
										if(angular.isDefined(addresses[i].pharmacy)) {
											scope.form.pharmacy = addresses[i].pharmacy;
											scope.form.address = addresses[i].pharmacy.address; //l'indirizzo della farmacia potrebbe non essere valorizzato
											notFound = false;
										}
										break;
									}
								} 
							}
							if(notFound) {
								var filter = {
										'size' : 1
								}; 
								PatientPharmacyService.searchReferencePatientPharmacy({patientId : scope.form.user.id}, filter, function(result){
									var result = result.data;
									if(result != undefined && result["0"] != undefined && result["0"].pharmacy != undefined && result["0"].pharmacy.address != undefined) {
										scope.form.address = result["0"].pharmacy.address;
										scope.form.pharmacy = result["0"].pharmacy;
									} else {
										scope.form.address = {};
									}
								}, function(error) {
									if(error.status == 404) {
										$state.go('access.not-found');
									};
								}); 
							}
							}, function(error) {
								if(error.status == 404) {
									$state.go('access.not-found');
							};
						});
					} else {
						scope.form.address = {};
						scope.form.pharmacy = {};
					}
//					if(scope.form.user.type == 'PATIENT') {
//						var notFound = true;
//						var filter = {
//								'size' : 1
//						}; 
//						PatientPharmacyService.searchReferencePatientPharmacy({patientId : scope.form.user.id}, filter, function(result){
//							var result = result.data;
//							if(result != undefined && result["0"] != undefined && result["0"].pharmacy != undefined && result["0"].pharmacy.address != undefined) {
//								scope.form.address = result["0"].pharmacy.address;
//								scope.form.pharmacy = result["0"].pharmacy;
//							} else {
//								scope.form.address = {};
//							}
//						}, function(error) {
//							if(error.status == 404) {
//								$state.go('access.not-found');
//							};
//						}); 
//					} else {
//						scope.form.address = {};
//					}
				} else if(scope.form.locationType == 'OTHER'){
					if(scope.form.address === undefined){
						scope.form.address = {};
						scope.form.pharmacy = {};
					}
				} else {
					scope.form.address = {};
					scope.form.pharmacy = {};
				}
			}
		}
	}
}]);
app.directive("formulaEditor", ["$rootScope", function($rootScope) {
	return {
		restrict: 'EA',
		scope: {
			questions : '=',
			formula: '='
		},
		templateUrl: 'tpl/app/directive/formula-editor.html',
		link: function(scope, element, attrs) {
			console.log('******** formula editor **********');

			scope.questionTags = [];
			scope.choicesTags = [];
			scope.formulaTags = [];
			
			scope.edit = false;
			console.log('edit: '+scope.edit);

			scope.mathTags = [
				{title:'+',value:'+',type:'math', description: '+'},
				{title:'-',value:'-',type:'math', description: '-'},
				{title:'x',value:'*',type:'math', description: 'x'},
				{title:'/',value:'/',type:'math', description: '/'},
				{title:'(',value:'(',type:'math', description: '('},
				{title:')',value:')',type:'math', description: ')'}
			];
			
			scope.mathSymbols = [];
			for(var s=0; s<scope.mathTags.length; s++) {
				scope.mathSymbols.push(scope.mathTags[s].value);
			}
			console.log('Math symbol');
			console.log(scope.mathSymbols);
			
			scope.resetFormula = function() {
				console.log('resetFormula');
				scope.formulaTags = [];
				scope.formula='';
			}
			
			scope.compileFormula = function() {
				
				for(var t=0; t<scope.formulaTags.length; t++) {
					if(t==0) scope.formula = ''+scope.formulaTags[t].value;
					else
						scope.formula = scope.formula+''+scope.formulaTags[t].value;
				}
				console.log('CompileFormula');
				console.log(scope.formula);
				scope.closeFormula();
			}
			
			scope.addTag = function(item) {
				var copyItem = angular.copy(item);
				scope.formulaTags.push(copyItem);
			}
			
			scope.removeTag = function(item) {
				var index = scope.formulaTags.indexOf(item);
				scope.formulaTags.splice(index,1);
			}
			
			scope.addConstant = function(val) {
				scope.formulaTags.push({ title: val, value: val, type: 'constant', description: 'Costante numerica di valore '+val });
			}
			
			/* Legge la formula al load della pagina e converte in tags */
			scope.parseFormula = function() {
				if(!scope.edit) return;
				scope.formulaTags = [];
				
				if(scope.formula!=undefined && scope.formula!=null) 
					scope.formula != '';
				var formula = angular.copy(scope.formula);
				if(formula==undefined || formula==null) formula='';
				var opIndex = 0;
				while(formula.length>0) {
					
					var op = formula.match(/[\*\+\-\/\(\)]/);
					if(op!=undefined && op!=null && op!='') {
						opIndex = formula.indexOf(op);
						if(opIndex==-1) opIndex = formula.length;
					}
					else
						opIndex = formula.length;
					var tk = formula.substring(0,opIndex);
					console.log('Tk:');
					console.log(tk);

					if(/Q[0-9]+_[0-9]+/.test(tk)) {
						var t = tk.replace('Q','D ');
						t = t.replace('_',' R ');
						var nDom = t.substring(1+t.indexOf('D'),t.indexOf('R')).trim();
						var nAns = t.substring(1+t.indexOf('R'),t.length).trim();
						scope.formulaTags.push({ title: t, value: tk, type: 'choices', description: 'Valore corrispondente alla scelta '+nAns+' della domanda '+nDom });
						console.log('Multi');
						console.log(t);
					}
					else if(/Q[0-9]+/.test(tk)) {
						var t = tk.replace('Q','D ');
						var nDom = t.substring(1+t.indexOf('D'),t.length).trim();
						scope.formulaTags.push({ title: t, value: tk, type: 'question', description: 'Valore corrispondente alla risposta data alla domanda '+nDom });
						console.log('question');
						console.log(t);
					}
					else if(/[0-9]+/.test(tk)) {
						scope.formulaTags.push({ title: tk, value: tk, type: 'constant', description: 'Costante numerica di valore '+tk});
						console.log('Consta');
						console.log(tk);
					}
					
					
					//inserisce l'operatore
					var str = formula.charAt(opIndex)=='*' ? 'x':formula.charAt(opIndex);
					if(str!='') {
						scope.formulaTags.push({ title: str, value: formula.charAt(opIndex), type: 'math', description: str});
						console.log('Math');
						console.log(str);
					}
					
					formula = formula.substring(opIndex+1,formula.length);
					console.log('formula: '+formula);
					
					
				}
				
				
				/*
				for(var j=lastTokenIndex; j<formula.length; j++) {
					var cc = formula.charAt(j);
					var val = cc=='*' ? 'x' : cc;
					scope.formulaTags.push({ title: val, value: cc, type: 'math' });
				}
				*/
			}
			
			scope.$watchCollection( "questions", function(nv,ov) {
				console.log('Change questions!');
				console.log(nv);
				scope.questionTags = [];
				scope.formulaTags = [];
				scope.choicesTags = [];
				
				if(nv!=undefined && nv!=null) {
					
					for(var i=0; i<nv.length; i++) {
						
						if(nv[i].type!='FREE' && nv[i].type!='MULTIPLE_CHOICE' && nv[i].type!='TEXT_BLOCK' && nv[i].type!='PAGE_JUMPS') {
							var fTag = nv[i].formulaTag!=undefined && nv[i].formulaTag!=null ? nv[i].formulaTag : 'Q'+(i+1);
							console.log('Add Tag:'+fTag);
							scope.questionTags.push({ title: 'D '+parseInt(i+1), value:fTag, type:"question", description: 'Valore corrispondente alla risposta data alla domanda '+(i+1)});
						}
						else if(nv[i].type=='MULTIPLE_CHOICE') {
							for(var k=0; k<nv[i].replies.length; k++) {
								var fTag = nv[i].replies[k].formulaTag!=undefined && nv[i].replies[k].formulaTag!=null ? nv[i].replies[k].formulaTag : 'Q'+(i+1)+'_'+(k+1);
								console.log('Add Tag:'+fTag);
								scope.choicesTags.push({ title: 'D '+parseInt(i+1)+" R "+parseInt(k+1), value: fTag, type: "choices", description: 'Valore corrispondente alla scelta '+(k+1)+' della domanda '+(i+1)});
							}
						}	
					}
					
					console.log('Tags:');
					console.log(scope.choicesTags);
					console.log(scope.questionsTags);
				}
				
				console.log('nv');
				console.log(nv);
			},true);
			
			
			
			scope.editFormula = function() {
				scope.edit = true;
				scope.parseFormula();
			}
			scope.closeFormula = function() {
				scope.edit = false;
			}

			 scope.sortableOptions = {
					 'ui-floating': true,
					 containment: 'parent',
					 zIndex: 999,
					 opacity: 0.8,
					 axis: 'x'
			 };
		   
			
		}
	}
}]);

app.directive('viewerSetDimensionsOnload', function(){
	return {
	    restrict: 'A',
	    link: function(scope, element, attrs){
	    	var viewerHeight = 'calc(100vh - 100px)';
	    	element.css('height', viewerHeight);
	    }
	}
});

/************************************************************************
 * Directive che va estesa e non utilizzata direttamente: va estesa con *
 * dependencies
 * templateUrl
 * controllerName
 * modalSize
 * 
 * Vedere sotto per il dettaglio dei possibili parametri, tenendo presente che
 * - se isModal e view non vengono passati verrà stampata la lista
 * 
 ************************************************************************/
app.directive("baseList", function($rootScope, $modal, $ocLazyLoad, $controller, $sce, $templateRequest, $compile, $state, MessageService) {
	return {
		restrict: 'A',
		scope: {
			isModal : "=?", // Se aprire al click con una modale
			view : "=?", // Se indicata una view verrà fatto un redirect alla view al click sull'element della directive
			showTitle : "=?",
			searchFixedFilters : "=?searchFilters", // Filtri di ricerca
			searchFixedObjectFilters : "=?searchObjectFilters", // Filtri di ricerca
			showFilters : "<?", // Se visualizzare i filtri di ricerca
			preInit : "=?", // Funzione da chiamare prima dell'init del controller
			detailView : "=?", // Se indicata una view verrà fatto un redirect alla view al click sull'element della directive
			detailNewDefaultData : "<?",
			isSelection : "=?",
			selectedData : "=?",
			selectedId : "=?",
			isMultipleSelection : "=?",
			postConfirmSelectionFn : "=?",
			postCancelSelectionFn : "=?",
			postModalConfirmFn : "=?",
			filterStoreKey : "@?",
			forceModalMode : "=?",
			customOptions : "=?" //Bidone della spazzatura per passare qualsiasi cosa (poi va gestito dove serve eh...)
		},
		link: function($scope, element, attrs, ctrls) {
			$scope.constants = $rootScope.constants;
			$scope.isInternalStaff = $rootScope.isInternalStaff;
			$scope.isCustomer = $rootScope.isCustomer;
			$scope.isHcp = $rootScope.isHcp;
			$scope.isPatient = $rootScope.isPatient;
			$scope.isSupplier = $rootScope.isSupplier;
			$scope.isLogistic = $rootScope.isLogistic;
			
			$scope.dependencies = $scope.dependencies || [];
			$scope.templateUrl = $scope.templateUrl || undefined;
			$scope.controllerName = $scope.controllerName || undefined;
			$scope.modalSize = $scope.modalSize || 'lg';
			
			$scope.showFilters = $scope.showFilters !== undefined ? $scope.showFilters : true;
			$scope.isSelection = $scope.isSelection || false;
			$scope.isMultipleSelection = $scope.isMultipleSelection || false;
			
			$scope.uiSettings = $rootScope.uiSettings;
			
			if($scope.forceModalMode!=undefined) 
				$scope.isModal = $scope.forceModalMode;
			else 
				$scope.isModal = $scope.isModal || $scope.isSelection;
			
			$scope.detailNewDefaultData = $scope.detailNewDefaultData || {};
			
			$ocLazyLoad.load( $scope.dependencies ).then(
				function() {
					if($scope.isModal) {
						if($scope.templateUrl !== undefined && $scope.controllerName !== undefined) {
							$scope.templateUrl = $sce.getTrustedResourceUrl($scope.templateUrl);
							$scope.detailView = undefined;
							element.css('cursor','pointer');
							element.bind("click", function() {
								
								if($scope.isSelection) {
									if($scope.selectedData === undefined) {
										$scope.selectedData = [];
										$scope.selectedId = [];
									}
									else {
										if(!angular.isArray($scope.selectedData)) {
											$scope.isMultipleSelection = false;
											$scope.singleSelectedData = $scope.selectedData;
											$scope.selectedData = [];
											$scope.selectedData.push($scope.singleSelectedData);
											$scope.selectedId = [];
											$scope.selectedId.push($scope.singleSelectedData.id);
										}
										else {
											$scope.selectedId = [];
											for(var i=0; i<$scope.selectedData.length; i++) {
												$scope.selectedId.push($scope.selectedData[i].id);
											}
										}
									}
								}
								
								$scope.closeButton = true;
								$scope.popup = true;
								$scope.modal =  $modal.open({
									scope: $scope,
									templateUrl: $scope.templateUrl,
									controller: $scope.controllerName,
									size: $scope.modalSize,
									windowClass: $scope.modalSize === 'xl' ? 'largeModal' : undefined
								});

								$scope.modal.result.then(function() {
									if(!$scope.isMultipleSelection) {
										$scope.selectedData = ($scope.selectedData !==undefined && $scope.selectedData.length>0) ? $scope.selectedData[0] : undefined;
										$scope.selectedId = ($scope.selectedData !==undefined) ? $scope.selectedData.id : undefined;
									}
									if($scope.postModalConfirmFn) {
										$scope.postModalConfirmFn()
									}
								}, function () {
									if(!$scope.isMultipleSelection) {
										$scope.selectedData = $scope.singleSelectedData;
										$scope.selectedId = $scope.singleSelectedData !== undefined ? $scope.singleSelectedData.id : undefined;
									}
								});
							});
						}
						else {
							MessageService.showError('Errore', 'Errore imprevisto');
						}
					}
					else {
						if($scope.view !== undefined) {
							element.bind("click", function() {
								$state.go($scope.view, { id: $scope.id });
							});
						}
						else {
							if($scope.templateUrl !== undefined && $scope.controllerName !== undefined) {
								$scope.templateUrl = $sce.getTrustedResourceUrl($scope.templateUrl);
								$templateRequest($scope.templateUrl).then(function(template) {
									element.html(template);
									$compile(element.contents())($scope);
									var controller = $controller($scope.controllerName, { $scope: $scope });
								}, function() {
									MessageService.showError('Errore', 'Errore imprevisto');
							    });
							}
						}
					}
				}
			)
		}
	};
});


/************************************************************************
 * Directive che va estesa e non utilizzata direttamente: va estesa con *
 * dependencies
 * templateUrl
 * controllerName
 * modalSize
 ************************************************************************/
app.directive("baseDetail", function($rootScope, $modal, $ocLazyLoad, $controller, $sce, $templateRequest, $compile, $state, MessageService) {
	return {
		restrict: 'A',
		scope: {
			id : "=detailId",
			newDefaultData : "=?",
			isModal : "=?",
			view : "=?view",
			showTitle : "=?",
			customData : "=customData",
			postInit : "=?",
			tabsIndex : "=?",
			postFn : "=?",
			detailObject : "=?"
		},
		link: function($scope, element, attrs, ngModel) {
			$scope.constants = $rootScope.constants;
			
			$scope.showTab = function(index) {
				if($scope.tabsIndex === undefined) {
					return true;
				}
				else if(angular.isArray($scope.tabsIndex)) {
					return $scope.tabsIndex.indexof(index) >= 0;
				}
				else {
					return $scope.tabsIndex === index;
				}
			}
			
			$scope.dependencies = $scope.dependencies || [];
			$scope.templateUrl = $scope.templateUrl || undefined;
			$scope.controllerName = $scope.controllerName || undefined;
			$scope.modalSize = $scope.modalSize || 'lg';
			$scope.windowClass = $scope.windowClass || 'largeModal',
			
			$scope.isModal = $scope.isModal || false;
			$scope.showTitle = $scope.showTitle || true;
			$scope.newDefaultData = $scope.newDefaultData || {};
			$scope.detailObject = $scope.detailObject || {};
			
			$ocLazyLoad.load( $scope.dependencies ).then(
				function() {
					if($scope.isModal) {
						if($scope.templateUrl !== undefined && $scope.controllerName !== undefined) {
							$scope.templateUrl = $sce.getTrustedResourceUrl($scope.templateUrl);
							element.css('cursor','pointer');
							element.bind("click", function() {
								$scope.closeButton = true;
								$scope.popup = true;
								
								if($scope.detailObject !== undefined) {
									$scope.detailReturn = [];
								}
								
								$scope.modal =  $modal.open({
									scope: $scope,
									templateUrl: $scope.templateUrl,
									controller: $scope.controllerName,
									size: $scope.modalSize,
									windowClass: $scope.windowClass
								});
	
								$scope.modal.result.then(function() {
									$scope.detailObject = ($scope.detailReturn !==undefined && $scope.detailReturn.length>0) ? $scope.detailReturn[0] : undefined;
									
									if($scope.postFn) {
										$scope.postFn()
									}
//									else if($scope.$parent && $scope.$parent.refresh) {
//										$scope.$parent.refresh()
//									}
									
								}, function () {
									$scope.detailObject = ($scope.detailReturn !==undefined && $scope.detailReturn.length>0) ? $scope.detailReturn[0] : undefined;
									
									if($scope.postFn) {
										$scope.postFn()
									}
//									else if($scope.$parent && $scope.$parent.refresh) {
//										$scope.$parent.refresh()
//									}
								});
							});
						}
					}
					else {
						if($scope.view !== undefined) {
							element.bind("click", function() {
								$state.go($scope.view, { id: $scope.id });
							});
						}
						else {
							if($scope.templateUrl !== undefined && $scope.controllerName !== undefined) {
								$scope.templateUrl = $sce.getTrustedResourceUrl($scope.templateUrl);
								$templateRequest($scope.templateUrl).then(function(template) {
									element.html(template);
									$compile(element.contents())($scope);
									var controller = $controller($scope.controllerName, { $scope: $scope });
								}, function() {
									MessageService.showError('Errore', 'Errore imprevisto');
							    });	
							}
						}
					}
				}
			)
		}
	};
});

'use strict';

app.directive("documentCategoryList", function(baseListDirective) {
	return angular.extend({}, baseListDirective[0], { controller: function($scope) {
		$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-document-category.js' ];
		$scope.templateUrl = 'tpl/directives/document-category/document-category-list.html';
		$scope.controllerName = 'DocumentCategoryListCtrl';
		$scope.modalSize = 'lg';
	} });
});

app.directive("documentCategoryDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-document-category.js' ];
			$scope.templateUrl = 'tpl/directives/document-category/document-category-detail.html';
			$scope.controllerName = 'DocumentCategoryDetailCtrl';
			$scope.modalSize = 'md';
		}
	});
});

app.directive("documents", function($modal, $ocLazyLoad, $state, PermissionService, DocumentService, UploaderService, MessageService, $rootScope, SMART_TABLE_CONFIG, $window) {
	return {
		restrict: 'A',
		scope : {
			entityType: "=entityType",
			entityId: "=documents",
			filterStoreKey : "@?"
		},
		controller : function($scope, $state) {
			$scope.isInternalStaff = $rootScope.isInternalStaff;
			$scope.isHcp = $rootScope.isHcp;
			$scope.isPatient = $rootScope.isPatient;
			$scope.constants = $rootScope.constants;
			$scope.init = function() {
				$scope.categories = [];
				DocumentService.getCategories({ 'entityType': $scope.entityType }, {}, function(results) {
					$scope.categories = results.data;
					UploaderService.initFileUploader($scope, $scope.entityId, $scope.entityType);
					$scope.loadDocuments();
				}, function(error) {
					MessageService.showError('Errore','Problema nel recupero delle categorie documenti')
				});
			};
			
			$scope.loadDocuments = function() {
				
				$scope.showList = false;
				$scope.searchFilters = {
						filters: { 'ENTITY_ID' : $scope.entityId || "", 'ENTITY_TYPE' : $scope.entityType }
				}
				PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
					$scope.permissions = result.data;
					$scope.initPermission(result.data);
					$scope.initPaginationAndFilter();
					$scope.showList = true;
				}, function(error) {
					$scope.list = [];
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			};
			
			$scope.search = function(tableState, smCtrl) {
				$scope.smCtrl = $scope.smCtrl || smCtrl;
				if (tableState === undefined) {
					if ($scope.smCtrl !== undefined) {
						$scope.smCtrl.tableState().pagination.start = 0;
						$scope.smCtrl.pipe();
					}
					return;
				}
				if(tableState !== undefined) {
					tableState.pagination.number = $scope.pagination.size;
					
					$scope.pagination.start = tableState.pagination.start;
					if (tableState.sort != null && tableState.sort.predicate !== undefined) {
						$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
					}
					if (tableState.pagination.number !== undefined) {
						$scope.pagination.size = tableState.pagination.number;
					}
				}
				$scope.list = $scope.list || [];
				
				angular.extend($scope.searchFilters, $scope.pagination);
				delete $scope.searchFilters.total;
				
				DocumentService.search({}, $scope.searchFilters, function(result) {
					$scope.list = result.data;
					$scope.permissions = result.permissions;
					$scope.pagination.start = result.start;
					$scope.pagination.size = result.size;
					$scope.pagination.sort = result.sort;
					$scope.pagination.total = result.total;
					if (tableState !== undefined) {
						tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
					}
					
					if($scope.filterStoreKey) {
						try {
							$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
							$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
						}
						catch(e) {
							console.log(e);
						}
					}
	            }, function(error) {
	            	$scope.list = [];
	            	$scope.initPaginationAndFilter();
	            	if(error.status == 404) {
	            		$state.go('access.not-found');
	            	}
	        	});
			}
			
			$scope.edit = function(id) {
				$scope.popup = true;
				$scope.id = id;
				$scope.modal =  $modal.open({
					scope: $scope,
					templateUrl: 'tpl/directives/document/document-detail.html',
					size: 'lg',
					windowClass: 'largeModal',
					controller: function() {
						$scope.initDetails = function() {
							$scope.detail = {};
							
							DocumentService.get({ id : $scope.id }, function(result) {
								$scope.detail = result.data;
								$scope.permissions = result.permissions;
								$scope.fieldPermissions = result.fieldPermissions;
							}, function(error) {
								$scope.detail = {};
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});
						}
						
						$scope.undo = function() {
							$scope.initDetails();
							MessageService.showSuccess('Le modifiche sono state annullate');
						}
						
						$scope.save = function() {
							$scope.errors = $scope.validate !== undefined ? $scope.validate() : {};
							if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
								var request = $scope.loadRequest();
								DocumentService.update({ id : $scope.id }, request, function(result) {
									$scope.detail = result.data;
									$scope.warnings = result.responseWarnings;
									if($scope.warnings!==undefined){
										MessageService.showWarning('Aggiornamento completato con presenza di warning.');
									}else{
										MessageService.showSuccess('Aggiornamento completato con successo');
									}
									$scope.modal.close();
									$scope.loadDocuments();
								}, function(error) {
									$scope.errors = error;
									if (error.status == 404) {
										$state.go('access.not-found');
									}
									else {
										MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
									}
								});
							} else {
								MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
							}
						}
						
						$scope.loadRequest = function() {
							var categoryIds = [];
							for(var i=0; i<$scope.detail.categories.length; i++) {
								categoryIds.push($scope.detail.categories[i].id)
							}
							var request = angular.copy($scope.detail);
							request['categoryIds'] = categoryIds;
							return request;
						}
						
						$scope.initDetails();
					}
				});
			}
			
			$scope.initPaginationAndFilter = function() {
				$scope.pagination = {
						start: 0,
						size: SMART_TABLE_CONFIG.TABLE_SIZE,
						sort: []
				};
				
				$scope.searchFilters = $scope.searchFilters || {};
				$scope.searchFilters.filters = $scope.searchFilters.filters || {};
				
				$scope.objectFilters = {};
				
				$scope.initialSearchFilters = angular.copy($scope.searchFilters);
				
				if($scope.filterStoreKey) {
					try {
						var savedFilters = $window.sessionStorage[$scope.filterStoreKey];
						if(savedFilters) {
							savedFilters = angular.fromJson(savedFilters);
						}
						if(savedFilters) {
							angular.extend($scope.searchFilters, savedFilters);
							$scope.pagination.start =  savedFilters.start;
							$scope.pagination.size =  savedFilters.size;
							$scope.pagination.sort =  savedFilters.sort;
						}
						
						var objectFilters = $window.sessionStorage[$scope.filterStoreKey + '-object'];
						if(objectFilters) {
							objectFilters = angular.fromJson(objectFilters);
						}
						if(objectFilters) {
							angular.extend($scope.objectFilters, objectFilters);
						}
					}
					catch(e) {
						console.log(e);
					}
				}
			};
			
			$scope.initPermission = function(permissions) {
				if (permissions != undefined && permissions != null) {
					$scope.canInsert = permissions.INSERT;
					$scope.canDelete = permissions.DELETE;
					$scope.canUpdate = permissions.UPDATE;
				} 
				else {
					$scope.canInsert = false;
					$scope.canDelete = false;
					$scope.canUpdate = false;
				}
			}
			
			$scope.clearFilters = function(){
				$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
				
				if($scope.filterStoreKey) {
					try {
						$window.sessionStorage.removeItem($scope.filterStoreKey);
						$window.sessionStorage.removeItem($scope.filterStoreKey + '-object');
					}
					catch(e) {
						console.log(e);
					}
				}
			}
		},
		templateUrl: 'tpl/directives/document/document-list.html',
		link: function (scope, element, attrs, ngModel) {
			scope.init();
		}
	};
});

app.directive("documentViewer", function($http, $modal, $sce, MessageService, DocumentService, RESTURL, $rootScope, $cookies, $window) {
	return {
		restrict: 'A',
		scope : {
			id : "=id",
			title : "=title",
			action : "=action",
			ownerId: "=ownerId",
			filename: "=filename",
			callbackFn: '&callbackFn'
		},
		controller: function($scope) {
			$scope.openInNewWindow =  function() {
				$window.open($scope.pdf, '_blank');
			}
		},
		link: function (scope, element, attrs, ngModel) {
			element.bind("click", function() {
				scope.h = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
				//Determina azione in base all'estensione del file
				scope.act = ('delete'!=scope.action) ? 'download' : 'delete';
				if(scope.act=='delete') {
					//Elimina documento
					var dialogTitle = "Conferma eliminazione documento";
					var dialogText = "Vuoi eliminare questo documento?";
					var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-danger','sm');
					modalInstance.result.then(
						function(confirm) {
							DocumentService.deleteDocument({ id: scope.id }, function(result) {
								MessageService.showSuccess('Documento eliminato con successo','');
								if(scope.callbackFn!=undefined) scope.callbackFn();
							}, 
							function(error) {
								MessageService.showError('Operazione non riuscita', '');
							});
						});
				}
				else {
					DocumentService.getDownloadInfo({ id: scope.id }, undefined, function(result) {
						var type = result.data.type;
						var filename = result.data.fileName;
						var extension = filename.substr(1+filename.lastIndexOf('.'),filename.length-1);
						
						if(result.data.disclaimerMessage) {
							var dialogTitle = "";
							var dialogText = result.data.disclaimerMessage;
							var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-danger');
							modalInstance.result.then(
								function(confirm) {
									$http({method: 'GET', url: RESTURL.DOCUMENT+'/'+scope.id+'/url/'+result.data.disclaimerId, headers: { 'X-AUTH-TOKEN' : scope.h }}).success(function(response) {
										scope.pdf = $sce.trustAsResourceUrl(response.data);
										scope.videoType = 'video/'+extension;
										scope.vgPlaysInline = false;
										if(type == 'VIDEO' && !($rootScope.deviceDetector.isMobile() || $rootScope.deviceDetector.isTablet())) {
											 $modal.open({
												scope: scope,
												templateUrl: 'tpl/directives/document/video-viewer.html',
												size: 'lg',
												windowClass: 'videoModal',
												controller: function() {
													
													scope.onPlayerReady = function(API) {
														scope.API = API;
														if(scope.API.playsInline == undefined)
														scope.API.playsInline = scope.vgPlaysInline;
													};
													
													scope.onChangeSource = function($source) {
														scope.API.play();
													};
													
													scope.videoPlayerConfig = {
														preload: "none",
														
											            sources: [{ src: scope.pdf, type : ''+scope.videoType }],
											            tracks: [],
											            theme: {
										                    url: "libs/addons/videogular/videogular-custom.css"
										                }
										                
													}
													
													
												}
											});
										}
										else if(type=='DOCUMENT_PDF' && !($rootScope.deviceDetector.isMobile() || $rootScope.deviceDetector.isTablet())) {
											$modal.open({
												scope: scope,
												templateUrl: 'tpl/directives/document/pdf-viewer.html',
												windowClass: 'largeModal',
												size: 'lg'
											});
										}
										else if(type=='IMAGE' && !($rootScope.deviceDetector.isMobile() || $rootScope.deviceDetector.isTablet())) {
											$modal.open({
												scope: scope,
												templateUrl: 'tpl/directives/document/image-viewer.html',
												windowClass: 'largeModal',
												size: 'lg'
											});
										}
										else {
											var element = angular.element('<a/>');
											element.attr({
												download:filename,
												href: response.data,
												target: '_blank',
			
											})[0].click();
											
										}
									}).error(function(data, status, headers, config) {
										MessageService.showError('Download non riuscito', '');
									});
								});
						}
						else {
							$http({method: 'GET', url: RESTURL.DOCUMENT+'/'+scope.id+'/url', headers: { 'X-AUTH-TOKEN' : scope.h }}).success(function(response) {
								scope.pdf = $sce.trustAsResourceUrl(response.data);
								scope.videoType = 'video/'+extension;
								scope.vgPlaysInline = false;
								if(type == 'VIDEO' && !($rootScope.deviceDetector.isMobile() || $rootScope.deviceDetector.isTablet())) {
									 $modal.open({
										scope: scope,
										templateUrl: 'tpl/directives/document/video-viewer.html',
										size: 'lg',
										windowClass: 'videoModal',
										controller: function() {
											
											scope.onPlayerReady = function(API) {
												scope.API = API;
												if(scope.API.playsInline == undefined)
												scope.API.playsInline = scope.vgPlaysInline;
											};
											
											scope.onChangeSource = function($source) {
												scope.API.play();
											};
											
											scope.videoPlayerConfig = {
												preload: "none",
												
									            sources: [{ src: scope.pdf, type : ''+scope.videoType }],
									            tracks: [],
									            theme: {
								                    url: "libs/addons/videogular/videogular-custom.css"
								                }
								                
											}
											
											
										}
									});
								}
								else if(type=='DOCUMENT_PDF' && !($rootScope.deviceDetector.isMobile() || $rootScope.deviceDetector.isTablet())) {
									$modal.open({
										scope: scope,
										templateUrl: 'tpl/directives/document/pdf-viewer.html',
										windowClass: 'largeModal',
										size: 'lg'
									});
								}
								else if(type=='IMAGE' && !($rootScope.deviceDetector.isMobile() || $rootScope.deviceDetector.isTablet())) {
									$modal.open({
										scope: scope,
										templateUrl: 'tpl/directives/document/image-viewer.html',
										windowClass: 'largeModal',
										size: 'lg'
									});
								}
								else {
									var element = angular.element('<a/>');
									element.attr({
										download:filename,
										href: response.data,
										target: '_blank',
	
									})[0].click();
									
								}
							}).error(function(data, status, headers, config) {
								MessageService.showError('Download non riuscito', '');
							});
						}
					});
				}
			});
		}
	};
});

app.directive('staticInclude', function($templateRequest, $compile) {
	return {
		restrict: 'A',
		transclude: true,
		replace: true,
		scope: false,
		link: function($scope, element, attrs, ctrl, transclude) {
			$templateRequest(attrs.staticInclude).then(function(response) {
	            var linkFn = $compile(response);
	            var content = linkFn($scope);
				element.append(content);
			});
		}
	};
});

app.directive('regionSelect', function(TerritoryService) {
	return {
		restrict: 'A',
		scope : {
			country: "=?",
			region: "=regionSelect",
			province: "=?",
			city: "=?",
			label: "=?",
			disabled: "=?"
		},
		templateUrl: 'tpl/app/directive/region-select.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.label = angular.isDefined($scope.label) ? $scope.label : "Regione";
				$scope.country = angular.isDefined($scope.country) ? $scope.country : "IT"; 
				$scope.disabled = angular.isDefined($scope.disabled) ? $scope.disabled : false;
				$scope.address = {
						country: $scope.country,
						region: $scope.region
				}
			}
		},
		link: function(scope) {
			
			scope.$watch('country', function(nv, ov) {
				if(angular.isDefined(nv)) {
					scope.address.country = nv;
					scope.regions = [];
					scope.filters = {
							filters: { 'COUNTRY_CODE': scope.address.country }
					};
					TerritoryService.searchRegions({}, scope.filters, function(result) {
						scope.regions = result.data;
					}, function(error) {
						scope.regions = [];
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
			}, true);
			
			scope.$watch('address', function(nv, ov) {
				scope.region = scope.address.region;
			}, true);
			
			scope.$watch('region', function(nv, ov) {
				scope.address.region = scope.region;
				scope.province = undefined;
				scope.city = undefined;
			}, true);
			
			scope.init();
		}
	}
});

app.directive('provinceSelect', function(TerritoryService) {
	return {
		restrict: 'A',
		scope : {
			country: "=?",
			region: "=?",
			province: "=provinceSelect",
			city: "=?",
			label: "=?",
			disabled: "=?"
		},
		templateUrl: 'tpl/app/directive/province-select.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.label = angular.isDefined($scope.label) ? $scope.label : "Provincia";
				$scope.country = angular.isDefined($scope.country) ? $scope.country : "IT"; 
				$scope.disabled = angular.isDefined($scope.disabled) ? $scope.disabled : false;
				$scope.address = {
						country: $scope.country,
						region: $scope.region,
						province: $scope.province
				}
			}
		},
		link: function(scope) {
			
			scope.$watch('address', function(nv, ov) {
				scope.province = nv.province;
			}, true);
			
			scope.$watch('country', function(nv, ov) {
				if(angular.isDefined(nv)) {
					scope.address.country = nv;
					scope.provinces = [];
					scope.filters = {
							filters: { 
								'COUNTRY_CODE': scope.address.country,
								'REGION_CODE': scope.address.region
							}
					};
					TerritoryService.searchProvinces({}, scope.filters, function(result) {
						scope.provinces = result.data;
					}, function(error) {
						scope.provinces = [];
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
			}, true);
			
			scope.$watch('region', function(nv, ov) {
				scope.address.region = nv;
				scope.provinces = [];
				scope.filters = {
						filters: { 
							'COUNTRY_CODE': scope.address.country,
							'REGION_CODE': scope.address.region
						}
				};
				TerritoryService.searchProvinces({}, scope.filters, function(result) {
					scope.provinces = result.data;
				}, function(error) {
					scope.provinces = [];
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, true);
			
			scope.$watch('province', function(nv, ov) {
				scope.address.province = nv;
				scope.city = undefined;
			}, true);
			
			scope.init();
		}
	}
});

app.directive('citySelect', function(TerritoryService) {
	return {
		restrict: 'A',
		scope : {
			country: "=?",
			region: "=?",
			province: "=?",
			city: "=citySelect",
			label: "=?",
			disabled: "=?"
		},
		templateUrl: 'tpl/app/directive/city-select.html',
		controller: function($scope) {
			$scope.init = function() {
				$scope.label = angular.isDefined($scope.label) ? $scope.label : "Città";
				$scope.country = angular.isDefined($scope.country) ? $scope.country : "IT"; 
				$scope.disabled = angular.isDefined($scope.disabled) ? $scope.disabled : false;
				$scope.address = {
						country: $scope.country,
						region: $scope.region,
						province: $scope.province,
						city: $scope.city
				}
			}
		},
		link: function(scope) {
			
			scope.$watch('address', function(nv, ov) {
				if(angular.isDefined(nv.city) && nv.city.name) {
					scope.city = nv.city.name;
				} else {
					scope.city = undefined;
				}
			}, true);
			
			scope.$watch('country', function(nv, ov) {
				if(angular.isDefined(nv)) {
					scope.address.country = nv;
					scope.cities = [];
					scope.filters = {
							filters: { 
								'COUNTRY_CODE': scope.address.country,
								'REGION_CODE': scope.address.region,
								'PROVINCE_CODE': scope.address.province
							}
					};
					TerritoryService.searchTowns({}, scope.filters, function(result) {
						scope.cities = result.data;
					}, function(error) {
						scope.cities = [];
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
			}, true);
			
			scope.$watch('region', function(nv, ov) {
				scope.address.region = nv;
				scope.cities = [];
				scope.filters = {
						filters: { 
							'COUNTRY_CODE': scope.address.country,
							'REGION_CODE': scope.address.region,
							'PROVINCE_CODE': scope.address.province
						}
				};
				TerritoryService.searchTowns({}, scope.filters, function(result) {
					scope.cities = result.data;
				}, function(error) {
					scope.cities = [];
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, true);
			
			scope.$watch('province', function(nv, ov) {
				scope.address.province = nv;
				scope.cities = [];
				scope.filters = {
						filters: { 
							'COUNTRY_CODE': scope.address.country,
							'REGION_CODE': scope.address.region,
							'PROVINCE_CODE': scope.address.province
						}
				};
				TerritoryService.searchTowns({}, scope.filters, function(result) {
					scope.cities = result.data;
				}, function(error) {
					scope.cities = [];
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, true);
			
			scope.$watch('city', function(nv, ov) {
				if(angular.isDefined(nv)) {
					scope.address.city = {
							name: nv
					};
				} else {
					delete scope.address.city;
				}
			}, true);
			
			scope.init();
		}
	}
});

/**
 * @author:christian
 * @directive: error-directive ----> errorTooltip
 * <p error-validation    placement="bottom" clazz="error"  trigger="hover" timeout="2340" />
 */
~(function () {
	'use strict';
	angular.module('app').directive('errorTooltip', ErrorValidatorDirective);
	
	ErrorValidatorDirective.$inject = ['$compile'];
	
	function ErrorValidatorDirective($compile) {
		var directive = {
	        bindToController: true,
	        controller: ValidatorController,
	        replace: true,
	        controllerAs: 'vm',
	        link: errorsObserver,
	        restrict: 'AE'
	    };
	    return directive;
	
	    function errorsObserver(scope, element, attrs, observeFn) {
	        observeFn(scope, element, attrs);
	    }
	}
	
	/* @ngInject */
	function ValidatorController() {
	
	    /**
	     * @param: scope - angular scope
	     * @param: element - angular element ($element)
	     * @param: attrs - angular attrs
	     */
	    var observeFn = function (scope, element, attrs) {
	        scope.$watch('errors', function (newValue, oldValue) { //observer for error rest response ($scope.errors)
	            if (newValue != oldValue) {
	                if (!isEmpty(newValue)) {
	                    directiveCallback(scope, attrs, element, newValue, false);
	                }
	            }
	        }, true);
	
	        scope.$watch('warnings', function (newValue, oldValue) { //observer for warning rest response ($scope.warnings)
	            if (newValue !== oldValue) {
	                if (!isEmpty(newValue)) {
	                    directiveCallback(scope, attrs, element, newValue, true);
	                }
	            }
	        }, true);
	
	        scope.$$postDigest(function () {
	        }); //si puo usare come callback, invocata al termine del $digest cycle
	    };
	
	
	    /**
	     * @param: error - Response Data from Rest Service
	     */
	    function directiveCallback(scope, attrs, element, error, isWarning) {
	    	var fieldtoValidate = attrs['errorTooltip'] || attrs['ngModel'] || attrs['checkList'];
	        var responseData = undefined;
	        var highlightedClass = '';
	        var tooltipClass = '';
	        if (!isWarning) {
	        	responseData = {
	                statusText: error.statusText,
	                alerts: error.data.errors
	            }
	        	highlightedClass = 'error';
	        	tooltipClass = 'tooltip-error';
	        }
	        else {
	        	responseData = {
	    			statusText: error.statusText,
	    			alerts: error.warnings
	            }
	        	highlightedClass = 'warning';
	        	tooltipClass = 'tooltip-warning';
	        }
	        
	        var fieldNormalized, binding;
	        if (fieldtoValidate !== undefined && responseData.alerts !== undefined) {
	        	if(fieldtoValidate.split('.').length > 1 && fieldtoValidate.split('.').length < 3) {
	        		fieldNormalized = fieldtoValidate.substring(fieldtoValidate.indexOf('.') + 1);
	        	}else if(fieldtoValidate.split('.').length > 1 && fieldtoValidate.split('.').length < 4) {
	        		fieldNormalized = fieldtoValidate.substring(fieldtoValidate.indexOf('ctrl.') + 1);
	        	}
	        	else {
	        		fieldNormalized = fieldtoValidate.split('.')[1];	
	        	}
	        	if(fieldNormalized.indexOf('form.') == 0) {
	        		fieldNormalized = fieldNormalized.substring(5);
	        	}
	
	            if (responseData.alerts[fieldNormalized] !== undefined) {
	                binding = {
	                    field: fieldNormalized || fieldtoValidate.substr(0, 3),
	                    message: responseData.alerts[fieldNormalized].message
	                };
	            }
	        }
	        
	        var highlightedElement = angular.element(element);
	        var tooltipElement = angular.element(element);
	        if(attrs['checkList'] !== undefined) {
	        	tooltipElement = element.closest('.radio');
	        	highlightedElement = tooltipElement.find('i');
	        }
	        else if(attrs['type'] !== undefined && attrs['type'] === 'checkbox') {
	        	tooltipElement = element.closest('div');
	        	highlightedElement = tooltipElement.find('i');
	        }
	        
	        highlightedElement.removeClass(highlightedClass);
	        tooltipElement.popover('destroy');
	        
	        if (typeof binding !== "undefined") {
	        	highlightedElement.addClass(highlightedClass);
	        	
	        	tooltipElement.popover({
	                content: binding.message,
	                placement: 'top',
	                container: false,
	                trigger: 'hover',
	                template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
	            });
	        	
	        	tooltipElement.on('click', function() {
	        		if(!$(element).is(':disabled')) {
	        			highlightedElement.removeClass(highlightedClass);
	            		tooltipElement.popover('destroy');
	        		}
	        	});
	        }
	//        else {
	//        	highlightedElement.removeClass(highlightedClass);
	//    		tooltipElement.popover('destroy');
	//        }
	    };
	    return observeFn;
	}
})(angular);

app.directive("userDisclaimerDetail", function(baseDetailDirective) {
	return angular.extend({}, baseDetailDirective[0], {
		controller: function($scope) {
			$scope.dependencies = [ 'ui.select', 'smart-table', 'toaster', 'js/controllers/directives/crm-user-disclaimer.js' ];
			$scope.templateUrl = 'tpl/app/directive/userdisclaimer/user-disclaimer-detail.html';
			$scope.controllerName = 'UserDisclaimerDetailCtrl';
			$scope.modalSize = 'xl';
		}
	});
});

app.directive("questionnaireComplementaryActivityWarningLabel", function($rootScope,QuestionnaireService,MessageService) {
	return {
		restrict: 'E',
		transclude: true,
		replace: true,
		scope : {
			id : '=',
			userType : '='
		},
		template: '<label><sub><i>{{message}}</i></sub></label>',
		controller: function($scope,QuestionnaireService,MessageService) {
			$scope.init = function() {
				$scope.message = "Se l'attività non viene inclusa non sarà creato nemmeno il questionario";
				var selfApprovable = false;
				if($scope.id!=undefined) {
					QuestionnaireService.get({ id: $scope.id}, function(result) {
						if(result!=undefined) {
							if(result.data.compilers!=undefined) {
								for(var i=0; i<result.data.compilers.length; i++) {
									if(result.data.compilers[i].userType==$scope.userType) {
										selfApprovable = true;
										break;
									}
								}
							}
							if(!selfApprovable && result.data.approvers!=undefined) {
								for(var i=0; i<result.data.compilers.length; i++) {
									if(result.data.approvers[i].userType==$scope.userType) {
										selfApprovable = true;
										break;
									}
								}
							}
							if(selfApprovable) {
								$scope.message = "Se l'attività non viene inclusa non sarà creato nemmeno il questionario ad essa collegato e pertanto il destinatario non potrà compilarlo";
							}
						}
					}, function(error) {
						MessageService.showError('Errore nel recupero del questionario');
					})
				}
			}
		},
		link: function(scope) {
			scope.init();
		}
	}
});