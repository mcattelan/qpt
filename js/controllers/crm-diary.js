'use strict'

app.controller('DiaryController', function(
		$scope,
		$stateParams,
		$state,
		PatientService,
		PhuturemedService,
		UserQuestionnaireService,
		RESTURL,
		$sce,
		$modal,
		$http) {

	
	$scope.init = function() {
		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if(angular.isDefined($scope.id) && !$scope.isNew()) {
			PatientService.get({id: $scope.id}, function(response) {
				$scope.patient = response.data;
				$scope.initList();	
			}, function(error) {
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			})
			
		}
	}
	
	$scope.initList = function() {
		$scope.initPaginationAndFilter();
		$scope.showList = true;
	}
	
	$scope.search = function(tableState, smCtrl) {
		$scope.showChart = false;
		$scope.chartDataTaken = [];
		$scope.chartDataExpectedTaken = [];
		$scope.chartDataDate = [];
		
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
        
		PhuturemedService.patientAdherences({ patientId: $scope.id }, function(result) {
			$scope.states = result.data;
			
			//chiamata 
			$scope.listUserQuest = [];
			$scope.questSearchFilters = { 
					filters : {
						'USER_ID' : $scope.id,
						'PERMANENT': true
					}
			};
		
			UserQuestionnaireService.search({}, $scope.questSearchFilters, function(result) {
				$scope.listUserQuest = result.data;
				
				$scope.list = [];
				var item = {};
				var year;
				var month;
				for(var i = 0; i < $scope.states.length; i++) {
					if($scope.states[i].month === month && $scope.states[i].year === year) {
						item.adherencePercentage.push($scope.states[i].adherencePercentage);
						item.endPeriod.push($scope.states[i].endPeriod);
						item.startPeriod.push($scope.states[i].startPeriod);
						item.totalExpectedTaken.push($scope.states[i].totalExpectedTaken);
						item.totalTaken.push($scope.states[i].totalTaken);
						item.patientInactive.push($scope.states[i].patientInactive);
					} else {
						item = {
								year: $scope.states[i].year,
								month: moment.months()[$scope.states[i].month-1],
								adherencePercentage: [$scope.states[i].adherencePercentage],
								endPeriod: [$scope.states[i].endPeriod],
								startPeriod: [$scope.states[i].startPeriod],
								totalExpectedTaken: [$scope.states[i].totalExpectedTaken],
								totalTaken: [$scope.states[i].totalTaken],
								patientInactive: [$scope.states[i].patientInactive]
						}
						$scope.list.push(item);
					}
					for(var x = 0; x < $scope.listUserQuest.length; x++) {
						var userQuest = $scope.listUserQuest[x];
						
						if(userQuest.userQuestionnaire.submissionDate >= $scope.states[i].startPeriod && userQuest.userQuestionnaire.submissionDate <= $scope.states[i].endPeriod) {
							if(userQuest.questionnaire.code === 'QUIPERTE_QUEST_022'){
								item['soddisfazioneQuest'] = userQuest.userQuestionnaire.finalScore.Score;
							} else if(userQuest.questionnaire.code === 'QUIPERTE_QUEST_023'){
								item['monitoraggioQuest'] = userQuest.userQuestionnaire.finalScore.Score;
							} else if(userQuest.questionnaire.code === 'QUIPERTE_QUEST_020'){
								item['moriskyQuest'] = userQuest.userQuestionnaire.finalScore.Score;
							} 
						}
					}
					month = $scope.states[i].month;
					year = $scope.states[i].year;
				}
			}, function(error) {
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			

			$scope.showChart = true;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 10,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);		
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.init();
});

app.controller('DiaryControllerCustom', function(
		$scope,
		$stateParams,
		$state,
		PatientService,
		RESTURL,
		$sce,
		$modal,
		$http) {

	
	$scope.init = function() {
		$scope.selects = {};
		$scope.options = {};
		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if(angular.isDefined($scope.id) && !$scope.isNew()) {
			PatientService.get({id: $scope.id}, function(response) {
				$scope.patient = response.data;
				$scope.initList();	
			}, function(error) {
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			})
			
		}
		$scope.customFilter = {};
	}
	
	$scope.initList = function() {
		$scope.initPaginationAndFilter();
		$scope.showList = true;
	}
	
	$scope.search = function(tableState, smCtrl) {
		$scope.showChart = false;
		$scope.chartDataTaken = [];
		$scope.chartDataExpectedTaken = [];
		$scope.chartDataDate = [];
		
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
        // {'drug','year','month','usePluggymed','period':
		//[{'adherencePercentage','range','moriskyQuest','totalTaken',
		// 'satisfactionScore', 'diaryScore', 'bgColor', 'warning', 'bgWidth'}..]}
		PatientService.diaryDetails({ id: $scope.id },{}, function(result) {
			$scope.totalItems = result.data;
			$scope.options.usePluggymed = false;
			var drugs = [];
			for(var j = 0; j < result.data.length; j++) {
				var mainObj = result.data[j];
				$scope.options.usePluggymed = mainObj.usePluggymed;
				if(drugs.indexOf(mainObj.drug) == -1){
					drugs.push(mainObj.drug);
				}
				for(var i = 0; i < mainObj.monthlyTrend.length; i++) {
					var item = {};
					var monthlyTrend = mainObj.monthlyTrend[i];
					item.drug = mainObj.drug;
					item.usePluggymed = mainObj.usePluggymed;
					item.year = monthlyTrend.year;
					item.month = monthlyTrend.month;
					item.period = [];
					for(var x = 0; x < monthlyTrend.periods.length; x++) {
						var period = monthlyTrend.periods[x];
						var p = {};
						p.patientActive = period.patientActive;
						if(mainObj.usePluggymed){
							p.totalTaken = period.totalTaken;
							if(period.patientActive){
								p.adherencePercentage = period.adherencePercentage;
								p.bgColor = $scope.getBgColorByPercentage(period.adherencePercentage);
								if(p.adherencePercentage === undefined) {
									p.range = 'Dal ' + period.startDay + ' al ' + period.endDay + ': no rilevazioni';
								}
							}else{
								p.bgColor = '#a9a9a9';
								p.range = 'Dal ' + period.startDay + ' al ' + period.endDay + ': sospensione';
							}
						}
						p.moriskyScore = period.moriskyScore;
						p.bgWidth = $scope.getWidthByPercentage(p.adherencePercentage);
						p.satisfactionScore = period.satisfactionScore;
						p.diaryScore = period.diaryScore;
						if(p.adherencePercentage != undefined){
							p.warning = p.adherencePercentage > 100; 
							p.adherencePercentage = p.adherencePercentage + '%'; 
						}
						item.period.push(p);
					}
					$scope.list.push(item);
				}
			}
			$scope.selects['Drugs'] = drugs;
			if($scope.selects['Drugs'].length > 0){
				$scope.customFilter.drug = $scope.selects['Drugs'][0].id;
			}
			console.log($scope.list);
			$scope.showChart = true;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.getBgColorByPercentage = function(p){
		var color = undefined;
		if(p != undefined){
			if(p < 80){
				color = '#F20A0A';
			}else if(p >= 80 && p <= 89.99){
				color = '#ED7708';
			}else if(p >= 90 && p <= 96.99){
				color = '#F2DD0F';
			}else if(p >= 97 && p <= 99.99){
				color = '#78EA74';
			}else if(p >= 100){
				color = '#0DA409';
			}
		}
		return color;
	}
	
	$scope.getWidthByPercentage = function(p){
		var w = '';
		if(p != undefined){
			if(p > 100){
				w = 100;
			}else{
				w = p;
			}
		}else{
			w = 100;
		}
		return w + '%';
	}
	
	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 10,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);		
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.init();
});