'use strict'

app.controller('SummaryController', function($rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state, 
		PatientService, 
		PatientTherapyService,
		PatientAssumptionService,
		PatientMedicalExaminationService,
		UserServiceConfService,
		UserActivityService,
		UserEventService,
		TherapyService,
		FormUtilService,
		MessageService,
		REGEXP) {
	
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		
		if ($scope.loggedUser != undefined && $scope.loggedUser) {
			$scope.initDetails();
		}
	}
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.therapies = [];
		$scope.timeline = [];
		
		PatientService.get({ id : $scope.loggedUser.id}, function(result) {
			$scope.detail = result.data;
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		var therpaySearchFilters = {
				filters: { "PATIENT_ID" : $scope.loggedUser.id }
		}
		PatientTherapyService.search({}, therpaySearchFilters, function(result) {
			$scope.therapies = result.data;
		}, function(error) {
			$scope.therapies = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
		
		var searchDate = moment(moment().format('YYYY-MM-DD'));
		
		var assumptionSearchFilters = {
				filters: {}
		}		
		var medicalExaminationSearchFilters = {
				filters: {}
		}
		var userActivityFilters = {
				filters: { 
					"ALL_USER_ACTIVITIES" : $scope.loggedUser.id,
					"STATUS" : ['CONFIRMED', 'OCCURRED', 'COMPLETED', 'NOT_OCCURRED', 'CONFIRMED_NO_RESULT']
				}
		}
		var userPersonalEventFilters = {
				filters: { 
					"TYPE" : 'PERSONAL_EVENT',
					"RELATED_USER_ID": $scope.loggedUser.id
				}
		}
		
		PatientAssumptionService.search({ patientId : $scope.loggedUser.id }, assumptionSearchFilters, function(result) {
			if (result.data != undefined && result.data) {
				for(var i = 0; i < result.data.length; i++) { 
					var item = {
							date : result.data[i].date,
							data : result.data[i],
							text : 'ASSUMPTION'
					};
					$scope.timeline.push(item);
				}
			}
			
			PatientMedicalExaminationService.search({ patientId : $scope.loggedUser.id }, medicalExaminationSearchFilters, function(result) {
				if (result.data != undefined && result.data) {
					for(var i = 0; i < result.data.length; i++) { 
						var item = {
								date : result.data[i].date,
								data : result.data[i],
								text : 'EXAMINATION'
						};
						$scope.timeline.push(item);
						
					}
				}
				UserActivityService.search({}, userActivityFilters, function(result) {
					if (result.data != undefined && result.data) {
						for(var i = 0; i < result.data.length; i++) { 
							var item = {
									date : result.data[i].date,
									data : result.data[i],
									text : 'ACTIVITY'
							};
							$scope.timeline.push(item);
						}
					}
					$scope.oderTimelineEvents();
//					UserEventService.search({}, userPersonalEventFilters, function(result) {
//						if (result.data != undefined && result.data) {
//							for(var i = 0; i < result.data.length; i++) { 
//								var item = {
//										date : result.data[i].date,
//										data : result.data[i],
//										text : 'ACTIVITY'
//								};
//								$scope.timeline.push(item);
//							}
//						}
//						$scope.oderTimelineEvents();
//					}, function(error) {
//						$scope.activities = [];
//						if(error.status == 404) {
//							$state.go('access.not-found');
//						}
//					});
				}, function(error) {
					$scope.activities = [];
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.therapies = [];
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.therapies = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
	}
		
	$scope.oderTimelineEvents  = function(){
		var item = {
				date : new Date().getTime(),
				data : "NOW",
				text : "NOW"
		}
		$scope.timeline.push(item);
		for (var k=0; k<$scope.timeline.length; k++) {
			for (var j=0; j<$scope.timeline.length; j++) {
				if ($scope.timeline[j].date<=$scope.timeline[k].date) {
					var tmp = $scope.timeline[j];
					$scope.timeline[j] = $scope.timeline[k];
					$scope.timeline[k] = tmp;
				}
			}
		}
		var index = 0;
		for (var k=0; k<$scope.timeline.length; k++) {
			if ($scope.timeline[k].text == 'NOW') {
				index = k;
				break;
			}
		}
		$scope.timelineTemp = [];
		var count = 5;
		for (var k=0; k<$scope.timeline.length; k++) {
			if (k >= (index - 3) && k < index) {
				$scope.timelineTemp.push($scope.timeline[k]);
				count--;
			} 
		}
		for (var k=index; k<$scope.timeline.length; k++) {
			if (k <= (index + count)) {
				$scope.timelineTemp.push($scope.timeline[k]);
			} else {
				break;
			}
		}
		$scope.timeline = angular.copy($scope.timelineTemp);
	}
	
	$scope.init()
});