'use strict'

app.controller('UserServiceConfController', function(
		$rootScope, 
		$scope, 
		$timeout, 
		$stateParams, 
		$http, 
		$state,
		UserServiceConfService) {
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		
		if ($scope.id == null || $scope.id == undefined) {
			$scope.id = $stateParams.id;
		}
		$scope.initDetails();
	}

	$scope.initDetails = function() {
		$scope.canModify = false;
		$scope.isEditable = $scope.canModify;
		$scope.detail = {};
		
		UserServiceConfService.get({id: $scope.id}, function(result) {
			$scope.detail = result.data;
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.init();
});
