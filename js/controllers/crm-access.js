'use strict';

app.controller('LoginController', function(
		$rootScope,
		$scope,
		$http,
		$state,
		$cookies,
		UserService,
		Base64,
		RESTURL,
		$window) {

	$scope.errors = undefined;

	$scope.login = function(isCheckLogin) {
		if(isCheckLogin === undefined) isCheckLogin = false;
		$scope.errors = undefined;
		//create auth cookie
		if(!isCheckLogin) {
			$http.post(RESTURL.LOGIN, { username: $scope.username, password: $scope.password }, { headers: { 'X-AUTH-CLIENTID' : 'CRM-WEB-UI'} })
			.success(function (result, status, headers) {
				var token = headers('X-AUTH-TOKEN');
				if(token !== undefined && token != null) {
					$cookies.putObject('italiassistenza_ua', [ $scope.username, token ], { path: '/', domain: $rootScope.baseDomain, secure: $rootScope.secure });
					UserService.getLoggedUser(
							{},
							function(response) {
								$rootScope.loggedUser = response.data;
								var date = new Date();
								date.setTime(date.getTime() + (3 * 60 * 1000));
								$cookies.putObject('login', [ $scope.username, token ], { path: '/', expires: date, domain: $rootScope.baseDomain, secure: $rootScope.secure });
								$state.go('app.dashboard');
							},
							function(error) {
								if(!isCheckLogin) {
									$cookies.remove('italiassistenza_ua', { path: '/', domain: $rootScope.baseDomain });
									$scope.errors = 'Nome utente o password errati';
								}
							}
					);
				}
			})
			.error(function (result, status, headers) {
				if(!isCheckLogin) {
					$cookies.remove('italiassistenza_ua', { path: '/', domain: $rootScope.baseDomain });
					$cookies.remove('login', { path: '/', domain: $rootScope.baseDomain });
					$scope.errors = 'Nome utente o password errati';
				}
			});
		}
		else {
			UserService.getLoggedUser(
					{},
					function(response) {
						$rootScope.loggedUser = response.data;
						//$cookies.putObject('italiassistenza_ua', [ $rootScope.loggedUser.nickName, $cookies.getObject('italiassistenza_ua')[1] ], { path: '/', domain: $rootScope.baseDomain, secure: $rootScope.secure });
						$state.go('app.dashboard');	
					},
					function(error) {
						if(!isCheckLogin) {
							$cookies.remove('italiassistenza_ua', { path: '/', domain: $rootScope.baseDomain });
							$scope.errors = 'Nome utente o password errati';
						}
						else {
							$cookies.remove('italiassistenza_ua', { path: '/', domain: $rootScope.baseDomain });
						}
					}
			);
		}
	}

	$scope.init = function() {
		$scope.login(true);
	}

	$scope.init();
});

app.controller('LogoutController', function(
		$rootScope,
		$scope,
		$http,
		$state,
		$cookies,
		RESTURL) {

	if ($cookies.getObject('italiassistenza_ua') ===undefined || $cookies.getObject('italiassistenza_ua') == null) {
		$state.go('access.login');
		$rootScope.loggedUser = undefined;
	} else {
		var authToken = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
		$http.post(RESTURL.LOGOUT, { }, { headers: { 'X-AUTH-TOKEN' : authToken} }).success(function (result, status, headers) {
			$cookies.remove('italiassistenza_ua', { path: '/', domain: $rootScope.baseDomain });
			$cookies.remove('login', { path: '/', domain: $rootScope.baseDomain });
			$cookies.remove('hide_disclaimer', { path: '/', domain: $rootScope.baseDomain });
			$state.go('access.login');
			$rootScope.loggedUser = undefined;
		}).error(function (result, status, headers) {
			$cookies.remove('italiassistenza_ua', { path: '/', domain: $rootScope.baseDomain });
			$cookies.remove('login', { path: '/', domain: $rootScope.baseDomain });
			$cookies.remove('hide_disclaimer', { path: '/', domain: $rootScope.baseDomain });
			$state.go('access.login');
		});
	}
});

app.controller('PasswordRecoveryController', ['$rootScope', '$scope', '$http', '$state', '$stateParams', '$location', '$timeout', '$cookies', 'Base64', 'MessageService', 'UserService', '$anchorScroll', 'FormUtilService', 'RESTURL','REGEXP', function(
		$rootScope,
		$scope,
		$http,
		$state,
		$stateParams,
		$location,
		$timeout,
		$cookies,
		Base64,
		MessageService,
		UserService,
		$anchorScroll,
		FormUtilService,
		RESTURL,
		REGEXP) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.detail = {};
		$scope.forgotPasswordToken = $stateParams.token;
		console.log($stateParams, $scope.forgotPasswordToken);

		if(angular.isDefined($scope.forgotPasswordToken) && !angular.equals($scope.forgotPasswordToken,"")) {

			UserService.changeForgotPasswordRequest({ 'token' : $scope.forgotPasswordToken }, function(response) {
				if (response) {
					if (!response.data == true){
						MessageService.showError('Impossibile completare il processo di recupero password', 'Link non valido');
						$state.go('access.login');
					}
				} else {
					MessageService.showError('Impossibile completare il processo di recupero password', 'Link non valido');
					$state.go('access.login');
				}
			}, function(error) {
				MessageService.showError('Impossibile completare il processo di recupero password', error.data !== undefined ? error.data.message :  'Link non valido');
				console.log(error);
				$state.go('access.login');
			} );
		}
	}

	$scope.validateForgotPasswordRequest = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.nickName', value : $scope.detail.nickName, type: 'string', dependRequired: 'detail.email', pattern: REGEXP.NICK_NAME, customMessage:'Username non valido e/o troppo corto'},
					{ id:'detail.email', value : $scope.detail.email, type: 'email', dependRequired: 'detail.nickName', pattern: REGEXP.EMAIL,customMessage:'Indirizzo email non valido'},
					]
		};
		errors = FormUtilService.validateForm(form);
		return errors;
	}

	$scope.validateChangePasswordRequest = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.password', value : $scope.detail.password, type: 'password', required:true,   pattern: REGEXP.PASSWORD },
					{ id:'detail.passwordConfirmation', value : $scope.detail.passwordConfirmation, required:true,  type: 'password',  pattern: REGEXP.PASSWORD},
					]
		};
		errors = FormUtilService.validateForm(form);
		if($scope.detail.password != $scope.detail.passwordConfirmation){
			errors['detail.passwordConfirmation'] = "Le password non coincidono";
		}
		return errors;
	}

	$scope.forgotPasswordRequest = function() {
		MessageService.hideToaster();
		$scope.errors = $scope.validateForgotPasswordRequest();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			UserService.forgotPasswordConfirm({}, $scope.detail, function(response) {
				MessageService.showSuccess('Richiesta recupero password completata con successo', 'A breve riceverai un\'email con il link per procedere alla modifica della tua password');
				$state.go('access.login');
			}, function(error) {
				MessageService.showError('Richiesta recupero password fallita', error.data !== undefined ? error.data.message : '');
				/*console.log(MessageService.showError('gfgfgfdgdf','fggfdgdfg'));
				window.m = MessageService;*/
				console.log(error);
			});
		} else {
			MessageService.showError('Richiesta recupero password fallita', 'Alcuni dati inseriti non sono corretti.');
		}
	}

	$scope.changeForgotPassword = function() {
		MessageService.hideToaster();

		$scope.errors = $scope.validateChangePasswordRequest();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {


			UserService.changeForgotPasswordConfirm({ 'token': $scope.forgotPasswordToken }, $scope.detail, function(response) {
				MessageService.showSuccess('Processo di modifica password completato con successo', '');

				if (response.data.nickName !== undefined && $scope.detail.password !== undefined) {
					$http.post(RESTURL.LOGIN, { username: response.data.nickName, password: $scope.detail.password }, { headers: { 'X-AUTH-CLIENTID' : 'CRM-WEB-UI'} }).success(function (result, status, headers) {
						console.log(headers);
						var token = headers('X-AUTH-TOKEN');
						if (token !== undefined && token != null) {
							$cookies.putObject('italiassistenza_ua', [ response.data.nickName, token ], { path: '/', domain: $rootScope.baseDomain, secure: $rootScope.secure });
							$state.go('app.dashboard');
						} else{
							$state.go('access.login');
						}
					});
				} else{
					$state.go('access.login');
				}
			}, function(error) {
				MessageService.showError('Impossibile completare il processo di modifica password', error.data !== undefined ? error.data.message : '');
				console.log(error);
			});
		} else {
			MessageService.showError('Impossibile completare il processo di modifica password', 'Alcuni dati inseriti non sono corretti.');
		}
	}

	$scope.init();
}]);


app.controller('ActivationController', ['$rootScope', '$scope', '$http', '$state', '$stateParams', '$location', '$timeout', '$cookies', 'Base64', 'MessageService', 'UserService', '$anchorScroll', 'FormUtilService', 'RESTURL','REGEXP', function(
		$rootScope,
		$scope,
		$http,
		$state,
		$stateParams,
		$location,
		$timeout,
		$cookies,
		Base64,
		MessageService,
		UserService,
		$anchorScroll,
		FormUtilService,
		RESTURL,
		REGEXP) {

	$scope.init = function() {
		$scope.detail = {};
		$scope.errors = {};
		$scope.showForm = false;

		$scope.activationToken = $stateParams.token;

		if(angular.isDefined($scope.activationToken)) {
			UserService.activationRequest({ 'token' : $scope.activationToken }, function(response) {
				if (response) {
					if (!response.data == true){
						MessageService.showError('Impossibile completare il processo di attivazione', 'Link non valido');
						$state.go('access.not-found');
					}else{
						$scope.showForm = true;
					}
					
				} else {
					MessageService.showError('Impossibile completare il processo di attivazione', 'Link non valido');
				}
			}, function(error) {
				MessageService.showError('Impossibile completare il processo di attivazione', error.data !== undefined ? error.data.message : 'Link non valido');
				$state.go('access.not-found');
			});
		} else {
			MessageService.showError('Impossibile completare il processo di attivazione', 'Link non valido');
			$state.go('access.not-found');
		}
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.nickName', value : $scope.detail.nickName, type: 'string', required : true, pattern: REGEXP.NICK_NAME, customMessage:'Username non valido e/o troppo corto'},
					{ id:'detail.password', value : $scope.detail.password, required:true, type: 'password',pattern:REGEXP.PASSWORD, customMessage:'La password deve essere alemeno lunga 8 caratteri contenere almeno una minuscola, una maiuscola e un numero.' },
					{ id:'detail.passwordConfirmation', value : $scope.detail.passwordConfirmation, required:true, type: 'password',pattern:REGEXP.PASSWORD   },
					]
		};
		errors = FormUtilService.validateForm(form);
		if(($scope.detail.password != '' || $scope.detail.passwordConfirmation != '') && $scope.detail.password != $scope.detail.passwordConfirmation){
			errors['detail.passwordConfirmation'] = "Le password non coincidono";
		}
		return errors;
	}

	$scope.activate = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {

			UserService.activationConfirm({'token': $scope.activationToken }, {nickName: $scope.detail.nickName, password: $scope.detail.password}, function(response) {
				MessageService.showSuccess('Processo di attivazione completato con successo', 'Da ora puoi accedere al portale e a tutte le funzionalità a te dedicate');

				if($scope.detail.nickName !== undefined && $scope.detail.password !== undefined) {
					$http.post(RESTURL.LOGIN, { username: $scope.detail.nickName, password: $scope.detail.password }, { headers: { 'X-AUTH-CLIENTID' : 'CRM-WEB-UI'} })
					.success(function (result, status, headers) {
						console.log(headers);
						var token = headers('X-AUTH-TOKEN');
						if(token !== undefined && token != null) {
							$cookies.putObject('italiassistenza_ua', [ $scope.detail.nickName, token ], { path: '/', domain: $rootScope.baseDomain, secure: $rootScope.secure });
							$state.go('app.dashboard');
						}
						else{
							$state.go('access.login');
						}
					});
				}
				else{
					$state.go('access.login');
				}
			},
			function(error) {
				MessageService.showError('Impossibile completare il processo di attivazione', error.data !== undefined ? error.data.message : '');
				console.log(error);
			}
			);
		}
		else {
		
			MessageService.showError('Impossibile completare il processo di attivazione', 'Alcuni dati inseriti non sono corretti.');
		}
	}

	$scope.init();
}]);
