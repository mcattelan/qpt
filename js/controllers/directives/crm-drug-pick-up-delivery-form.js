'use strict'

app.controller('DrugPickUpDeliveryFormController', function(
		$rootScope,
		$scope,
		$state,
		$modal,
		PharmacyService,
		PatientTherapyDrugService,
		MessageService) {

	$scope.init = function() {
		$scope.isInternalStaff = $rootScope.isInternalStaff;
		$scope.selects = [];
		
		PatientTherapyDrugService.search({ patientId: $scope.formActivity.user.id }, {}, function(result) {
			$scope.selectedDrugIds = [];
			for(var i=0; i<result.data.length; i++) {
				if(angular.isUndefined($scope.selects['Drug'])) {
					$scope.selects['Drug'] = [];
				}
				if($scope.selectedDrugIds.indexOf(result.data[i].drug.id) < 0) {
					$scope.selectedDrugIds.push(result.data[i].drug.id);
					$scope.selects['Drug'].push(result.data[i].drug);
				}
			}
		}, function(error) {
			MessageService.showError('Errore in reperimento farmaci');
			if(error.status == 404) {
				$state.go('access.not-found');
			} 
		})
		$scope.initDetails();
	};
	
	$scope.initDetails = function() {
		$scope.detail = $scope.form.entity;
		$scope.detail.patient = $scope.formActivity.user;
		$scope.detail.pickUpOwner = $scope.formActivity.owner;
		$scope.detail.pickUpDate = angular.isDefined($scope.detail.pickUpDate) ? $scope.detail.pickUpDate : $scope.formActivity.date;
		if(angular.isDefined($scope.detail.pickUpDate)) {
			PharmacyService.getPatientActivePharmacy({patientId: $scope.detail.patient.id}, {date: $scope.detail.pickUpDate}, function(result) {
				$scope.detail.pharmacy = result.data;
				$scope.detail.pickUpAddress = $scope.detail.pharmacy.address;
			}, function(error) {
				MessageService.showError('Errore in reperimento farmacia attiva del paziente');
				if(error.status == 404) {
					$state.go('access.not-found');
				} 
			})
		} 
		
		$scope.detail.pickUpLocationType = 'PHARMACY';
		//$scope.detail.items()
		$scope.detail.deliveryOwner = $scope.formActivity.owner;
		$scope.detail.deliveryDate = $scope.formActivity.date;
		$scope.detail.deliveryLocationType = $scope.formActivity.locationType;
		$scope.detail.deliveryAddress = $scope.formActivity.address;
	};
	
	$scope.editRow = function(index) {
		if(angular.isDefined(index)) {
			$scope.row = $scope.detail.items[index];
			if(angular.isDefined($scope.row.drug)) {
				$scope.selects['MeasurementUnits'] = [];
				for(var i=0; i<$scope.row.drug.measurementUnits.length; i++) {
					$scope.selects['MeasurementUnits'].push($scope.row.drug.measurementUnits[i].text);
				}
//				if(angular.isDefined($scope.row.measurementUnit)) {
//					if($scope.selects['MeasurementUnits'].indexOf($scope.row.measurementUnit) < 0)
//						$scope.selects['MeasurementUnits'].push($scope.row.measurementUnit);
//				}
			}
		} else {
			$scope.row = {};
			if($scope.selects['Drug'].length == 1) {
				$scope.row.drug = $scope.selects['Drug'][0];
				if(angular.isDefined($scope.row.drug)) {
					$scope.selects['MeasurementUnits'] = [];
					for(var i=0; i<$scope.row.drug.measurementUnits.length; i++) {
						$scope.selects['MeasurementUnits'].push($scope.row.drug.measurementUnits[i].text);
					}
//					if(angular.isDefined($scope.row.measurementUnit)) {
//						if($scope.selects['MeasurementUnits'].indexOf($scope.row.measurementUnit) < 0)
//							$scope.selects['MeasurementUnits'].push($scope.row.measurementUnit);
//					}
				}
			}
		}
		
		$scope.emptyMeasurementUnits = true;
		if(angular.isDefined($scope.selects['MeasurementUnits'])) {
			if($scope.selects['MeasurementUnits'].length > 0) {
				if(angular.isDefined($scope.row.measurementUnit)) {
					if($scope.selects['MeasurementUnits'].indexOf(measurementUnit) < 0) {
						$scope.emptyMeasurementUnits = true;
					}
				}
				$scope.emptyMeasurementUnits = false;
			} else {
				
			}
		}
		
		$scope.modalItem =  $modal.open({
			scope: $scope,
			size: 'lg',
			row: $scope.row,
			templateUrl: 'tpl/app/directive/activiti/drugpickupdelivery/' + $scope.status + '-drug-pick-up-delivery-item.html',
		});
		
		
		$scope.modalItem.result.then(
			function() {
				if(angular.isDefined(index)) {
					$scope.detail.items[index] = $scope.row;
				} else {
					$scope.detail.items.push($scope.row);
				}
			}, function() { 
				
			}
		);
	}
	
	$scope.removeRow = function(index) {
		$scope.detail.items.splice(index, 1);
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}	
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.changeDrugMeasurementUnit = function(drug, measurementUnit) {
		$scope.selects['MeasurementUnits'] = [];
		if(angular.isDefined(drug) && angular.isDefined(drug.measurementUnits)) {
			for(var i=0; i<drug.measurementUnits.length; i++) {
				$scope.selects['MeasurementUnits'].push(drug.measurementUnits[i].text);
			}
			if(drug.measurementUnits.length == 1) {
				$scope.row.measurementUnit = drug.measurementUnits[0].text; 
			}
			if(angular.isDefined(measurementUnit)) {
				if($scope.selects['MeasurementUnits'].indexOf(measurementUnit) < 0) {
					$scope.selects['MeasurementUnits'].push(measurementUnit);
				}
			}
		}
	}
	
	$scope.init();
});