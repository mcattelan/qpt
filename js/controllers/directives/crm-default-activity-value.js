'use strict'
app.controller('DefaultActivityValueListCtrl', function(
		$scope,
		$controller,
		DefaultActivityValueService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DEFAULT_ACTIVITY_VALUE', DefaultActivityValueService);
});

app.controller('DefaultActivityValueDetailCtrl', function(
		$scope,
		$controller,
		$modal,
		DefaultActivityValueService) {
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('DEFAULT_ACTIVITY_VALUE', DefaultActivityValueService);

	$scope.newMasterActivities = [];
	
	$scope.deleteMasterActivity = function(index){
		if (index > -1 && $scope.detail.masterActivities && $scope.detail.masterActivities.length > index) {
			$scope.detail.masterActivities.splice(index, 1);
		}
	}
	
	
	
});