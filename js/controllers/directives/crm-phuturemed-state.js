'use strict'

app.controller('PhuturemedStateController', function(
		$rootScope,
		$scope,
		$stateParams,
		$state,
		PhuturemedService,
		PhuturemedStateService) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.isInternalStaff = $rootScope.isInternalStaff;
		$scope.errors = {};
		$scope.selects = {};

		if (angular.isDefined($scope.id)) {
			$scope.initDetail();
		} else {
			$scope.initList();
		}
	}
	
	$scope.initList = function() {
		$scope.showList = false;
//		PermissionService.getAll({ entityType : 'QUALIFICATION' }, function(result) {
//			$scope.initPermission(result.data);
//			$scope.selection = $scope.selection || false;
//			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			if (angular.isDefined($scope.phuturemedId) && $scope.phuturemedId) {
				$scope.showList = true;
			} else if (angular.isDefined($scope.patientId) && $scope.patientId) {
				var phuturemedFilters = {
						filters: {
							'PATIENT_ID' : $scope.patientId,
							'STATUS' : 'VERIFIED'
						}
				};
				PhuturemedService.search({}, phuturemedFilters, function(result) {
					if (result.data.length > 0) {
						$scope.phuturemedId = result.data[0].id;
						$scope.showList = true;
					} else {
						$scope.showList = undefined;
					}
				}, function(error) {
					$scope.showList = undefined;
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
//		}, function(error) {
//			$scope.list = [];
//			if(error.status == 404) {
//				$state.go('access.not-found');
//			}
//		});
	};
	
	$scope.initDetail = function(){
		$scope.detail = {};
//		$scope.fieldPermissions = {};
		if(!$scope.isNew()){
			PhuturemedStateService.get({id:$scope.id}, function(result) {
				$scope.detail = result.data;
//				$scope.fieldPermissions = result.fieldPermissions;
//				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
//				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
//		else {
//			PermissionService.getAll({ entityType : 'QUALIFICATION' }, function(result) {
//				$scope.initPermission(result.data);
//				
//				PermissionService.getAllFields({ entityType : 'QUALIFICATION' }, function(result) {
//					$scope.fieldPermissions = result.data;
//				}, function(error) {
//					$scope.detail = {};
//					$scope.fieldPermissions = {};
//					if(error.status == 404) {
//						$state.go('access.not-found');
//					}
//				});
//			}, function(error) {
//				$scope.detail = {};
//				if(error.status == 404) {
//					$state.go('access.not-found');
//				}
//			});
//		}
	}
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		if (angular.isUndefined($scope.searchFilters.filters)) {
			$scope.searchFilters = {
					filters : {}
			}
		}
		$scope.searchFilters.filters['PHUTUREMED_ID'] = $scope.phuturemedId;		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

//		$scope.selectedIds = []
//		for(var i=0; i<$scope.selected.length; i++) {
//			$scope.selectedIds[i] = $scope.selected[i].id;
//		}
		PhuturemedStateService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
//				$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
//				for (var i=0; i<$scope.list.length; i++) {
//				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
//				}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});		
	}
	
	$scope.updateState = function() {
		if (angular.isDefined($scope.phuturemedId) && $scope.phuturemedId) {
			$scope.showList = false;
			PhuturemedService.state({ id : $scope.phuturemedId }, function(result) {
				$scope.showList = true;
			}, function(error) {
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}; 
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 25,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);		
	}
	
	$scope.ignore = function(id) {
		PhuturemedStateService.ignore({ id: id }, {}, function(result) {
			$scope.search();
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}
	
	$scope.activate = function(id) {
		PhuturemedStateService.activate({ id: id }, {}, function(result) {
			$scope.search();
		}, function(error) {
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}

//	$scope.initPermission = function(permissions) {
//		if (permissions != undefined && permissions != null) {
//			$scope.canInsert = permissions.INSERT;
//			$scope.canDelete = permissions.DELETE;
//			$scope.canExport = permissions.EXPORT;
//			if ($scope.isNew()) {
//				$scope.canUpdate =  $scope.canInsert;
//			}
//			else {
//				$scope.canUpdate = permissions.UPDATE;
//			}
//		} 
//		else {
//			$scope.canInsert = false;
//			$scope.canDelete = false;
//			$scope.canUpdate = false;
//			$scope.canExport = false;
//		}
//	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.phuturemeds", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.loadRequest = function() {
		return {
			patientId:			$scope.detail.patient.id,
			serialNumber:		$scope.detail.serialNumber,
			pin:				$scope.detail.pin,
			internalNotes:		$scope.detail.internalNotes
		}
	}

	$scope.init();
});