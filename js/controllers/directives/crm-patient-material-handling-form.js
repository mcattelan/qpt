'use strict'

app.controller('PatientMaterialHandlingFormController', function(
		$rootScope,
		$scope,
		$state,
		$modal,
		MessageService,
		MaterialService) {

	$scope.init = function() {
		$scope.selects = [];
		$scope.initDetails();
	};
	
	$scope.initDetails = function() {
		$scope.detail = $scope.form.entity;
		
	};
	
	$scope.editPickUpMaterial = function(index) {
		MaterialService.search({}, {}, function(result) {
			$scope.selects['Material'] = result.data;
		}, function(error) {
			
		});
		
		$scope.modalScope = $scope.$new();
		if(angular.isDefined(index)) {
			$scope.modalScope.detail = $scope.detail.pickUpMaterials[index];
		} else {
			$scope.modalScope.detail = {};
		}
		
		$scope.modalItem =  $modal.open({
			scope: $scope.modalScope,
			size: 'lg',
			windowClass: 'largeModal',
			templateUrl: 'tpl/app/directive/activiti/patientmaterialhandling/' + $scope.status + "/" + $scope.status + '-patient-material-handling-item.html',
			controller: function($scope) {
				
				$scope.$watch('detail.requestReplacement', function(newValue, oldValue) {
					if(newValue != undefined && newValue != oldValue) {
						$scope.detail.replacementMaterial = undefined;
					}
					if(newValue) {
						if(angular.isUndefined($scope.detail.replacementMaterial)) {
							$scope.detail.replacementMaterial = {
									'material': $scope.detail.material.material
							}
						}
					}
				});
				
				$scope.$watch('detail.replacementMaterial.material', function(newValue, oldValue) {
					if(newValue != undefined && newValue != oldValue) {
						$scope.detail.replacementMaterial.userMaterials = [];
						if(angular.isDefined(newValue.materials)) {
							var j = 0;
							for(var i = 0; i < newValue.materials.length; i++) {
								var material = newValue.materials[i];
								var x = 0;
								while(x < material.quantity) {
									$scope.detail.replacementMaterial.userMaterials[j] = {
											'material': material.material,
									}
									x++;
									j++;
								}
							}
						}
					}
				});
				
				$scope.editReplacementUserMaterial = function(index) {
					MaterialService.search({}, {}, function(result) {
						$scope.selects['Material'] = result.data;
					}, function(error) {
						
					});
					
					$scope.modalScope = $scope.$new();
					if(angular.isDefined(index)) {
						$scope.modalScope.detail = angular.copy($scope.detail.replacementMaterial.userMaterials[index]);
					} else {
						$scope.modalScope.detail = {};
					}
					
					$scope.modalItemComponent =  $modal.open({
						scope: $scope.modalScope,
						size: 'lg',
						windowClass: 'largeModal',
						templateUrl: 'tpl/app/directive/activiti/patientmaterialhandling/'  + $scope.status + "/" + $scope.status + '-patient-material-handling-item-component.html',
					});
					
					
					$scope.modalItemComponent.result.then(
						function() {
							if(angular.isDefined(index)) {
								$scope.detail.replacementMaterial.userMaterials[index] = $scope.modalScope.detail;
							} else {
								$scope.detail.replacementMaterial.userMaterials.push($scope.modalScope.detail);
							}
						}, function() { 
							
						}
					);
				}
			}
		});
		
		
		$scope.modalItem.result.then(
			function() {
				if(angular.isDefined(index)) {
					$scope.detail.pickUpMaterials[index] = $scope.modalScope.detail;
				} else {
					$scope.detail.pickUpMaterials.push($scope.modalScope.detail);
				}
			}, function() { 
				
			}
		);
	}
	
	$scope.removePickUpMaterial = function(index) {
		$scope.detail.pickUpMaterials.splice(index, 1);
	}
	
	$scope.editNewMaterial = function(index) {
		MaterialService.search({}, {}, function(result) {
			$scope.selects['Material'] = result.data;
		}, function(error) {
			
		});
		
		$scope.modalScope = $scope.$new();
		if(angular.isDefined(index)) {
			$scope.modalScope.detail = $scope.detail.newMaterials[index];
		} else {
			$scope.modalScope.detail = {};
		}
		
		$scope.modalNewMaterial =  $modal.open({
			scope: $scope.modalScope,
			size: 'lg',
			windowClass: 'largeModal',
			templateUrl: 'tpl/app/directive/activiti/patientmaterialhandling/'  + $scope.status + "/" + $scope.status + '-patient-material-handling-new-material.html',
			controller: function($scope) {
				
				$scope.materialChange = function() {
					$scope.detail.userMaterials = [];
					if(angular.isDefined($scope.detail.material) && angular.isDefined($scope.detail.material.materials)) {
						var j = 0;
						for(var i = 0; i < $scope.detail.material.materials.length; i++) {
							var material = $scope.detail.material.materials[i];
							var x = 0;
							while(x < material.quantity) {
								$scope.detail.userMaterials[j] = {
										'material': material.material,
								}
								x++;
								j++;
							}
						}
					}
				}
				
				$scope.editNewUserMaterial = function(index) {
					MaterialService.search({}, {}, function(result) {
						$scope.selects['Material'] = result.data;
					}, function(error) {
						
					});
					
					$scope.modalScope = $scope.$new();
					if(angular.isDefined(index)) {
						$scope.modalScope.detail = angular.copy($scope.detail.userMaterials[index]);
					} else {
						$scope.modalScope.detail = {};
					}
					
					$scope.modalUserMaterial =  $modal.open({
						scope: $scope.modalScope,
						size: 'lg',
						windowClass: 'largeModal',
						templateUrl: 'tpl/app/directive/activiti/patientmaterialhandling/'  + $scope.status + "/" + $scope.status + '-patient-material-handling-new-material-component.html',
					});
					
					$scope.modalUserMaterial.result.then(
						function() {
							if(angular.isDefined(index)) {
								$scope.detail.userMaterials[index] = $scope.modalScope.detail;
							} else {
								$scope.detail.userMaterials.push($scope.modalScope.detail);
							}
						}, function() { 
							
						}
					);
				}
			}
		});
		
		$scope.modalNewMaterial.result.then(
			function() {
				if(angular.isDefined(index)) {
					$scope.detail.newMaterials[index] = $scope.modalScope.detail;
				} else {
					$scope.detail.newMaterials.push($scope.modalScope.detail);
				}
			}, function() { 
				
			}
		);
	}
	
	$scope.removeNewMaterials = function(index) {
		$scope.detail.newMaterials.splice(index, 1);
	}
	
	$scope.init();
});