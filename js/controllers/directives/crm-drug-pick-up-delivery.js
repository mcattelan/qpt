//app.controller('DrugPickUpDeliveryDetailCtrl', function(
//		$scope,
//		$controller,
//		DrugPickUpDeliveryService) {
//	
//	$controller('BaseDetailCtrl', { $scope: $scope });
//	
//	$scope.init('DRUG_PICK_UP', DrugPickUpDeliveryService);
//});

app.controller('DrugPickUpDeliveryDetailCtrl', function(
		$scope,
		$state,
		$rootScope,
		DrugPickUpDeliveryService,
		PermissionService,
		MessageService) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.complementaryId)) {
			$scope.initDetails();
		}
	}


	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.searchFilters = $scope.searchFilters || {};
		//tramite l'id della complementare cerco di capire se l'oggetto DrugPickUp è stato già salvato oppure no
		DrugPickUpDeliveryService.get({ complementaryId : $scope.complementaryId }, function(result) {
			$scope.detail = result.data;

			//Sovrascrivo con dati della form che passo alla direttiva
			if(angular.isDefined($scope.activitiForm)) {
				//il patient non lo sovrascrivo perchè non può essere cambiato
				if(angular.isDefined($scope.activitiForm.pharmacy) && angular.isDefined($scope.activitiForm.pharmacy.id)) {
					$scope.detail.pharmacy = $scope.activitiForm.pharmacy;
				}
				if(angular.isDefined($scope.activitiForm.owner) && angular.isDefined($scope.activitiForm.owner.id)) {
					$scope.detail.pickUpOwner = $scope.activitiForm.owner;
				}
				if(angular.isDefined($scope.activitiForm.owner) && angular.isDefined($scope.activitiForm.owner.id)) {
					$scope.detail.deliveryOwner = $scope.activitiForm.owner;
				}
				if(angular.isDefined($scope.activitiForm.date)) {
					$scope.detail.pickUpDate = $scope.activitiForm.date;
				}
				$scope.detail.pickUpLocationType = 'PHARMACY';
				//					res.setPickUpAddress(farmacia riferimento);
				if(angular.isDefined($scope.activitiForm.date)) {
					$scope.detail.deliveryDate = $scope.activitiForm.date;
				}
				if(angular.isDefined($scope.activitiForm.locationType)) {
					$scope.detail.deliveryLocationType = $scope.activitiForm.locationType;
				}
				if(angular.isDefined($scope.activitiForm.address) && angular.isDefined($scope.activitiForm.address.id)) {
					$scope.detail.deliveryAddress = $scope.activitiForm.address;
				}
			} 

			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
		}, function(error) {
			$scope.detail = {};
			$scope.fieldPermissions = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.save = function() {
		if ($scope.isNew()) {
			$scope.insert();
		} else {
			$scope.update();
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		DrugPickUpDeliveryService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		DrugPickUpDeliveryService.update({ id : $scope.detail.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	};

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					DrugPickUpService.delete({ id : id, }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initList();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	};

	$scope.removeItem = function(index) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					$scope.detail.items.splice(index, 1);
				}
		);
	};

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.isNew = function() {
		return ($scope.detail.id == undefined || $scope.detail.id == '');
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.loadRequest = function() {
//		private Set<DrugPickUpDeliveryItem> items;
//		private Address pickUpAddress;
//		private Address deliveryAddress;
		
//		var items = [];
//		for(var i = 0; i < $scope.detail.items.length; i++) {
//			var item = $scope.detail.items[i];
//			if(angular.isDefined(item)) {
//				items.push({
//					drugId: item.drug.id,
//					collectedQuantity: item.collectedQuantity,
//					batchNumber: item.batchNumber,
//					drugExpiringDate: item.drugExpiringDate,
//					internalNotes: item.internalNotes
//				})
//			}
//		}
		return {
			patientId : $scope.detail.patient.id,
			pharmacyId : $scope.detail.pharmacy.id,
			pickUpOwnerId : $scope.detail.pickUpOwner.id,
			pickUpDate : $scope.detail.pickUpDate,
			pickUpCode : $scope.detail.pickUpCode,
			pickUpLocationType : $scope.detail.pickUpLocationType,
			pickUpNotes: $scope.detail.pickUpNotes,
			pickUpInternalNotes : $scope.detail.pickUpInternalNotes,
			deliveryOwnerId: $scope.detail.deliveryOwner.id,
			deliveryDate: $scope.detail.deliveryDate,
			deliveryCode: $scope.detail.deliveryCode,
			deliveryLocationType: $scope.detail.deliveryLocationType,
			deliveryNotes: $scope.detail.deliveryNotes,
			deliveryInternalNotes: $scope.detail.deliveryInternalNotes,
//			address : $scope.detail.address,
//			complementaryActivityId : $scope.complementaryId,
//			items : items
		}
	}
	
	$scope.init();

});