'use strict'

app.controller('PatientListCtrl', function(
		$scope,
		$controller,
		PatientService,
		DiseaseService,
		DepartmentService,
		TherapyService,
		$modal,
		MessageService,
		UserService,
		$state) {

	$scope.preInit = function() {
		DiseaseService.search({}, {  start: 0, sort: ['name'] }, function(result) {
			$scope.selects['Disease'] = result.data;
		});
		
		DepartmentService.search({}, {  start: 0, sort: ['name'] }, function(result) {
			$scope.selects['Department'] = result.data;
		});
		
		TherapyService.search({}, {  start: 0, sort: ['name'] }, function(result) {
			$scope.selects['Therapy'] = result.data;
		});
		
		PatientService.delegateHcpsActive(function(result){
			$scope.isDelegateHcp = result.data != undefined ? result.data : false;
		});
	}
	
	$scope.inviteAll = function() {
		$scope.countPatientToInvite();
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/user/invite-all-users.html',
			size: 'md',
		});
	};
	
	$scope.countPatientToInvite = function() {
		$scope.usersToInvite = 0;
		PatientService.countSearchInvite({}, $scope.searchFilters, function(result) {
			$scope.usersToInvite = result.data;
		});
	}
	
	$scope.confirmInvite = function(){
		PatientService.inviteActivationAll({}, $scope.searchFilters, function(result) {
			MessageService.showSuccess('Invito registrazione paziente creato con successo');
		}, function (error){
			MessageService.showError('Invito registrazione paziente non creato');
		});
		$scope.$modalInstance.close();
	};
	
	$scope.undoInvite = function(){
		$scope.$modalInstance.dismiss('cancel');
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					UserService.impersonate({ 'nickName' : nickName }, function(response) {
						$state.go('access.login');
					}, function(error) {
						MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
					});
				}
		);
	}
	
	$scope.invite = function(id) {
		var modalInstance = MessageService.activationConfirm();
		modalInstance.result.then(
				function(confirm) {				
					UserService.activation({ id : id }, {}, function(result) {
						MessageService.showSuccess('Operazione avvenuta con successo', '');
					}, function(error) {
						if(error.status == 404) {
							MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
						}
					});
				}
		);
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
			$scope.canExport = permissions.EXPORT;
			$scope.canInvite = permissions.INVITE;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
			$scope.canInvite = false;
		}
	}
	
	$scope.init('PATIENT', PatientService);
});