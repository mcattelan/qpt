'use strict'

app.controller('MedicalCenterListCtrl', function(
		$scope,
		$controller,
		MedicalCenterService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('MEDICAL_CENTER', MedicalCenterService);
});

app.controller('MedicalCenterDetailCtrl', function(
		$scope,
		$controller,
		MedicalCenterService) {
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('MEDICAL_CENTER', MedicalCenterService);
});