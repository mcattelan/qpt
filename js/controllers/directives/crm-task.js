'use strict';

app.controller('MyTaskListController', function(
		$scope,
		$controller) {
	
	$controller('BaseTaskListController', { $scope: $scope });
	
	$scope.init('todo');
});

app.controller('NotAssignedTaskListController', function(
		$scope,
		$controller) {
	
	$controller('BaseTaskListController', { $scope: $scope });
	
	$scope.init('not-assigned');
});

app.controller('AssignedToOthersTaskListController', function(
		$scope,
		$controller) {
	
	$controller('BaseTaskListController', { $scope: $scope });
	
	$scope.init('assigned-to-others');
});

app.controller('OtherUserTaskListController', function(
		$scope,
		$controller,
		$state, 
		UserService,
		MessageService) {
	
	$controller('BaseTaskListController', { $scope: $scope });
	
	$scope.init('other-user-task');
	
});

app.controller('TaskDetailController', function(
		$rootScope,
		$scope,
		$controller) {
	
	$controller('BaseTaskDetailController', { $scope: $scope });
	
	$scope.init('todo');
});

app.controller('BaseTaskListController', function(
		$scope,
		$rootScope,
		$state,
		PermissionService,
		WorkflowService,
		UserServiceConfService,
		UserActivityService,
		UserService,
		MessageService,
		$modal,
		$window) {
	
	$scope.init = function(type) {
		$scope.type = type;
		if($scope.type === undefined) return;
//		if($scope.type == 'other-user-task' && $rootScope.isInternalStaff()) {
//			$scope.impersonateUser = $rootScope.impersonateUser;
//		}
		$scope.entityType = 'TASK';
		$scope.showList = false;
		
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};
	
		PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
			
			$scope.serviceActivityGroups = [];
			UserServiceConfService.getServiceActivityGroups({}, function(result) {
				$scope.serviceActivityGroups = result.data;
			});
			
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.$watch('searchFilters.filters.SERVICE_ACTIVITY_GROUP_CODE', function(v) {
		$scope.activityGroups = [];
		if(v) {
			UserActivityService.getActivityGroups({ groupCode: v }, function(result) {
				$scope.activityGroups = result.data;
			});	
		}
	}, true);
	
	$scope.search = function(tableState, smCtrl) {
		if(!$scope.showList) return;
		$rootScope.countTasks();
		
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		
		$scope.searchFilters.filters.ASSIGNEE = $scope.objectFilters.supplier ? $scope.objectFilters.supplier.nickName : '';
		$scope.searchFilters.filters.ASSIGNEE = $scope.searchFilters.filters.ASSIGNEE + ($scope.objectFilters.internalStaff ? $scope.objectFilters.internalStaff.nickName : ''); // Faccio la concatenazione così il filtro non restituirà risultati nel caso ci sia supplier e internal staff

		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		
		if($scope.type == 'todo') {
			WorkflowService.searchMyTasks({}, $scope.searchFilters, function(result) {
				$scope.list = result.data;
				$scope.permissions = result.permissions;
				$scope.pagination.start = result.start;
				$scope.pagination.size = result.size;
				$scope.pagination.sort = result.sort;
				$scope.pagination.total = result.total;
				for (var i=0; i<$scope.list.length; i++) {
					$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
				}
				if (tableState !== undefined) {
					tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
				}
				
				if($scope.filterStoreKey) {
					try {
						$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
						$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
					}
					catch(e) {
						console.log(e);
					}
				}
				
				$scope.completeTaskList();
			}, function(error) {
				$scope.list = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		else if($scope.type == 'not-assigned') {
			WorkflowService.searchNotAssignedTasks({}, $scope.searchFilters, function(result) {
				$scope.list = result.data;
				$scope.permissions = result.permissions;
				$scope.pagination.start = result.start;
				$scope.pagination.size = result.size;
				$scope.pagination.sort = result.sort;
				$scope.pagination.total = result.total;
				for (var i=0; i<$scope.list.length; i++) {
					$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
				}
				if (tableState !== undefined) {
					tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
				}
				
				if($scope.filterStoreKey) {
					try {
						$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
						$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
					}
					catch(e) {
						console.log(e);
					}
				}
				$scope.completeTaskList();
			}, function(error) {
				$scope.list = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});	
			$rootScope.queuedTasks = WorkflowService.countNotAssignedTasks
		}
		else if($scope.type == 'assigned-to-others') {
			WorkflowService.searchAssignedToOthersTasks({}, $scope.searchFilters, function(result) {
				$scope.list = result.data;
				$scope.permissions = result.permissions;
				$scope.pagination.start = result.start;
				$scope.pagination.size = result.size;
				$scope.pagination.sort = result.sort;
				$scope.pagination.total = result.total;
				for (var i=0; i<$scope.list.length; i++) {
					$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
				}
				if (tableState !== undefined) {
					tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
				}
				
				if($scope.filterStoreKey) {
					try {
						$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
						$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
					}
					catch(e) {
						console.log(e);
					}
				}
				$scope.completeTaskList();
			}, function(error) {
				$scope.list = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});	
		} else if($scope.type == 'other-user-task') {
			WorkflowService.searchOtherUserTasks({ userId : $scope.userId, userActivityId : $scope.userActivityId}, $scope.searchFilters, function(result) {
				$scope.list = result.data;
				$scope.permissions = result.permissions;
				$scope.pagination.start = result.start;
				$scope.pagination.size = result.size;
				$scope.pagination.sort = result.sort;
				$scope.pagination.total = result.total;
				for (var i=0; i<$scope.list.length; i++) {
					$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
				}
				if (tableState !== undefined) {
					tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
				}
				
				if($scope.filterStoreKey) {
					try {
						$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
						$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
					}
					catch(e) {
						console.log(e);
					}
				}
				$scope.completeTaskList();
			}, function(error) {
				$scope.list = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});	
		}
		else {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		}
	}
	
	$scope.completeTaskList = function() {
		if($scope.list !== undefined) {
			for(var i=0; i<$scope.list.length; i++) {
				if($scope.list[i].variables !== undefined) {
					for(var j=0; j<$scope.list[i].variables.length; j++) {
						if($scope.list[i].variables[j].name === 'assigneeUserName') {
							$scope.list[i].assigneeUserName	= $scope.list[i].variables[j].value;
						}
						if($scope.list[i].variables[j].name === 'userName') {
							$scope.list[i].userName	= $scope.list[i].variables[j].value;
						}
						if($scope.list[i].variables[j].name === 'userType') {
							$scope.list[i].userType	= $scope.list[i].variables[j].value;
						}
						if($scope.list[i].variables[j].name === 'userActivityCode') {
							$scope.list[i].userActivityCode	= $scope.list[i].variables[j].value;
						}
					}
					if(!$scope.list[i].assigneeUserName) {
						$scope.list[i].assigneeUserName = $scope.list[i].assignee;
					}
				}
			}
		}
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 10,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.searchFilters.filters = $scope.searchFilters.filters || {};
		if($scope.searchFixedFilters) {
			angular.extend($scope.searchFilters.filters, $scope.searchFixedFilters);
		}
		
		$scope.objectFilters = {};
		if($scope.searchFixedObjectFilters) {
			angular.extend($scope.objectFilters, $scope.searchFixedObjectFilters);
		}
		
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		
		if($scope.filterStoreKey) {
			try {
				var savedFilters = $window.sessionStorage[$scope.filterStoreKey];
				if(savedFilters) {
					savedFilters = angular.fromJson(savedFilters);
				}
				if(savedFilters) {
					angular.extend($scope.searchFilters, savedFilters);
					$scope.pagination.start =  savedFilters.start;
					$scope.pagination.size =  savedFilters.size;
					$scope.pagination.sort =  savedFilters.sort;
				}
				
				var objectFilters = $window.sessionStorage[$scope.filterStoreKey + '-object'];
				if(objectFilters) {
					objectFilters = angular.fromJson(objectFilters);
				}
				if(objectFilters) {
					angular.extend($scope.objectFilters, objectFilters);
				}
			}
			catch(e) {
				console.log(e);
			}
		}
		
		$scope.patientFilter = undefined;
		$scope.hcpFilter = undefined;
		$scope.supplierFilter = undefined;
		$scope.internalStaffFilter = undefined;
		$scope.preferredInternalStaffFilter = undefined;
		$scope.logisticFilter = undefined;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.patientFilter = undefined;
		$scope.hcpFilter = undefined;
		$scope.supplierFilter = undefined;
		$scope.internalStaffFilter = undefined;
		$scope.preferredInternalStaffFilter = undefined;
		$scope.logisticFilter = undefined;
		
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
		if($scope.searchFixedObjectFilters) {
			angular.extend($scope.objectFilters, $scope.searchFixedObjectFilters);
		}
		
		if($scope.filterStoreKey) {
			try {
				$window.sessionStorage.removeItem($scope.filterStoreKey);
				$window.sessionStorage.removeItem($scope.filterStoreKey + '-object');
			}
			catch(e) {
				console.log(e);
			}
		}
	}
	
	$scope.reloadTasksList = function() {
		$scope.init($scope.type);
	}
	
	//alice 2019-09-13: duplicata function di impersonificazione per problemi di accesso alla variabile globale $modalInstance (non riconosciuta a livello di crm-main.js). 
	//Al momento, questo risulta essere l'unico modo per chiudere la modale con la lista di otheruserTasks prima di impersonificare il responsabile  
	$scope.impersonateUserFromOtherTaskList = function(nickName) {
		if($scope.popup && $scope.closeModal) {
			$scope.closeModal();
		}
		UserService.impersonate({ 'nickName' : nickName }, function(response) {
			$state.go('access.login');
		}, function(error) {
			MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
		});
	} 
});

app.controller('BaseTaskDetailController', function(
		$rootScope,
		$scope,
		$state,
		$sce,
		MessageService,
		FormUtilService,
		WorkflowService,
		UserQuestionnaireService,
		PatientTherapyService,
		DefaultActivityValueService,
		RESTURL,
		REGEXP,
		$modal,
		$timeout,
		UserService,
		NotificationConfService,
		$window,
		VirtualMeetingService,
		UserActivityService,
		SupplierService) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.entityType = 'TASK';
	
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};
		$scope.initDetails();
	};
	
	$scope.initDetails = function() {
		$scope.warnings = [];
		$scope.detail = {};
		$scope.fieldPermissions = {};

		//alice - 20190913 : qualifications e isInternalStaff copiate nello scope perchè non visibili da dentro la directive anche se in rootScope
		$scope.qualifications = angular.copy($rootScope.qualifications);
		$scope.isInternalStaff = $rootScope.isInternalStaff;
		
		$scope.textBlockMenu = [
            ['bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript'],
            ['font'],
            ['font-size'],
            ['font-color', 'hilite-color'],
            ['remove-format'],
            ['ordered-list', 'unordered-list', 'outdent', 'indent'],
            ['left-justify', 'center-justify', 'right-justify'],
            ['code', 'quote', 'paragragh'],
            ['link']
	    ];
		
		$scope.medicalSupportServiceActive = false;
		SupplierService.isMedicalServiceSupport(function(result){
			$scope.medicalSupportServiceActive = result.data != undefined ? result.data : false;
		});
		
		$scope.activityOwnerTypes = {};
		SupplierService.isMedicalServiceSupport(function(result){
			if(result.data != undefined && result.data) {
				$scope.activityOwnerTypes = $rootScope.constants['ActivityOwnerType'];
			}else {
				for(var i= 0; i < $rootScope.constants['ActivityOwnerType'].length; i++){
					if($rootScope.constants['ActivityOwnerType'][i].id  !== 'SUPPLIER_MEDICAL_SUPPORT' && 
							$rootScope.constants['ActivityOwnerType'][i].active)
					$scope.activityOwnerTypes[i] = $rootScope.constants['ActivityOwnerType'][i];
				}
			}
		});
		
		WorkflowService.getTask({ taskId : $scope.id },
			function(result) {
				$scope.detail = result.data;
				$scope.initPermission(result.permissions);
				$scope.fieldPermissions = result.fieldPermissions;
				
				WorkflowService.getTaskForm({ taskId : $scope.id },
					function(result) {
						$scope.detail.form = result.data;
						FormUtilService.completeForm($scope.detail.form, $scope, true);
						//conta delle altre attività (task) relative allo user del task correntemente selezionato
						if($rootScope.isInternalStaff()) {
							$scope.countOtherUserTasks();
						}
						$scope.changeDate();
						//imposto default dei km se presenti 
						var occurredForm = $scope.detail.form.formProperties[0].value;
						if($scope.detail.form.formProperties[0].type == 'UserActivityOccurred' && occurredForm.type == 'MEETING'
							&& occurredForm.user.type == 'PATIENT' && occurredForm.locationType == 'PATIENT_HOME' && occurredForm.ownerType == 'LOGISTIC'
						 	&& (occurredForm.ownerRoundtripKm == undefined || occurredForm.ownerRoundtripKm == 0)){
							var defaultActivityValueFilters = {
									filters:{
										'USER_ACTIVITY_ID': occurredForm.id
									}
							};
							DefaultActivityValueService.search({}, defaultActivityValueFilters, function(result) {
								if(result.data !== undefined && result.data[0] !== undefined && result.data[0].km !== undefined){
									$scope.detail.form.formProperties[0].value.ownerRoundtripKm = result.data[0].km;
								}else{
									$scope.detail.form.formProperties[0].value.ownerRoundtripKm = 0;
								}
							}, function(error) {
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});
							
						}
						UserService.getUser({userId: $scope.detail.form.formProperties[0].value.user.id}, function(result) {
							$scope.user = result.data; 
						}, function(error) {
							if(error.status == 404) {
								$state.go('access.not-found');
							}
						});
						
						if(($scope.detail.form.formProperties[0].type == 'UserActivityOccurred' || $scope.detail.form.formProperties[0].type == 'UserActivityConfirmed') && occurredForm.type == 'USER_EXIT') {
							if(occurredForm.notifications==undefined) {
								occurredForm.notifications = [];
							}
							
							
							$scope.detail.notificationEnabledTypes = [];
							NotificationConfService.search({},{ filters: {'CODE':'DEACTIVATE', 'RECIPIENT_TYPE': occurredForm.user.type }}, function(result) {
								if(result.data!=undefined) {
									for(var i=0; i<result.data.length; i++) {
										$scope.detail.notificationEnabledTypes.push(result.data[i].type);
									}
								}
								occurredForm.emailNotification = occurredForm.notifications.indexOf('EMAIL')>=0;
								occurredForm.smsNotification = occurredForm.notifications.indexOf('SMS')>=0;;
							}, function(error) {
								MessageService.showError('Errore','Problema nel recupero del tipo di notifiche abilitate');
							});
						}
						
//						if($scope.detail.form !== undefined && $scope.detail.assignee === undefined) {
//							for(var i=0; i<$scope.detail.form.formProperties.length; i++) {
//								$scope.detail.form.formProperties[i].writable = false;
//							}
//						}
						
					},
					function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					}
				);
			},
			function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			}
		);	
	};
	
	$scope.isRecallRequest = function(){
		if($scope.isInternalStaff && $scope.detail.form.formProperties[0].value.user && $scope.detail.form.formProperties[0].value.user.cellPhoneNumber 
				&& $scope.detail.form.formProperties[0].value.type == 'OUTGOING_CALL'){
			if($scope.detail.form.formProperties[0].value.retryHours !== undefined && $scope.detail.form.formProperties[0].value.retryHours > 0){
				if($scope.detail.form.formProperties[0].value.retryIndex >= $scope.detail.form.formProperties[0].value.retryHours.length){
					return true;
				}
			}
		}
		return false;
	};

	$scope.save = function() {	
		
		$scope.errors = FormUtilService.validateForm($scope.detail.form);
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			
			if( $scope.detail.form.formProperties[0].type=="UserActivityConfirmed" || $scope.detail.form.formProperties[0].type=="UserActivityOccurred" ) {
				if($scope.detail.form.formProperties[0].value.type=='USER_EXIT') {
					$scope.detail.form.formProperties[0].value.notifications = [];
					if($scope.detail.form.formProperties[0].value.emailNotification) {
						$scope.detail.form.formProperties[0].value.notifications.push('EMAIL');
					}
					if($scope.detail.form.formProperties[0].value.smsNotification) {
						$scope.detail.form.formProperties[0].value.notifications.push('SMS');
					}
				}
			}
			
			WorkflowService.saveTaskForm({ taskId: $scope.id }, { name: $scope.detail.name, description: $scope.detail.description, properties: FormUtilService.getPropertiesToSubmit($scope.detail.form) },
				function(result) {
				
					$scope.detail.form = result.data;
					$timeout(function() {	
						FormUtilService.completeForm($scope.detail.form, $scope, true);
						$scope.fieldPermissions = result.fieldPermissions;
						$scope.initPermission(result.permissions);
						
						$scope.detail.form.formProperties[0].value.emailNotification = $scope.detail.form.formProperties[0].value.notifications.indexOf('EMAIL')>=0;
						$scope.detail.form.formProperties[0].value.smsNotification = $scope.detail.form.formProperties[0].value.notifications.indexOf('SMS')>=0;;
						
						MessageService.showSuccess('Salvataggio completato con successo');
					}, 0);
				},
				function(error) {
					error.data = error.data || {}
					if (error.status == 404) {
						$state.go('access.not-found');
					} else {
						MessageService.showError('Errore in fase di salvataggio', error.data.message);
					}
				}
			)
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.saveAndContinue = function() {	
		$scope.errors = FormUtilService.validateForm($scope.detail.form);
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			var modalInstance = MessageService.simpleConfirm('Salvataggio e chiusura attivit\u00E0', 'Continuando l\'attivit\u00E0 verr\u00E0 completata e non sar\u00E0 possibile annullare l\'operazione.<br/>Successivamente verr\u00E0 presentata l\'eventuale attivit\u00E0 successiva.<br />Procedere?', 'btn-info','sm');
			modalInstance.result.then(
				function(confirm) {
					if(confirm) {
						
						if( $scope.detail.form.formProperties[0].type=="UserActivityConfirmed" || $scope.detail.form.formProperties[0].type=="UserActivityOccurred" ) {
							if($scope.detail.form.formProperties[0].value.type=='USER_EXIT') {
								$scope.detail.form.formProperties[0].value.notifications = [];
								if($scope.detail.form.formProperties[0].value.emailNotification) {
									$scope.detail.form.formProperties[0].value.notifications.push('EMAIL');
								}
								if($scope.detail.form.formProperties[0].value.smsNotification) {
									$scope.detail.form.formProperties[0].value.notifications.push('SMS');
								}
							}
						}
						
						WorkflowService.saveTaskFormAndContinue({ taskId: $scope.id }, { name: $scope.detail.name, description: $scope.detail.description, properties: FormUtilService.getPropertiesToSubmit($scope.detail.form)},
							function(result) {
								MessageService.showSuccess('Salvataggio completato con successo');
								var businessKey = result.data;
								if(businessKey !== undefined) {
									WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
										function(result) {
											if(result.data !== undefined) {
												if($scope.popup) {
													$scope.id = result.data.id;
													$scope.initDetails();
												} else {
													$state.go('app.task', { 'id': result.data.id });
												}
											}
											else {
												if($scope.popup && $scope.closeModal) {
													$scope.closeModal();
												} else {
													$state.go('app.my-tasks');	
												}
											}
										},
										function(error) {
											if($scope.popup && $scope.closeModal) {
												$scope.closeModal();
											} else {
												$state.go('app.my-tasks');	
											}
										}
									)
								}
								else {
									if($scope.popup && $scope.closeModal) {
										$scope.closeModal();
									}
									else {
										$state.go('app.my-tasks');	
									}
								}
							},
							function(error) {
								error.data = error.data || {}
								if (error.status == 404) {
									$state.go('access.not-found');
								} else {
									MessageService.showError('Errore in fase di salvataggio', error.data.message);
								}
							}
						)
					}
				}
			)
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.change = function() {	
		if($scope.detail.form.changeButton !== undefined) {
			var modalInstance = MessageService.simpleConfirm('Modifica attivit\u00E0', 'L\'attivit\u00E0 verr\u00E0 riportata in stato Pianificata e l\'eventuale ordine di servizio, confermato o no, sar\u00E0 annullato.<br />Si desidera procedere?', 'btn-info');
			modalInstance.result.then(
				function(confirm) {
					if(confirm) {
						var formData = {
								reason: $scope.detail.form.failedReason,
								reasonDetail: $scope.detail.form.failedReasonDetail,
								skipActivity: $scope.detail.form.skipActivity,
								skipOption: $scope.detail.form.skipOption
						};
						WorkflowService.deleteTask({'messageName': 'change', 'taskId': $scope.detail.id },
								formData,
								function(result) {
							MessageService.showSuccess('Salvataggio completato con successo');
							var businessKey = result.data;
							if(businessKey !== undefined) {
								WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
										function(result) {
									if(result.data !== undefined) {
										if($scope.popup) {
											$scope.id = result.data.id;
											$scope.initDetails();
										} else {
											$state.go('app.task', { 'id': result.data.id }, {reload: true});
										}
									}
									else {
										if($scope.popup && $scope.closeModal) {
											$scope.closeModal();
										} else {
											$state.go('app.my-tasks');	
										}
									}
								},
								function(error) {
									if($scope.popup && $scope.closeModal) {
										$scope.closeModal();
									} else {
										$state.go('app.my-tasks');	
									}
								}
								)
							}
							else {
								if($scope.popup && $scope.closeModal) {
									$scope.closeModal();
								}
								else {
									$state.go('app.my-tasks');	
								}
							}
						},
						function(error) {
							MessageService.showError('Errore in fase di modifica attivit\u00E0', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
						}
						);
					}
				}
			)
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.cancelTask = function(canSkipActivity) {
		$scope.cancelTaskForm = {};
		$scope.canSkipActivity = canSkipActivity || true;
		if($scope.detail.form.cancelButton !== undefined) {
			$scope.cancelTaskModalInstance =  $modal.open({
				scope: $scope,
				templateUrl: 'tpl/common/cancel-task-modal.html',
				size: 'lg',
				windowClass: 'largeModal'
			});
		}
	}
	
	$scope.confirmCancelTask = function() {
		var errors = {};
		var cancelTaskForm = {
				formProperties : [
					{ id:'failedReason', value: $scope.cancelTaskForm.failedReason, required:true, type:'string' }
					]
		};
		if($scope.canSkipActivity) {
			cancelTaskForm.formProperties.push({ id:'skipActivity', value: $scope.cancelTaskForm.skipActivity, required:true, type:'boolean' });
			if($scope.cancelTaskForm.skipActivity) {
				cancelTaskForm.formProperties.push({ id:'skipOption', value: $scope.cancelTaskForm.skipOption, required:true, type:'string' });	
			}
		}
		$scope.errors = FormUtilService.validateForm(cancelTaskForm);
		if($scope.errors !== undefined && !angular.equals($scope.errors, {})) {
			console.log($scope.errors);
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
		else {
			var formData = {
				reason: $scope.cancelTaskForm.failedReason,
				reasonDetail: $scope.cancelTaskForm.failedReasonDetail,
				skipActivity: $scope.cancelTaskForm.skipActivity,
				skipOption: $scope.cancelTaskForm.skipOption,
				properties: FormUtilService.getPropertiesToSubmit($scope.detail.form)
			}
			WorkflowService.deleteTask({'messageName': 'cancel', 'taskId': $scope.detail.id },
				formData,
				function(result) {
					MessageService.showSuccess('Cancellazione completata con successo');
					var businessKey = result.data;
					if(businessKey !== undefined) {
						WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
							function(result) {
								if(result.data !== undefined) {
									$scope.cancelTaskModalInstance.close(true);
//									if($scope.popup) {
										$scope.id = result.data.id;
										$scope.initDetails();
//									} else {
//										$state.go('app.task', { 'id': result.data.id });
//									}
								} else {
									$scope.cancelTaskModalInstance.close(true);
									$state.go('app.my-tasks');
								}
							},
							function(error) {
								$scope.cancelTaskModalInstance.close(true);
								$state.go('app.my-tasks');
							}
						)
					} else {
						$scope.cancelTaskModalInstance.close(true);
						$state.go('app.my-tasks');
					}
				},
				function(error) {
					MessageService.showError('Errore in fase di cancellazione attivit\u00E0', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
				}
			);
//			$scope.cancelTaskModalInstance.close(true);
		}
	};

	$scope.undoCancelTask = function () {
		$scope.cancelTaskModalInstance.dismiss('cancel');
	};
	
	$scope.cancelOld = function(canSkipActivity) {
		if($scope.detail.form.cancelButton !== undefined) {
			var modalInstance =  $modal.open({
				scope: $scope,
				templateUrl: 'tpl/common/delete-task-modal.html',
				size: 'lg',
				controller: function ($scope, $modalInstance ,$state, FormUtilService) {
					$scope.form = {};
					$scope.canSkipActivity = canSkipActivity || true;
					
					$scope.confirm = function() {
						var errors = {};
						var form = {
								formProperties : [
									{ id:'failedReason', value: $scope.form.failedReason, required:true, type:'string' }
									]
						};
						if($scope.canSkipActivity) {
							form.formProperties.push({ id:'skipActivity', value: $scope.form.skipActivity, required:true, type:'boolean' });
							if($scope.form.skipActivity) {
								form.formProperties.push({ id:'skipOption', value: $scope.form.skipOption, required:true, type:'string' });	
							}
						}
						$scope.errors = FormUtilService.validateForm(form);
						if($scope.errors !== undefined && !angular.equals($scope.errors, {})) {
							console.log($scope.errors);
							MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
						}
						else {
							var formData = {
								reason: $scope.form.failedReason,
								reasonDetail: $scope.form.failedReasonDetail,
								skipActivity: $scope.form.skipActivity,
								skipOption: $scope.form.skipOption
							}
							WorkflowService.deleteTask({'messageName': 'cancel', 'taskId': $scope.detail.id },
								formData,
								function(result) {
									MessageService.showSuccess('Salvataggio completato con successo');
									var businessKey = result.data;
									if(businessKey !== undefined) {
										WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
											function(result) {
												if(result.data !== undefined) {
													if($scope.popup) {
														$scope.id = result.data.id;
														$scope.initDetails();
													} else {
														$state.go('app.task', { 'id': result.data.id });
													}
												} else {
													if($scope.popup && $scope.closeModal) {
														$scope.closeModal();
													} else {
														$state.go('app.my-tasks');
													}
												}
											},
											function(error) {
												if($scope.popup && $scope.closeModal) {
													$scope.closeModal();
												} else {
													$state.go('app.my-tasks');
												}
											}
										)
									} else {
										if($scope.popup && $scope.closeModal) {
											$scope.closeModal();
										} else {
											$state.go('app.my-tasks');
										}
									}
								},
								function(error) {
									MessageService.showError('Errore in fase di cancellazione attivit\u00E0', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
								}
							);
							$modalInstance.close(true);
						}
					};

					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
					};
				}
			});
		}
	}
	
	$scope.assignTaskToMe = function() {
		WorkflowService.assignTaskToMe({'taskId': $scope.detail.id }, {},
			function(result) {
				MessageService.showSuccess('Salvataggio completato con successo');
				if(result.data !== undefined) {
					if($scope.popup) {
						$scope.id = result.data.id;
						$scope.init();
					} else {
						$state.go('app.task', { 'id': result.data.id }, {reload: true});
					}
				} else {
					if($scope.popup && $scope.closeModal) {
						$scope.closeModal();
					} else {
						$state.go('app.my-tasks');
					}
				}
			},
			function(error) {
				MessageService.showError('Errore in fase di assegnazione dell\'attivit\u00E0', '');
			}
		);
	}

	$scope.takeInCharge = function() {
		WorkflowService.takeInCharge({taskId: $scope.detail.id}, {}, function(result) {
			if(result.data != undefined) {
				MessageService.showSuccess('Salvataggio completato con successo');
				$state.go('app.my-tasks');
			} else {
				$state.go('app.my-tasks');
			}
		},
		function(error) {
			MessageService.showError('Errore in fase di presa in carico dell\'attivit\u00E0', '');
		}
	  );
	}

	$scope.help = function() {
		if($scope.detail.form.helpButton !== undefined) {
			var modalInstance = $modal.open({
				keyboard: false,
				templateUrl: 'tpl/common/help-task-modal.html',
				size: 'lg',
				scope: $scope,
				controller: function ($scope, $modalInstance, $state) {
					$scope.dialogTitle = 'Help - ' + $scope.detail.name;
					$scope.dialogText = $scope.detail.form.helpButton;

					$scope.closeHelp = function() {
						$modalInstance.close(true);
					};
				}
			});
		}
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
			$scope.canTakeInCharge = permissions.TAKE_IN_CHARGE;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canTakeIncharge = false;
		}
	}
	
	$scope.saveSelections = function(){
		if($scope.selected != undefined) {
			for(var i=0; i<$scope.selected.length; i++) {
				//				$scope.selectedIds[i] = $scope.selected[i].id;
			}
		}
	}

	$scope.countOtherUserTasks = function(){
		$scope.otherUserTasks = 0;
		$scope.searchFilters = $scope.searchFilters || {};
		WorkflowService.countSearchOtherUserTasks( { userActivityId : $scope.detail.form.formProperties[0].value.id }, $scope.searchFilters, function(result){
			$scope.otherUserTasks = result.data;
		}, function(error) {
			$scope.otherUserTasks = 0;
		});
	};
	
	$scope.controllSuggestedDate = function(index){
		$scope.warnings = [];
		if($scope.detail.form.formProperties[0].value.dependingActivities[index]){
			$scope.da = $scope.detail.form.formProperties[0].value.dependingActivities[index];
			if($scope.da.suggestedMinDate != undefined && $scope.da.suggestedMaxDate != undefined && ($scope.da.suggestedDate < $scope.da.suggestedMinDate || $scope.da.suggestedDate > $scope.da.suggestedMaxDate)){
				$scope.warnings['suggestedDate_'+index] = 'Data suggerita fuori range!'; 
			}		
		}
	}
	
	$scope.occurredChange = function() {
		var form = $scope.detail.form.formProperties[0].value;
		if(form.recallRequest == undefined){
			form.recallRequest = false;
		}
		if(angular.isDefined(form)) {
			switch($scope.detail.form.formProperties[0].type) {
			case "UserActivityConfirmed":
				switch(form.type) {
				case 'USER_EXIT':
					if(form.occurred==false) {
						form.emailNotification = false;
						form.smsNotification = false;
					}
					
					break;	
				default:
					if(form.complementaryActivities !== undefined && form.complementaryActivities.length > 0) {
						for(var i = 0; i <= form.complementaryActivities.length - 1; i++) {
							var complementaryActivity = form.complementaryActivities[i];
							switch(complementaryActivity.type) {
							case 'DRUG_ADMINISTRATION':
								if(angular.isDefined(form.occurred) && !form.occurred) {
									complementaryActivity.status = 'NOT_COMPLETED';
								} else {
									delete complementaryActivity.status;
									delete complementaryActivity.drugAdministrationResult;
								}
								break;	
							default:
								break;
							}
						}
					}
					break;
				}
				break;	
			default:
				break;
			}
		}
	}
	
	$scope.changeDate = function() {
		var form = $scope.detail.form.formProperties[0].value;
		if(form.onDemandSomministration) {
			if(form.complementaryActivities) {
				for(var i = 0; i <= form.complementaryActivities.length - 1; i++) {
					if(form.complementaryActivities[i].type == 'DRUG_ADMINISTRATION') {
						var complementaryActivity = form.complementaryActivities[i];
						if(angular.isUndefined(complementaryActivity.entityId) && form.date) {
							var patientTherapyFilters = {
									filters:{
										'PATIENT_ID': form.user.id,
										'HAS_STATUS_ON_PERIOD': {
											status: 'ONGOING',
											fromDate: form.date,
											toDate: form.date
											}
									}
							};
							PatientTherapyService.search({}, patientTherapyFilters, function(result) {
								$scope.patientTherapyDrugs = [];
								switch(result.data.length) {
								case 0:
									complementaryActivity.alternativeEntityId = undefined;
									break;
								case 1:
									var patientTherapy = result.data[0];
									delete complementaryActivity.alternativeEntityId;
									$scope.patientTherapyDrugs = patientTherapy.drugs;
									if(patientTherapy.drugs && patientTherapy.drugs.length == 1) {
										complementaryActivity.alternativeEntityId = patientTherapy.drugs[0].id;
									}
									break;
								default:
									delete complementaryActivity.alternativeEntityId;
									for(var x = 0; x <= result.data.length - 1; x++) {
										$scope.patientTherapyDrugs.push(...result.data[x].drugs);
									}
									break;
								}
							}, function(error) {
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							});
						}
					}
				}
			}
		}
	}
	
	$scope.failedReasonChange = function() {
		var form = $scope.detail.form.formProperties[0].value;
		if(form) {
			if(form.complementaryActivities !== undefined && form.complementaryActivities.length > 0) {
				for(var i = 0; i <= form.complementaryActivities.length - 1; i++) {
					var complementaryActivity = form.complementaryActivities[i];
					switch(complementaryActivity.type) {
					case 'DRUG_ADMINISTRATION':
						if(form.failedReason && $scope.constants['ComplementaryActivityType'][17].properties.failedReasons.indexOf(form.failedReason) >= 0) {
							complementaryActivity.failedReason = form.failedReason;
						} else {
							delete complementaryActivity.failedReason;
						}
						break;
					default:
						break;
					}
				}
			}  
		}
	}
	
	$scope.$watch('detail.form.formProperties[0].value.type', function(v) {
		if(v) {
			$scope.buildVirtualMeeting();	
		}
	}, true);
	
	$scope.$watch('detail.form.formProperties[0].value.ownerType', function(v, old) {
		if(angular.isDefined(old) && $scope.detail && $scope.detail.form && $scope.detail.form.formProperties && $scope.detail.form.formProperties[0] && $scope.detail.form.formProperties[0].value && angular.isString($scope.detail.form.formProperties[0].value) == false) {
			$scope.detail.form.formProperties[0].value.owner = undefined;
		}
	}, true);
	
	$scope.$watch('detail.form.formProperties[0].value.owner', function(v) {
		$scope.buildVirtualMeeting();
	}, true);
	
	$scope.$watch('detail.form.formProperties[0].value.ownerQualification', function(v) {
		$scope.buildVirtualMeeting();
	}, true);
	
	$scope.buildVirtualMeeting = function() {
		$scope.buildInProgress = $scope.buildInProgress || 0;
		var form = $scope.detail.form && $scope.detail.form.formProperties[0] ? $scope.detail.form.formProperties[0].value : undefined;
		if(form && angular.isString(form) == false && $scope.buildInProgress === 0) {
			$scope.buildInProgress = 1;
			$scope.virtualMeetingForm = $scope.virtualMeetingForm || angular.copy(form.virtualMeeting);
			if(form.virtualMeeting === undefined) {
				form.virtualMeeting = {
					"title": form.name,
					"participants": []
				};
			}
			form.virtualMeeting.participants = [];
			var vmActivities = [];
			if(form.virtualMeeting.id) {
				var uaFilters = {
					"filters": {
						"VIRTUAL_MEETING_ID": form.virtualMeeting.id,
						"-STATUS": [ "PLANNING", "PLANNED" ],
						"-ID": form.id
					}
				}
				
				UserActivityService.search({}, uaFilters, function(result) {
					vmActivities = result.data;
					vmActivities.push($scope.detail.form.formProperties[0].value);
					for(var i=0; i<vmActivities.length; i++) {
						var vmAct = vmActivities[i];
						if(vmAct.user && vmAct.user.id) {
							var vmPart = $scope.findParticipant(form.virtualMeeting.participants, vmAct.user.id);
							if(vmPart === undefined) {
								// Ne creo uno e lo aggiungo
								vmPart = {
									"user": vmAct.user,
									"email": vmAct.user.email,
									"qualification": undefined
								}
								form.virtualMeeting.participants.push(vmPart);
							}
							// Aggiorno l'eventuale email 
							if($scope.virtualMeetingForm !== undefined && $scope.virtualMeetingForm.participants !== undefined) {
								var vmPartForm = $scope.findParticipant($scope.virtualMeetingForm.participants, vmAct.user.id);
								if(vmPartForm && vmPartForm.email) {
									vmPart.email = vmPartForm.email;
								}
							}
						}
						if(vmAct.owner && vmAct.owner.id) {
							var vmPart = $scope.findParticipant(form.virtualMeeting.participants, vmAct.owner.id);
							if(vmPart === undefined) {
								// Ne creo uno e lo aggiungo
								vmPart = {
									"user": vmAct.owner,
									"email": vmAct.owner.email,
									"qualification": vmAct.ownerQualification
								}
								form.virtualMeeting.participants.push(vmPart);
							}
							// Aggiorno l'eventuale email
							if($scope.virtualMeetingForm !== undefined && $scope.virtualMeetingForm.participants !== undefined) {
								var vmPartForm = $scope.findParticipant($scope.virtualMeetingForm.participants, vmAct.owner.id);
								if(vmPartForm && vmPartForm.email) {
									vmPart.email = vmPartForm.email;
								}
							}
						}
						if(vmAct.otherParticipants) {
							for(var j=0; j<vmAct.otherParticipants.length; j++) {
								var op = vmAct.otherParticipants[j];
								if(op && op.id) {
									var vmPart = $scope.findParticipant(form.virtualMeeting.participants, op.id);
									if(vmPart === undefined) {
										// Ne creo uno e lo aggiungo
										vmPart = {
											"user": op,
											"email": op.email,
											"qualification": undefined
										}
										form.virtualMeeting.participants.push(vmPart);
									}
									// Aggiorno l'eventuale email
									if($scope.virtualMeetingForm !== undefined && $scope.virtualMeetingForm.participants !== undefined) {
										var vmPartForm = $scope.findParticipant($scope.virtualMeetingForm.participants, op.id);
										if(vmPartForm && vmPartForm.email) {
											vmPart.email = vmPartForm.email;
										}
									}
								}
							}
						}
					}
					$scope.buildInProgress = 0;
				}, function(error) {
					vmActivities = [];
					$scope.buildInProgress = 0;
				});
			}
			else {
				vmActivities.push($scope.detail.form.formProperties[0].value);
				for(var i=0; i<vmActivities.length; i++) {
					var vmAct = vmActivities[i];
					if(vmAct.user && vmAct.user.id) {
						var vmPart = $scope.findParticipant(form.virtualMeeting.participants, vmAct.user.id);
						if(vmPart === undefined) {
							// Ne creo uno e lo aggiungo
							vmPart = {
								"user": vmAct.user,
								"email": vmAct.user.email,
								"qualification": undefined
							}
							form.virtualMeeting.participants.push(vmPart);
						}
						// Aggiorno l'eventuale email
						if($scope.virtualMeetingForm !== undefined && $scope.virtualMeetingForm.participants !== undefined) {
							var vmPartForm = $scope.findParticipant($scope.virtualMeetingForm.participants, vmAct.user.id);
							if(vmPartForm && vmPartForm.email) {
								vmPart.email = vmPartForm.email;
							}
						}
					}
					if(vmAct.owner && vmAct.owner.id) {
						var vmPart = $scope.findParticipant(form.virtualMeeting.participants, vmAct.owner.id);
						if(vmPart === undefined) {
							// Ne creo uno e lo aggiungo
							vmPart = {
								"user": vmAct.owner,
								"email": vmAct.owner.email,
								"qualification": vmAct.ownerQualification
							}
							form.virtualMeeting.participants.push(vmPart);
						}
						// Aggiorno l'eventuale email
						if($scope.virtualMeetingForm !== undefined && $scope.virtualMeetingForm.participants !== undefined) {
							var vmPartForm = $scope.findParticipant($scope.virtualMeetingForm.participants, vmAct.owner.id);
							if(vmPartForm && vmPartForm.email) {
								vmPart.email = vmPartForm.email;
							}
						}
					}
					if(vmAct.otherParticipants) {
						for(var j=0; j<vmAct.otherParticipants.length; j++) {
							var op = vmAct.otherParticipants[j];
							if(op && op.id) {
								var vmPart = $scope.findParticipant(form.virtualMeeting.participants, op.id);
								if(vmPart === undefined) {
									// Ne creo uno e lo aggiungo
									vmPart = {
										"user": op,
										"email": op.email,
										"qualification": undefined
									}
									form.virtualMeeting.participants.push(vmPart);
								}
								// Aggiorno l'eventuale email
								if($scope.virtualMeetingForm !== undefined && $scope.virtualMeetingForm.participants !== undefined) {
									var vmPartForm = $scope.findParticipant($scope.virtualMeetingForm.participants, op.id);
									if(vmPartForm && vmPartForm.email) {
										vmPart.email = vmPartForm.email;
									}
								}
							}
						}
					}
				}
			}
			$scope.buildInProgress = 0;
		}
	}
	
	$scope.findParticipant = function(participants, userId) {
		if(participants && participants.length > 0 && userId) {
			for(var i=0; i<participants.length; i++) {
				if(participants[i].user && participants[i].user.id && participants[i].user.id === userId) {
					return participants[i];
				}
			}
		}
		return undefined;
	}
	
	$scope.confirmLinkCopy = function() {
		MessageService.showSuccess('Il link copiato negli appunti');
	}
	
	$scope.openLink = function(link, newTab) {
		if(newTab) {
			$window.open(link, "_blank");
		}
		else {
			$window.open(link);
		}
	} 
});
