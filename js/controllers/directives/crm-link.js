'use strict'

app.controller('LinkListController', function(
		$scope,
		$controller,
		LinkService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('LINK', LinkService);
});