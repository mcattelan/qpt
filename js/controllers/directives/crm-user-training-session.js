'use strict'

app.controller('TrainingSessionController', function(
		$scope,
		$rootScope,
		$controller,
		$stateParams,
		MessageService,
		PermissionService,
		TrainingSessionService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.searchFilters = $scope.searchFilters || {};
	$scope.searchFilters['filters'] = {
			'PARTICIPANT_ID': $scope.participantId
	};
	
	$scope.init('TRAINING_SESSION', TrainingSessionService);
});

app.controller('TrainingSessionDetailController', function(
		$rootScope,
		$scope,
		$stateParams,
		$state,
		TrainingSessionService,
		TrainingSubjectService,
		FormUtilService,
		PermissionService,
		MessageService,
		$filter) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.programProperties = $rootScope.programProperties;
		$scope.errors = {};
		if (($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if (angular.isDefined($scope.id)) {
			$scope.initDetail();
		}
	};
	
	$scope.initDetail = function(){
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.searchFilters = $scope.searchFilters || {};
		$scope.selects = {
				'TrainingSubjects' : []
		};
				
		if(!$scope.isNew()){
			TrainingSessionService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			PermissionService.getAll({ entityType : 'TRAINING_SESSION' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'TRAINING_SESSION' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		TrainingSessionService.insert({}, request, function(result) {
			$scope.detail = result.data;		
//			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			if ($scope.close) {
				$scope.close();
			}
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		TrainingSessionService.update({ id : $scope.id },request,function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
			$scope.close();
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					TrainingSessionService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.close();
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.trainingDate', value: $scope.detail.trainingDate, required:true, type:'string' },
					{ id:'detail.durationHours', value: $scope.detail.durationHours, required:true, type:'string' },
					{ id:'detail.type', value: $scope.detail.type, required:true, type:'string' },
					{ id:'detail.trainerType', value: $scope.detail.trainerType, required:true, type:'string' },
					{ id:'detail.trainingSubject', value: $scope.detail.trainingSubject, required:true, type:'string' }
				]					
		};
		
		errors = FormUtilService.validateForm(form);
		 
		 if(angular.isDefined($scope.detail.type) && $scope.detail.type == 'CLASSROOM' && !angular.isDefined($scope.detail.location)) {
			 errors['detail.location'] = "il luogo \u00e8 obbligatorio nel caso di formazione in aula";
	     }
		 
		 if(angular.isDefined($scope.detail.trainerType) && $scope.detail.trainerType == 'OTHER' && !angular.isDefined($scope.detail.trainerTypeOther)) {
			 errors['detail.trainerTypeOther'] = "\u00e8 obbligatorio specificare la tipologia di trainer";
	     }
		 
//		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
		$scope.permissions = permissions;
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$scope.detail = {}; 	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.loadRequest = function() {
		return {
			trainingDate:		$scope.detail.trainingDate,
			trainingSubjectId: 	$scope.detail.trainingSubject.id,
			durationHours:		$scope.detail.durationHours,
			type:				$scope.detail.type,
			location:			$scope.detail.location,
			trainerType:		$scope.detail.trainerType,
			trainerTypeOther:	$scope.detail.trainerTypeOther,
			trainerNotes:		$scope.detail.trainerNotes,
			internalNotes:		$scope.detail.internalNotes,
			participantId:		$scope.participantId
		}
	}
	
	$scope.init();
	
});

