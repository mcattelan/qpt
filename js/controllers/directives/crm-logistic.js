'use strict'

app.controller('LogisticListCtrl', function(
		$scope,
		$controller,
		LogisticService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('LOGISTIC', LogisticService);
});