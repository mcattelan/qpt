'use strict'

app.controller('VirtualMeetingListCtrl', function(
		$scope,
		$controller,
		$rootScope,
		VirtualMeetingService) {
	
	$scope.isHcp = $rootScope.isHcp();
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('VIRTUAL_MEETING', VirtualMeetingService);
	
	$scope.preSearch = function(){
		if($scope.searchFilters && $scope.searchFilters.filters && $scope.searchFilters.filters.PARTICIPANT_ID) {
			if(angular.isString($scope.searchFilters.filters.PARTICIPANT_ID)) {
				var participantId = $scope.searchFilters.filters.PARTICIPANT_ID;
				$scope.searchFilters.filters.PARTICIPANT_ID = [ participantId ];
			}
		}
		else {
			$scope.searchFilters.filters.PARTICIPANT_ID = [];
		}
		
		if($scope.objectFilters.hcpId){
			$scope.searchFilters.filters.PARTICIPANT_ID.push($scope.objectFilters.hcpId);
		}
		if($scope.objectFilters.patientId){
			$scope.searchFilters.filters.PARTICIPANT_ID.push($scope.objectFilters.patientId);
		}
		if($scope.objectFilters.supplierId){
			$scope.searchFilters.filters.PARTICIPANT_ID.push($scope.objectFilters.supplierId);
		}
		 
	}
});

app.controller('VirtualMeetingDetailCtrl', function(
		$scope,
		$controller,
		MessageService,
		$window,
		VirtualMeetingService) {
	
	
	$scope.postInit = function(){
		
		if($scope.fieldPermissions['qualificationsRequired'].VIEW){
			if(!$scope.isNew()){
				VirtualMeetingService.getQualifications({ id: $scope.id},{}, function(result) {
					$scope.selects['Qualifications'] = result.data;
				}, function(error) {
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}else{
				VirtualMeetingService.getAllQualifications({},{}, function(result) {
					$scope.selects['Qualifications'] = result.data;
				}, function(error) {
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
		}
	}
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.confirmLinkCopy = function() {
		MessageService.showSuccess('Link copiato negli appunti');
	}
	
	$scope.openLink = function(link, newTab) {
		if(newTab) {
			$window.open(link, "_blank");
		}
		else {
			$window.open(link);
		}
	}
	
	$scope.prepareUpdateRequest = function(request) {
		var request = angular.copy(request !== undefined ? request : $scope.detail);
		for(var key in request) {
			if(request[key]) {
				if(angular.isArray(request[key])) {
					for(var i=0; i<request[key].length; i++) {
						if(angular.isObject(request[key][i])) {
							if(request[key][i].id !== undefined && request[key][i].id != null) {
								if(request[key + 'Ids'] === undefined) {
									request[key + 'Ids'] = [];
								}
								request[key + 'Ids'].push(request[key][i].id);
							}
							else {
								request[key][i] = $scope.prepareStandardRequest(request[key][i]);
							}
						}
					}
				}
				else if(angular.isObject(request[key])) {
					if(request[key].id !== undefined && request[key].id != null) {
						request[key + 'Id'] = request[key].id;	
					}
					else {
						request[key] = $scope.prepareStandardRequest(request[key]);
					}
				}
			}
		}
		return request;
	}
	
	$scope.init('VIRTUAL_MEETING', VirtualMeetingService);
});

app.controller('VirtualMeetingOrganizerCtrl', function(
		$scope,
		$controller,
		$state,
		$rootScope,
		PermissionService,
		MessageService,
		VirtualMeetingService) {
	
	$scope.init = function(){
		$scope.detail = {};
		$scope.entityType = 'VIRTUAL_MEETING';
	
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.isHcp = $rootScope.isHcp();
		
		if($scope.isHcp){
			$scope.detail.hcp = $scope.loggedUser;
			$scope.detail.hcpEmail = $scope.loggedUser.email;
		}
		
		$scope.errors = {};
		$scope.warnings = {};
		$scope.selects = {};
		
		PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
			$scope.permissions = result.data;
			if(!$scope.permissions['INSERT']) {
				$scope.detail = undefined;
				MessageService.showError('Utente non abilitato', '');
			}
		}, function(error){
			$scope.detail = undefined;
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function(){
		var request = $scope.prepareOrganizeRequest();
		VirtualMeetingService.organize({}, request, function(result) {
			$scope.warnings = result.responseWarnings;
			if($scope.warnings!==undefined){
				MessageService.showWarning('Aggiornamento completato con presenza di warning.');
			}else{
				MessageService.showSuccess('Aggiornamento completato con successo');
			}
			$scope.modal.close();
			$state.go('app.virtual-meeting-detail', { id: result.data.id }, true);
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di inserimento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.undo = function(){
		$scope.init();
	}
	
	$scope.prepareOrganizeRequest = function(){
		var req = {};
		req.title = $scope.detail.title;
		req.date = $scope.detail.date;
		req.startTime = $scope.detail.startTime;
		req.endTime = $scope.detail.endTime;
		req.participants = [];
		if($scope.detail.hcp != undefined){
			var hcpParticipant = {userId : $scope.detail.hcp.id, email : $scope.detail.hcpEmail};
			req.participants.push(hcpParticipant);
		}
		if($scope.detail.patient != undefined){
			var patientParticipant = {userId : $scope.detail.patient.id, email : $scope.detail.patientEmail};
			req.participants.push(patientParticipant);
		}
		return req;
	}
	
	$scope.$watch('detail.hcp', function(newValue, oldValue) {
		if(newValue!=undefined){
			$scope.detail.hcpEmail = newValue.email;
		}else{
			$scope.detail.hcpEmail = undefined;
		}
		
	}, true);
	
	$scope.$watch('detail.patient', function(newValue, oldValue) {
		if(newValue!=undefined){
			$scope.detail.patientEmail = newValue.email;	
		}else{
			$scope.detail.patientEmail = undefined;
		}
	}, true);
	
	$scope.init();
});

//accessibile solo da HCP
app.controller('VirtualMeetingRequesterCtrl', function(
		$scope,
		$controller,
		$state,
		$rootScope,
		$timeout,
		PermissionService,
		MessageService,
		VirtualMeetingService) {
	
	$scope.init = function(){
		$scope.detail = {};
		$scope.entityType = 'VIRTUAL_MEETING';
	
		$scope.loggedUser = $rootScope.loggedUser;
			
		$scope.detail.hcp = $scope.loggedUser;
		$scope.detail.hcpEmail = $scope.loggedUser.email;
		
		$scope.errors = {};
		$scope.warnings = {};
		$scope.selects = {};
		
		VirtualMeetingService.getAllQualifications({},{},function(result) {
			$scope.detail.qualificationsRequired = result.data;
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di inserimento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
		
		PermissionService.getAll({ entityType : $scope.entityType }, function(result) {
			$scope.permissions = result.data;
			if($scope.permissions['INSERT']) {
				PermissionService.getAllFields({ entityType : $scope.entityType }, function(result) {
					$scope.fieldPermissions = result.data;
					$timeout(function() {
						$scope.changesInProgressListener = true;
					}, 0, false);	
				}, function(error) {
					$scope.detail = undefined;
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});	
			}
			else {
//				$scope.detail = undefined;
//				MessageService.showError('Utente non abilitato', '');
			}
		}, function(error){
			$scope.detail = undefined;
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function(){
		var request = $scope.prepareRequestRequest();
		VirtualMeetingService.insert({}, request, function(result) {
			$scope.warnings = result.responseWarnings;
			if($scope.warnings!==undefined){
				MessageService.showWarning('Incontro richiesto con warinigs');
			}else{
				MessageService.showSuccess('Incontro richiesto correttamente');
			}
			$scope.modal.close();
			$state.go('app.virtual-meeting-detail', { id: result.data.id }, true);
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di inserimento';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}
	
	$scope.undo = function(){
		$scope.init();
	}
	
	$scope.prepareRequestRequest = function(){
		var req = {};
		req.title = $scope.detail.title;
		req.requestNotes = $scope.detail.requestNotes;
		req.participants = [];
		if($scope.detail.hcp != undefined){
			var hcpParticipant = {userId : $scope.detail.hcp.id, email : $scope.detail.hcpEmail};
			req.participants.push(hcpParticipant);
		}
		if($scope.detail.patient != undefined){
			var patientParticipant = {userId : $scope.detail.patient.id, email : $scope.detail.patientEmail};
			req.participants.push(patientParticipant);
		}
		req.medicalSupportServiceRequired = $scope.detail.medicalSupportServiceRequired;
		req.qualificationsRequiredIds = [];
		$scope.detail.qualificationsRequired.forEach(function(item) {
			if(item.selected){
				req.qualificationsRequiredIds.push(item.id);
			}
		});
		return req;
	}
	
	$scope.$watch('detail.patient', function(newValue, oldValue) {
		if(newValue!=undefined){
			$scope.detail.patientEmail = newValue.email;	
		}else{
			$scope.detail.patientEmail = undefined;
		}
	}, true);
	
	$scope.init();
});