'use strict'

app.controller('PersonalEventListCtrl', function(
		$scope,
		$controller,
		PersonalEventService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PERSONAL_EVENT', PersonalEventService);

});

app.controller('PersonalEventDetailCtrl', function(
		$scope,
		$rootScope,
		$controller,
		PersonalEventService,
		PersonalEventTypeService,
		PatientService) {

	$scope.postInit = function() {
		$scope.detail.isOwner = $scope.isOwner();
		//sanofi tuning fase 2 il medico non può notificare il paziente
		$scope.hidePatientNotification = $scope.loggedUser.type == 'HCP';
		$scope.userNotificationConf = {};
		//N.B. MARZO 2020: attualmente si da la possibilità di inserire un unica tipologia di remind per evento. Quindi solo Sms o email o push. 
		if(angular.isDefined($scope.detail.otherUsers) && angular.isDefined($scope.detail.otherUsers[0])) {
			var otherUser = $scope.detail.otherUsers[0];
			if(angular.isDefined(otherUser.user)) {
				if(angular.isDefined(otherUser.notificationConfs) && angular.isDefined(otherUser.notificationConfs[0])) {
					var userNotificationConf = otherUser.notificationConfs[0];
					$scope.userNotificationConf = {
							time: userNotificationConf.time,
							timeUnit: userNotificationConf.timeUnit,
							type: userNotificationConf.type,
							code: userNotificationConf.code,
							recipient: userNotificationConf.recipient
					}
				}
			}
		}
		$scope.searchFilters = $scope.searchFilters || {};
		$scope.selects = {
				'PersonalEventConfTypes' : [] 
		};
		PersonalEventTypeService.search($scope.searchFilters, function(result) {
			$scope.types = [];
			$scope.types = result.data;
			//carico in lista solo i tipi che l'utente può effettivamente manipolare in inserimento
			for(var i=0; i<$scope.types.length; i++) {
				if($scope.types[i].status == 'DELETED') continue;
				
				if($rootScope.isPatient() && $scope.types[i].userType == 'PATIENT') {
					$scope.selects['PersonalEventConfTypes'].push($scope.types[i]);
				} else if($rootScope.isHcp() && $scope.types[i].userType == 'HCP') {
					$scope.selects['PersonalEventConfTypes'].push($scope.types[i]);
				} 
			}
			if(!$scope.isNew()) {
				//se sto guardando un evento già inserito devo caricare in lista anche i tipi che non sono utilizzabili in inserimento/aggiornamento dall'utente loggato ma che sono comunque per lui visibili
				var found = false;
				for(var i=0; i<$scope.selects['PersonalEventConfTypes'].length; i++) {
					if($scope.selects['PersonalEventConfTypes'][i].id.indexOf($scope.detail.type.id) >= 0) {
						found = true;
					}
				}
				if(!found) {
					$scope.selects['PersonalEventConfTypes'].push($scope.detail.type);
				}
			}else{
				if(angular.isDefined($scope.relatedUserId)) {
					$scope.detail.otherUsers = [];
					PatientService.get({ id : $scope.relatedUserId }, function(result) {
						$scope.patient = result.data;
						$scope.detail.otherUsers[0] = {
								user : {}, 
								visible : false
						}; 
						$scope.detail.otherUsers[0].user = $scope.patient;
						
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
			}
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		//se sto caricando un nuovo evento personale dal calendario imposto la data che mi viene passata dal chiamante
		if(angular.isDefined($scope.defaultStartDate)) {
			$scope.detail.date = $scope.defaultStartDate;
		}
	}
	
	$scope.$watch('detail.otherUsers[0].user', function(newValue, oldValue) {
		if(newValue!=undefined && newValue.length > 0){
			if($scope.detail.otherUsers[0].visible == undefined){
				$scope.detail.otherUsers[0].visible = true;
			} 	
		}
	}, true);
	
	$scope.prepareInsertRequest = function() {
		return $scope.prepareRequest();
	}
	
	$scope.prepareUpdateRequest = function() {
		return $scope.prepareRequest();
	}
	
	$scope.prepareRequest = function() {
		var request = $scope.prepareStandardRequest();
		request.ownerId = $scope.loggedUser.id;
		if(!angular.isDefined(request.recursive)) {
			request.recursive = false;
		} 
		//workaround per problema di serializzazione proprietà notificationConfs: anche se si tratta di una lista viene serializzato come oggetto (ovvero nel json viene rappresentato tra parentesi graffe(oggetto) e non tra parentesi quadre (array)) 
		if(angular.isDefined(request.notificationConfs) && angular.isDefined(request.notificationConfs[0])) {
			var notificationConf = request.notificationConfs[0];
			if(angular.isDefined(notificationConf.time) || angular.isDefined(notificationConf.timeUnit)) {
				notificationConf.code = 'REMIND';
				notificationConf.recipient = 'OWNER';
				request.notificationConfs = [];
				request.notificationConfs.push(notificationConf);
			} else {
				delete request.notificationConfs;
			}
		}
		if(angular.isDefined(request.otherUsers) && angular.isDefined(request.otherUsers[0])) {
			var otherUser = request.otherUsers[0];
			if(angular.isDefined(otherUser.user)) {
				otherUser.userId = otherUser.user.id;
				if($scope.userNotificationConf) {
					if(angular.isDefined($scope.userNotificationConf.time) || angular.isDefined($scope.userNotificationConf.timeUnit)) {
						$scope.userNotificationConf.code = 'REMIND';
						$scope.userNotificationConf.recipient = 'OTHER_USER';
						otherUser.notificationConfs = [];
						otherUser.notificationConfs.push($scope.userNotificationConf);
					} else {
						delete otherUser.notificationConfs;
					}
				} else {
					delete otherUser.notificationConfs;
				}
				//12.03.2020: per ora lasciamo inserire solamente 1 otherUser ma la proprietà lato backend è una lista.
				//l'elemento otherUser viene serializzato come object perciò ricreo l'array per gli otherUsers e facci opush dell'oggetto
				request.otherUsers = [];
				request.otherUsers.push(otherUser);
			} else {
				request.otherUsers = undefined;
			} 
		}
		return request;
	}
	
	$scope.isOwner = function(){
		return $scope.isNew() || ($scope.detail.owner != undefined && $scope.detail.owner.id == $scope.loggedUser.id);
	}
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('PERSONAL_EVENT', PersonalEventService);
	
});