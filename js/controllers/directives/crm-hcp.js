'use strict'

app.controller('HcpListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		HcpService,
		$modal,
		MessageService,
		UserService,
		$state,
		GatewayHcpService,
		PortalService) {
	
	$scope.inviteAll = function() {
		$scope.countHcpToInvite();
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/user/invite-all-users.html',
			size: 'md',
		});
	};
	
	$scope.confirmInvite = function(){
		HcpService.inviteActivationAll({}, $scope.searchFilters, function(result) {
			MessageService.showSuccess('Invito di registrazione medico creato con successo');
		}, function (error){
			MessageService.showError('Invito di registrazione medico non creato');
		});
		$scope.$modalInstance.close();
	};
	
	$scope.undoInvite = function(){
		$scope.$modalInstance.dismiss('cancel');
	}
	
	$scope.countHcpToInvite = function() {
		$scope.usersToInvite = 0;
		HcpService.countSearchInvite({}, $scope.searchFilters, function(result) {
			$scope.usersToInvite = result.data;
		});
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					UserService.impersonate({ 'nickName' : nickName }, function(response) {
						$state.go('access.login');
					}, function(error) {
						MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
					});
				}
		);
	}
	
	
	
	$scope.invite = function(id) {
		var modalInstance = MessageService.activationConfirm();
		modalInstance.result.then(
				function(confirm) {				
					UserService.activation({ id : id }, {}, function(result) {
						MessageService.showSuccess('Operazione avvenuta con successo', '');
					}, function(error) {
						if(error.status == 404) {
							MessageService.showError('Impossibile procedere con l\'operazione', (error.data !== undefined && error.data != null && error.data.message !== undefined && error.data.message != null) ? error.data.message : '');
						}
					});
				}
		);
	}
	
	$controller('BaseListCtrl', { $scope: $scope });

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canUpdate = permissions.UPDATE;
			$scope.canExport = permissions.EXPORT;
			$scope.canInvite = permissions.INVITE;
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
			$scope.canInvite = false;
		}
	}

	$scope.init('HCP', HcpService);
	
});