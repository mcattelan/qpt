'use strict'

app.controller('UserEventListCtrl', function(
		$rootScope,
		$scope,
		$controller,
		MessageService,
		UserEventService,
		PersonalEventService,
		UserServiceConfService,
		PatientService) {
	
	$scope.preInit = function() {
		$scope.searchFixedFilters = {
				//'DATE_FROM': moment(new Date()).subtract(1, 'months').valueOf(),
				//'DATE_TO': moment(new Date()).add(1, 'months').valueOf(),
				'RELATED_USER_ID': $scope.relatedUserId
		}
		
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.isHcp = $rootScope.isHcp;
		$scope.manualActivitiesDetail = [];
		$scope.testDBSActive = false;
		
		if($scope.isHcp) {
			PatientService.get({ id : $scope.relatedUserId }, function(result) {
				var patient = result.data;
				if(patient && patient.testDBSService) {
					$scope.testDBSActive = patient.testDBSService; 
				}
			}, function(error) {
			});
		
			UserServiceConfService.getUserManualMasterActivities({ userId : $scope.relatedUserId }, undefined, function(result) {
				var list = result.data;
				if(list && list.length > 0) {
					for(var i = 0; i < list.length; i++) {
						if($scope.loggedUser.type == list[i].starterUserType){
							$scope.manualActivitiesDetail.push(list[i]);
						}
					}
				}
			}, function(error) {
				$scope.manualActivitiesDetail = [];
			});
		}
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	//TODO sistemare gestione permessi per USER_EVENT. temporaneament ecarico quelli del personal evento conf. SBAGLIATO!!! chiedere a Michele (11/03/2020) 
	$scope.init('PERSONAL_EVENT', UserEventService);

	//12.03.2020: la delete è possibile solo per i personal event
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PersonalEventService.delete({ id : id }, {}, function(result) {
						$scope.search();
						MessageService.showSuccess('Cancellazione completata con successo');
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.startMasterActivityDetail = function(masterActivity) {
		
		var dialogTitle='Richiedi Test DBS';
		var dialogText='Confermi la richiesta di un Test DBS per il paziente?';
		
		var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info', 'sm');
		modalInstance.result.then(
			function(confirm) {
				var notify = false;
				
				UserServiceConfService.startUserMasterActivityManually({ userId : $scope.relatedUserId, masterActivityId : masterActivity.id},
					function(result) {
						MessageService.showSuccess('Avvio attivit\u00E0 completato con successo');
						
					}, function(error) {
						if(error && error.data && error.data.message) {
							MessageService.showError('Avvio attivit\u00E0 non riuscito', error.data.message);
						}
						else {
							MessageService.showError('Avvio attivit\u00E0 non riuscito');
						}
						if (error.status == 404) {
							$state.go('access.not-found');
						}
					}
				);
		});
	}

});