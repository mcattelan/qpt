'use strict'

app.controller('PatientWeightListCtrl', function(
		$scope,
		$controller,
		PatientWeightService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PATIENT_WEIGHT', PatientWeightService);
});

app.controller('PatientWeightDetailCtrl', function(
		$scope,
		$controller,
		PatientWeightService,
		$q) {
	
	$scope.postInit = $scope.postInit || function() {
		var d = $q.defer();
		if($scope.isNew() && $scope.detail || ($scope.complementaryId !== undefined)) {
			var userComplementaryId = $scope.complementaryId !== undefined ? $scope.complementaryId : ($scope.detail.userComplementaryActivityId !== undefined ? $scope.detail.userComplementaryActivityId : undefined);
			if(userComplementaryId !== undefined) {
				PatientWeightService.countSearch({}, {'USER_COMPLEMENTARY_ACTIVITY_ID': userComplementaryId}, function(result) {
					if(result.data > 0) {
						PatientWeightService.getByUserComplementaryActivity({userComplementaryActivityId: userComplementaryId}, {}, function(result) {
							$scope.detail = result.data;
							$scope.id = $scope.detail.id;
							
							if($scope.detailObject && $scope.detailReturn) {
								$scope.detailReturn[0] = $scope.detail;
							}
							
							$scope.permissions = result.permissions;
							$scope.fieldPermissions = result.fieldPermissions;
						}, function(error) {
							$scope.detail = undefined;
							d.resolve(undefined);
						});
					}
				}, function(error) {
					$scope.detail = undefined;
					d.resolve(undefined);
				});
			}
		} else {
			d.resolve(undefined);
		}
		return d.promise;
	}
	
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('PATIENT_WEIGHT', PatientWeightService);
	
});