'use strict'

app.controller('AvatarListCtrl', function(
		$scope,
		$controller,
		AvatarService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.pagination = {
			start: 0,
			size: undefined,
			sort: []
	};
	
	$scope.changeSelection = function(row) {
		if($scope.isMultipleSelection) {
			row.selected = !row.selected;
		} else {
			for(var i = 0; i < $scope.list.length; i++) {
				$scope.list[i].selected = false;
			}
			row.selected = true;
		}
	}
	
	$scope.init('AVATAR', AvatarService);
});