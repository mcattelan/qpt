'use strict'

app.controller('InternalStaffUserListCtrl', function(
		$scope,
		$controller,
		InternalStaffUserService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('INTERNAL_STAFF_USER', InternalStaffUserService);
});