'use strict'
app.controller('PhuturemedListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		PhuturemedService,
		MessageService) {
	
		$scope.unpairing = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PhuturemedService.unpairing({ id : id }, function(result) {
						$scope.search();
						MessageService.showSuccess('Disaccoppiamento completato con successo');
						$state.go("app.phuturemeds");
					}, function(error) {
						MessageService.showError('Disaccoppiamento non riuscito');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('PHUTUREMED', PhuturemedService);
	
});

app.controller('PhuturemedController', function(
		$rootScope,
		$scope,
		$stateParams,
		$state,
		PatientService,
		PhuturemedService,
		MessageService,
		FormUtilService) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;

		$scope.errors = {};
		$scope.selects = {};

		if (($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		//	Filtri utilizzati nella direttiva che estra i pazienti
		$scope.patientSearchFilters = {
				filters : {
					'CUSTOM_PROPERTY' :  {code: 'SERVICE_CONFIGURATION', values: 'EXTRA'}
				}
		}
		
		if (angular.isDefined($scope.id)) {
			$scope.initDetail();
		} else {
			$scope.initList();
		}
	}
	
	$scope.initList = function() {
		PatientService.search({}, { start: 0, sort: [ 'lastName','firstName' ]}, function(result) {
			$scope.selects['Patient'] = [];
			for(var i=0; i<result.data.length; i++) {
				$scope.selects['Patient'].push(result.data[i]);
			}
		});
		$scope.showList = false;
//		PermissionService.getAll({ entityType : 'QUALIFICATION' }, function(result) {
//			$scope.initPermission(result.data);
//			$scope.selection = $scope.selection || false;
//			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
//		}, function(error) {
//			$scope.list = [];
//			if(error.status == 404) {
//				$state.go('access.not-found');
//			}
//		});
	};
	
	$scope.initDetail = function(){
		$scope.detail = {};
//		$scope.fieldPermissions = {};
		if(!$scope.isNew()){
			PhuturemedService.get({id:$scope.id}, function(result) {
				$scope.detail = result.data;
//				$scope.fieldPermissions = result.fieldPermissions;
//				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
//				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
//		else {
//			PermissionService.getAll({ entityType : 'QUALIFICATION' }, function(result) {
//				$scope.initPermission(result.data);
//				
//				PermissionService.getAllFields({ entityType : 'QUALIFICATION' }, function(result) {
//					$scope.fieldPermissions = result.data;
//				}, function(error) {
//					$scope.detail = {};
//					$scope.fieldPermissions = {};
//					if(error.status == 404) {
//						$state.go('access.not-found');
//					}
//				});
//			}, function(error) {
//				$scope.detail = {};
//				if(error.status == 404) {
//					$state.go('access.not-found');
//				}
//			});
//		}
	}
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		if(!angular.isDefined($scope.searchFilters.filters)){ 
			$scope.searchFilters['filters'] = {};
		}
		$scope.searchFilters.filters.PATIENT_ID = $scope.objectFilters.patient != undefined ? $scope.objectFilters.patient.id : undefined;
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

//		$scope.selectedIds = []
//		for(var i=0; i<$scope.selected.length; i++) {
//			$scope.selectedIds[i] = $scope.selected[i].id;
//		}

		PhuturemedService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
//			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
//			for (var i=0; i<$scope.list.length; i++) {
//				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
//			}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}
	
	$scope.save = function(){
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};
	
	$scope.insert = function() {
		var request = $scope.loadRequest();
		PhuturemedService.insert({}, request, function(result){
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
//			$scope.fieldPermissions = result.fieldPermissions;
//			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error){
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}

	$scope.update = function() {
		var request = $scope.loadRequest();
		PhuturemedService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
//			$scope.fieldPermissions = result.fieldPermissions;
//			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
			if ($scope.popup) { 
				$scope.close();
			}
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PhuturemedService.delete({ id : id }, function(result) {
						$scope.search();
						MessageService.showSuccess('Cancellazione completata con successo');
						$state.go("app.phuturemeds");
					}, function(error) {
						MessageService.showError('Disaccoppiamento non riuscito');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.unpairing = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PhuturemedService.unpairing({ id : id }, function(result) {
						$scope.search();
						MessageService.showSuccess('Disaccoppiamento completato con successo');
						$state.go("app.phuturemeds");
					}, function(error) {
						MessageService.showError('Disaccoppiamento non riuscito');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.generatePin = function(id) {
		PhuturemedService.generatePin({ id : id }, function(result) {
			$scope.detail = result.data;
			MessageService.showSuccess('Pin generato con successo');
		}, function(error) {
			MessageService.showError('Generazione pin non riuscita');
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.serialNumber', value : $scope.detail.serialNumber, type: 'string', required : true },
					{ id:'detail.patient', value : $scope.detail.patient, type: 'string', required : true },
					]
		};
		if ($scope.detail.status == 'ADDED') {
			form.formProperties.push({ id:'detail.pin', value : $scope.detail.pin, type: 'string', required : true });
		}
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: 10,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.objectFilters = $scope.objectFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);		
	}

//	$scope.initPermission = function(permissions) {
//		if (permissions != undefined && permissions != null) {
//			$scope.canInsert = permissions.INSERT;
//			$scope.canDelete = permissions.DELETE;
//			$scope.canExport = permissions.EXPORT;
//			if ($scope.isNew()) {
//				$scope.canUpdate =  $scope.canInsert;
//			}
//			else {
//				$scope.canUpdate = permissions.UPDATE;
//			}
//		} 
//		else {
//			$scope.canInsert = false;
//			$scope.canDelete = false;
//			$scope.canUpdate = false;
//			$scope.canExport = false;
//		}
//	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
	}

	$scope.undo = function() {
		console.log($scope.isNew())
		if ($scope.isNew()) {
			$state.go("app.phuturemeds", {}, {reload: true});	
		} else {
			$scope.initDetail();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.phuturemeds", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.loadRequest = function() {
		return {
			patientId:			$scope.detail.patient.id,
			serialNumber:		$scope.detail.serialNumber,
			pin:				$scope.detail.pin,
			internalNotes:		$scope.detail.internalNotes,
			startDate:			$scope.detail.startDate
		}
	}

	$scope.init();
});