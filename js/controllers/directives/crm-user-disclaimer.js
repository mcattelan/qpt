'use strict'

app.controller('UserDisclaimerDetailCtrl', function(
		$scope,
		$controller,
		UserDisclaimerService,
		UserService,
		$q,
		$state,
		$cookies,
		$rootScope) {

	$scope.preInit = $scope.preInit || function() {
		var d = $q.defer();
		UserService.getRequiredDisclaimers({}, function(result) {
			if(result.data.length > 0) {
				
				for(var i = 0; i < result.data.length; i++) {
					if(result.data[i].code == 'NO_MEDICAL_DEVICE_WEB') {
						var authToken = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
						var loginName = angular.fromJson($cookies.getObject('italiassistenza_ua'))[0];
						var date = new Date();
						date.setTime(date.getTime() + (3 * 60 * 1000));
						$cookies.putObject('hide_disclaimer', [ loginName, authToken ], { path: '/', expires: date, domain: $rootScope.baseDomain, secure: $rootScope.secure });
					}
				}
				
				$scope.id = result.data[0].id
				$scope.init('USER_DISCLAIMER', UserDisclaimerService);
			} else {
				$state.go('app.dashboard');
			}
		}, function(error) {
			$scope.detail = undefined;
			d.resolve(undefined);
		});
		return d.promise;
	}
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.prepareUpdateRequest = function() {
		var request = {'approvals': []};
		if(angular.isDefined($scope.detail) && angular.isDefined($scope.detail.approvals)) {
			for(var i = 0; i < $scope.detail.approvals.length; i++) {
				var approval = $scope.detail.approvals[i];
				request.approvals.push({code: approval.code, value: approval.value});
			}
		}
		return request;
	}
	
});