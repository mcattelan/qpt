'use strict'

app.controller('PatientDbsTestActivityListCtrl', function(
		$rootScope,
		$scope,
		$controller,
		$modal,
		MessageService,
		PatientDbsTestActivityService) {
	
	$scope.preInit = function() {
		
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.isHcp = $rootScope.isHcp;
		$scope.manualActivitiesDetail = [];
	}
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DRIED_BLOOD_SPOT_TEST', PatientDbsTestActivityService);
	
});

app.controller('PatientDbsTestActivityRequestCtrl', function(
		$scope,
		$controller,
		$state,
		$rootScope,
		MessageService,
		UserServiceConfService) {
	
	$scope.init = function(){
		$scope.detail = {};
	
		$scope.loggedUser = $rootScope.loggedUser;
			
		$scope.detail.hcp = $scope.loggedUser;
		
		$scope.selectedPatient = undefined;
		$scope.userMasterActivity = undefined;
		$scope.execute = false;
	}
		
	$scope.confirm = function() {
		var dialogTitle='Conferma richiesta';
		var dialogText='Confermi la richiesta di un Test DBS per il paziente selezionato?';
		
		var modalInstance = MessageService.simpleConfirm(dialogTitle, dialogText, 'btn-info','sm');
		modalInstance.result.then(
			function(confirm) {
				var notify = false;
				$scope.execute = true;
				
			UserServiceConfService.getUserManualMasterActivities({ userId : $scope.selectedPatient.id }, undefined, function(result) {
				var list = result.data;
				if(list && list.length > 0) {
					for(var i = 0; i < list.length; i++) {
						if(list[i].code.indexOf('_HCP_REQUEST_TEST_DBS') >= 0) {
							$scope.userMasterActivity = list[i];
						}
					}
					
					if($scope.userMasterActivity != undefined) {
						UserServiceConfService.startUserMasterActivityManually({ userId : $scope.selectedPatient.id, masterActivityId : $scope.userMasterActivity.id},
							function(result) {
								$scope.close();
								MessageService.showSuccess('Avvio attivit\u00E0 completato con successo');
								$state.go('app.patient-dbs-test-activity');
								
							}, function(error) {
								$scope.close();
								if(error && error.data && error.data.message) {
									MessageService.showError('Avvio attivit\u00E0 non riuscito', error.data.message);
									$state.go('app.patient-dbs-test-activity');
								}
								else {
									MessageService.showError('Avvio attivit\u00E0 non riuscito');
								}
								if(error.status == 404) {
									$state.go('access.not-found');
								}
							}
						);
					}
				}
			}, function(error) {
				$scope.userMasterActivity = undefined;
			});
		});
	}

	$scope.undo = function(){
		$scope.close();
	}

	$scope.init();
});