'use strict'

app.controller('PatientLeftoverDrugController', function(
		$scope,
		$state,
		PatientLeftoverDrugService,
		PatientTherapyService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {
	
	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};
	
		if (angular.isDefined($scope.patientId) && $scope.patientId) {
			if(angular.isDefined($scope.id)) {
				$scope.initDetails();
			} else {
				$scope.initList();
			}
		}
	}
	
	$scope.initList = function() {
		$scope.showList = false;
		
		PermissionService.getAll({ entityType : 'PATIENT_LEFTOVER_DRUG' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};

		$scope.selects = {
				'Drugs' : []
		};
		
		var therapySearchFilters = {
				filters: { 'PATIENT_ID' : $scope.patientId }
		}
		PatientTherapyService.search({ }, therapySearchFilters, function(result) {
			$scope.therapies = result.data;
			for(var i = 0; i <$scope.therapies.length; i++) {
				for(var x = 0; x <$scope.therapies[i].drugs.length; x++) {
					var therapyDrug = {
							'id' : $scope.therapies[i].drugs[x].id,
							'name' : $scope.therapies[i].drugs[x].drug.name,
							'therapy' : $scope.therapies[i].name
					}
					$scope.selects['Drugs'].push(therapyDrug);
				}
			}
			if ($scope.isNew() && $scope.selects['Drugs'].length == 1) {
				$scope.detail.drug = {
						'id' : $scope.selects['Drugs'][0].id
				}
			}
		}, function(error) {
			if (error == '404') {
				$state.go('access.404');
			}
		})
		
		if (!$scope.isNew()) {
			PatientLeftoverDrugService.get({ id : $scope.id, patientId : $scope.patientId }, function(result) {
				$scope.detail = result.data;
				if(angular.isDefined($scope.detail) && angular.isDefined($scope.detail.percentageAdherence)){
					$scope.detail.percentageAdherence = Math.round($scope.detail.percentageAdherence);
				}
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {
					'status' : 'TAKEN'
			};
			PermissionService.getAll({ entityType : 'PATIENT_LEFTOVER_DRUG' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'PATIENT_LEFTOVER_DRUG' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if (error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		
		PatientLeftoverDrugService.search({ patientId : $scope.patientId }, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}	
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}
	
	$scope.insert = function() {
		var request = $scope.loadRequest();
		if (angular.isUndefined($scope.validate()) || angular.equals($scope.errors, {})) {
			PatientLeftoverDrugService.insert({ patientId : $scope.patientId }, request, function(result) {
				$scope.detail = result.data;
				if(angular.isDefined($scope.detail) && angular.isDefined($scope.detail.percentageAdherence)){
					$scope.detail.percentageAdherence = Math.round($scope.detail.percentageAdherence);
				}
				$scope.initPermission(result.permissions);
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.id = $scope.detail.id;
				MessageService.showSuccess('Inserimento completato con successo');
			}, function(error) {
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					if(error.data && error.data.message) {
						MessageService.showError('Errore in fase di salvataggio: ' + error.data.message);
					} else {
						MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
					}
				}
			});
		}
	}
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		PatientLeftoverDrugService.update({ id : $scope.id, patientId : $scope.patientId }, request, function(result) {
			$scope.detail = result.data;
			if(angular.isDefined($scope.detail) && angular.isDefined($scope.detail.percentageAdherence)){
				$scope.detail.percentageAdherence = Math.round($scope.detail.percentageAdherence);
			}
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				if(error.data && error.data.message) {
					MessageService.showError('Errore in fase di salvataggio: ' + error.data.message);
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
				}
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PatientLeftoverDrugService.delete({ id : id, patientId : $scope.patientId }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initList();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.drug' , value : $scope.detail.drug, type: 'string',  required: true},
					{ id: 'detail.date' , value : $scope.detail.date, type: 'number',  required: true}
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}
	
	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}
	
	$scope.loadRequest = function() {
		return {
			drugId:			$scope.detail.drug.id,
			drugRemained:	$scope.detail.drugRemained,
			date:			$scope.detail.date,
			complementaryId:$scope.detail.complementaryId,
			internalNotes:	$scope.detail.internalNotes
		}
	}
	
	$scope.init();
});