'use strict'
app.controller('DrugPickUpItemController', function(
		$scope,
		$state,
		$rootScope,
		DrugPickUpItemService,
		PatientTherapyDrugService,
		PermissionService,
		FormUtilService,
		MessageService) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.selects = {
			'Drugs': [],
			'MeasurementUnits': []
		};
		$scope.editable = true; //per drug e measureUnit solo
		
		PatientTherapyDrugService.search({ patientId: $scope.drugPickUp.patient.id }, {}, function(result) {
			for(var i=0; i<result.data.length; i++) {
				$scope.selects['Drugs'].push(result.data[i].drug);
			}
			
			if($scope.isNew() && $scope.selects['Drugs'].length == 1) {
				if(!angular.isDefined($scope.detail)) {
					$scope.detail = {};
				}
				$scope.detail.drug = $scope.selects['Drugs'][0];
				if(angular.isDefined($scope.detail.drug.measurementUnits)){
					for(var i=0; i<$scope.detail.drug.measurementUnits.length; i++) {
						$scope.selects['MeasurementUnits'].push($scope.detail.drug.measurementUnits[i].text);
					}
					if($scope.selects['MeasurementUnits'].length == 1) {
						$scope.detail.measurementUnit = $scope.selects['MeasurementUnits'][0];
					}
				}
			}
		});
		$scope.initDetail();
	}
	
	$scope.initDetail = function() {
		if(!angular.isDefined($scope.detail)){
			$scope.detail = {};
		}
		$scope.emptyMeasurementUnits = true;
		if(!$scope.isNew()) {
			DrugPickUpItemService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.editable = false;
				if(angular.isDefined($scope.detail.drug.measurementUnits)){
					for(var i=0; i<$scope.detail.drug.measurementUnits.length; i++) {
						$scope.selects['MeasurementUnits'].push($scope.detail.drug.measurementUnits[i].text);
					}
//					if($scope.selects['MeasurementUnits'].length == 1) {
//						$scope.detail.measurementUnit = $scope.selects['MeasurementUnits'][0];
//					}
				}
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			if(angular.isDefined($scope.itemIndex)) {
				$scope.detail = $scope.drugPickUp.items[$scope.itemIndex];
			} 
			PermissionService.getAll({ entityType : 'DRUG_PICK_UP_ITEM' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'DRUG_PICK_UP_ITEM' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
		
		if(angular.isDefined($scope.detail.measurementUnit) && $scope.detail.measurementUnit.lenght > 0) {
			$scope.emptyMeasurementUnits = false;
		}
		
	};
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}
	
	$scope.isNew = function() {
		return ($scope.id == undefined || $scope.id == '');
	};
	
	$scope.undo = function() {
		$scope.initDetail();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.drug' , value : $scope.detail.drug, type: 'string',  required: true },
				]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}
	
	$scope.addItem = function(selected) {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			for(var i=0; i<$scope.drugPickUp.items.length; i++) {
				if ((angular.isDefined($scope.drugPickUp.items[i].id) && $scope.drugPickUp.items[i].id == $scope.detail.id) ||
						(angular.isUndefined($scope.drugPickUp.items[i].id) && i == $scope.itemIndex)) {
					$scope.drugPickUp.items.splice(i, 1);
				}
			}
			$scope.drugPickUp.items.splice($scope.itemIndex || 0, 0, $scope.detail);
			if ($scope.modal) {
				$scope.modal.close();
			}
		}
	}
	
	$scope.changeDrugMeasurementUnit = function(drug, measurementUnit) {
		$scope.selects['MeasurementUnits'] = [];
		if(angular.isDefined(drug) && angular.isDefined(drug.measurementUnits)) {
			for(var i=0; i<drug.measurementUnits.length; i++) {
				$scope.selects['MeasurementUnits'].push(drug.measurementUnits[i].text);
			}
			if(angular.isDefined(measurementUnit)) {
				$scope.selects['MeasurementUnits'].push(measurementUnit);
			}
		}
	}
	
	$scope.init();
	
});