'use strict'

app.controller('DriedBloodSpotTestListCtrl', function(
		$scope,
		$controller,
		DriedBloodSpotTestService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DRIED_BLOOD_SPOT_TEST', DriedBloodSpotTestService);
});

app.controller('DriedBloodSpotTestDetailCtrl', function(
		$scope,
		$controller,
		DriedBloodSpotTestService,
		UserComplementaryActivityService) {
	
	$controller('BaseDetailCtrl', { $scope: $scope });
	
	$scope.init('DRIED_BLOOD_SPOT_TEST', DriedBloodSpotTestService);
});

app.controller('DriedBloodSpotTestModalCtrl', function(
		$scope,
		$controller,
		DriedBloodSpotTestService,
		PermissionService) {
	
	$scope.init = function(){
		var searchFilters = {};
		$scope.entityType = 'DRIED_BLOOD_SPOT_TEST';
		if(angular.isDefined($scope.complementaryType)) {
			if($scope.complementaryType == 'DBS_TEST_BLOOD_SAMPLING'){
				searchFilters['filters'] = {'BLOOD_SAMPLING_COMPLEMENTARY_ACTIVITY_ID' : $scope.complementaryId};
			}else if($scope.complementaryType == 'DBS_TEST_PICK_UP'){
				searchFilters['filters'] = {'PICK_UP_COMPLEMENTARY_ACTIVITY_ID' : $scope.complementaryId};
			}
			PermissionService.getAllFields({ entityType : $scope.entityType }, function(result) {
				$scope.fieldPermissions = result.data;
				DriedBloodSpotTestService.search({}, searchFilters , function(result) {
					$scope.detail = result.data[0];
					$scope.initPermission(result.permissions);
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = undefined;
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});	
		}
	}
		
	$scope.initPermission = function(permissions) {
		$scope.canInsert = false;
		$scope.canDelete = false;
		$scope.canUpdate = false;
		$scope.canExport = false;
	}
	
	$scope.init();
});