'use strict'

app.controller('UserServiceController', function(
		$rootScope,
		$scope,
		$state,
		UserServiceConfService,
		MessageService,
		FormUtilService,
		PermissionService,
		REGEXP,
		SMART_TABLE_CONFIG,
		WorkflowService,
		ServiceConfService) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.isInternalStaff = $rootScope.isInternalStaff;
		$scope.isPersonified = $rootScope.isPersonified;

		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.id)) {
			$scope.initDetail();
		} else {
			$scope.initList();
		}
	}

	$scope.initList = function() {	
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'USER_SERVICE_CONF' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	

	$scope.checkIfCanSwitch = function() {
		$scope.canSwitch = $scope.detail.alternativeServices && $scope.detail.alternativeServices.length > 0;
	}
		
	$scope.initDetail = function(){
		$scope.detail = {};
		$scope.serviceConfs = {};
		$scope.fieldPermissions = {};

		if(!$scope.isNew()) {
			UserServiceConfService.get({ id:$scope.id }, function(result) {
				$scope.detail = result.data;
				$scope.detail.recursiveDelayTime = undefined;
				$scope.detail.recursiveDelayTimeUnit = undefined;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
				$scope.checkIfCanSwitch();
				if(angular.isDefined($scope.detail.dependingServices)) {
					var dependingServices = $scope.detail.dependingServices;
					for(var i = 0; i < dependingServices.length; i++) {
						var uniqueKey = dependingServices[i].uniqueKey;
						UserServiceConfService.getDependingServices({ id: $scope.detail.id, uniqueKey: uniqueKey }, function(result) {
							$scope.serviceConfs[uniqueKey] = result.data;
						}, function(error) {
							$scope.detail = {};
							$scope.serviceConfs = {};
							if(error.status == 404) {
								$state.go('access.not-found');
							}
						});
					}
				}
				
				if(angular.isDefined($scope.detail.activities)) {
					var activities = $scope.detail.activities;
					for(var i=0; i<activities.length; i++) {
						if(angular.isDefined(activities[i].code) && activities[i].code == 'QUIPERTE_DRUG_DELIVERY') {
							if(angular.isDefined(activities[i].recursiveDelayTimes)) {
								$scope.detail.recursiveDelayTime = activities[i].recursiveDelayTimes[0].time;
								$scope.detail.recursiveDelayTimeUnit = activities[i].recursiveDelayTimes[0].timeUnit;
							}
						}
					}
				}
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			$scope.detail = {};

			PermissionService.getAll({ entityType : 'USER_SERVICE_CONF' }, function(result) {
				$scope.initPermission(result.data);
				$scope.checkIfCanSwitch();
				
				PermissionService.getAllFields({ entityType : 'USER_SERVICE_CONF' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	}

	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		UserServiceConfService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});			
	}

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})){
			if ($scope.isNew()) {
				$scope.update();	
			} else {
				$scope.update(); //aggiorna anche le proprietà aggiuntive relative ai delay per l'attività di consegna
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		UserServiceConfService.insert({}, request, function(result) {
			$scope.detail = result.data;
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		})
	}

	$scope.update = function(){
		var request = $scope.loadRequest();
		//aggiorna anche le proprietà aggiuntive relative ai delay per l'attività di consegna
		UserServiceConfService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			
			if(angular.isDefined($scope.detail.activities)) {
				var activities = $scope.detail.activities;
				for(var i=0; i<activities.length; i++) {
					if(angular.isDefined(activities[i].code) && activities[i].code == 'QUIPERTE_DRUG_DELIVERY') {
						if(angular.isDefined(activities[i].recursiveDelayTimes)) {
							$scope.detail.recursiveDelayTime = activities[i].recursiveDelayTimes[0].time;
							$scope.detail.recursiveDelayTimeUnit = activities[i].recursiveDelayTimes[0].timeUnit;
						}
					}
				}
			}
			
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					UserServiceConfService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}

	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.name', value : $scope.detail.name, type: 'string', required : true },				
					]
		};

		if(angular.isDefined($scope.detail.code) && $scope.detail.code == 'QUIPERTE_DRUG_PICK_UP_AND_DELIVERY' && angular.isDefined($scope.detail.activities)) {
			var activities = $scope.detail.activities;
			for(var i=0; i<activities.length; i++) {
				if(angular.isDefined(activities[i].code) && activities[i].code == 'QUIPERTE_DRUG_DELIVERY') {
					if(angular.isDefined(activities[i].recursiveDelayTimes) && angular.isDefined(activities[i].recursiveDelayTimes[0].time) && activities[i].recursiveDelayTimes[0].timeUnit) {
						if(activities[i].recursiveDelayTimes[0].time <= 0) {
							errors['detail.recursiveDelayTime'] = "E' necessario impostare una frequenza per la consegna del farmaco con valore maggiore di zero";
						}
						if(activities[i].recursiveDelayTimes[0].timeUnit == undefined) {
							errors['detail.recursiveDelayTimeUnit'] = "E' necessario impostare un'unità di misura per la frequenza di consegna del farmaco";
						}
					} else {
						errors['detail.recursiveDelayTime'] = "E' necessario impostare i valori per la frequenza di consegna del farmaco";
					}
				}
			}
		}

		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		if(angular.isUndefined($scope.searchFilters['filters'])) {
			$scope.searchFilters['filters'] = {};
		}
		$scope.searchFilters['filters']['USER_ID'] = $scope.userId;
		$scope.searchFilters['filters']['WITH_ACTIVITIES'] = true;
		$scope.searchFilters['filters']['STATUS'] = 'ACTIVE';
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
			$scope.canActivate = permissions.ACTIVATE;
			$scope.canSuspend = permissions.SUSPEND;
			$scope.canTerminate = permissions.TERMINATE;
			
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canActivate = false;
			$scope.canSuspend = false;
			$scope.canTerminate = false;
		}
	}
	
	$scope.suspendServicesConf = function(serviceId) {
		UserServiceConfService.suspend({ id : serviceId }, undefined, function(result) {
			MessageService.showSuccess('Sospensione completata con successo');
			$state.reload();
			
		}, function(error) {	
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio', error.message !== undefined ? error.message : 'Errore imprevisto');
			}
		});
	}
	
	$scope.terminateServicesConf = function(serviceId) {
		UserServiceConfService.terminate({ id : serviceId }, undefined, function(result) {
			MessageService.showSuccess('Terminazione completata con successo');
			$state.reload();
			
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio', error.message !== undefined ? error.message : 'Errore imprevisto');
			}
		});
	}
	
	$scope.switchServicesConf = function(serviceId) {
		ServiceConfService.getService({ id: $scope.detail.serviceConfRefId}, function(result) {
			if(result!=undefined) {
				if(result.data.status=='DELETED') {
					MessageService.showError('Errore','Servizio eliminato, impossibile effettuare uno switch');
				}
				else {
					UserServiceConfService.switchToAlternatives({ id : serviceId }, {}, function(result) {
						MessageService.showSuccess('Switch servizio avviato con successo');
						
						var businessKey = result.data;
						if(businessKey !== undefined) {
							WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
								function(result) {
									if ($scope.modal) {
										$scope.modal.close();
									}
									if(result.data !== undefined) {
										if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
											$state.go('app.task', { 'id': result.data.id });
										}
									}
									else {
										if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
											$state.go('app.my-tasks');
										}
									}
								},
								function(error) {
									if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
										$state.go('app.my-tasks');
									}
								}
							)
						}
						else {
							$state.reload();
						}
						
					}, function(error) {
						if (error.status == 404) {
							$state.go('access.not-found');
						} else {
							MessageService.showError('Errore in fase di switch del servizio', error.message !== undefined ? error.message : 'Errore imprevisto');
						}
					});
				}
			}
			else
				MessageService.showError('Errore','Servizio eliminato, impossibile effettuare uno switch');
			
		}, function(error) {
			console.log('Errore nella verifica della configurazione servizio');
			MessageService.showError('Errore','Errore imprevisto nella lettura della configurazione servizio');
		});
		
	}
	
	$scope.activateServicesConf = function(serviceId) {
		UserServiceConfService.activate({ id : serviceId }, undefined, function(result) {
			MessageService.showSuccess('Attivazione completata con successo');
			$state.reload();
			
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio', error.message !== undefined ? error.message : 'Errore imprevisto');
			}
		});
	}
	
	$scope.checkServices = function() {
		UserServiceConfService.checkServices({ userId : $scope.userId }, undefined, function(result) {
			MessageService.showSuccess('Aggiornamento servizi completato con successo');
			$state.reload();
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio', error.message !== undefined ? error.message : 'Errore imprevisto');
			}
		});
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.diseases");	
		} else {
			$scope.initDetail();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.loadRequest = function() {
		if(angular.isDefined($scope.detail.activities)) {
			var activities = $scope.detail.activities;
			for(var i=0; i<activities.length; i++) {
				if(angular.isDefined(activities[i].code) && activities[i].code == 'QUIPERTE_DRUG_DELIVERY') {
					if(angular.isDefined(activities[i].recursiveDelayTimes)) {
						activities[i].recursiveDelayTimes[0].time = $scope.detail.recursiveDelayTime;
						activities[i].recursiveDelayTimes[0].timeUnit = $scope.detail.recursiveDelayTimeUnit;
					}
				}
			}
		}
		return angular.copy($scope.detail);
	}

	$scope.init();
});