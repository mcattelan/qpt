'use strict'

app.controller('PatientLeftoverDrugDetectionController', function(
		$scope,
		$state,
		PatientLeftoverDrugService,
		PatientTherapyService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};

		if (angular.isDefined($scope.patientId) && $scope.patientId) {
			$scope.initList();
		}
	}

	$scope.initList = function() {
		$scope.showList = false;

		PermissionService.getAll({ entityType : 'PATIENT_LEFTOVER_DRUG' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if (tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;

			$scope.pagination.start = tableState.pagination.start;
			if (tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if (tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];

		angular.extend($scope.searchFilters, $scope.pagination);
		$scope.searchFilters.total = 0;
		$scope.searchFilters.complementaryId = $scope.complementaryId;

		var therapySearchFilters = {
				filters: { 'PATIENT_ID' : $scope.patientId }
		}

		PatientTherapyService.search({}, therapySearchFilters, function(result) {
			$scope.therapies = result.data;
			
			PatientLeftoverDrugService.searchDetection({ patientId : $scope.patientId }, $scope.searchFilters, function(result) {
				if(angular.isDefined(result.data) && result.data) {
					for(var i = 0; i <result.data.length; i++) {
						for(var y = 0; y <$scope.therapies.length; y++) {
							for(var x = 0; x <$scope.therapies[y].drugs.length; x++) {
								if ($scope.therapies[y].drugs[x].drug.id === result.data[i].drug.drug.id) {
									$scope.list[x] = {
											id: result.data[i].id,
											key: result.data[i].id,
											date: result.data[i].date,
											'drug': result.data[i].drug,
											drugRemained: result.data[i].drugRemained,
											complementaryId: result.data[i].complementaryId
									}
									break;
								}
							}
						}
					}
				}
				$scope.permissions = result.permissions;
				$scope.pagination.start = result.start;
				$scope.pagination.size = result.size;
				$scope.pagination.sort = result.sort;
				$scope.pagination.total = result.total;
				var index = 0;
				for(var i = 0; i <$scope.therapies.length; i++) {
					for(var x = 0; x <$scope.therapies[i].drugs.length; x++) {
						var found = false;
						for (var y = 0; y < $scope.list.length; y++) {
							if ($scope.list[y].drug.drug.id == $scope.therapies[i].drugs[x].drug.id) {
								found = true;
							}
						}
						if(!found) {
							$scope.list[index] = {
									id: '',
									key: new Date(),
									date: $scope.date,
									'drug': $scope.therapies[i].drugs[x],
									drugRemained: 0,
									complementaryId: $scope.complementaryId
							}
							$scope.permissions[$scope.list[index].key] = { UPDATE: $scope.canInsert };
							$scope.pagination.total++;
							index++;
						}
					}
				}
				if (tableState !== undefined) {
					tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
				}
			}, function(error) {
				$scope.list = [];
				$scope.initPaginationAndFilter();
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}, function(error) {
			$scope.list = {};
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}

	$scope.save = function(row) {
		$scope.errors = $scope.validate(row);
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew(row.id)) {
				$scope.insert(row);
			} else {
				$scope.update(row);
			}	
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	}

	$scope.insert = function(row) {
		var request = $scope.loadRequest(row);
		PatientLeftoverDrugService.insert({ patientId : $scope.patientId }, request, function(result) {
			for (var i = 0; i < $scope.list.length; i++) {
				if ($scope.list[i].drug.id = row.drug.id) {
					$scope.list[i] = result.data;
					break;
				}
			}
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.id = result.data.id;
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				if(error.data && error.data.message) {
					MessageService.showError('Errore in fase di salvataggio: ' + error.data.message);
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
				}
			}
		});
	}

	$scope.update = function(row) {
		var request = $scope.loadRequest(row);
		PatientLeftoverDrugService.update({ id : row.id, patientId : $scope.patientId }, request, function(result) {
			for (var i = 0; i < $scope.list.length; i++) {
				if ($scope.list[i].id = row.id) {
					$scope.list[i] = result.data;
					break;
				}
			}
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				if(error.data && error.data.message) {
					MessageService.showError('Errore in fase di salvataggio: ' + error.data.message);
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
				}
			}
		});
	}

	$scope.validate = function(row) {
		var errors = {};
		var form = {
				formProperties : [
					{ id: row.drug.id + '.drug' , value : row.drug, type: 'string',  required: true},
					{ id: row.drug.id + '.date' , value : row.date, type: 'number',  required: true},
					{ id: row.drug.id + '.drugRemained' , value : row.drugRemained, type: 'string',  required: true}
					]
		};
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.isNew = function(id) {
		return id == '';
	};

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.loadRequest = function(row) {
		return {
			status: 			row.status,
			drugId:				row.drug.id,
			date:				row.date,
			drugRemained:		row.drugRemained,
			complementaryId:	row.complementaryId,
			notes:				row.notes,
			internalNotes:		row.internalNotes
		}
	}

	$scope.init();
});