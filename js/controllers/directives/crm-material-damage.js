'use strict'
app.controller('MaterialDamageListCtrl', function(
		$scope,
		$rootScope,
		$controller,
		MaterialDamageService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('MATERIAL_DAMAGE', MaterialDamageService);
	
});

app.controller('MaterialDamageDetailCtrl', function(
		$rootScope,
		$scope,
		$stateParams,
		$state,
		MaterialDamageService,
		FormUtilService,
		PermissionService,
		MessageService,
		$filter) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.errors = {};
		if (($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		if (angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
	};
	
	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};

		if (!$scope.isNew()) {
			MaterialDamageService.get({ id : $scope.id }, function(result) {
				$scope.detail = angular.extend($scope.detail,result.data);
				
				// Se chiamato dall'attività eseguo i controlli per impostare i campi di default
				// in caso siano ancora non definiti
				if (angular.isDefined($scope.activitiForm) && $scope.activitiForm) {
					if (angular.isUndefined($scope.detail.organizer)) {
						$scope.detail.reporter = $scope.activitiForm.owner;
					}
					if ($scope.activitiForm.date != undefined) {
						if ($scope.detail.status == 'DRAFT') {
							$scope.detail.notificationDate = $scope.activitiForm.date;
						}
					}
				}
				for (var i = 0; i < $scope.detail.materials.length; i++) {
					$scope.detail.materials[i].selected = false;
					if ($scope.detail.materials[i].quantity > 0) {
						$scope.detail.materials[i].selected = true;
					}
				}
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				if($scope.detail==undefined) {
					$scope.detail = {};
				}
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			if ($scope.detail==undefined) {
				$scope.detail = {};
			}
			PermissionService.getAll({ entityType : 'MATERIAL_DAMAGE' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'MATERIAL_DAMAGE' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					if($scope.detail==undefined)
						$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				if($scope.detail==undefined)
					$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		MaterialDamageService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
			$scope.close();
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		MaterialDamageService.update({ id : $scope.id },request,function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Aggiornamento completato con successo');
			$scope.close();
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					MaterialDamageService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.close();
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.send = function() {
		var title = 'Conferma Spedizione/Consegna';
		var text = 'Una volta impostata come confermata non sarà possibile modificare i dati della segnalazione Malfunzionamenti/Rotture, si desidera procedere?';
		var style = 'bg-secondary';
		var modalInstance = MessageService.simpleConfirm(title,text,style,'sm');
		modalInstance.result.then(
		function(confirm) {
			$scope.detail.status = 'CONFIRMED';
			$scope.save();
			if (angular.isDefined($scope.errors) && $scope.errors) {
				$scope.detail.status = 'DRAFT';
			}
		});
	}
	
	$scope.selectedChange = function(material) {
		material.serialNumber = undefined;
		material.damageDescription = undefined;
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detail.code', value: $scope.detail.code, required: true, type: 'string' },
					{ id:'detail.notificationDate', value: $scope.detail.notificationDate, required: true, type: 'string' },
					{ id:'detail.eventDate', value: $scope.detail.eventDate, required: true, type: 'string' },
					{ id:'detail.status', value: $scope.detail.status, required: true, type: 'string' },
					{ id:'detail.reporter', value: $scope.detail.reporter, required: true },
					{ id:'detail.recipient', value: $scope.detail.recipient, required: true },
				]					
		};
		
		var hasMaterials = false;
		for (var i=0; i<$scope.detail.materials.length; i++) {
			if ($scope.detail.materials[i].selected != undefined && 
					!hasMaterials && $scope.detail.materials[i].selected == true) {
				hasMaterials = true;
				break;
			}
		}
		
		errors = FormUtilService.validateForm(form);

		// In caso non sia stato impostato neanche un materiale in spedizione emetto il messaggio di errore
		if (!hasMaterials) {
			errors['detail.materials'] = "E' necessario selezionare almeno uno dei materiali nella lista";
		}
		
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
		$scope.permissions = permissions;
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.material-deliveries");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}

	$scope.loadRequest = function() {
		var materials = [];
		for (var i=0; i<$scope.detail.materials.length; i++) {
			materials.push({ 
				materialId: $scope.detail.materials[i].material.id,
				id: $scope.detail.materials[i].id,
				quantity: $scope.detail.materials[i].selected ? 1 : 0,
				damageDescription: $scope.detail.materials[i].damageDescription,
				serialNumber: $scope.detail.materials[i].serialNumber
			})
		}
		return {
			notificationDate:	$scope.detail.notificationDate,
			eventDate : 		$scope.detail.eventDate,
			reporterId: 		$scope.detail.reporter.id,
			recipientId: 		$scope.detail.recipient.id,
			notes: 				$scope.detail.notes,
			internalNotes: 		$scope.detail.internalNotes,
			status:				$scope.detail.status,
			code:				$scope.detail.code,
			materials:			materials,
			notificationManufacturer:		$scope.detail.notificationManufacturer
		}
				
	}
	
	$scope.init();
	
});