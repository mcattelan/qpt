'use strict'
app.controller('DrugDeliveryListCtrl', function(
		$scope,
		$controller,
		DrugDeliveryService) {
	
	$controller('BaseListCtrl', { $scope: $scope });
	
	$scope.init('DRUG_DELIVERY', DrugDeliveryService);
});

app.controller('DrugDeliveryDetailController', function(
		$scope,
		$state,
		$rootScope,
		$modal,
		DrugDeliveryService,
		DrugDeliveryItemService,
		PermissionService,
		FormUtilService,
		MessageService,
		SMART_TABLE_CONFIG) {

	$scope.init = function() {
		$scope.errors = {};
		$scope.selects = {};

		if(angular.isDefined($scope.complementaryId)) {
			$scope.initDetails();
		}
	}


	$scope.initDetails = function() {
		$scope.detail = {};
		$scope.fieldPermissions = {};
		$scope.searchFilters = $scope.searchFilters || {};
		
		if(angular.isDefined($scope.complementaryType) && $scope.complementaryType == 'DRUG_PICK_UP_DELIVERY') {
			DrugDeliveryService.getByUserComplementaryId({ complementaryId : $scope.complementaryId }, function(result) {
				$scope.detail = result.data;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
		//tramite l'id della complementare cerco di capire se l'oggetto DrugPickUp è stato già salvato oppure no
			DrugDeliveryService.getWithDefaults({ complementaryId : $scope.complementaryId }, function(result) {
				$scope.detail = result.data;
				//Sovrascrivo con dati della form che passo alla direttiva
				if(angular.isDefined($scope.activitiForm)) {
					//il patient non lo sovrascrivo perchè non può essere cambiato
					if(angular.isDefined($scope.activitiForm.locationType)) {
						$scope.detail.locationType	= $scope.activitiForm.locationType;
					}
					if(angular.isDefined($scope.activitiForm.address)) {
						$scope.detail.address = $scope.activitiForm.address;
					}
					if(angular.isDefined($scope.activitiForm.date)) {
						$scope.detail.deliveryDate = $scope.activitiForm.date;
					}
					if(angular.isDefined($scope.activitiForm.owner)) {
						$scope.detail.owner	= $scope.activitiForm.owner;
					}
				} 
	
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};

	$scope.save = function() {
		$scope.errors = $scope.validate();
		if (angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		DrugDeliveryService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};

	$scope.update = function() {
		var request = $scope.loadRequest();
		DrugDeliveryService.update({ id : $scope.detail.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.initPermission(result.permissions);
			$scope.fieldPermissions = result.fieldPermissions;
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
			}
		});
	};

	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					DrugDeliveryService.delete({ id : id, }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initList();
					}, function(error) {
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	};

	$scope.removeItem = function(index) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					$scope.detail.items.splice(index, 1);
				}
		);
	};


	$scope.validate = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id: 'detail.patient' , value : $scope.detail.patient, type: 'string',  required: true},
					{ id: 'detail.deliveryDate' , value : $scope.detail.deliveryDate, type: 'string',  required: true},
					{ id: 'detail.owner' , value : $scope.detail.owner, type: 'string',  required: true}
					]
		};

		// Se l'address è valorizzato controllo l'obbligatorietà di tutti i campi necessari per la spedizione
		if (angular.isDefined($scope.detail.address) && $scope.detail.address) {
			form.formProperties.push({ id:'address.country', value : $scope.detail.address.country, required: true });
			form.formProperties.push({ id:'address.county', value : $scope.detail.address.county, required: true });
			form.formProperties.push({ id:'address.province', value : $scope.detail.address.province, required: true });
			form.formProperties.push({ id:'address.city', value : $scope.detail.address.city, required: true });
			form.formProperties.push({ id:'address.address', value : $scope.detail.address.address, required: true });
		}
		errors = FormUtilService.validateForm(form);
		if(!angular.isDefined($scope.detail.items) || $scope.detail.items.length <= 0) {
			errors['detail.items'] = "E' necessario aggiungere almeno un farmaco in lista";
		}
		if(angular.isDefined($scope.detail.patient) && !angular.isDefined($scope.detail.patient.id)) {
			errors['detail.patient'] = "E' obbligatorio inserire il paziente";
		}
		//TODO in analisi non e' stato messo ma l'ho gestito comunque, come per le altre complementari sorelle, non lo rendo obbligatorio per ora
//		if(angular.isDefined($scope.detail.owner) && !angular.isDefined($scope.detail.owner.id)) {
//			errors['detail.owner'] = "E' obbligatorio inserire il responsabile dell'attività prima di confermare i dati di ritiro!";
//		}
		console.log(errors);
		return errors;
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
	}

	$scope.isNew = function() {
		return ($scope.detail.id == undefined || $scope.detail.id == '');
	}

	$scope.undo = function() {
		$scope.initDetails();
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.loadRequest = function() {
		var items = [];
		for(var i = 0; i < $scope.detail.items.length; i++) {
			var item = $scope.detail.items[i];
			if(angular.isDefined(item)) {
				items.push({
					drugId: item.drug.id,
					deliveredQuantity: item.deliveredQuantity,
					batchNumber: item.batchNumber,
					drugExpiringDate: item.drugExpiringDate,
					internalNotes: item.internalNotes,
					measurementUnit: item.measurementUnit
				})
			}
		}
		
		return {
			patientId : $scope.detail.patient.id,
			ownerId : $scope.detail.owner.id,
			deliveryDate : $scope.detail.deliveryDate,
			code : $scope.detail.code,
			internalNotes : $scope.detail.internalNotes,
			address : $scope.detail.address,
			locationType : $scope.detail.locationType,
			complementaryActivityId : $scope.complementaryId,
			items : items
		}
	}
	
	$scope.init();

});