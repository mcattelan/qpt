'use strict'

app.controller('PatientController', function(
		$rootScope,
		$scope, 
		$stateParams, 
		$http, 
		$state,
		$modal,
		PatientService, 
		PatientUserService,
		PatientTherapyService,
		UserServiceConfService,
		TherapyService,
		PatientMedicalExaminationService,
		NationalityService,
		FormUtilService,
		MessageService,
		PermissionService,
		REGEXP,
		DiseaseService,
		DepartmentService,
		DeviceTypeService,
		WorkflowService,
		EntityCustomPropertyConfService,
		SMART_TABLE_CONFIG,
		RESTURL) {


	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.programProperties = $rootScope.programProperties;
		$scope.isInternalStaff = $rootScope.isInternalStaff;
		$scope.isPersonified = $rootScope.isPersonified;
		$scope.isLogistic = $rootScope.isLogistic;
		$scope.isPatient = $rootScope.isPatient;
		$scope.errors = {};
		$scope.selects = {
				'Nationalities' : []
		};
		$scope.entityPropertyConf = [];
		
		NationalityService.getAll({ }, function(result) {
			$scope.selects['Nationalities'] = result.data;
		}, function(error) {
			$scope.selects['Nationalities'] = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		})
		
		if(($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}
		
		$scope.service = PatientService;
		
		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		}
		else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'PATIENT' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
		DiseaseService.search({}, {  start: 0, sort: ['name'] }, function(result) {
			$scope.selects['Disease'] = result.data;
		});
		
		DepartmentService.search({}, {  start: 0, sort: ['name'] }, function(result) {
			$scope.selects['Department'] = result.data;
		});
		
		TherapyService.search({}, {  start: 0, sort: ['name'] }, function(result) {
			$scope.selects['Therapy'] = result.data;
		});

	};
	
	$scope.initDetails = function() {
		$scope.selection = false;
		$scope.selectedIds = [];
		$scope.newTherapy = undefined;
		$scope.detail = {};
		$scope.detailActivate = {};
		$scope.fieldPermissions = {};
		$scope.delegate = {
			delegate1: undefined,
			delegate2: undefined
		};
		$scope.caregivers = {
				caregiver1:undefined
		};
		$scope.userServices = [];
		$scope.serviceConfs = {};
		
		$scope.contactMethods = {};
		for(var i= 0; i < $rootScope.constants['PreferredContactMethod'].length; i++){
			$scope.contactMethods[$rootScope.constants['PreferredContactMethod'][i].id] = false;
		}
		
		$scope.contactMethodsType = {};
		for(var i= 0; i < $rootScope.constants['PreferredContactType'].length; i++){
			$scope.contactMethodsType[$rootScope.constants['PreferredContactType'][i].id] = null;
		}
		
		$scope.remindAssumptionTypes = {};
		for(var i= 0; i < $rootScope.constants['RemindAssumptionType'].length; i++){
			$scope.remindAssumptionTypes[$rootScope.constants['RemindAssumptionType'][i].id] = false;
		}
		
		$scope.remindTherapyExpiryTypes = {};
		for(var i= 0; i < $rootScope.constants['RemindTherapyExpiryType'].length; i++){
			$scope.remindTherapyExpiryTypes[$rootScope.constants['RemindTherapyExpiryType'][i].id] = false;
		}
		
		$scope.initPaginationAndFilterServiceConf();
		
		if (!$scope.isNew()) {
			PatientService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				
				$scope.fieldPermissions = result.fieldPermissions;
				
				$scope.initPermission(result.permissions);
				
				for(var i= 0; i < $scope.detail.preferredContactMethods.length; i++) {
					$scope.contactMethods[$scope.detail.preferredContactMethods[i]] = true;
				}
				
				for(var i = 0; i < $scope.detail.preferredContacts.length; i++) {
					$scope.contactMethodsType[$scope.detail.preferredContacts[i].type] = $scope.detail.preferredContacts[i].preference;
				}
				
				if(angular.isDefined($scope.detail.remindAssumptionTypes)) {
					for(var i= 0; i < $scope.detail.remindAssumptionTypes.length; i++) {
						$scope.remindAssumptionTypes[$scope.detail.remindAssumptionTypes[i]] = true;
					}
				}
				
				if(angular.isDefined($scope.detail.remindTherapyExpiryTypes)) {
					for(var i= 0; i < $scope.detail.remindTherapyExpiryTypes.length; i++) {
						$scope.remindTherapyExpiryTypes[$scope.detail.remindTherapyExpiryTypes[i]] = true;
					}
				}

				$scope.initListUser();
				
				$scope.detail.patientMedicalExaminations = [];
				PatientMedicalExaminationService.get({ patientId : $scope.id }, function(result) {
					$scope.detail.patientMedicalExaminations = result.data;
				}, function(error) {
					$scope.detail.patientMedicalExaminations = {};
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
				PatientService.getDelegate({id : $scope.id, delegateIndex : 'DELEGATE_1'}, function(result) {
					$scope.delegate.delegate1 = result.data;
				}, function(error) {
					$scope.delegate.delegate1 = undefined;
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				})
				
				PatientService.getDelegate({id : $scope.id, delegateIndex : 'DELEGATE_2'}, function(result) {
					$scope.delegate.delegate2 = result.data;
				}, function(error) {
					$scope.delegate.delegate2 = undefined;
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				})
				
				PatientService.getCaregiver({id : $scope.id}, function(result) {
					$scope.caregivers.caregiver1 = result.data;
				}, function(error) {
					$scope.caregivers.caregiver1 = undefined;
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				})
				
				UserServiceConfService.searchByUser({userId: $scope.id}, {}, function(result) {
					if(angular.isDefined(result.data)) {
						var userServices = result.data;
						for(var i = 0; i < userServices.length; i++) {
							var dependingServices = userServices[i].dependingServices;
							if(angular.isDefined(dependingServices) && dependingServices.length > 0) {
								$scope.userServices.push(userServices[i]);
								for(var x = 0; x < dependingServices.length; x++) {
									var uniqueKey = dependingServices[x].uniqueKey;
									UserServiceConfService.getDependingServices({ id: userServices[i].id, uniqueKey: uniqueKey }, function(result) {
										$scope.serviceConfs[uniqueKey] = result.data;
									}, function(error) {
										$scope.userServices = [];
										$scope.serviceConfs = {};
										if(error.status == 404) {
											$state.go('access.not-found');
										}
									});
								}
							}
						}
					}
				}, function(error) {
					$scope.userServices = [];
					$scope.serviceConfs = {};
					if (error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}, function(error) {
				$scope.detail = {};
				$scope.delegate = {
						delegate1: undefined,
						delegate2: undefined
				};
				$scope.caregivers = {
						caregiver1:undefined
				};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});

			$scope.getTherapies();
		} else {
			$scope.detail = {
					departments : []
			};
			
			PermissionService.getAll({ entityType : 'PATIENT' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'PATIENT' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
				EntityCustomPropertyConfService.search({filters :{'DATA_TYPE_NAME':'PATIENT'}}, function(result) {
					if(angular.isDefined(result.data) && result.data.length > 0) {
						$scope.detail.customProperties = [];
						for(var i = 0; i < result.data.length; i++) {
							var customPropertyConf = result.data[i];
							var customProperty = {
									name: customPropertyConf.name,
									code: customPropertyConf.code,
									list: customPropertyConf.list,
									mandatory: customPropertyConf.mandatory,
									type: customPropertyConf.type,
									choices: customPropertyConf.choices
							}
							$scope.detail.customProperties.push(customProperty);
						}
					}
				}, function(error) {
				});
				
				PatientService.searchApprovals({}, function(result) {
					$scope.detail.approvals = result.data || [];
				}, function(error) {
				});
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
		}
		$scope.therapyTemplates = [];
		$scope.loadTherapyTemplates();
	};
	
	$scope.initListUser = function() {
		$scope.listUser = [];
		$scope.permissionUser = {};
		$scope.showListUser = false;
		
		PermissionService.getAll({ entityType : 'PATIENT_USER' }, function(result) {
			$scope.canInsertUser = result.data.INSERT;
			$scope.canExportUser = result.data.EXPORT;
			$scope.initPaginationAndFilterUser();
			$scope.showListUser = true;
		}, function(error) {
			$scope.listUser = [];
			$scope.permissionUser = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		
		$scope.list = $scope.list || [];
		
		if(!angular.isDefined($scope.searchFilters.filters)){
			$scope.searchFilters['filters'] = {};
		}
		
		$scope.searchFilters.filters.HCP_ID = $scope.objectFilters.hcp != undefined ? $scope.objectFilters.hcp.id : undefined;
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		
		$scope.selectedIds = []
		
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}

		PatientService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for(var i=0; i<$scope.list.length; i++) {
				$scope.list[i].selected = $scope.selectedIds.indexOf($scope.list[i].id) >= 0;
			}			
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.searchUser = function(tableStateUser, smCtrlUser) {
		$scope.smCtrlUser = $scope.smCtrlUser || smCtrlUser;
		if (tableStateUser === undefined) {
			if ($scope.smCtrlUser !== undefined) {
				$scope.smCtrlUser.tableState().pagination.start = 0;
				$scope.smCtrlUser.pipe();
			}
			return;
		}
		if (tableStateUser !== undefined) {
			tableStateUser.pagination.number = $scope.paginationUser.size;
			
			$scope.paginationUser.start = tableStateUser.pagination.start;
			if (tableStateUser.sort != null && tableStateUser.sort.predicate !== undefined) {
				$scope.paginationUser.sort = tableStateUser.sort.reverse ? [ "-" + tableStateUser.sort.predicate ] : [ tableStateUser.sort.predicate ];
			}
			if (tableStateUser.pagination.number !== undefined) {
				$scope.paginationUser.size = tableStateUser.pagination.number;
			}
		}
		$scope.listUser = $scope.listUser || [];
		
		$scope.searchFilterUser = {filters: { 'PATIENT_ID' : $scope.id }};
		angular.extend($scope.searchFiltersUser, $scope.paginationUser);
		delete $scope.searchFiltersUser.total;
		
		PatientService.searchAllUser({id: $scope.id}, $scope.searchFiltersUser, function(result) {
			$scope.listUser = result.data;
			$scope.permissionUser = result.permissions;
			$scope.paginationUser.start = result.start;
			$scope.paginationUser.size = result.size;
			$scope.paginationUser.sort = result.sort;
			$scope.paginationUser.total = result.total;
			if (tableStateUser !== undefined) {
				tableStateUser.pagination.numberOfPages = Math.ceil($scope.paginationUser.total / ($scope.paginationUser.size !== undefined && $scope.paginationUser.size != null ? $scope.paginationUser.size : 1));
			}
		}, function(error) {
			$scope.listUser = [];
			$scope.initPaginationAndFilterUser();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			if(allData !== undefined && allData != null && allData == true) {
				$scope.searchFilters.start = 0;
				delete $scope.searchFilters.size;
			} 
			var allRequest = {
					filters : $scope.searchFilters.filters,
					exportTypes : [],
					start : $scope.searchFilters.start,
					size : $scope.searchFilters.size,
					sort : $scope.searchFilters.sort
			};
			$http.post(RESTURL.PATIENT + '/search/export-xls', allRequest, {responseType: 'arraybuffer'}).success(function (response) {
				MessageService.showSuccess('L\'export richiesto \u00e8 in generazione, a breve sar\u00e0 disponibile nella sezione Export', '', 0);
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione xls non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}
	
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.insert();
			} else {
				$scope.update();
			}
		} else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};

	$scope.insert = function() {
		var request = $scope.loadRequest();
		PatientService.insert({}, request, function(result) {
			$scope.detail = result.data;		
			$scope.id = $scope.detail.id;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			if(angular.isDefined($scope.userServices) && $scope.userServices.length > 0) {
				for(var i = 0; i < $scope.userServices.length; i++) {
					UserServiceConfService.update({ id : $scope.userServices[i].id }, $scope.userServices[i], function(result) {
						MessageService.showSuccess('Inserimento completato con successo');
						$state.go("app.patient", {id: $scope.id});
					}, function(error) {
						MessageService.showError('Inserimento non riuscito');
						if (error.status == 404) {
							$state.go('access.not-found');
						} 
					});
				}
			} else {
				MessageService.showSuccess('Inserimento completato con successo');
				$state.go("app.patient", {id: $scope.id});
			}
		}, function(error) {
			if(error.data && error.data.message) {
				MessageService.showError('Inserimento non riuscito: ' + error.data.message);
			}
			else {
				MessageService.showError('Inserimento non riuscito');
			}
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	};
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		PatientService.update({ id : $scope.id }, request, function(result) {
			$scope.detail = result.data;
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			$scope.markDelegate($scope.delegate.delegate1, 'DELEGATE_1');
			$scope.markDelegate($scope.delegate.delegate2, 'DELEGATE_2');
			$scope.markCaregiver($scope.caregivers.caregiver1);
			if(angular.isDefined($scope.userServices) && $scope.userServices.length > 0) {
				for(var i = 0; i < $scope.userServices.length; i++) {
					UserServiceConfService.update({ id : $scope.userServices[i].id }, $scope.userServices[i], function(result) {
						MessageService.showSuccess('Aggiornamento completato con successo');
						if(!$scope.popup){
							$state.go("app.patient", {id: $scope.id});
						}
					}, function(error) {
						MessageService.showError('Aggiornamento non riuscito');
						if (error.status == 404) {
							$state.go('access.not-found');
						} 
					});
				}
			} else {
				MessageService.showSuccess('Aggiornamento completato con successo');
				if(!$scope.popup){
					$state.go("app.patient", {id: $scope.id}, {reload: true});
				}
			}
		}, function(error) {
			if(error.data && error.data.message) {
				MessageService.showError('Aggiornamento non riuscito: ' + error.data.message);
			}
			else {
				MessageService.showError('Aggiornamento non riuscito');
			}
			if (error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	/*
	$scope.activate = function() {
		if(!$scope.detail.email) {
			$scope.detailActivate.emailNotification = false;
		}
		if(!$scope.detail.cellPhoneNumber){
			$scope.detailActivate.smsNotification = false;
		}
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/common/user-activate.html',
			size: 'md',
		});
	}
	*/
	$scope.confirmActivate = function(detailActivate) {
		//$scope.errors = $scope.validateActivateNotification();
		
		PatientService.activateNotification({ id : $scope.id }, detailActivate, function(result) {
			MessageService.showSuccess('Attivazione completata con successo');
			$state.reload();
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			}
			else {
				if(error.data && error.data.message) {
					MessageService.showError('Attivazione non riuscita: ' + error.data.message);
				}
				else {
					MessageService.showError('Attivazione non riuscita');
				}
			}
		});
		
	}
	
	/*
	$scope.validateActivateNotification = function() {
		var errors = {};
		var form = {
				formProperties : [
					{ id:'detailActivate.emailNotification', value: $scope.detailActivate.emailNotification, type:'boolean', required: true },
					{ id:'detailActivate.smsNotification', value: $scope.detailActivate.smsNotification, type:'boolean', required: true }
					]
		};
		if($scope.canInvite){
			form.formProperties.push({ id:'detailActivate.invitedUser', value: $scope.detailActivate.invitedUser, type:'boolean', required: true });
		}
		errors = FormUtilService.validateForm(form);
		return errors;
	}
	*/
	$scope.suspend = function() {
		PatientService.suspend({ id : $scope.id }, undefined, function(result) {
			MessageService.showSuccess('Sospensione completata con successo');
			$state.reload();
		}, function(error) {
			MessageService.showError('Sospensione non riuscita');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.terminate = function() {
		PatientService.terminate({ id : $scope.id }, undefined, function(result) {
			MessageService.showSuccess('Uscita dal programma completata con successo');
			$state.reload();
		}, function(error) {
			MessageService.showError('Uscita dal programma non riuscita');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
			function(confirm) {
				PatientService.delete({ id : id }, {}, function(result) {
					$scope.initList();
					MessageService.showSuccess('Cancellazione completata con successo');
					$state.go("app.patients");
				}, function(error) {
					MessageService.showError('Cancellazione non riuscita');
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
		);
	}
	
	$scope.deleteUser = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					PatientUserService.delete({ id : id }, {}, function(result) {
						$scope.initList();
						MessageService.showSuccess('Cancellazione completata con successo');
						$scope.initListUser();
					}, function(error) {
						MessageService.showError('Cancellazione non riuscita');
						if(error.status == 404) {
							$state.go('access.not-found');
						}
					});
				}
		);
	}
	
	$scope.markDelegate = function(delegate, delegateIndex) {
		if (delegate != undefined && delegate.id != undefined) {
			PatientService.markDelegate({ id: $scope.id, delegateIndex: delegateIndex }, delegate.id, function(result) {
			}, function(error) {
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
				}
			});
		} else {
			PatientService.deleteDelegate({ id: $scope.id, delegateIndex: delegateIndex }, function() {
			}, function(error) {
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');	
				}
			});
		}
	}
	
	$scope.markCaregiver = function(caregiver) {
		if (caregiver != undefined && caregiver.id != undefined) {
			PatientService.markCaregiver({ id: $scope.id }, caregiver.id, function(result) {
			}, function(error) {
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
				}
			});
		} else {
			PatientService.deleteCaregiver({ id: $scope.id}, function() {
			}, function(error) {
				if (error.status == 404) {
					$state.go('access.not-found');
				} else {
					MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');	
				}
			});
		}
	}
	
	$scope.invite = function(id) {
		var modalInstance = MessageService.activationConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.inviteUser(id);
				}
		);
	}
	
	$scope.impersonate = function(nickName){
		var modalInstance = MessageService.impersonateConfirm();
		modalInstance.result.then(
				function(confirm) {				
					$scope.impersonateUser(nickName);
				}
		);
	}
	
	$scope.validate = function() {
		var errors = {};
		$scope.detail = $scope.loadRequest();
		var form = {
				formProperties : [
					/*{ id:'detail.role' , value : $scope.detail.role, required:true, type: 'string'},*/
					{ id:'detail.lastName', value: $scope.detail.lastName, required:true, type:'string' },
					{ id:'detail.sex', value: $scope.detail.sex, required:true, type:'string' },
					{ id:'detail.firstName', value: $scope.detail.firstName, required:true, type:'string' },
					{ id:'detail.faxNumber', value : $scope.detail.faxNumber, type: 'string', pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.phoneNumber', value: $scope.detail.phoneNumber, type:'string' , pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.cellPhoneNumber', value: $scope.detail.cellPhoneNumber, type:'string' , pattern: REGEXP.PHONE_NUMBER },
					{ id:'detail.fiscalCode', value: $scope.detail.fiscalCode, type:'string',pattern: REGEXP.FISCAL_CODE},
					{ id:'detail.email' , value: $scope.detail.email, type:'email', pattern:REGEXP.EMAIL},
					{ id:'detail.privacyEnrollment', value: $scope.detail.privacyEnrollment, type:'string', required:true},
					//{ id:'detail.hcpId' , value: $scope.detail.hcpId, type:'String', required:true},
					{ id:'detail.enrollmentDate',  value: $scope.detail.enrollmentDate, type:'string', required:($scope.isPatient ? false : true)}
					]
		};
		errors = FormUtilService.validateForm(form);
		if($scope.detail['customProperties'] != undefined && $scope.detail['customProperties'] != null){
			for(var i = 0; i < $scope.detail.customProperties.length; i++){
				var prop = $scope.detail.customProperties[i];
				if(prop.mandatory == true && (prop.values == '' || prop.values == undefined || prop.values == null ) ){
					errors['detail.customProperties_'+i] = 'Campo obbligatorio';
				}
			}
		}	
		if ($scope.delegate.delegate1 != undefined && $scope.delegate.delegate2 != undefined && $scope.delegate.delegate1.id == $scope.delegate.delegate2.id) {
			errors['delegate.delegate1'] = 'I due delegati non possono essere la stessa persona';
			errors['delegate.delegate2'] = 'I due delegati non possono essere la stessa persona';
		}
		if($scope.detail['preferredTimeSlots'] != undefined && $scope.detail['preferredTimeSlots'] != null && $scope.detail['preferredTimeSlots'].length > 0) {
			for(var i = 0; i < $scope.detail.preferredTimeSlots.length; i++) {
				var val = $scope.detail.preferredTimeSlots[i];
				if(val.from === undefined && val.to !== undefined) {
					errors['detail.preferredTimeSlots_'+i+'_from'] = 'Campo obbligatorio';
				} else if(val.to === undefined && val.from !== undefined){
					errors['detail.preferredTimeSlots_'+i+'_to'] = 'Campo obbligatorio';
				} else if(val.to === undefined && val.from === undefined){
					errors['detail.preferredTimeSlots_'+i+'_to'] = 'Campo obbligatorio';
					errors['detail.preferredTimeSlots_'+i+'_from'] = 'Campo obbligatorio';
				} else if(val.to <= val.from){
					errors['detail.preferredTimeSlots_'+i+'_to'] = 'Fascia oraria non corretta';
					errors['detail.preferredTimeSlots_'+i+'_from'] = 'Fascia oraria non corretta';
				}
			}
		}
		console.log(errors);
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.objectFilters = $scope.objectFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		$scope.inviteFilters = $scope.inviteFilters || {};
	}
	
	$scope.initPaginationAndFilterUser = function() {
		$scope.paginationUser = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFiltersUser = $scope.searchFiltersUser || {};
		$scope.initialSearchFiltersUser = angular.copy($scope.searchFiltersUser);
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			$scope.canInvite = permissions.INVITE; 
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
			$scope.canInvite = false;
		}
		$scope.permissions = permissions;
	}
	
	$scope.clearFilters = function() {
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
	}
	
	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.patients");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}
	
	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.patient", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	};
	
	$scope.selectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = true;
		}
	}

	$scope.unselectAll = function() {
		for(var i=0; i<$scope.list.length; i++) {
			$scope.list[i].selected = undefined;
		}
	}

	$scope.confirmSelection = function() {
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		for(var i=0; i<$scope.list.length; i++) {
			if ($scope.list[i].selected) {
				if ($scope.selectedIds.indexOf($scope.list[i].id) < 0) {
					$scope.selected.push($scope.list[i]);
					$scope.selectedIds.push($scope.list[i].id);
				} else {
					// Già presente nella lista
				}
			} else {
				if($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.selected.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
					$scope.selectedIds.splice($scope.selectedIds.indexOf($scope.list[i].id), 1);
				}
			}
		}
		if ($scope.modal) {
			$scope.modal.close();
		}
	}

	$scope.undoSelection = function() {
		$scope.modal.dismiss();
	}

	$scope.haveOtherData = function() {
		var res = angular.isDefined($scope.detail.customProperties) && $scope.detail.customProperties.length > 0;
		if(!res) {
			if(angular.isDefined($scope.userServices) && $scope.userServices.length > 0) {
				for(var i = 0; i < $scope.userServices.length; i++) {
					if(angular.isDefined($scope.userServices[i].dependingServices) && $scope.userServices[i].dependingServices.length > 0) {
						res = true;
						break;
					} 
				}
			}
		}
		return res;
	}

	$scope.loadRequest = function() {
		if ($scope.detail['preferredTimeSlots'] == undefined || $scope.detail['preferredTimeSlots'] == null){
			$scope.detail['preferredTimeSlots'] = [];
		}
		$scope.detail['preferredContactMethods'] = [];
		for(var key in $scope.contactMethods) {
			if($scope.contactMethods[key] == true) {
				$scope.detail['preferredContactMethods'].push(key);
			}
		}
		$scope.detail['preferredContacts'] = [];
		for(var key in $scope.contactMethodsType) {
			$scope.detail['preferredContacts'].push({type: key, preference: $scope.contactMethodsType[key]});
		}
		$scope.detail['remindAssumptionTypes'] = [];
		for(var key in $scope.remindAssumptionTypes) {
			if($scope.remindAssumptionTypes[key] == true) {
				$scope.detail['remindAssumptionTypes'].push(key);
			}
		}
		$scope.detail['remindTherapyExpiryTypes'] = [];
		for(var key in $scope.remindTherapyExpiryTypes) {
			if($scope.remindTherapyExpiryTypes[key] == true) {
				$scope.detail['remindTherapyExpiryTypes'].push(key);
			}
		}
		if ($scope.detail['diseases'] != undefined && $scope.detail['diseases'] != null && $scope.detail.diseases.length != 0) {
			$scope.detail.diseaseIds = [];
			for(var i = 0; i < $scope.detail.diseases.length; i++){
				$scope.detail.diseaseIds.push($scope.detail.diseases[i].id)
			}
		}
		if($scope.detail.hcp !== undefined) {
			$scope.detail.hcpId = $scope.detail.hcp.id;
		}
		else{
			$scope.detail.hcpId = undefined;
		}
		
		if($scope.detail.preferredInternalStaffUser !== undefined) {
			$scope.detail.preferredInternalStaffUserId = $scope.detail.preferredInternalStaffUser.id;
		} else {
			$scope.detail.preferredInternalStaffUserId = undefined;
		}
		if ($scope.detail.delegateHcps != undefined && $scope.detail.delegateHcps != null && $scope.detail.delegateHcps.length != 0) {
			$scope.detail.delegateHcpsIds = [];
			for(var i = 0; i < $scope.detail.delegateHcps.length; i++){
				$scope.detail.delegateHcpsIds.push($scope.detail.delegateHcps[i].id)
			}
		}
		
		return angular.copy($scope.detail);
	}
	
	$scope.closeActivate = function(){
		$scope.$modalInstance.close();
	}
	
	/***************************************************THERAPY********************************************************************/
	$scope.loadTherapyTemplates = function() {
		TherapyService.search({},{},function(result) {
			$scope.therapyTemplates = result.data;
		});
	}

	$scope.addNewTherapy = function() {
		$scope.isTherapyEditable[$scope.patientTherapies.length-1]=true;
		for(var i=0; i<$scope.patientTherapies.length-1; i++) {
			$scope.isTherapyEditable[i]=false;
		}
	}

	$scope.getTherapies = function() {
		
		DeviceTypeService.getAll({},function(result) {
			$scope.deviceTypes = result.data;
		}, function(error) {
			
		});
		
		$scope.isTherapyEditable = [];
		$scope.canInsertTherapy = $rootScope.isInternalStaff() && !$rootScope.isPersonified();
		$scope.canDeleteTherapy = true;
		$scope.canModifyTherapy = false;
		$scope.patientTherapies = [];
		var therapySearchFilters = {
				filters: { 'PATIENT_ID' : $scope.id }
		}
		PatientTherapyService.search({}, therapySearchFilters, function(result) {
			for(var i = 0; i < result.data.length; i++){
				$scope.patientTherapies[i] = {
						"id":result.data[i].id,
						"patient":$scope.id,
						"therapy":result.data[i]
				};
				$scope.patientTherapies[i].therapy.initialStart = angular.copy($scope.patientTherapies[i].therapy.start);
			
				if(!$scope.canInsertTherapy || ($scope.patientTherapies[i].therapy.patient.status == 'ACTIVE' && $scope.patientTherapies[i].therapy.start != null)) {
					$scope.isTherapyEditable[i] = false;
				} else {
					$scope.isTherapyEditable[i] = true; 
				}
			}			
		});
	}

	$scope.undoTherapy = function(index) {
		if($scope.patientTherapies[index].id==undefined || $scope.patientTherapies[index].id.startsWith("id-")) {
			//toglie dalla lista se stavi aggiungendo una terapia
			$scope.patientTherapies.splice(index,1);
		}
		else {
			PatientTherapyService.get({id : $scope.patientTherapies[index].id}, function(result) {
				$scope.patientTherapies[index].therapy = result.data;
				$scope.patientTherapies[index].therapy.initialStart = angular.copy($scope.patientTherapies[index].therapy.start);
			});
		}
		$scope.newTherapy = undefined;
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.insertTherapy = function(index) {
		var request = $scope.loadTherapyCreateRequest($scope.patientTherapies[index]);
		delete request.duration;
		delete request.durationUnit;
		delete request.expectedEnd;
		PatientTherapyService.insert({}, request ,function(result) {
			$scope.getTherapies();
			MessageService.showSuccess('Inserimento completato con successo');
			$scope.checkServices();
			
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di inserimento terapia';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}

	$scope.saveTherapy = function(index) {
		if($scope.patientTherapies[index]==undefined) return;
		if($scope.patientTherapies[index].id.indexOf("id") != -1) 
			$scope.insertTherapy(index);
		else 
			$scope.updateTherapy(index);
	}

	$scope.updateTherapy = function(index) {
		if($scope.patientTherapies[index]==undefined) return;
		var request = $scope.loadTherapyRequest($scope.patientTherapies[index]);
		delete request.duration;
		delete request.durationUnit;
		delete request.expectedEnd;
		PatientTherapyService.update({ id : $scope.patientTherapies[index].id }, request ,function(result) {
			$scope.getTherapies();
			MessageService.showSuccess('Aggiornamento completato con successo');
			if($scope.detail.status=='WAITING')
				$scope.checkServices();
		}, function(error) {
			$scope.errors = error;
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				var title = 'Errore in fase di aggiornamento terapia';
				var message = 'Alcuni dati inseriti non sono corretti';
				if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
					message = $scope.errors.data.message;
				}
				MessageService.showError(title, message);
			}
		});
	}

	$scope.loadTherapyCreateRequest = function(therapy) {
		var r = $scope.loadTherapyRequest(therapy);
		r['patientId'] = $scope.id;
		r['therapyId'] = therapy.therapy.id;
		return r;
	}
	
	$scope.resetExpectedEnd = function(therapy) {
		therapy.expectedEnd = undefined;
	}
	
	$scope.loadTherapyRequest = function(therapy) {
		
		var r = {
				start : therapy.therapy.start,
				end : therapy.therapy.end,
				notes : therapy.therapy.notes,
				internalNotes : therapy.therapy.internalNotes,
				duration : therapy.therapy.duration!=undefined ? therapy.therapy.duration.duration : undefined,
				durationUnit : therapy.therapy.duration!=undefined ? therapy.therapy.duration.durationUnit : undefined,
				expectedEnd : therapy.therapy.expectedEnd,
				drugs: [],
		}

		for(var d=0;d<therapy.therapy.drugs.length; d++) {
			console.log(therapy.therapy.drugs[d]);
			r.drugs.push({
				id : therapy.therapy.drugs[d].id,
				expectedDosage : therapy.therapy.drugs[d].expectedDosage,
				expectedDosageUnit : therapy.therapy.drugs[d].expectedDosageUnit,
				expectedDosageMethod : therapy.therapy.drugs[d].expectedDosageMethod,
				assumptionHours: therapy.therapy.drugs[d].assumptionHours!=undefined ? Object.values(therapy.therapy.drugs[d].assumptionHours) : undefined,
				assumptionFrequency : therapy.therapy.drugs[d].assumptionFrequency,
				assumptionFrequencyUnit : therapy.therapy.drugs[d].assumptionFrequencyUnit,
				dailyAssumptionNumber: therapy.therapy.drugs[d].dailyAssumptionNumber,
				duration: therapy.therapy.drugs[d].duration,
				durationUnit: therapy.therapy.drugs[d].durationUnit,
				deviceTypeId: therapy.therapy.drugs[d].selectedDeviceType !== undefined ? therapy.therapy.drugs[d].selectedDeviceType.id : undefined,
				smsAssumptionsRemind: therapy.therapy.drugs[d].smsAssumptionsRemind,
				smsAssumptionsRemindMessage: therapy.therapy.drugs[d].smsAssumptionsRemindMessage,
				createAssumptions: therapy.therapy.drugs[d].createAssumptions,
				createAssumptionsMaxTime: therapy.therapy.drugs[d].createAssumptionsMaxTime,
				createAssumptionsMaxTimeUnit: therapy.therapy.drugs[d].createAssumptionsMaxTimeUnit,
				createAssumptionsMaxDailyNumber: therapy.therapy.drugs[d].createAssumptionsMaxDailyNumber,
				deviceLotNumber: therapy.therapy.drugs[d].deviceLotNumber,
				deviceSerialNumber: therapy.therapy.drugs[d].deviceSerialNumber,
				lastDeviceConsignation: therapy.therapy.drugs[d].lastDeviceConsignation,
				toleranceDays: therapy.therapy.drugs[d].toleranceDays,
				suggestedDateControlStatus: therapy.therapy.drugs[d].suggestedDateControlStatus
			});
		} 
		return r;
	}
	/***************************************************THERAPY*************************************************************************/

	/***************************************************SERVICE CONF********************************************************************/
	$scope.checkServices = function() {
		UserServiceConfService.checkServices({ userId : $scope.id }, undefined, function(result) {
			MessageService.showSuccess('Aggiornamento servizi completato con successo');
			$state.reload();
		}, function(error) {
			if (error.status == 404) {
				$state.go('access.not-found');
			} else {
				MessageService.showError('Errore in fase di salvataggio', error.message !== undefined ? error.message : 'Errore imprevisto');
			}
		});
	}
	
	
	
	$scope.initPaginationAndFilterServiceConf = function() {
		$scope.paginationServiceConf = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFiltersServiceConf = $scope.searchFiltersServiceConf || { filters: { USER_ID: $scope.id } };
		$scope.searchFiltersServiceConf = angular.copy($scope.searchFiltersServiceConf);
	}
	
	$scope.loadServiceConfRequest = function(serviceConf) {
		return {
			startDelay : serviceConf.startDelay,
			startDelayUnit : serviceConf.startDelayUnit,
			startEvent: serviceConf.startEvent
		}
	}
	/***************************************************SERVICE CONF********************************************************************/
	
	
	/***************************************************ACTIVITY CONF*******************************************************************/
	$scope.startService = function(service) {
		UserServiceConfService.start({ id : service.id }, { start: new Date(), startDelay: service.startDelay, startDelayUnit: service.startDelayUnit },
			function(result) {
				MessageService.showSuccess('Avvio servizio completato con successo');
				var businessKey = result.data;
				if(businessKey !== undefined) {
					WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
						function(result) {
							if(result.data !== undefined) {
								if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
									$state.go('app.task', { 'id': result.data.id });
								}
							}
							else {
								if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
									$state.go('app.my-tasks');
								}
							}
						},
						function(error) {
							if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
								$state.go('app.my-tasks');
							}
						}
					)
				}
				else {
					if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
						$state.go('app.my-tasks');
					}
				}
			}, function(error) {
				$scope.errors = error;
				if (error.status == 404) {
					$state.go('access.not-found');
				}
				else {
					var title = 'Errore in fase di avvio servizio';
					var message = 'Alcuni dati inseriti non sono corretti';
					if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
						message = $scope.errors.data.message;
					}
					MessageService.showError(title, message);
				}
			}
		);
	}
	
	$scope.startMasterActivity = function(masterActivity) {
		UserServiceConfService.startUserMasterActivityManually({ userId : $scope.id, masterActivityId : masterActivity.id }, 
				function(result) {
				MessageService.showSuccess('Avvio attivit\u00E0 completato con successo');
				var businessKey = result.data;
				if(businessKey !== undefined) {
					WorkflowService.nextMyTasks({ businessKey: businessKey }, {},
						function(result) {
							if(result.data !== undefined) {
								if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
									$state.go('app.task', { 'id': result.data.id });
								}
							}
							else {
								if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
									$state.go('app.my-tasks');
								}
							}
						},
						function(error) {
							if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
								$state.go('app.my-tasks');
							}
						}
					)
				}
				else {
					if($rootScope.isInternalStaff() || $rootScope.isSupplier()) {
						$state.go('app.my-tasks');
					}
				}
			}, function(error) {
				$scope.errors = error;
				if (error.status == 404) {
					$state.go('access.not-found');
				}
				else {
					var title = 'Errore in fase di avvio attivit\u00E0';
					var message = 'Alcuni dati inseriti non sono corretti';
					if($scope.errors && $scope.errors.data && $scope.errors.data.message) {
						message = $scope.errors.data.message;
					}
					MessageService.showError(title, message);
				}
			}
		);
	}
	
	/***************************************************ACTIVITY CONF*******************************************************************/
	
	$scope.addSlot = function(){
		
		if($scope.detail['preferredTimeSlots'] == null || $scope.detail['preferredTimeSlots'] == undefined ){
			$scope.detail['preferredTimeSlots'] = [];
		}
		$scope.detail['preferredTimeSlots'].push({from: undefined, to: undefined});
		
	};
	
	$scope.removeSlot = function(indexToRemove){
	
		if($scope.detail['preferredTimeSlots'] == null || $scope.detail['preferredTimeSlots'] == undefined ){
			$scope.detail['preferredTimeSlots'] = [];
		}
		
		var toKeep = [];
		for(var i = 0; i < $scope.detail['preferredTimeSlots'].length; i++){
			if(i != indexToRemove ){
				toKeep.push($scope.detail['preferredTimeSlots'][i]);
			}
		}
		
		$scope.detail['preferredTimeSlots'] = toKeep;
		
	};
	
	$scope.inviteAll = function() {
		$scope.countPatientToInvite();
		$scope.$modalInstance = $modal.open({
			scope: $scope,
			templateUrl: 'tpl/app/user/invite-all-users.html',
			size: 'md',
		});
	};
	
	$scope.confirmInvite = function(){
		PatientService.inviteActivationAll({}, $scope.searchFilters, function(result) {
			MessageService.showSuccess('Invito registrazione paziente creato con successo');
		}, function (error){
			MessageService.showError('Invito registrazione paziente non creato');
		});
		$scope.$modalInstance.close();
	};
	
	$scope.undoInvite = function(){
		$scope.$modalInstance.dismiss('cancel');
	}
	
	$scope.countPatientToInvite = function() {
		$scope.usersToInvite = 0;
		PatientService.countSearchInvite({}, $scope.searchFilters, function(result) {
			$scope.usersToInvite = result.data;
		});
	}
	
	/*************PATIENT RELATIVES ****************/
	
	$scope.editRelative = function(index){
		//se index == undefined sto chiamando la funzione per aggiungere un nuovo relative in lista, altrimenti ho un index 
		if(angular.isDefined(index) && angular.isDefined($scope.detail.relatives)) {
			if(index > $scope.detail.relatives.lenght) {
				MessageService.showError('Errore in apertura dettaglio. Indice ' + index + ' non valido');
			}
		}
		$scope.modalInstance = $modal.open({
			templateUrl: 'tpl/app/directive/user/patient/relatives/patient-relative-detail.html',
			scope: $scope,
			size: 'lg',
			controller: function ($scope, $modalInstance, $state) {
				$scope.isNewRelative = !angular.isDefined(index);
				$scope.relativeDetail = $scope.isNewRelative ? {} : angular.copy($scope.detail.relatives[index]);
				$scope.relativeDetail.idList = [];
				if(angular.isDefined($scope.detail.id)) {
					$scope.relativeDetail.idList.push($scope.detail.id);
				}
				$scope.relativeDetail.patient = undefined;
				if(!$scope.isNewRelative) {
					PatientService.get({ id : $scope.relativeDetail.id }, function(result) {
							$scope.relativeDetail.patient = result.data;
							$scope.fieldPermissions = result.fieldPermissions;
							$scope.initPermission(result.permissions);
					});
				}
				
				if(angular.isDefined($scope.detail.relatives)) {
					for(var i = 0; i<$scope.detail.relatives.length; i++) {
						$scope.relativeDetail.idList.push($scope.detail.relatives[i].id);
					}	
				}
				
				$scope.saveRelative = function() {
					if(!angular.isDefined($scope.relativeDetail.patient)) {
						MessageService.showError('Errore in fase di salvataggio','E\' obbligatorio scegliere un paziente');
						return;
					}
					
					if(!angular.isDefined($scope.relativeDetail.hasSameAddress)) {
						MessageService.showError('Errore in fase di salvataggio','E\' obbligatorio segnalare se stesso indirizzo');
						return;
					}

					if($scope.isNewRelative && angular.isDefined($scope.detail.relatives)) {
						for(var i=0; i<$scope.detail.relatives.length; i++) {
							if($scope.detail.relatives[i] != undefined && $scope.detail.relatives[i].id != undefined && $scope.detail.relatives[i].id == $scope.relativeDetail.patient.id) {
								MessageService.showError('Errore in fase di salvataggio','Il paziente scelto \u00E8 gi\u00E0 presente in lista');
								return;
							}
						}
					}

					if($scope.isNewRelative) {
						$scope.relativeDetail.id = $scope.relativeDetail.patient.id;
						$scope.relativeDetail.firstName = $scope.relativeDetail.patient.firstName;
						$scope.relativeDetail.lastName = $scope.relativeDetail.patient.lastName;
						if(!angular.isDefined($scope.detail.relatives)) {
							$scope.detail.relatives = [];
						}
						$scope.detail.relatives.push($scope.relativeDetail);
					} else {
						$scope.detail.relatives[index].hasSameAddress = $scope.relativeDetail.hasSameAddress;	
					}
					$scope.close();
				};
				
				$scope.undoRelative = function () {
					if(!$scope.isNewRelative) {
						 $scope.relativeDetail.hasSameAddress = $scope.detail.relatives[index].hasSameAddress;
					} else {
						$scope.relativeDetail = {};
						$scope.relativeDetail.patient = undefined;
					}
				};
				
//				$scope.checkHasSameAddress = function(){
//					$scope.warnings = [];
//					if(!$scope.relativeDetail.hasSameAddressWarning){
//							$scope.warnings['hasSameAddress'] = 'Gli indirizzi sono diversi!'; 
//					}
//				};
				
				$scope.close = function(){
					$modalInstance.dismiss('cancel');
				}
			}
		})
	};
	
	$scope.removeRelative = function(index){
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
				function(confirm) {
					$scope.detail.relatives.splice(index, 1);
				}
		);
	};
	
	$scope.init();
});