'use strict'

app.controller('PharmacoVigilanceEventController', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		PharmacoVigilanceEventService,
		PatientTherapyService,
		FormUtilService,
		PermissionService,
		MessageService,
		REGEXP,
		RESTURL,
		$filter,
		PatientService,
		$sce,
		$modal,
		TerritoryService,
		SMART_TABLE_CONFIG,
		$window) {

	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		
		$scope.errors = {};
		$scope.selects = {};
		
		$scope.fillableTemplates = {};
		$scope.blankTemplates = {};

		for(var i= 0; i < $rootScope.constants['PharmacoVigilanceEventReportType'].length; i++){
			if($rootScope.constants['PharmacoVigilanceEventReportType'][i].active && $rootScope.constants['PharmacoVigilanceEventReportType'][i].properties['formTemplateFileName']!=undefined) {
				$scope.fillableTemplates[$rootScope.constants['PharmacoVigilanceEventReportType'][i].id]=$rootScope.constants['PharmacoVigilanceEventReportType'][i];
			}
			if($rootScope.constants['PharmacoVigilanceEventReportType'][i].active && $rootScope.constants['PharmacoVigilanceEventReportType'][i].properties['formFileName']!=undefined) {
				$scope.blankTemplates[$rootScope.constants['PharmacoVigilanceEventReportType'][i].id]=$rootScope.constants['PharmacoVigilanceEventReportType'][i];
			}
		}
		
		if(angular.isDefined($scope.id)) {
			$scope.initDetails();
		} else {
			$scope.initList();
		}
	};
	
	$scope.initList = function() {
		$scope.showList = false;
		$scope.selectedRowId = undefined;
		$scope.selectedFillableTemplatesList = {};
		$scope.selectedBlankTemplatesList = {};
		
		PermissionService.getAll({ entityType : 'PHARMACO_VIGILANCE_EVENT' }, function(result) {
			$scope.initPermission(result.data);
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.highlight = function(prop) {
		if($rootScope.constants['PharmacoVigilanceEventField']!=undefined) {
			for(var i=0; i<$rootScope.constants['PharmacoVigilanceEventField'].length; i++) {
				if($rootScope.constants['PharmacoVigilanceEventField'][i].id==prop) 
					return true;
			}
			return false;
		}
		else 
			return false;
		
	}
	
	$scope.initDetails = function() {
		$scope.enableWatches = 0;
		$scope.detail = {};
		$scope.reportTypes = {};
		$scope.fieldPermissions = {};
		$scope.productWatches = [];
		$scope.initPaginationAndFilterFollowUps();
		
		$scope.selectedFillableTemplates = undefined;
		$scope.selectedBlankTemplates = undefined;
		
		$scope.detail.associatedDrugs = [];
		$scope.detail.principalDrugs = [];
		
		TerritoryService.searchRegions({},{ filters: {}, start: 0, sort: ['name']}, function(result) {
			$scope.regions = result.data;
		}, function(error) {
			MessageService.showErorr('Errore','Problema nel recupero delle regioni');
		});
		for(var i= 0; i < $rootScope.constants['PharmacoVigilanceEventReportType'].length; i++){
			$scope.reportTypes[$rootScope.constants['PharmacoVigilanceEventReportType'][i].id] = false;	
		}
		
		if (!$scope.isNew()) {
			$scope.enableWatches = 0;
			PharmacoVigilanceEventService.get({ id : $scope.id }, function(result) {
				$scope.detail = result.data;
				//$scope.detail.reporterType = result.data.reporter.type;
				$scope.fieldPermissions = result.fieldPermissions;
				$scope.initPermission(result.permissions);
			
				var therapySearchFilters = {
					filters: { 'PATIENT_ID' : $scope.detail.patient.id }
				}

				$scope.searchFollowUpEvents();
				if($scope.detail.reportTypes!=undefined) {
					for(var r=0; r<$scope.detail.reportTypes.length; r++) {
						$scope.reportTypes[$scope.detail.reportTypes[r]]=true;
					}
				}
				if($scope.detail.patient.id!=undefined) {
					PatientService.get({id: $scope.detail.patient.id},function(result) {
						$scope.detail.patient.diseases = result.data.diseases;
						
						PatientTherapyService.search({}, therapySearchFilters, function(result) {
							$scope.detail.patient.therapies = result.data;
							if($scope.detail.patient.therapies!==undefined) {
								for(var j=0; j<$scope.detail.products.length; j++) {
									
									for(var i=0; i<$scope.detail.patient.therapies.length; i++) {
										if($scope.detail.patient.therapies[i].id == $scope.detail.products[j].therapyId) {
											$scope.detail.products[j].therapy = $scope.detail.patient.therapies[i];
											$scope.onChangeTherapyIdFn($scope.detail.products[j]);
											break;
										}
									}
								}
							}
							$timeout( function() { 
								
								$scope.enableWatches = 1;
								
							});
						});
					});
				}
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		} else {
			var today = new Date();
			$scope.detail = {
				products: ($scope.parentEvent!=undefined) ? angular.copy($scope.parentEvent.products) : [],
				eventDate: today.getTime(),
				status: 'EFFECTIVE',
				eventType: ($scope.parentEvent!=undefined) ? 'FOLLOW_UP' : 'EVENT',
				code : ($scope.parentEvent!=undefined) ? $scope.generateEventCode($scope.parentEvent.eventDate,'FOLLOW_UP',$scope.parentEvent.followUpEvents.length) : $scope.generateEventCode(today.getTime(),'EVENT',0),
				parentEvent: $scope.parentEvent,
				patient : ($scope.parentEvent!=undefined) ? $scope.parentEvent.patient : undefined,
				reporter : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporter : undefined,	
				reporterName : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterName : undefined,
				reporterEmail : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterEmail : undefined,
				reporterPhone : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterPhone : undefined,
				reporterAddress : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterAddress : undefined,
				reporterAsl : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterAsl : undefined,
				reporterCounty : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterCounty : undefined,
				reporterQualification : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterQualification : undefined,
				reporterType : ($scope.parentEvent!=undefined) ? $scope.parentEvent.reporterType : undefined,
				ethnicGroup:  ($scope.parentEvent!=undefined) ? $scope.parentEvent.ethnicGroup : undefined,
				weight: ($scope.parentEvent!=undefined) ? $scope.parentEvent.weight : undefined,
				height: ($scope.parentEvent!=undefined) ? $scope.parentEvent.height : undefined,
				lastMenstruationDate: ($scope.parentEvent!=undefined) ? $scope.parentEvent.lastMenstruationDate : undefined,
				pregnancyType: ($scope.parentEvent!=undefined) ? $scope.parentEvent.pregnancyType : undefined,
				breastFeeding: ($scope.parentEvent!=undefined) ? $scope.parentEvent.breastFeeding : undefined,
				examinations: ($scope.parentEvent!=undefined) ? $scope.parentEvent.examinations : undefined,
				monitoringTitle: ($scope.parentEvent!=undefined) ? $scope.parentEvent.monitoringTitle : undefined,
				monitoringNumber: ($scope.parentEvent!=undefined) ? $scope.parentEvent.monitoringNumber : undefined,
				monitoringType: ($scope.parentEvent!=undefined) ? $scope.parentEvent.monitoringType : undefined,
				eventScope: ($scope.parentEvent!=undefined) ? $scope.parentEvent.eventScope : undefined,
				eventReason: ($scope.parentEvent!=undefined) ? $scope.parentEvent.eventReason : undefined,
				eventSeriousness: ($scope.parentEvent!=undefined) ? $scope.parentEvent.eventSeriousness : undefined,
				eventEndingDate: ($scope.parentEvent!=undefined) ? $scope.parentEvent.eventEndingDate : undefined,
				eventEnding: ($scope.parentEvent!=undefined) ? $scope.parentEvent.eventEnding : undefined,
				deathReason: ($scope.parentEvent!=undefined) ? $scope.parentEvent.deathReason : undefined,
				usageOfNaturalProducts: ($scope.parentEvent!=undefined) ? $scope.parentEvent.usageOfNaturalProducts : undefined,
				otherConditions: ($scope.parentEvent!=undefined) ? $scope.parentEvent.otherConditions : undefined,
				otherInformations: ($scope.parentEvent!=undefined) ? $scope.parentEvent.otherInformations : undefined,			
			};
			//auto valorizzazione dei campi dati relativi al pazient ese sto chiamando la direttiva dal dettaglio paziente
			if(angular.isDefined($scope.patientId)){
				
				var therapySearchFilters = {
						filters: { 'PATIENT_ID' : $scope.patientId }
				}
				
				PatientService.get({ id : $scope.patientId }, function(result) {
					$scope.detail.patient = result.data;
					
					PatientTherapyService.search({}, therapySearchFilters, function(result) {
						$scope.detail.patient.therapies = result.data;
						$scope.detail['products'] = [];
						$scope.addPrincipalDrug();
					});
				}, function(error) {
					$scope.detail = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
			
			var reportTypeKeys = Object.keys($scope.reportTypes);
			if(reportTypeKeys.length==1) {
				$scope.reportTypes[reportTypeKeys[0]]=true;
			}
			
			$scope.enableWatches = 1;
			PermissionService.getAll({ entityType : 'PHARMACO_VIGILANCE_EVENT' }, function(result) {
				$scope.initPermission(result.data);
				
				PermissionService.getAllFields({ entityType : 'PHARMACO_VIGILANCE_EVENT' }, function(result) {
					$scope.fieldPermissions = result.data;
				}, function(error) {
					$scope.detail = {};
					$scope.fieldPermissions = {};
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
				
			}, function(error) {
				$scope.detail = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
		}
	};
	
	$scope.hasFillable = function(id) {
		return $scope.selectedFillableTemplatesList!=undefined && $scope.selectedFillableTemplatesList[id]!=undefined && Object.keys($scope.selectedFillableTemplatesList[id]).length>0
	}
	
	$scope.hasBlank = function(id) {
		return $scope.selectedBlankTemplatesList!=undefined && $scope.selectedBlankTemplatesList[id]!=undefined && Object.keys($scope.selectedBlankTemplatesList[id]).length>0
	}
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
		
		if(!angular.isDefined($scope.searchFilters.filters)){
			$scope.searchFilters['filters'] = {};
		}
		
		$scope.searchFilters.filters.PATIENT_ID = $scope.objectFilters.patient != undefined ? $scope.objectFilters.patient.id : $scope.searchFilters.filters.PATIENT_ID;
		
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;

		PharmacoVigilanceEventService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.manageTemplatesAfterSearch(result.data);
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			
			if(tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
			
			if($scope.filterStoreKey) {
				try {
					$window.sessionStorage.setItem($scope.filterStoreKey, angular.toJson($scope.searchFilters));
					$window.sessionStorage.setItem($scope.filterStoreKey + '-object', angular.toJson($scope.objectFilters));
				}
				catch(e) {
					console.log(e);
				}
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.manageTemplatesAfterSearch = function(list) {
		for(var i=0; i<list.length; i++) {
			var reportTypes = list[i].reportTypes || [];
			$scope.selectedBlankTemplatesList[list[i].id] = {};
			$scope.selectedFillableTemplatesList[list[i].id] = {};
			for(var j=0; j<reportTypes.length; j++) {
				if($scope.fillableTemplates[reportTypes[j]]!=undefined) {		
					$scope.selectedFillableTemplatesList[list[i].id][reportTypes[j]]=$scope.fillableTemplates[reportTypes[j]];
				}
				if($scope.blankTemplates[reportTypes[j]]!=undefined) {
					$scope.selectedBlankTemplatesList[list[i].id][reportTypes[j]]=$scope.blankTemplates[reportTypes[j]];
				}
			}	
		}
	}
	
	$scope.manageFormGenerationDownload = function(id,action,fillableTypes,blankTypes,initFn,fillable,blank) {
		if(action=='generate' && fillableTypes.length==1) {
			//Se c'è un solo template scaricabile, lo scarica direttamente
			$scope.generateForm(id,fillableTypes[0]);
		}
		else if(action=='download' && blankTypes.length==1) {
			//Se c'è un solo template scaricabile, lo scarica direttamente
			$scope.downloadTemplate(id,blankTypes[0]);
		}
		else {
			var modalInstance =  $modal.open({
				scope: $scope,
				templateUrl: 'tpl/app/directive/pharmaco-vigilance/event-forms-modal.html',
				size: 'md',
				controller: function ($scope, $modalInstance, MessageService, RESTURL, $state) {
					$scope.action = action;
					if(action=='generate') {
						$scope.templates = fillable;
					}
					else if(action=='download') {
						$scope.templates = blank;
					}
					$scope.init = initFn;
					$scope.cancel = function () {
						$modalInstance.dismiss('cancel');
						$scope.init();
					};
				}
			});
		}
	}
	
	$scope.selectForms = function(id,action,withUpdate) {
		if(withUpdate==undefined) 
			withUpdate=true;
		var fillable = $scope.selectedFillableTemplates==undefined ? $scope.selectedFillableTemplatesList==undefined ? undefined : $scope.selectedFillableTemplatesList[id] : $scope.selectedFillableTemplates;
		var blank = $scope.selectedBlankTemplates==undefined ? $scope.selectedBlankTemplatesList==undefined ? undefined : $scope.selectedBlankTemplatesList[id] : $scope.selectedBlankTemplates;
		var initFn = $scope.init;
		var fillableTypes = fillable==undefined ? [] : Object.keys(fillable);
		var blankTypes = blank==undefined ? [] : Object.keys(blank);
		$scope.selectedRowId = id;
		if(withUpdate) {
			var request = $scope.loadRequest();
			PharmacoVigilanceEventService.update({ id : $scope.id }, request, function(result) {
				
				$scope.manageFormGenerationDownload(id,action,fillableTypes,blankTypes,initFn,fillable,blank);
				
			}, function(error) {
				MessageService.showError('Aggiornamento non riuscito');
				if (error.status == 404) {
					$state.go('access.not-found');
				} 
			});
		}
		else {
			$scope.manageFormGenerationDownload(id,action,fillableTypes,blankTypes,initFn,fillable,blank);
		}
	}
	
	$scope.generateForm = function(id, type) {
		//if($scope.cancel!=undefined) $scope.cancel();
		var modalInstance = MessageService.simpleConfirm('Conferma generazione Form','Si desidera generare il Form di Farmaco Vigilanza selezionato?','bg-secondary');
		modalInstance.result.then(
			function(confirm) {
				
				if(!angular.isDefined(id) && angular.isDefined($scope.selectedRowId)) {
					id = $scope.selectedRowId;
				}
				
				$scope.showWait = true;
				PharmacoVigilanceEventService.generateForm({ id: id, formType: type }, function(success) {
					var url = success.data;
					var element = angular.element('<a/>');
					element.attr({
						href: url,
						target: '_blank'
					})[0].click();
					
					$scope.showWait = false;
					if($scope.cancel==undefined)
						$scope.init();
					
					MessageService.showSuccess('Generazione Form completata con successo');
				}, function(error) {
					MessageService.showError('Errore in fase di generazione Form');
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
		);
	}
	
	$scope.downloadTemplate = function(id, type) {
		var exportFilter = {
				filters : {}
		}
		//if($scope.cancel!=undefined) $scope.cancel();
		$http.post(RESTURL.PHARMACOVIGILANCEEVENT + '/' +id +'/download-template/'+type, exportFilter, {responseType:'arraybuffer'})
		.success(function (response, status, xhr) {
			var file = new Blob([response], {type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'});
			var fileURL = URL.createObjectURL(file);
			$scope.pdf = $sce.trustAsResourceUrl(fileURL);
			var link = angular.element('<a/>');
			link.attr({
				href : fileURL,
				//target: '_blank',
				download: 'pv-template_'+type+'.docx'
			})[0].click();
		}).error(function(data, status, headers, config) {
			MessageService.showError('Generazione export non riuscita', '');
		});
	}

	$scope.searchFollowUpEvents = function() {
		if($scope.selectedBlankTemplatesList==undefined)
			$scope.selectedBlankTemplatesList = [];
		if($scope.selectedFillableTemplatesList==undefined)
			$scope.selectedFillableTemplatesList = [];
		
		PharmacoVigilanceEventService.search({}, { start: 0, filters: { 'PARENT_EVENT_ID' : $scope.detail.id, 'EVENT_TYPE' : 'FOLLOW_UP' }}, function(result) {
			$scope.detail.followUpEvents = result.data;
			$scope.followUpPermissions = result.permissions;
			
			$scope.manageTemplatesAfterSearch(result.data);
			
		}, function(error) {
			
		});
	}
	
	$scope.delete = function(id) {
		var modalInstance = MessageService.deleteConfirm();
		modalInstance.result.then(
			function(confirm) {
				PharmacoVigilanceEventService.delete({ id: id }, function(result) {
					if($scope.popup) {
						$scope.close();
					} else {
						$scope.init();
					}
					MessageService.showSuccess('Cancellazione completata con successo');
				}, function(error) {
					MessageService.showError('Cancellazione non riuscita');
					if(error.status == 404) {
						$state.go('access.not-found');
					}
				});
			}
		);
	}
	
	$scope.save = function() {
		$scope.errors = $scope.validate();
		if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			if ($scope.isNew()) {
				$scope.createNew();
			} else {
				$scope.update();
			}
		}
		else {
			MessageService.showError('Errore in fase di salvataggio','Alcuni dati inseriti non sono corretti');
		}
	};
	
	$scope.update = function() {
		var request = $scope.loadRequest();
		PharmacoVigilanceEventService.update({ id : $scope.id }, request, function(result) {
			
			$scope.fieldPermissions = result.fieldPermissions;
			$scope.initPermission(result.permissions);
			$scope.detail.id = result.data.id;
			$scope.initDetails();
			MessageService.showSuccess('Aggiornamento completato con successo');
		}, function(error) {
			MessageService.showError('Aggiornamento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.createNew = function() {
		var request = $scope.loadRequest();
		PharmacoVigilanceEventService.insert({ }, request, function(result) {
			$scope.detail.id = result.data.id;
			if($scope.popup) {
				$scope.id = $scope.detail.id;
				$scope.initDetails();
			} else {
				$state.go('app.pharmacovigilanceevents');
			}
			MessageService.showSuccess('Inserimento completato con successo');
		}, function(error) {
			MessageService.showError('Inserimento non riuscito');
			if (error.status == 404) {
				$state.go('access.not-found');
			} 
		});
	}
	
	$scope.validate = function() {
		var errors = {};
		var form = {
			formProperties : [
				{ id: 'detail.code', value : $scope.detail.code, type: 'string', required: true },
				{ id: 'detail.eventDate', value : $scope.detail.eventDate, required: true },
				{ id: 'detail.eventType', value : $scope.detail.eventType, type: 'string', required: true },
				{ id: 'detail.patient', value : $scope.detail.patient, required: true }
			]
		};
		
		for(var i=0; i<$scope.detail.products.length; i++) {
			form.formProperties.push({ id: 'detail.products['+i+'].lotNumber', value : $scope.detail.products[i].lotNumber, type: 'string', required: true });
			form.formProperties.push({ id: 'detail.products['+i+'].includedInReports', value : $scope.detail.products[i].includedInReports, required: true });
			if(!$scope.detail.products[i].associatedDrug) {
				form.formProperties.push({ id: 'detail.products['+i+'].therapyId', value : $scope.detail.products[i].therapyId, type: 'string', required: true });
				form.formProperties.push({ id: 'detail.products['+i+'].diseaseId', value : $scope.detail.products[i].diseaseId, type: 'string', required: true });
				form.formProperties.push({ id: 'detail.products['+i+'].drugId', value : $scope.detail.products[i].drugId, type: 'string', required: true });
			}
			else {
				form.formProperties.push({ id: 'detail.products['+i+'].drugName', value : $scope.detail.products[i].drugName, type: 'string', required: true });
			}
		}
		
		
		errors = FormUtilService.validateForm(form);
		console.log(errors);
		
		return errors;
	}

	$scope.initPaginationAndFilter = function() {
		$scope.selects = { 'Patient' : undefined };
		
		/*
		PatientService.search({},{start: 0, sort: ['lastName','firstName']},function(success) {
			$scope.selects['Patient'] = success.data;
		}, function(error) {
			MessageService.showError('Errore','Problema nel recupero della lista pazienti');
		});
		*/
		
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.objectFilters = $scope.objectFilters || {};
		$scope.searchFilters.filters = $scope.searchFilters.filters || {};
		if($scope.patientId) {
			
			PatientService.get({ id: $scope.patientId }, function(result) {
				$scope.objectFilters.patient = result.data;
			}, function(error) {});
		
			$scope.searchFilters.filters.PATIENT_ID = $scope.patientId;
		}
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
		
		if($scope.filterStoreKey) {
			try {
				var savedFilters = $window.sessionStorage[$scope.filterStoreKey];
				if(savedFilters) {
					savedFilters = angular.fromJson(savedFilters);
				}
				if(savedFilters) {
					angular.extend($scope.searchFilters, savedFilters);
					$scope.pagination.start =  savedFilters.start;
					$scope.pagination.size =  savedFilters.size;
					$scope.pagination.sort =  savedFilters.sort;
				}
				
				var objectFilters = $window.sessionStorage[$scope.filterStoreKey + '-object'];
				if(objectFilters) {
					objectFilters = angular.fromJson(objectFilters);
				}
				if(objectFilters) {
					angular.extend($scope.objectFilters, objectFilters);
				}
			}
			catch(e) {
				console.log(e);
			}
		}
	}
	
	$scope.initPaginationAndFilterFollowUps = function() {
		$scope.paginationFollowUps = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};
	}
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			$scope.canGenerateForm = permissions.GENERATE_FORM;
			$scope.canDownload = permissions.DOWNLOAD;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
				$scope.canGenerateForm = false;
				$scope.canDownload = false;
			}
			else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} 
		else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
			$scope.canGenerateForm = false;
			$scope.canDownload = false;
		}
	}
	
	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
		$scope.objectFilters = {};
		if($scope.filterStoreKey) {
			try {
				$window.sessionStorage.removeItem($scope.filterStoreKey);
				$window.sessionStorage.removeItem($scope.filterStoreKey + '-object');
			}
			catch(e) {
				console.log(e);
			}
		}
	}

	$scope.undo = function() {
		if ($scope.isNew()) {
			$state.go("app.pharmacovigilanceevents");	
		} else {
			$scope.initDetails();
		}
		MessageService.showSuccess('Le modifiche sono state annullate');
	}

	$scope.edit = function(id) {
		if (!$scope.selection) {
			$state.go("app.pharmacovigilanceevent", {id: id});
		}
	}

	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.loadRequest = function() {
		var r = {
			breastFeeding: $scope.detail.breastFeeding,
			code : $scope.detail.code,
			customerNotificationDate : $scope.detail.customerNotificationDate,
			customerNotificationReceivedDate : $scope.detail.customerNotificationReceivedDate,
			deathReason : $scope.detail.deathReason,
			description : $scope.detail.description,
			ethnicGroup : $scope.detail.ethnicGroup,
			eventReceiverId: $scope.detail.eventReceiver ? $scope.detail.eventReceiver.id : undefined,
			eventDate : $scope.detail.eventDate,
			eventEnding : $scope.detail.eventEnding,
			eventEndingDate : $scope.detail.eventEndingDate,
			eventNotificationDate : $scope.detail.eventNotificationDate,
			eventReason : $scope.detail.eventReason,
			eventScope : $scope.detail.eventScope,
			eventSeriousness : $scope.detail.eventSeriousness,
			eventType : $scope.detail.eventType,
			examinations : $scope.detail.examinations,
			followUpEvents : $scope.detail.followUpEvents,
			formGenerationDate : $scope.detail.formGenerationDate,
			formUploadDate : $scope.detail.formUploadDate,
			height : $scope.detail.height,
			lastMenstruationDate : $scope.detail.lastMenstruationDate,
			monitoringNumber: $scope.detail.monitoringNumber,
			monitoringTitle: $scope.detail.monitoringTitle,
			monitoringType: $scope.detail.monitoringType,
			otherConditions: $scope.detail.otherConditions,
			otherInformations: $scope.detail.otherInformations,
			pregnancyType: $scope.detail.pregnancyType,
			reporterType : $scope.detail.reporterType,
			reporterName : $scope.detail.reporterName,
			reporterEmail : $scope.detail.reporterEmail,
			reporterPhone : $scope.detail.reporterPhone,
			reporterAddress : $scope.detail.reporterAddress,
			reporterAsl : $scope.detail.reporterAsl,
			reporterCounty : $scope.detail.reporterCounty,
			reporterQualification : $scope.detail.reporterQualification,
			patientId : $scope.detail.patient.id,
			products : [],
			parentEventId : $scope.detail.parentEvent!=undefined ? $scope.detail.parentEvent.id : undefined,
			takenActions: $scope.detail.takenActions,
			usageOfNaturalProducts: $scope.detail.usageOfNaturalProducts,
			weight: $scope.detail.weight
		}
		var reportTypes = [];
		if($scope.reportTypes!=undefined) {
			var reportTypesKeys = Object.keys($scope.reportTypes);
			for(var k=0; k<reportTypesKeys.length; k++) {
				if($scope.reportTypes[reportTypesKeys[k]])
					reportTypes.push(reportTypesKeys[k]);
			}
		}
		r.reportTypes = (reportTypes==[]) ? undefined : reportTypes;
		
		for(var i=0; i<$scope.detail.products.length; i++) {
			r.products.push({
				adrAfterResume: $scope.detail.products[i].adrAfterResume,
				associatedDrug:  $scope.detail.products[i].associatedDrug,
				dosage: $scope.detail.products[i].dosage,
				dosageMethod: $scope.detail.products[i].dosageMethod,
				dosageUnit: $scope.detail.products[i].dosageUnit,
				drugId: $scope.detail.products[i].drugId,
				drugName: $scope.detail.products[i].drugName,
				drugResumed: $scope.detail.products[i].drugResumed,
				drugSuspended: $scope.detail.products[i].drugSuspended,
				drugUsageEndTime: $scope.detail.products[i].drugUsageEndTime,
				drugUsageReason: $scope.detail.products[i].drugUsageReason,
				drugUsageStartTime: $scope.detail.products[i].drugUsageStartTime,
				frequency: $scope.detail.products[i].frequency,
				frequencyUnit: $scope.detail.products[i].frequencyUnit,
				id: $scope.detail.products[i].id,
				includedInReports: $scope.detail.products[i].includedInReports,
				lotNumber: $scope.detail.products[i].lotNumber,
				reliefAfterDrugSuspended: $scope.detail.products[i].reliefAfterDrugSuspended,
				therapyStartDate: $scope.detail.products[i].therapyStartDate,
				diseaseId: $scope.detail.products[i].diseaseId,
				therapyId: $scope.detail.products[i].therapyId
			});
		}
		
		return r;
	}
	
	$scope.exportXls = function(allData) {
		if(angular.isDefined($scope.list) && $scope.list.length > 0) {
			$scope.list = $scope.list || [];
			
			angular.extend({filters:{"PHARMACO_VIGILANCE_EVENT_ID":$scope.id}}, $scope.pagination);
			//delete $scope.searchFilters.total;
			
			var exportFilter = angular.copy({filters:{"PHARMACO_VIGILANCE_EVENT_ID":$scope.id}});
			if(allData !== undefined && allData != null && allData == true) {
				exportFilter.start = 0;
				delete exportFilter.size;
			} 
	
			$http.post(RESTURL.PHARMACOVIGILANCEEVENT + '/search/export-xls', exportFilter, {responseType:'arraybuffer'})
			.success(function (response, status, xhr) {
				var file = new Blob([response], {type: 'application/vnd.ms-excel'});
				var fileURL = URL.createObjectURL(file);
				$scope.pdf = $sce.trustAsResourceUrl(fileURL);
				var link = angular.element('<a/>');
				link.attr({
					href : fileURL,
					target: '_blank',
					download: 'export.xls'
				})[0].click();
			}).error(function(data, status, headers, config) {
				MessageService.showError('Generazione export non riuscita', '');
			});
		} else {
			MessageService.showError('Non sono presenti dati da esportare', '');
		}
	}
	
	$scope.addAssociatedDrug = function() {
		$scope.addProduct(true);
	}
	
	$scope.addPrincipalDrug = function() {
		$scope.addProduct(false);
	}
	
	$scope.addProduct = function(isAssociated) {
 		var p = {
       	   lotNumber: undefined,
       	   associatedDrug: isAssociated,
       	   includedInReports: true
       	}
 		if(!isAssociated) {
 			p.diseaseId = ($scope.detail.patient.diseases!==undefined && $scope.detail.patient.diseases.length==1) ? $scope.detail.patient.diseases[0].id : undefined;
 			p.therapyId = ($scope.detail.patient.therapies!==undefined && $scope.detail.patient.therapies.length==1) ? $scope.detail.patient.therapies[0].id : undefined;
 		}
 		
 		//$scope.detail.products.push(p);
 		$scope.detail.products.splice(0,0,p);
 		if(!isAssociated) {
	 		$scope.onChangeTherapyIdFn(p);       
 		}
 	}
	
	$scope.onChangeTherapyIdFn = function(product) {
		//if($scope.enableWatches!=1) return;
		console.log('************** onChangeTherapyIdFn **********');
		console.log(product);
		console.log('*******************************************');
		var therapy = undefined;
		if($scope.detail.patient !== undefined && $scope.detail.patient.therapies!==undefined) {
			for(var i=0; i<$scope.detail.patient.therapies.length; i++) {
				if($scope.detail.patient.therapies[i].id == product.therapyId) {
					therapy = $scope.detail.patient.therapies[i];
					break;
				}
			}
			product.therapy = therapy;
			
			product.therapyStartDate = (therapy!=undefined) ? therapy.start : undefined;
			product.therapyEndDate = (therapy!=undefined) ? therapy.end : undefined;
			product.drugUsageStartTime = (therapy!=undefined) ? therapy.start : undefined;
			product.drugUsageEndTime = (therapy!=undefined && therapy.end!=null) ? therapy.end : product.drugUsageEndTime;
			product.drugId = (therapy!=undefined && therapy.drugs!=undefined && therapy.drugs.length==1) ? therapy.drugs[0].id : undefined;
			if(therapy!=undefined && therapy.drugs!=undefined && therapy.drugs.length==1)
				$scope.onChangeDrugIdFn(product);
		} else {
			$scope.detail.products = ($scope.parentEvent!=undefined) ? angular.copy($scope.parentEvent.products) : [];
		}
	}
	
	$scope.onChangeDrugIdFn = function(product) {
		//if($scope.enableWatches!=1) return;
		console.log('************** onChangeDrugIdFn **********');
		console.log(product);
		console.log('*******************************************');
		var drug = undefined;
		if($scope.detail.patient.therapies!==undefined) {
			for(var i=0; i<$scope.detail.patient.therapies.length; i++) {
				if($scope.detail.patient.therapies[i].id == product.therapyId) {
					for(var j=0; j<$scope.detail.patient.therapies[i].drugs.length; j++) {
						if($scope.detail.patient.therapies[i].drugs[j].id==product.drugId) {
							drug = $scope.detail.patient.therapies[i].drugs[j];
							product.frequency = (drug!=undefined) ? drug.assumptionFrequency : undefined;
							product.frequencyUnit = (drug!=undefined) ? drug.assumptionFrequencyUnit : undefined;
							break;
						}
					}
				}
			}
			
			product.dosage = (product.dosage!=undefined) ? product.dosage : (drug!=undefined) ? drug.expectedDosage : undefined;	
			product.dosageUnit = (product.dosageUnit!=undefined) ? product.dosageUnit : (drug!=undefined) ? drug.expectedDosageUnit : undefined;
			if(drug!=null && angular.isDefined(drug) && angular.isDefined($rootScope.constants) && angular.isDefined($rootScope.constants['DosageMeasure'])){
				var constantMatch = false;
				for(var c=0; c < $rootScope.constants['DosageMeasure'].length && !constantMatch; c++){
					if($rootScope.constants['DosageMeasure'][c].id == drug.expectedDosageUnit){
						product.dosageUnit = $rootScope.constants['DosageMeasure'][c].label;
						constantMatch = true;
					}
				}
			}
			product.dosageMethod = (drug!=undefined) ? drug.expectedDosageMethod : undefined;		
		}
	}
	
	$scope.$watch('detail.reporterType', function(nv,ov) {
		if(nv!=undefined && nv!=ov && $scope.enableWatches==1) {
			$scope.detail.reporter = undefined;
		}
	});
	
	$scope.$watch('detail.eventDate', function(nv,ov) {
		if($scope.enableWatches!=1) return;
		if(nv!=undefined && nv!=ov) {
			var size = $scope.detail.followUpEvents!=undefined ? $scope.detail.followUpEvents.length : 0;
			if($scope.detail.eventType!='FOLLOW_UP') {
				$scope.detail.code = $scope.generateEventCode(nv,$scope.detail.eventType,size);
			}
		}
	},true);
	
	$scope.$watch('reportTypes', function(nv,ov) {
		if(nv!=undefined) {
			var types = nv;
			var typesKeys = Object.keys(types);
			
			for(var i=0; i<typesKeys.length; i++) {
				if(types[typesKeys[i]]==true) {
					if($scope.fillableTemplates[typesKeys[i]]!=undefined) {
						if($scope.selectedFillableTemplates==undefined) 
							$scope.selectedFillableTemplates = {};
						$scope.selectedFillableTemplates[typesKeys[i]] = $scope.fillableTemplates[typesKeys[i]];
					}
					if($scope.blankTemplates[typesKeys[i]]!=undefined) {
						if($scope.selectedBlankTemplates==undefined) 
							$scope.selectedBlankTemplates = {};
						$scope.selectedBlankTemplates[typesKeys[i]] = $scope.blankTemplates[typesKeys[i]];
					}
				}
				else {
					if($scope.fillableTemplates[typesKeys[i]]!=undefined && $scope.selectedFillableTemplates!=undefined) {
						delete $scope.selectedFillableTemplates[typesKeys[i]];
					}
					if($scope.blankTemplates[typesKeys[i]]!=undefined && $scope.selectedBlankTemplates!=undefined) {
						delete $scope.selectedBlankTemplates[typesKeys[i]];
					}
					if($scope.selectedFillableTemplates!=undefined && Object.keys($scope.selectedFillableTemplates).length==0) 
						$scope.selectedFillableTemplates=undefined;
					if($scope.selectedBlankTemplates!=undefined && Object.keys($scope.selectedBlankTemplates).length==0) 
						$scope.selectedBlankTemplates=undefined;
				}
			}
		}
	},true);
	
	$scope.$watchCollection('detail.products', function(nv,ov) {
		if(nv!=undefined) {
			$scope.detail.associatedDrugs = [];
			$scope.detail.principalDrugs = [];
			for(var i=0; i<nv.length; i++) {
				if(nv[i].associatedDrug) {
					$scope.detail.associatedDrugs.push(nv[i]);
				} else {
					$scope.detail.principalDrugs.push(nv[i]);
				}
			}
		}
	},true);
	
	$scope.generateEventCode = function(date,eventType,size) {
		var c = $filter('date')(date,'yyyyMMddhhmmss');
		if(eventType!='FOLLOW_UP') {
			c = c +'S00';
		} else {
			c = c + 'F0'+(1+parseInt(size));
		}
		return c;
	}
	
	$scope.removeLastPrincipalDrug = function() {
		$scope.removeLastProduct(false);
	}
	
	$scope.removeLastAssociatedDrug = function() {
		$scope.removeLastProduct(true);
	}
	
	$scope.removeLastProduct = function(isAssociated) {
		$scope.errors = {};
		for(var i=0; i<$scope.detail.products.length; i++) {
			var associated = $scope.detail.products[i].associatedDrug;
			if(associated==isAssociated) {
				$scope.detail.products.splice(i,1);
				break;
			}
		}
		
	}
	
	/*
	$scope.$watch('detail.reporter', function(nv,ov) {
		if(nv!=undefined && nv!=ov && $scope.detail.reporterType=='PATIENT') {
			$scope.detail.patient = nv;
		}
	},true);
	*/
	
	$scope.searchPatientDetail = function() {
		if (angular.isDefined($scope.detail.patient) && $scope.detail.patient &&
				angular.isArray($scope.detail.patient)) {
			$scope.detail.patient = $scope.detail.patient[0];
		}
		var therapySearchFilters = {
				filters: { 'PATIENT_ID' : $scope.detail.patient.id }
		}

		if($scope.detail.patient.id != undefined) {
			PatientService.get({ id: $scope.detail.patient.id },function(result) {
				$scope.detail.patient.diseases = result.data.diseases;
			});
		}

		PatientTherapyService.search({}, therapySearchFilters, function(result) {
			$scope.detail.patient.therapies = result.data;
			$scope.detail['products'] = [];
			$scope.addPrincipalDrug();
		});
	}

	$scope.init();
});