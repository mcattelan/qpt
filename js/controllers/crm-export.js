'use strict'
app.controller('ExportController', function(
		$rootScope,
		$scope,
		$stateParams,
		$http,
		$state,
		StoredExportService,
		MessageService,
		PermissionService,
		SMART_TABLE_CONFIG,
		RESTURL,
		$filter) {
	
	
	//Inizializza sia la lista che la scheda
	$scope.init = function() {
		$scope.loggedUser = $rootScope.loggedUser;
		$scope.userType = undefined;

		$scope.errors = {};
		$scope.selects = { 
				'XlsExportType' : $scope.constants['XlsExportType']
		}
		
		if($scope.constants['CustomXlsExportType'] != undefined){
			//creo lista unica di tipi export standard + tipi export personalizzati
			for(var i=0; i<$scope.constants['CustomXlsExportType'].length; i++) {
				//aggiungo solo se manca
				if($scope.selects['XlsExportType'].indexOf($scope.constants['CustomXlsExportType'][i]) < 0) {
					$scope.selects['XlsExportType'].push($scope.constants['CustomXlsExportType'][i]);
				}
			}
		}

		if (($scope.id === undefined || $scope.id == null) && !$scope.popup) {
			$scope.id = $stateParams.id;
		}

		if (angular.isUndefined($scope.id)) {
			$scope.initList();
		}		
	};

	$scope.initList = function() {
		$scope.showList = false;
		PermissionService.getAll({ entityType : 'STORED_EXPORT' }, function(result) {
			$scope.initPermission(result.data);
			$scope.selection = $scope.selection || false;
			$scope.selected = $scope.selected || [];
			$scope.initPaginationAndFilter();
			$scope.showList = true;
		}, function(error) {
			$scope.list = [];
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	};
	
	$scope.search = function(tableState, smCtrl) {
		$scope.smCtrl = $scope.smCtrl || smCtrl;
		if (tableState === undefined) {
			if ($scope.smCtrl !== undefined) {
				$scope.smCtrl.tableState().pagination.start = 0;
				$scope.smCtrl.pipe();
			}
			return;
		}
		if(tableState !== undefined) {
			tableState.pagination.number = $scope.pagination.size;
			
			$scope.pagination.start = tableState.pagination.start;
			if(tableState.sort != null && tableState.sort.predicate !== undefined) {
				$scope.pagination.sort = tableState.sort.reverse ? [ "-" + tableState.sort.predicate ] : [ tableState.sort.predicate ];
			}
			if(tableState.pagination.number !== undefined) {
				$scope.pagination.size = tableState.pagination.number;
			}
		}
		$scope.list = $scope.list || [];
				
		angular.extend($scope.searchFilters, $scope.pagination);
		delete $scope.searchFilters.total;
		$scope.selectedIds = []
		for(var i=0; i<$scope.selected.length; i++) {
			$scope.selectedIds[i] = $scope.selected[i].id;
		}
		
		if ($scope.searchFilters['filters'] != undefined && $scope.searchFilters.filters['STATUS'] != undefined && $scope.searchFilters.filters['STATUS'] != 'PENDING' && $scope.searchFilters.filters['STATUS'] != 'ACTIVE') {
			delete $scope.searchFilters.filters['STATUS'];
		}
		
		StoredExportService.search({}, $scope.searchFilters, function(result) {
			$scope.list = result.data;
			$scope.permissions = result.permissions;
			$scope.pagination.start = result.start;
			$scope.pagination.size = result.size;
			$scope.pagination.sort = result.sort;
			$scope.pagination.total = result.total;
			for (var i=0; i<$scope.list.length; i++) {
				if ($scope.selectedIds.indexOf($scope.list[i].id) >= 0) {
					$scope.list.splice(i, 1);
				} else {
					$scope.list[i].selected = false;
				}
				
				$scope.list[i]['exportTypeLabel'] = $filter('constantLabel')($scope.list[i].exportType, 'XlsExportType');
				if(angular.isDefined($scope.list[i]['exportTypeLabel']) && $scope.list[i]['exportTypeLabel'].length === 0) {
					if(angular.isDefined($scope.constants['CustomXlsExportType'])) {
						$scope.list[i]['exportTypeLabel'] = $filter('constantLabel')($scope.list[i].exportType, 'CustomXlsExportType');
					}
					if(angular.isDefined($scope.list[i]['exportTypeLabel']) && $scope.list[i]['exportTypeLabel'].length === 0) {
						$scope.list[i]['exportTypeLabel'] = $filter('constantLabel')($scope.list[i].exportType, 'DataTypeName');
					}
				}
			}
			if (tableState !== undefined) {
				tableState.pagination.numberOfPages = Math.ceil($scope.pagination.total / ($scope.pagination.size !== undefined && $scope.pagination.size != null ? $scope.pagination.size : 1));
			}
		}, function(error) {
			$scope.list = [];
			$scope.initPaginationAndFilter();
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
	}
	
	$scope.initPaginationAndFilter = function() {
		$scope.pagination = {
				start: 0,
				size: SMART_TABLE_CONFIG.TABLE_SIZE,
				sort: []
		};

		$scope.searchFilters = $scope.searchFilters || {};
		$scope.initialSearchFilters = angular.copy($scope.searchFilters);
	}

	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			if ($scope.isNew()) {
				$scope.canUpdate =  $scope.canInsert;
			} else {
				$scope.canUpdate = permissions.UPDATE;
			}
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
		}
	}

	$scope.clearFilters = function(){
		$scope.searchFilters.filters = angular.copy($scope.initialSearchFilters.filters);
	}
	
	
	$scope.isNew = function() {
		return $scope.id == '';
	}
	
	$scope.init();
});