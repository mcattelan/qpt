'use strict'

app.controller('PatientEnrollmentDetailCtrl', function(
		$rootScope,
		$scope,
		$timeout,
		$stateParams,
		$http,
		$state,
		PermissionService,
		MessageService,
		PatientEnrollmentService,
		FormUtilService,
		FileUploader,
		$window,
		$sce,
		$modal,
		$cookies,
		RESTURL_QUIPERTE) {

	$scope.init = function() {
		$scope.detail = {};
		$scope.errors = {};
		
		$scope.initFileUploader();
		//è sempre nuovo l'invito quindi i permessi li carico sempre da zero. 
		PermissionService.getAll({ entityType : 'PATIENT_ENROLLMENT' }, function(result) {
			$scope.initPermission(result.data);
			
			PermissionService.getAllFields({ entityType : 'PATIENT_ENROLLMENT' }, function(result) {
				$scope.fieldPermissions = result.data;
				
			}, function(error) {
				$scope.detail = {};
				$scope.fieldPermissions = {};
				if(error.status == 404) {
					$state.go('access.not-found');
				}
			});
			
		}, function(error) {
			$scope.detail = {};
			if(error.status == 404) {
				$state.go('access.not-found');
			}
		});
		
	};
	
	$scope.initPermission = function(permissions) {
		if (permissions != undefined && permissions != null) {
			$scope.canInsert = permissions.INSERT;
			$scope.canDelete = permissions.DELETE;
			$scope.canExport = permissions.EXPORT;
			$scope.canUpdate = permissions.UPDATE;
		} else {
			$scope.canInsert = false;
			$scope.canDelete = false;
			$scope.canUpdate = false;
			$scope.canExport = false;
		}
		$scope.permissions = permissions;
	}
	
	$scope.undo = function() {
		$state.go($state.current, {}, {reload: true});
	}

	$scope.completed = function() {
		$scope.loading = false;
		$scope.showConfirm = true;
	}
	
	$scope.initFileUploader = function() {

		$scope.errors = {};

		//FileUploader
		var authToken = angular.fromJson($cookies.getObject('italiassistenza_ua'))[1];
		$scope.uploader = new FileUploader({
		    url: RESTURL_QUIPERTE.PATIENT_ENROLLMENT+'/insert-upload',
		    headers: { 'X-AUTH-TOKEN' : authToken }
		});
		
		$scope.uploader.verifyAndUploadAll = function() {
			
			$scope.errors = $scope.validate();
			if(angular.isUndefined($scope.errors) || angular.equals($scope.errors, {})) {
			
				if($scope.uploader.queue==undefined || $scope.uploader.queue.length==0) {
					MessageService.showError("Errore","E' obbligatorio caricare il modulo di Adesione Paziente");
				}
				else {
					$scope.uploader.queue[0].formData = [];
					var modalInstance = MessageService.simpleConfirm("Invio Adesione Paziente", "Confermi di voler inviare i dati di questo Paziente?", 'btn-info','sm');
					modalInstance.result.then( function(confirm) {
						
						//c'è un solo file
						if($scope.detail.lastName!=undefined)
							$scope.uploader.queue[0].formData.push({lastName: $scope.detail.lastName});
						if($scope.detail.firstName!=undefined)
							$scope.uploader.queue[0].formData.push({firstName: $scope.detail.firstName});
						if($scope.detail.birthDate!=undefined)
							$scope.uploader.queue[0].formData.push({birthDate: $scope.detail.birthDate});
						
						
						$timeout( function() { 
							$scope.loading = true;
							$scope.customUploadAll();
						},1000);
					});
				}

			} else {
				var msg = ''; 
				if($scope.errors.lastName != undefined){
					msg += 'Cognome : ' + $scope.errors.lastName;
				}
				if($scope.errors.firstName != undefined){
					msg += msg != '' ? ' | ' : '';
					msg += 'Nome : ' + $scope.errors.firstName;
				}
				if($scope.errors.birthDate != undefined){
					msg += msg != '' ? ' | ' : '';
					msg += 'Data di Nascita : ' + $scope.errors.birthDate;
				}
				if(msg == ''){
					msg = 'Alcuni dati inseriti non sono corretti';
				}
				MessageService.showError('Errore in fase di salvataggio', msg);
			}
		}
		
		$scope.customUploadAll = function(){
			var item = $scope.uploader.queue[0];
			var xhr = new XMLHttpRequest();
			var form = new FormData();
			
			var that = $scope.uploader;
			
			angular.forEach(item.formData, function(obj) {
                 angular.forEach(obj, function(value, key) {
                     form.append(key, value);
                 });
             });
			var i;
			for(i = 0;i<$scope.uploader.queue.length;i++){
				form.append($scope.uploader.queue[i].alias, $scope.uploader.queue[i]._file, $scope.uploader.queue[i].file.name);
			}
			
			xhr.onload = function() {
                var headers = that._parseHeaders(xhr.getAllResponseHeaders());
                var response = that._transformResponse(xhr.response, headers);
                var gist = that._isSuccessCode(xhr.status) ? 'Success' : 'Error';
                var method = '_on' + gist + 'Item';
                that[method](item, response, xhr.status, headers);
                that._onCompleteItem(item, response, xhr.status, headers);
            };

            xhr.onerror = function() {
                var headers = that._parseHeaders(xhr.getAllResponseHeaders());
                var response = that._transformResponse(xhr.response, headers);
                that._onErrorItem(item, response, xhr.status, headers);
                that._onCompleteItem(item, response, xhr.status, headers);
            };

            xhr.onabort = function() {
                var headers = that._parseHeaders(xhr.getAllResponseHeaders());
                var response = that._transformResponse(xhr.response, headers);
                that._onCancelItem(item, response, xhr.status, headers);
                that._onCompleteItem(item, response, xhr.status, headers);
            };
			
		    xhr.open('POST', RESTURL_QUIPERTE.PATIENT_ENROLLMENT+'/insert-upload-multiple', true);
		    
		    xhr.withCredentials = false;
		    
		    var headers = { 'X-AUTH-TOKEN' : authToken};
		    angular.forEach(headers, function(value, name) {
                xhr.setRequestHeader(name, value);
            });
		    
		    xhr.send(form);
		}
	
		$scope.uploader.onAfterAddingFile =  function(item) {
		}
			
		$scope.uploader.onErrorItem = function(item, response, status, headers) {
			$scope.loading = false;
			if(response.message !== undefined) {
				MessageService.showError('Dati non validi', response.message + ' Correggere gli errori e ricaricare il modulo di Adesione');
			} else {
				MessageService.showError('Dati non validi', 'Correggere gli errori e ricaricare il modulo di Adesione');
			} 
				
			if(response.errors!=undefined) {	
				var fields = Object.keys(response.errors);
				for( var i=0; i<fields.length; i++) {
					$scope.errors[fields[i]]=response.errors[fields[i]].message;
				}
			}
		}
		
		$scope.uploader.onSuccessItem = function(item, response, status, headers) {
			$scope.detail = response.data;
		}
		
		$scope.uploader.onCompleteAll = function() {
			var notUploaded = [];
			var messages = [];
			
			
			for(var i=0; i<$scope.uploader.queue.length; i++) {
				if($scope.uploader.queue[i].isError) {
					notUploaded.push($scope.uploader.queue[i].name);
				}
			}
			$scope.uploader.queue = [];
			if(notUploaded.length==0) {
				$timeout( $scope.completed(), 100);
			}
		}
		
		$scope.validate = function() {
			var errors = {};
			var form = {
					formProperties : [
						{ id:'lastName', value: $scope.detail.lastName, required:true, type:'string' },
						{ id:'firstName', value: $scope.detail.firstName, required:true, type:'string' },
						{ id:'birthDate',  value: $scope.detail.birthDate, type:'string', required:true}
						]
			};
			errors = FormUtilService.validateForm(form);
			console.log(errors);
			return errors;
		}
		
	}
	
	$scope.init();
});