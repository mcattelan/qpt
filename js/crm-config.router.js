'use strict';

app.run(
	function ($rootScope, $state, $stateParams, $location, $localStorage) {
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
		
		$rootScope.toasterOptions = {
			'position-class': 'toast-top-right',
			'close-button':true
		};
		
		var tokens = $location.host().split('.');
		if(tokens.length == 3) {
			$rootScope.baseDomain = tokens[1] + '.' + tokens[2];
			$rootScope.domain = $location.host();
		}
		else {
			$rootScope.baseDomain = undefined;
			$rootScope.domain = undefined;
		}
		$rootScope.secure = $location.protocol() === 'https';
		
		$rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
			$rootScope.currentState = to.name;
			$rootScope.currentParams = toParams;
			$rootScope.prevState = from.name;
			$rootScope.prevParams = fromParams;
			$rootScope.tableForMobile();
		});
		
		/************************************
		 * Impostazioni generali per la UI
		 * Questi parametri consentono di modificare impostazioni globali della UI implementando visualizzazioni custom di parti comuni per specifici PSP
		 * Usato per tutor/quiperte
		 ************************************/
		$rootScope.uiSettings = {
			//Abilita possibilità di nascondere i filtri di ricerca mostrando solo un bottone (tutor/quiperte) per gli utenti del tipo indicato	
			enableSearchFilterCollapse : ['HCP','PATIENT']
		}
		
		$rootScope.tableForMobile = function(){
			jQuery('table.table > tbody tr').removeAttr('data-label');
        
          
			setTimeout(function(){
			  if(jQuery('table.table').length>0){
				jQuery('table.table').each(function(){
				  var thArray = [];
				  jQuery(this).find('> thead th').each(function(){
					thArray.push(jQuery(this).text());
				  });
				  jQuery(this).find('> tbody tr').each(function(){
					jQuery(this).find('td').each(function(i){
					  
					  jQuery(this).attr('data-label',thArray[i]);
					  if(jQuery(window).width()<801){
						jQuery(this).removeAttr('style');
						//jQuery(this).find('div').removeAttr('style');
					  }
					});
				  });
				});
		  
			  }
			},2000);
		}
		
		
	} 
);
app.config(
	function($stateProvider, $urlRouterProvider, JQ_CONFIG, $injector) {
		$urlRouterProvider.otherwise('/dashboard');
		$stateProvider
		.state('access', {
			abstract : true,
			url : '',
			templateUrl : 'tpl/access.html',
			resolve: {
				deps : ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['toaster']);
					}
				],
				constants : function($timeout, $q, $rootScope, PortalService) {
					return Constants($timeout, $q, $rootScope, PortalService);
				}
			}
		})
		.state('access.not-found', {
			url: '/not-found',
			templateUrl: 'tpl/access/not-found.html'
		})
		.state('access.denied', {
			url: '/access-denied',
			templateUrl: 'tpl/access/access-denied.html'
		})
		.state('access.login', {
			url : '/login',
			templateUrl : 'tpl/access/login.html',
			controller: 'LoginController',
			resolve: {
				deps: ['uiLoad',
					function( uiLoad ){
						return uiLoad.load( [ 'js/controllers/crm-access.js' ] );
					}
				]
			}
		})
		.state('access.logout', {
			url : '/logout',
			templateUrl : 'tpl/access/login.html',
			controller: 'LogoutController',
			resolve: {
				deps: ['uiLoad',
					function( uiLoad ){
						return uiLoad.load( ['js/controllers/crm-access.js'] );
					}
				]
			}
		})
		.state('access.activation', {
			url : '/activation/:token',
			templateUrl : 'tpl/access/activation.html',
			controller: 'ActivationController',
			resolve: {
				deps: ['uiLoad',
					function( uiLoad ){
						return uiLoad.load( ['js/controllers/crm-access.js'] );
					}
				]
			}
		})
		
		.state('access.password-recovery', {
			url : '/password-recovery/:token',
			templateUrl : function($stateParams) {
				if(angular.isUndefined($stateParams.token) || $stateParams.token == null || $stateParams.token === '') {
					return 'tpl/access/password-recovery.html';
				}
				else {
					return 'tpl/access/password-recovery-confirm.html';
				}
			},
			controller: 'PasswordRecoveryController',
			resolve: {
				deps: ['uiLoad',
					function( uiLoad ){
						return uiLoad.load( ['js/controllers/crm-access.js'] );
					}
				]
			}
		})
		.state('access.service-order-accept', {
			url : '/service-order-confirm/:token/accept',
			templateUrl : 'tpl/access/service-order-accept.html',
			controller: function($scope,  $stateParams, UserActivityService, MessageService, $state) {
				$scope.token = $stateParams.token;
				UserActivityService.acceptServiceOrder({ token: $scope.token }, {},
					function(result) {
						$scope.detail = result.data;
						$scope.permissions = result.permissions;
						$scope.fieldPermissions = result.fieldPermissions;
					},
					function(error) {
						if (error.status == 404) {
							$state.go('access.not-found');
						} else {
							MessageService.showError('Errore in fase di conferma', error.message !== undefined ? error.message : 'Errore imprevisto');
						}
					}
				)
			},
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table','toaster']);
					}
				]
			}
		})
		.state('access.service-order-reject', {
			url : '/service-order-confirm/:token/reject',
			templateUrl : 'tpl/access/service-order-reject.html',
			controller: function($scope,  $stateParams, UserActivityService, MessageService, $state) {
				$scope.token = $stateParams.token;
				UserActivityService.rejectServiceOrder({ token: $scope.token }, {},
					function(result) {
						$scope.detail = result.data;
						$scope.permissions = result.permissions;
						$scope.fieldPermissions = result.fieldPermissions;
					},
					function(error) {
						if (error.status == 404) {
							$state.go('access.not-found');
						} else {
							MessageService.showError('Errore in fase di rifiuto', error.message !== undefined ? error.message : 'Errore imprevisto');
						}
					}
				)
			},
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table','toaster']);
					}
				]
			}
		})
		.state('portals', {
			url : '/portals',
			templateUrl : 'tpl/app/portal/portals.html',
			controller: 'PortalsController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table','toaster']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-portal.js'] );
								}
						)
					}
				],
				LoggedInfo : function($timeout, $q, $rootScope, $cookies, $state, UserService, PortalService) {
					return LoggedInfo($timeout, $q, $rootScope, $cookies, $state, UserService, PortalService);
				}
			}
		})
		.state('disclaimer', {
			url : '/disclaimer',
			templateUrl: 'tpl/app/disclaimer/disclaimer.html',
			resolve: {
				deps : ['$ocLazyLoad','uiLoad',
					function($ocLazyLoad, uiLoad) {
						return uiLoad.load(
							JQ_CONFIG.fullcalendar.concat(['js/controllers/crm-main.js'])
		                ).then( function() {
		                	return $ocLazyLoad.load(['toaster']);
		                })
					}
				]
			},
		})
		.state('app', {
			abstract : true,
			url : '',
			templateUrl : 'tpl/app.html',
			controller: 'MainController',
			resolve : {
				deps : ['$ocLazyLoad','uiLoad',
					function($ocLazyLoad, uiLoad) {
						return uiLoad.load(
							JQ_CONFIG.fullcalendar.concat(['js/controllers/crm-main.js'])
		                ).then( function() {
		                	return $ocLazyLoad.load(['toaster','ui.calendar','ng.deviceDetector',/*'ngMap',*/'com.2fdevs.videogular',
							                         'com.2fdevs.videogular.plugins.controls','com.2fdevs.videogular.plugins.overlayplay',
							                         'com.2fdevs.videogular.plugins.poster','rzModule', 'angucomplete-alt', 'ui.tab.scroll']);
							                         
		                })
					}
				],
				
				LoggedUser : function($timeout, $q, $rootScope, $cookies, $state, UserService, PortalService) {
					return LoggedInfo($timeout, $q, $rootScope, $cookies, $state, UserService, PortalService);
				}
			}
		})
		.state('app.dashboard', {
			url : '/dashboard',
			templateProvider: function($templateFactory, $rootScope) {				
				var template = '';
				if(!$rootScope.constants || !$rootScope.loggedUser) {
					return undefined;
				}
				var userTypes = $rootScope.constants.UserType;
				if(userTypes[0].id == $rootScope.loggedUser.type || 
						userTypes[2].id == $rootScope.loggedUser.type ||
						userTypes[3].id == $rootScope.loggedUser.type ||
						userTypes[4].id == $rootScope.loggedUser.type){
					template = 'tpl/app/dashboard/dashboard.html';
				} else if(userTypes[1].id == $rootScope.loggedUser.type ||
						userTypes[6].id == $rootScope.loggedUser.type){
					template = 'tpl/app/calendar/calendar.html';
				}
				return $templateFactory.fromUrl(template);			
			},
			controllerProvider: function($rootScope) {				
				var controller = '';
				if(!$rootScope.constants || !$rootScope.loggedUser) {
					return undefined;
				}
				var userTypes = $rootScope.constants.UserType;
				if(userTypes[0].id == $rootScope.loggedUser.type || 
						userTypes[2].id == $rootScope.loggedUser.type ||
						userTypes[3].id == $rootScope.loggedUser.type) {
					controller = 'StatsController';
				} else if (userTypes[4].id == $rootScope.loggedUser.type) {
					controller = 'SummaryController';
				} else if(userTypes[1].id == $rootScope.loggedUser.type ||
						userTypes[6].id == $rootScope.loggedUser.type){
					controller = 'CalendarController';
				}
				return controller;				
			},
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table','ui.calendar']).then(	
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-stats.js', 'js/controllers/crm-calendar.js', 'js/controllers/crm-summary.js'] );
								}
						)
					}
				]
			},
			onEnter: function(MessageService, $cookies, $rootScope) {
				if($cookies.getObject('login') !== undefined) { 
					if($cookies.getObject('hide_disclaimer') == undefined) {
						$cookies.remove('login', { path: '/', domain: $rootScope.baseDomain });
						if($rootScope.disclaimer_NO_MEDICAL_DEVICE_WEB) {
							/* MOD MATTEO - aggiunto parametro per modale */
							MessageService.simpleAlert('Avviso', $rootScope.disclaimer_NO_MEDICAL_DEVICE_WEB.text, 'btn-info','','sm');	
							//MessageService.simpleAlert('Avviso', $rootScope.disclaimer_NO_MEDICAL_DEVICE_WEB.text, 'btn-info');	
						}
					} else {
						$cookies.remove('hide_disclaimer', { path: '/', domain: $rootScope.baseDomain });
					}
				}
			}
		})
		.state('app.documents', {
			url : '/documents',
			templateUrl : 'tpl/app/document/documents.html',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']);
					}
				]
			}
		})
		.state('app.shareddocuments', {
			url : '/shared-documents',
			templateUrl : 'tpl/app/document/shared-documents.html',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']);
					}
				]
			}
		})
		.state('app.personaldocuments', {
			url : '/personal-documents',
			templateUrl : 'tpl/app/document/personal-documents.html',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']);
					}
				]
			}
		})
		.state('app.profile', {
			url : '/profile/:id',
			templateProvider: function($templateFactory, $rootScope) {				
				
				
				var template = '';
				
				var userTypes = $rootScope.constants.UserType;
				//console.log($rootScope.loggedUser,userTypes);
				
				if(userTypes[0].id == $rootScope.loggedUser.type){
					//console.log("internal-staff");
					template = 'tpl/app/internal-staff/user.html';
				}
				else if(userTypes[1].id == $rootScope.loggedUser.type){
					//console.log("supplier");
					template = 'tpl/app/supplier/user.html';
				}
				else if(userTypes[2].id == $rootScope.loggedUser.type){
					//console.log("customer");
					template = 'tpl/app/customer/user.html';
				}
				else if(userTypes[4].id == $rootScope.loggedUser.type){
					//console.log("patient");
					template = 'tpl/app/patient/patient.html';
				}
				else if(userTypes[3].id == $rootScope.loggedUser.type){
					//console.log("hcp");
					template = 'tpl/app/hcp/hcp.html';
				}
				else if(userTypes[5].id == $rootScope.loggedUser.type){
					//console.log("patient_user");
					template = 'tpl/app/patient/user.html';
				}
				else if(userTypes[6].id == $rootScope.loggedUser.type){
					template = 'tpl/app/logistic/user.html';
				}
				
				return $templateFactory.fromUrl(template);			
			},
			
			controllerProvider: function($rootScope) {				
				var controller = '';
				
				var userTypes = $rootScope.constants.UserType;
				//console.log($rootScope.loggedUser,userTypes);
				
				if(userTypes[0].id == $rootScope.loggedUser.type){
					//console.log("internal-staff");
					controller = 'InternalStaffUserController';
				}
				else if(userTypes[1].id == $rootScope.loggedUser.type){
					//console.log("supplier");
					controller = 'SupplierUserController';
				}
				else if(userTypes[2].id == $rootScope.loggedUser.type){
					//console.log("customer");
					controller = 'CustomerUserController';
				}
				else if(userTypes[4].id == $rootScope.loggedUser.type){
					//console.log("patient");
					controller = 'PatientController';
				}
				else if(userTypes[3].id == $rootScope.loggedUser.type){
					//console.log("hcp");
					controller = 'HcpController';
				}
				else if(userTypes[5].id == $rootScope.loggedUser.type){
					//console.log("patient_user");
					controller = 'PatientUserController';
				}
				else if(userTypes[6].id == $rootScope.loggedUser.type){
					controller = 'LogisticUserController';
				}
				
				return controller;				
			},
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(	
								function() {
									
									return $ocLazyLoad.load( ['js/controllers/crm-internal-staff-user.js','js/controllers/crm-patient-user.js','js/controllers/crm-patient.js','js/controllers/crm-hcp.js','js/controllers/crm-customer-user.js','js/controllers/crm-supplier-user.js','js/controllers/crm-logistic-user.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.my-tasks', {
			url : '/my-tasks',
			templateUrl : 'tpl/app/routes/task-todo-list.html',
		})
		.state('app.not-assigned-tasks', {
			url : '/not-assigned-tasks',
			templateUrl : 'tpl/app/routes/task-not-assigned-list.html',
		})
		.state('app.assigned-to-others-tasks', {
			url : '/assigned-to-others-tasks',
			templateUrl : 'tpl/app/routes/task-assigned-to-others-list.html',
		})
		.state('app.task', {
			url : '/task/:id',
			templateUrl : 'tpl/app/activiti/task.html',
			controller: function($scope, $controller, $stateParams) {
				$scope.id = $stateParams.id;
				$controller('TaskDetailController', { $scope: $scope });
			},
			resolve : {
				deps : ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select', 'ngTagsInput']).then(
							function() {
								return $ocLazyLoad.load([ 'js/controllers/directives/crm-task.js' ]);
							}
						)
					}
				]
			}
		})
		.state('app.my-completed-tasks', {
			url : '/my-completed-tasks',
			templateUrl : 'tpl/app/activiti/tasks.html',
			controller: 'MyCompletedTaskListController',
			resolve : {
				deps : ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select', 'smart-table']).then(
							function() {
								return $ocLazyLoad.load([ 'js/controllers/directives/crm-task.js' ]);
							}
						)
					}
				]
			}
		})
		.state('app.customer-users', {
			url : '/customer-users',
			templateUrl : 'tpl/app/routes/customer-user-list.html'
		})
		.state('app.supplier-users', {
			url : '/supplier-users',
			templateUrl : 'tpl/app/routes/supplier-user-list.html',
		})
		.state('app.service', {
			url : '/service/:id',
			templateUrl : 'tpl/app/program/services/service.html',
			controller: 'ServiceController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-service.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.service-activity', {
			url : '/service/:serviceId/activity/:id',
			templateUrl : 'tpl/app/program/services/activityConf.html',
			controller: 'ServiceActivityController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-service-activity.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.completed-activity', {
			url : '/activity/completed',
			templateUrl : 'tpl/app/routes/user-activity-completed-list.html',
		})
		.state('app.patients', {
			url : '/patients',
			templateProvider: function($templateFactory, $rootScope, PortalService) {				
				
				var template = 'tpl/app/routes/patient-list.html';
				
				if($rootScope.loggedUser.type=='HCP' && ($rootScope.relatedPortals!==undefined && $rootScope.relatedPortals.length>0)) {
					template = 'tpl/app/routes/gateway/patient-list.html';
				}
				
				return $templateFactory.fromUrl(template);
			}
		})
		.state('app.patient', {
			url : '/patient/:id',
			templateUrl : 'tpl/app/patient/patient.html',
			controller: 'PatientController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( [
										'js/controllers/crm-patient.js',
										'js/controllers/crm-diary.js'
										] );
								}
						)
					}
				]
			}
		})
		.state('app.diary', {
			url : '/diary/:patientId',
			templateUrl : 'tpl/app/patient/diary.html'
		})
		.state('app.questionnaires', {
			url : '/patient/questionnaires/:patientId',
			templateUrl : 'tpl/app/patient/questionnaires.html'
		})
		.state('app.hcps', {
			url : '/hcps',
			templateUrl : 'tpl/app/routes/hcp-list.html',
		})
		.state('app.hcp', {
			url : '/hcp/:id',
			templateUrl : 'tpl/app/hcp/hcp.html',
			controller: 'HcpController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-hcp.js'] );
								}
						)
					}
				]
			}
		})	
		.state('app.customer', {
			url : '/customer/:id',
			templateUrl : 'tpl/app/customer/customer.html',
			controller: 'CustomerController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-customer.js'] );
								}
						)
					}
				]
			}
		})		
		.state('app.suppliers', {
			url : '/suppliers',
			templateUrl : 'tpl/app/routes/supplier-list.html',
		})
		.state('app.supplier', {
			url : '/supplier/:id',
			templateUrl : 'tpl/app/supplier/supplier.html',
			controller: 'SupplierController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-supplier.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.medicalcenters', {
			url : '/medicalcenters',
			templateUrl : 'tpl/app/routes/medical-center-list.html',
		})
		.state('app.medicalcenter', {
			url : '/medicalcenter/:id',
			templateUrl : 'tpl/app/medicalcenter/medicalcenter.html',
			controller: 'MedicalCenterController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-medicalcenter.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.medicalcenter-department', {
			url : '/medicalcenter/:id/deparment/:departmentId',
			templateUrl : 'tpl/app/medicalcenter/department.html',
			controller: 'MedicalCenterController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-medicalcenter.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.blood-drawing-centers', {
			url : '/blood-drawing-centers',
			templateUrl : 'tpl/app/routes/blood-drawing-center-list.html'
		})
		.state('app.internal-staff-users', {
			url : '/internal-staff-users',
			templateUrl : 'tpl/app/routes/internal-staff-user-list.html',
		})
		.state('app.internal-staff-user', {
			url : '/internal-staff-user/:id',
			templateUrl : 'tpl/app/internal-staff/user.html',
			controller: 'InternalStaffUserController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-internal-staff-user.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.diseases', {
			url : '/diseases',
			templateUrl : 'tpl/app/disease/diseases.html',
			controller: 'DiseaseController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-disease.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.disease', {
			url : '/disease/:id',
			templateUrl : 'tpl/app/disease/disease.html',
			controller: 'DiseaseController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-disease.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.documentcategories', {
			url : '/documentcategories',
			templateUrl : 'tpl/app/documentcategory/documentcategories.html',
			controller: 'DocumentCategoryController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-documentcategory.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.documentcategory', {
			url : '/documentcategory/:id',
			templateUrl : 'tpl/app/documentcategory/documentcategory.html',
			controller: 'DocumentCategoryController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-documentcategory.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.usermessages', {
			url : '/usermessages',
			templateUrl : 'tpl/app/routes/user-message-list.html',
		})
		.state('app.usermessage', {
			url : '/usermessage/:id',
			templateUrl : 'tpl/app/usermessage/usermessage.html',
			controller: 'UserMessageController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-usermessage.js','js/filters/propsfilter.js'] );
								}
						)
					}
				]
			}
		})		
		.state('app.calendar', {
			url : '/calendar',
			templateUrl : 'tpl/app/calendar/calendar.html',
			controller: 'CalendarController',
			resolve: {
                deps: ['$ocLazyLoad', 'uiLoad',
                  function( $ocLazyLoad, uiLoad ) {
                    return uiLoad.load(
                      JQ_CONFIG.fullcalendar.concat(['js/controllers/crm-calendar.js'])
                    ).then(
                      function(){
                        return $ocLazyLoad.load(['ui.calendar']);
                      }
                    )
                	
                }]
            }
	    })
	    .state('app.tutorial', {
			url : '/tutorial',
			templateUrl : 'tpl/app/dashboard.html',
			controller: undefined,
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(	
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-stats.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.exports', {
			url : '/exports',
			templateUrl : 'tpl/app/routes/export-list.html',
		})
		.state('app.pharmacovigilanceevents', {
			url : '/pharmacovigilanceevents',
			templateUrl : 'tpl/app/pharmaco-vigilance/events.html',
			controller: 'PharmacoVigilanceEventController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','toaster','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-pharmacovigilanceevent.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.pharmacovigilanceevent', {
			url : '/pharmacovigilanceevent/:id',
			templateUrl : 'tpl/app/pharmaco-vigilance/event.html',
			controller: function($scope, $stateParams) {
				$scope.id = $stateParams.id;
			},
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','toaster','smart-table','angularFileUpload']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-pharmacovigilanceevent.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.material-deliveries', {
			url : '/material-deliveries',
			templateUrl : 'tpl/app/routes/material-delivery-list.html'
		})
		.state('app.material-damages', {
			url : '/material-damages',
			templateUrl : 'tpl/app/routes/material-damage-list.html'
		})
		.state('app.phuturemeds', {
			url : '/pluggymeds',
			templateUrl : 'tpl/app/routes/phuturemed-list.html'
		})
		.state('app.phuturemed', {
			url : '/pluggymed/:id',
			templateUrl : 'tpl/app/phuturemed/phuturemed.html',
			controller: 'PhuturemedController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-phuturemed.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.pharmacies', {
			url : '/pharmacies',
			templateUrl : 'tpl/app/routes/pharmacy-list.html'
		})
		.state('app.patient-pharmacies', {
			url : '/patient-pharmacies',
			templateUrl : 'tpl/app/patient-pharmacy/patient-pharmacies.html',
			controller: 'PatientPharmacyController'
		})
		.state('app.drug-pick-up-list', {
			url : '/drug-pick-up-list',
			templateUrl : 'tpl/app/routes/drug-pick-up-list.html'
		})
		.state('app.drug-delivery-list', {
			url : '/drug-delivery-list',
			templateUrl : 'tpl/app/routes/drug-delivery-list.html'
		})
		.state('app.logistic-users', {
			url : '/logistic-users',
			templateUrl : 'tpl/app/user/logistic-users.html',
			controller: 'LogisticUserController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(	
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-logistic-user.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.logistics', {
			url : '/logistics',
			templateUrl : 'tpl/app/routes/logistic-list.html',
		})
		.state('app.logistic', {
			url : '/logistic/:id',
			templateUrl : 'tpl/app/logistic/logistic.html',
			controller: 'LogisticController',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
								function() {
									return $ocLazyLoad.load( ['js/controllers/crm-logistic.js'] );
								}
						)
					}
				]
			}
		})
		.state('app.drug-disposal-list', {
			url : '/drug-disposal-list',
			templateUrl : 'tpl/app/routes/drug-disposal-list.html'
		})
		.state('app.blood-sampling-list', {
			url : '/blood-sampling-list',
			templateUrl : 'tpl/app/routes/blood-sampling-list.html'
		})
		.state('app.virtual-meeting-list', {
			url : '/virtual-meeting-list',
			templateUrl : 'tpl/app/routes/virtual-meeting-list.html'
		})
		.state('app.virtual-meeting-detail', {
			url : '/virtual-meeting/:id',
			templateUrl : 'tpl/app/routes/virtual-meeting-detail.html',
			controller: function ($scope, $stateParams) {
				$scope.id = $stateParams.id;
			}
		})
		.state('app.dried-blood-spot-test-list', {
			url : '/dbst',
			templateUrl : 'tpl/app/routes/dried-blood-spot-test-list.html',
		})
		.state('app.dried-blood-spot-test-detail', {
			url : '/dbst/:id',
			templateUrl : 'tpl/app/routes/dried-blood-spot-test-detail.html',
			controller: function ($scope, $stateParams) {
				$scope.id = $stateParams.id;
	        }
		})
		.state('app.patient-activities-calendar', {
			url : '/patient-activities-calendar',
			templateUrl : 'tpl/app/calendar/patient-activities-calendar.html',
			controller: 'PatientActivitiesCalendar',
			resolve: {
                deps: ['$ocLazyLoad', 'uiLoad',
                  function( $ocLazyLoad, uiLoad ) {
                    return uiLoad.load(
                      JQ_CONFIG.fullcalendar.concat(['js/controllers/crm-calendar.js'])
                    ).then(
                      function(){
                        return $ocLazyLoad.load(['ui.calendar']);
                      }
                    )
                	
                }]
            }
	    })
	    .state('app.hcpQuestionnaires', {
			url : '/hcp/questionnaires/:hcpId',
			templateUrl : 'tpl/app/hcp/questionnaires.html'
		})
		.state('app.communication-messages', {
			url : '/communication-messages',
			templateUrl : 'tpl/app/communication-message/communication-messages.html'
		})
	    .state('app.patient-enrollment', {
			url : '/patient-enrollment',
			templateUrl : 'tpl/app/patient-enrollment/patient-enrollment-create.html',
			controller: 'PatientEnrollmentDetailCtrl',
			resolve: {
				deps: ['$ocLazyLoad',
					function($ocLazyLoad) {
						return $ocLazyLoad.load(['ui.select','angularFileUpload','smart-table']).then(
							function() {
								return $ocLazyLoad.load( ['js/controllers/crm-patient-enrollment.js'] );
							}
						)
					}
				]
			}
		})
		.state('app.patient-dbs-test-activity', {
			url : '/patient-dbs-test-activity',
			templateUrl : 'tpl/app/routes/patient-dbs-test-activity.html',
		})
		.state('privacy', {
			url : '/privacy',
			templateUrl : 'tpl/disclaimers.html',
			controller: function($scope,  $stateParams, UserDisclaimerPublicService, $state) {
				UserDisclaimerPublicService.getDisclaimersByCode({ code: 'PRIVACY_POLICY' }, {},
					function(result) {
						$scope.disclaimers = result.data;
						if($scope.disclaimers.length == 0) {
							$state.go('access.not-found');
						}
					},
					function(error) {
						$state.go('access.not-found');
					}
				)
			},
			resolve: {
				constants : function($timeout, $q, $rootScope, PortalService) {
					return Constants($timeout, $q, $rootScope, PortalService);
				}
			}
		})
		.state('terms-and-conditions', {
			url : '/terms-and-conditions',
			templateUrl : 'tpl/disclaimers.html',
			controller: function($scope,  $stateParams, UserDisclaimerPublicService, $state) {
				UserDisclaimerPublicService.getDisclaimersByCode({ code: 'TERMS_AND_CONDITIONS' }, {},
					function(result) {
						$scope.disclaimers = result.data;
						if($scope.disclaimers.length == 0) {
							$state.go('access.not-found');
						}
					},
					function(error) {
						$state.go('access.not-found');
					}
				)
			},
			resolve: {
				constants : function($timeout, $q, $rootScope, PortalService) {
					return Constants($timeout, $q, $rootScope, PortalService);
				}
			}
		})
	}
);

var LoggedInfo = function($timeout, $q, $rootScope, $cookies, $state, UserService, PortalService) {
	var d = $q.defer();

	if($cookies.getObject('italiassistenza_ua') !== undefined) {
		var loggedUser = UserService.getLoggedUser(
				{},
				function(response) {
					var loggedUserTmp = response.data;

					UserService.getLoggedUserPortals({}, function(result) {
						$rootScope.userPortals = result.data;
						
						//Cerca i portali collegati a questo (solo per i programs, l'admin non lo fa)
						PortalService.getRelatedPortals(function(result) {
							$rootScope.relatedPortals = result.data;

							PortalService.getAllViewPermissions({}, function(result) {
								loggedUserTmp.viewPermissions = result.data;

								PortalService.get(function(result) {
									$rootScope.constants = result.data;
								
									UserService.getRequiredDisclaimers({}, function(result) {
										
										var userTypes = $rootScope.constants.UserType;
										
										if(result.data.length > 0) {
											$state.go('disclaimer');
											
										} else {
											$rootScope.disclaimer_NO_MEDICAL_DEVICE_WEB = undefined;
											if(userTypes[3].id == loggedUserTmp.type || userTypes[4].id == loggedUserTmp.type ) {
												UserService.getUserDisclaimerByCode({code: 'NO_MEDICAL_DEVICE_WEB'},  function(result) {
													$rootScope.disclaimer_NO_MEDICAL_DEVICE_WEB = result.data;
													
													$rootScope.loggedUser = loggedUserTmp;
													d.resolve(loggedUser);
													
												}, function(error) {
													$rootScope.loggedUser = loggedUserTmp;
													d.resolve(loggedUser);
												});
												
											} else {
												$rootScope.loggedUser = loggedUserTmp;
												d.resolve(loggedUser);
											}
											
										}
									
									}, function(error) {
										d.resolve(undefined);
									});
								}, function(error) {
									d.resolve(undefined);
								});
							}, function(error) {
								d.resolve(undefined);
							});
						}, function(error) {
							d.resolve(undefined);
						});
					},function(error) {
						d.resolve(undefined);
					});
				}, function(error) {
					d.resolve(undefined);
				}
		);
	}
	else {
		d.resolve(undefined);
	}
	return d.promise;
}

var Constants = function($timeout, $q, $rootScope, PortalService) {
	var d = $q.defer();
	var constants = PortalService.get(
		function(result) {
			$rootScope.constants = result.data;
			$rootScope.loggedUser = undefined;
	
			d.resolve(constants);
		}, function(error) {
			d.resolve(undefined);
		}
	);
	return d.promise;
}