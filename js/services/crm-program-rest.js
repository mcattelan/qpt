'use strict';

app.factory('SupplierService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var SupplierService = $resource(RESTURL.SUPPLIER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.SUPPLIER + '/search'
		},
		update: {
			method: 'PUT'
		},
		getAllUser: {
			method : 'GET',
			url: RESTURL.SUPPLIER + '/:id/user'
		},
		searchAllUser: {
			method: 'POST',
			url: RESTURL.SUPPLIER + '/:id/user/search'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.SUPPLIER
		},
		isMedicalServiceSupport: {
			method: 'GET',
			url: RESTURL.SUPPLIER + '/medical-support-service'
		}
	});
	SupplierService.getUrl = function() {
		return ''+RESTURL.SUPPLIER;
	}
	return SupplierService;
}]);

app.factory('SupplierUserService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var SupplierUserService = $resource(RESTURL.SUPPLIER_USER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.SUPPLIER_USER + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER
		},
		update: {
			method: 'PUT',
			url: RESTURL.SUPPLIER_USER + '/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.SUPPLIER_USER + '/:id'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.SUPPLIER_USER
		}
	});
	SupplierUserService.getUrl = function() {
		return ''+RESTURL.SUPPLIER_USER;
	}
	return SupplierUserService;
}]);

app.factory('StatsService', function($resource, RESTURL) {
	return $resource(RESTURL.STATS , {}, {
		search: {
			url: RESTURL.STATS + '/search',
			method: 'POST'
		},
		searchNoUser: {
			url: RESTURL.STATS + '/search-no-user',
			method: 'POST'
		}
	});

});	

app.factory('PatientService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var PatientService =  $resource(RESTURL.PATIENT + '/:id', { id: '@id', delegateIndex: '@delegateIndex' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/search'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.PATIENT
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		},
		delete: {
			method: 'DELETE'
		},
		searchAllUser: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:id/user/search'
		},
		getDelegate: {
			method: 'GET',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		markDelegate: {
			method: 'POST',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		deleteDelegate: {
			method: 'DELETE',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		getCaregiver:{
			method: 'GET',
			url : RESTURL.PATIENT + '/:id/caregiver/'			
		},
		markCaregiver: {
			method: 'POST',
			url : RESTURL.PATIENT + '/:id/caregiver/'
		},
		deleteCaregiver: {
			method: 'DELETE',
			url : RESTURL.PATIENT + '/:id/caregiver/'
		},
		activate: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/activate'
		},
		suspend: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/suspend'
		},
		terminate: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/terminate'
		},
		updateAuthorizationKeys:{
			method: 'PUT',
			url: RESTURL.PATIENT + '/authorization-keys/:id'
		},
		activateNotification: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/activate-notification'
		},
		inviteActivationAll:{
			method: 'POST',
			url: RESTURL.PATIENT + '/search/invite-all'
		},
		countSearchInvite: {
			method: 'POST',
			url: RESTURL.PATIENT + '/search/count'
		},
		updateAvatar: {
			method: 'PUT',
			url: RESTURL.PATIENT + '/:id/avatar'
		},
		searchApprovals: {
			method: 'GET',
			url: RESTURL.PATIENT + '/approvals'
		},
		delegateHcpsActive: {
			method: 'PUT',
			url: RESTURL.PATIENT +'/delegate-hcp-active'
		}
	});
	PatientService.getUrl = function() {
		return ''+RESTURL.PATIENT;
	}
	return PatientService;
}]);

app.factory('PatientUserService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_USER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_USER + '/search'
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_USER + '/:id'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT_USER
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.PATIENT_USER + '/:id'
		}
	});
});

app.factory('InternalStaffUserService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var InternalStaffUserService = $resource(RESTURL.INTERNALSTAFFUSER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.INTERNALSTAFFUSER + '/search'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.INTERNALSTAFFUSER
		}
	});
	InternalStaffUserService.getUrl = function() {
		return ''+RESTURL.INTERNALSTAFFUSER;
	}
	return InternalStaffUserService;
}]);

app.factory('DeviceTypeService', function($resource, RESTURL) {
	return $resource(RESTURL.DEVICE_TYPE + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		getAll: {
			method: 'GET',
			url: RESTURL.DEVICE_TYPE
		},
		
	});
});

app.factory('HcpService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var HcpService =  $resource(RESTURL.HCP + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.HCP + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		},
		delete: {
			method: 'DELETE'
		},
		activate: {
			method: 'PUT',
			url : RESTURL.HCP + '/:id/activate'
		},
		suspend: {
			method: 'PUT',
			url : RESTURL.HCP + '/:id/suspend'
		},
		terminate: {
			method: 'PUT',
			url : RESTURL.HCP + '/:id/terminate'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.HCP
		},
		updateAuthorizationKeys:{
			method: 'PUT',
			url: RESTURL.HCP + '/authorization-keys/:id'
		},
		activateNotification: {
			method: 'PUT',
			url : RESTURL.HCP + '/:id/activate-notification'
		},
		inviteActivationAll:{
			method: 'POST',
			url: RESTURL.HCP + '/search/invite-all'
		},
		countSearchInvite: {
			method: 'POST',
			url: RESTURL.HCP + '/search/count'
		},
		searchApprovals: {
			method: 'GET',
			url: RESTURL.HCP + '/approvals'
		}
	});
	HcpService.getUrl = function() {
		return ''+RESTURL.HCP;
	}
	return HcpService;
}]);

app.factory('MedicalCenterService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var MedicalCenterService =  $resource(RESTURL.MEDICALCENTER + '/:id', { id: '@id', departmentId: '@departmentId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.MEDICALCENTER + '/search'
		},
		update: {
			method: 'PUT'
		},
		getAllDepartment: {
			method: 'GET',
			url: RESTURL.MEDICALCENTER + '/:id/department'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.MEDICALCENTER
		}
	});
	MedicalCenterService.getUrl = function() {
		return ''+RESTURL.MEDICALCENTER;
	}
	return MedicalCenterService;
}]);

app.factory('DepartmentService', function($resource, RESTURL) {
	return $resource(RESTURL.DEPARTMENT + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DEPARTMENT + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.DEPARTMENT
		},
		update: {
			method: 'PUT',
			url: RESTURL.DEPARTMENT +'/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('PharmacyService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var PharmacyService = $resource(RESTURL.PHARMACY + '/:id', { id: '@id', patientId: '@patientId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PHARMACY + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PHARMACY
		},
		update: {
			method: 'PUT',
			url: RESTURL.PHARMACY +'/:id'
		},
		delete: {
			method: 'DELETE'
		},
		activate: {
			method: 'PUT',
			url:  RESTURL.PHARMACY +'/:id/activate'
		},
		terminate: {
			method: 'PUT',
			url:  RESTURL.PHARMACY +'/:id/terminate'
		},
		getPatientActivePharmacy: {
			method: 'POST',
			url:  RESTURL.PHARMACY +'/patient/:patientId/active'
		}
	});
	PharmacyService.getUrl = function() {
		return ''+RESTURL.PHARMACY;
	}
	return PharmacyService;
}]);

app.factory('DiseaseService', function($resource, RESTURL) {
	return $resource(RESTURL.DISEASE + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DISEASE + '/search'
		},
		update: {
			method: 'PUT'
		}
	});
});

app.factory('PatientTherapyService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_THERAPY + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_THERAPY + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		}
	});
});

app.factory('PatientTherapyDrugService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT + '/:patientId/therapy-drug/:id', { id: '@id', patientId : '@patientId' , assumptionId: '@assumptionId'}, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/therapy-drug/search'
		},
		getInjectionAreaGroup: {
			method: 'GET',
			url: RESTURL.PATIENT + '/:patientId/therapy-drug/:id/injection-areas/assumption/:assumptionId'
		}
	});
});

app.factory('PatientAssumptionService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT + '/:patientId/assumption/:id', { id: '@id', patientId : '@patientId'  }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/assumption/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/assumption'
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT +'/:patientId/assumption/:id'
		},
		delete: {
			method: 'DELETE'
		},
		searchDetection: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/assumption/search/detection'
		}
	});
});

app.factory('PatientLeftoverDrugService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT + '/:patientId/leftover-drug/:id', { id: '@id', patientId : '@patientId'  }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/leftover-drug/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/leftover-drug'
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT +'/:patientId/leftover-drug/:id'
		},
		delete: {
			method: 'DELETE'
		},
		searchDetection: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/leftover-drug/search/detection'
		}
	});
});


app.factory('PatientMedicalExaminationService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT + '/:patientId/medical-examination/:id', { id: '@id', patientId : '@patientId'  }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:patientId/medical-examination/search'
		},
		allByPatientId: {
			method: 'GET',
			url: RESTURL.PATIENT + '/:patientId/medical-examination'
		}
	});
});

app.factory('TherapyService', function($resource, RESTURL) {
	return $resource(RESTURL.THERAPY + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.THERAPY + '/search'
		}
	});
});

app.factory('CustomerService', function($resource, RESTURL) {
	return $resource(RESTURL.CUSTOMER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.CUSTOMER + '/search'
		},
		update: {
			method: 'PUT'
		}
	});
});

app.factory('ProgramService', function($resource, RESTURL) {
	return $resource(RESTURL.PROGRAM_PROPERTY, { }, {
		get: {
			method: 'GET',
			url: RESTURL.PROGRAM_PROPERTY
		}
	});
});

app.factory('ServiceConfService', function($resource, RESTURL) {
	return $resource(RESTURL.SERVICE_CONF, { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.SERVICE_CONF
		},
		getService: {
			method: 'GET',
			url: RESTURL.SERVICE_CONF + '/:id'
		}
	});
});

app.factory('UserServiceConfService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_SERVICE_CONF, { id : '@id', userId : '@userId', masterActivityId: '@masterActivityId', uniqueKey: '@uniqueKey', serviceId: '@serviceId', selectectServiceConfId: '@selectectServiceConfId'}, {
		get: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.USER_SERVICE_CONF + '/search'
		},
		update: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id'
		},
		searchByUser: {
			method: 'POST', 
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/search'
		},
		activate: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/activate'
		},
		suspend: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/suspend'
		},
		terminate: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/terminate'
		},
		switchToAlternatives: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/switch-to-alternatives'
		},
		start: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/start'
		},
		getUserManualService: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/manual-services'
		},
		getUserManualMasterActivities: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/manual-activities'
		},
		startUserMasterActivityManually: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/activity/:masterActivityId/start-manually'
		},
		checkServices: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/check-services'
		},
		getDependingServices: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/:id/depending-services/:uniqueKey'
		},
		getUserDependingServicesGroups: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/depending-services-groups'
		},
		startUserServiceManually: {
			method: 'POST',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/service/:serviceId/start-manually'
		},
		updateDependingService: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/depending-services'
		},
		getServiceActivityGroups: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/activity-groups'
		},
		getServiceActivityGroupsForUser: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/activity-groups/user/:userId'
		},
		getServiceConfMasterActivityName: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/service-conf-master-activity-name'
		}
	});
});

app.factory('UserMasterActivityConfService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_MASTER_ACTIVITY_CONF, { id : '@id'}, {
		get: {
			method: 'GET',
			url: RESTURL.USER_MASTER_ACTIVITY_CONF + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.USER_MASTER_ACTIVITY_CONF + '/search'
		},
		update: {
			method: 'PUT',
			url: RESTURL.USER_MASTER_ACTIVITY_CONF + '/:id'
		}
	});
});

app.factory('UserActivityService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_ACTIVITY, { id : '@id', userId : '@userId', token: '@token', groupCode: '@groupCode' }, {
		get: {
			method: 'GET',
			url: RESTURL.USER_ACTIVITY + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.USER_ACTIVITY + '/search'
		},
		searchByUser: {
			method: 'POST',
			url: RESTURL.USER_ACTIVITY + '/user/:userId/search'
		},
		acceptServiceOrder: {
			method: 'GET',
			url: RESTURL.USER_ACTIVITY + '/service-order/accept/:token'
		},
		rejectServiceOrder: {
			method: 'GET',
			url: RESTURL.USER_ACTIVITY + '/service-order/reject/:token'
		},
		getActivityGroups: {
			method: 'GET',
			url: RESTURL.USER_ACTIVITY + '/groups/:groupCode'
		},
		getActivityGroupsForUser: {
			method: 'GET',
			url: RESTURL.USER_ACTIVITY + '/groups/:groupCode/user/:userId'
		},
		completeActivityNoResult: {
			method: 'POST',
			url: RESTURL.USER_ACTIVITY + '/:id/activity-no-result/complete'
		},
		changeActivityNoResult: {
			method: 'POST',
			url: RESTURL.USER_ACTIVITY + '/:id/activity-no-result/change'
		}
	});
});


app.factory('DocumentCategoryService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.DOCUMENT_CATEGORY + '/:userId', {userId: '@userId'}, {
		get : {
			method: 'GET',
			url: RESTURL.DOCUMENT_CATEGORY +'/:id'
		},
		getAll : {
			method: 'GET',
			url: RESTURL.DOCUMENT_CATEGORY
		},
		search : {
			method: 'POST',
			url: RESTURL.DOCUMENT_CATEGORY + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.DOCUMENT_CATEGORY
		},
		update: {
			method: 'PUT',
			url: RESTURL.DOCUMENT_CATEGORY +'/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
}]);

app.factory('CustomerUserService', function($resource, RESTURL) {
	return $resource(RESTURL.CUSTOMER_USER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.CUSTOMER_USER + '/search'
		},
		update: {
			method: 'PUT'
		}
	});
});

app.factory('CountryService', function($resource, RESTURL) {
	return $resource(RESTURL.TERRITORY + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.TERRITORY + '/search-country'
		},
		searchRegion: {
			method: 'POST',
			url: RESTURL.TERRITORY + '/search-region'
		}
	});
});

app.factory('DynamicPdfService', function($resource, RESTURL) {
	return $resource(RESTURL.DYNAMIC_PDF + '/:templateName', { templateName: '@templateName', type: '@type', url: '@url', filename: '@filename' }, {
		generate: {
			method: 'POST',
			url: RESTURL.DYNAMIC_PDF + '/:templateName/generate'
		},
		generatePdf: {
			method: 'POST',
			url: RESTURL.DYNAMIC_PDF + '/:type/generate'
		},
		download: {
			method: 'GET',
			url: RESTURL.DYNAMIC_PDF + '/:type/download/:url/:filename'
		}
	});
});


/*
 * app.factory('AvailabilityService',function($resource,RESTURL){ return
 * $resource('', { id: '@id' }, { get: {} }); });
 */

app.factory('AvailabilityService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.AVAILABILITIES, {userId: '@userId'}, {
		'search': {
			method: 'POST',
			url: RESTURL.AVAILABILITIES + '/search'
		},
		'searchForUser': {
			method: 'POST',
			url: RESTURL.AVAILABILITIES + '/:userId/search'
		},
		'create': {
			method: 'POST',
			url: RESTURL.AVAILABILITIES + '/:userId'
		},
		'update': {
			method: 'PUT',
			url: RESTURL.AVAILABILITIES + '/:userId'
		},
		'delete': {
			method: 'PUT',
			url: RESTURL.AVAILABILITIES + '/:userId/delete'
		}
	});
}]);

app.factory('StoredExportService', function($resource, RESTURL) {
	return $resource(RESTURL.STORED_EXPORT + '/:id', { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.STORED_EXPORT+ '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.STORED_EXPORT + '/search'
		},
		generate: {
			method: 'POST',
			url: RESTURL.STORED_EXPORT
		},
		download : {
			method: 'GET',
			url: RESTURL.STORED_EXPORT +'/:id/url'
		},
		deleteDocument : {
			method: 'DELETE',
			url: RESTURL.STORED_EXPORT + '/:id'
		},
		generateForDownload: {
			method: 'POST',
			url: RESTURL.STORED_EXPORT + '/generate-for-download/:exportType'
		},
		generateCheck: {
			method: 'POST',
			url: RESTURL.STORED_EXPORT + '/generate-check'
		},
		downloadByType: {
			method: 'GET',
			url: RESTURL.STORED_EXPORT + '/:type/download/:url/:filename'
		}
	});
});

app.factory('UserMessageService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_MESSAGE + '/:id', { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.USER_MESSAGE+ '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.USER_MESSAGE + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.USER_MESSAGE
		},
		update: {
			method: 'PUT',
			url: RESTURL.USER_MESSAGE + '/:id'
		},
		send:{
			method: 'PUT',
			url: RESTURL.USER_MESSAGE + '/:id/send'
		},
		read:{
			method: 'PUT',
			url: RESTURL.USER_MESSAGE + '/:id/read'
		},
		unreadMessagesNumber:{
			method: 'GET',
			url: RESTURL.USER_MESSAGE+ '/unread-message-number'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.USER_MESSAGE + '/:id'
		}
	});
});

app.factory('CommunicationMessageService', function($resource, RESTURL) {
	return $resource(RESTURL.COMMUNICATION_MESSAGE + '/:id', { id: '@id' }, {
		get: {
			url: RESTURL.COMMUNICATION_MESSAGE + '/:id',
			method: 'GET'
		},
		getAll: {
			url: RESTURL.COMMUNICATION_MESSAGE,
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.COMMUNICATION_MESSAGE + '/search'
		},
		
	});
});

app.factory('NationalityService', function($resource, RESTURL) {
	return $resource(RESTURL.NATIONALITY + '/:id', { id: '@id' }, {
		getAll: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.NATIONALITY + '/search'
		},
		
	});
});

app.factory('QuestionnaireService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	return $resource(RESTURL.QUESTIONNAIRE + '/:id', {id: '@id'}, {
		get : {
			method: 'GET',
			url: RESTURL.QUESTIONNAIRE +'/:id'
		},
		getAll : {
			method: 'GET',
			url: RESTURL.QUESTIONNAIRE
		},
		search : {
			method: 'POST',
			url: RESTURL.QUESTIONNAIRE + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.QUESTIONNAIRE
		},
		update: {
			method: 'PUT',
			url: RESTURL.QUESTIONNAIRE +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.QUESTIONNAIRE +'/:id'
		}
	});
}]);

app.factory('UserQuestionnaireService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var UserQuestionnaireService = $resource(RESTURL.USERQUESTIONNAIRE + '/:id/:activityId', {id: '@id',activityId: '@activityId'}, {
		get : {
			method: 'GET',
			url: RESTURL.USERQUESTIONNAIRE +'/:id/:activityId'
		},
		search : {
			method: 'POST',
			url: RESTURL.USERQUESTIONNAIRE + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.USERQUESTIONNAIRE
		},
		update: {
			method: 'PUT',
			url: RESTURL.USERQUESTIONNAIRE +'/:id/:activityId'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.USERQUESTIONNAIRE +'/:id/:activityId'
		}
	});
	UserQuestionnaireService.getUrl = function() {
		return ''+RESTURL.USERQUESTIONNAIRE;
	}
	return UserQuestionnaireService;
}]);

app.factory('PharmacoVigilanceEventService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var PharmacoVigilanceEventService = $resource(RESTURL.PHARMACOVIGILANCEEVENT + '/:id', {id: '@id', formType: '@formType'}, {
		get : {
			method: 'GET',
			url: RESTURL.PHARMACOVIGILANCEEVENT +'/:id'
		},
		search : {
			method: 'POST',
			url: RESTURL.PHARMACOVIGILANCEEVENT + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PHARMACOVIGILANCEEVENT
		},
		update: {
			method: 'PUT',
			url: RESTURL.PHARMACOVIGILANCEEVENT +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.PHARMACOVIGILANCEEVENT +'/:id'
		},
		generateForm: {
			method: 'GET',
			url: RESTURL.PHARMACOVIGILANCEEVENT +'/:id/generate-form/:formType'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.PHARMACOVIGILANCEEVENT + '/search/export-xls'
		},
	});
	PharmacoVigilanceEventService.getUrl = function() {
		return ''+RESTURL.PHARMACOVIGILANCEEVENT;
	}
	return PharmacoVigilanceEventService;
}]);

app.factory('MaterialDeliveryService', function($resource, RESTURL) {
	return $resource(RESTURL.MATERIAL_DELIVERY + '/:id', { id: '@id' }, {
		get: {
			url: RESTURL.MATERIAL_DELIVERY + '/:id',
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.MATERIAL_DELIVERY + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.MATERIAL_DELIVERY
		},
		update: {
			method: 'PUT',
			url: RESTURL.MATERIAL_DELIVERY +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.MATERIAL_DELIVERY +'/:id'
		},
		
	});
});

app.factory('EntityCustomPropertyConfService', function($resource, RESTURL) {
return $resource(RESTURL.ENTITY_CUSTOM_PROPERTY_CONF + '/:id', { id: '@id' }, {
	
	search: {
		method: 'POST',
		url: RESTURL.ENTITY_CUSTOM_PROPERTY_CONF + '/search'
	},

	
	});
});

app.factory('MaterialDamageService', function($resource, RESTURL) {
	return $resource(RESTURL.MATERIAL_DAMAGE + '/:id', { id: '@id' }, {
		get: {
			url: RESTURL.MATERIAL_DAMAGE + '/:id',
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.MATERIAL_DAMAGE + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.MATERIAL_DAMAGE
		},
		update: {
			method: 'PUT',
			url: RESTURL.MATERIAL_DAMAGE +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.MATERIAL_DAMAGE +'/:id'
		},
		
	});
});

app.factory('PatientPharmacyService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_PHARMACY + '/:id', { id: '@id', patientId: '@patientId', patientPharmacyId: '@patientPharmacyId' }, {
		get: {
			url: RESTURL.PATIENT_PHARMACY + '/:id',
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_PHARMACY + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT_PHARMACY
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_PHARMACY +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.PATIENT_PHARMACY +'/:id'
		},
		updateAllPatientPharmacy: {
			method: 'PUT',
			url: RESTURL.PATIENT_PHARMACY +'/patient/:patientId/update-all'
		},
		updateReferencePatientPharmacy: {
			method: 'PUT',
			url: RESTURL.PATIENT_PHARMACY +'/:patientPharmacyId/update-reference'
		},
		searchReferencePatientPharmacy: {
			method: 'POST',
			url: RESTURL.PATIENT_PHARMACY + '/:patientId/search-reference-pharmacy'
		},
	});
});

app.factory('MaterialService', function($resource, RESTURL) {
	return $resource(RESTURL.MATERIAL + '/:id', { id: '@id' }, {
		get: {
			url: RESTURL.MATERIAL + '/:id',
			method: 'GET'
		},
		getAll: {
			url: RESTURL.MATERIAL,
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.MATERIAL + '/search'
		},
		
	});
});

app.factory('TrainingSubjectService', function($resource, RESTURL) {
	return $resource(RESTURL.TRAINING_SUBJECT + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		delete: {
			method: 'DELETE'
		},
		search: {
			method: 'POST',
			url: RESTURL.TRAINING_SUBJECT + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		}
	});
});

app.factory('LoggedSmsService', function($resource, RESTURL) {
	return $resource(RESTURL.LOGGED_SMS + '/:id', { id: '@id' }, {
		get: {
			url: RESTURL.LOGGED_SMS + '/:id',
			method: 'GET'
		},
		getAll: {
			url: RESTURL.LOGGED_SMS,
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.LOGGED_SMS + '/search'
		},
		
	});
});

app.factory('TrainingSessionService', function($resource, RESTURL) {
	return $resource(RESTURL.TRAINING_SESSION + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		delete: {
			method: 'DELETE'
		},
		search: {
			method: 'POST',
			url: RESTURL.TRAINING_SESSION + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		}
	});
});
app.factory('BloodDrawingCenterService', function($resource, RESTURL) {
	return $resource(RESTURL.BLOOD_DRAWING_CENTER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.BLOOD_DRAWING_CENTER + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.BLOOD_DRAWING_CENTER
		},
		update: {
			method: 'PUT',
			url: RESTURL.BLOOD_DRAWING_CENTER +'/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('PatientBloodDrawingCenterService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_BLOOD_DRAWING_CENTER + '/:id', { id: '@id', patientId: '@patientId', patientBloodDrawingCenterId: '@patientBloodDrawingCenterId' }, {
		get: {
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER + '/:id',
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER +'/:patientBloodDrawingCenterId'
		},
		updateAllPatientBloodDrawingCenter: {
			method: 'PUT',
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER +'/patient/:patientId/update-all'
		},
		updateReferencePatientBloodDrawingCenter: {
			method: 'PUT',
			url: RESTURL.PATIENT_BLOOD_DRAWING_CENTER +'/:patientBloodDrawingCenterId/update-reference'
		}
	});
});
app.factory('DrugPickUpService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var DrugPickUpService = $resource(RESTURL.DRUG_PICK_UP + '/:id', { id: '@id', complementaryId : '@complementaryId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_PICK_UP + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DRUG_PICK_UP + '/:id'
		},
		delete: {
			method: 'DELETE'
		},
		getWithDefaults: {
			method: 'GET',
			url: RESTURL.DRUG_PICK_UP + '/:complementaryId/with-defaults'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.DRUG_PICK_UP
		},
		getByUserComplementaryId: {
			method: 'GET',
			url: RESTURL.DRUG_PICK_UP + '/:complementaryId/by-user-complementary-id'
		},
	});
	DrugPickUpService.getUrl = function() {
		return ''+RESTURL.DRUG_PICK_UP;
	}
	return DrugPickUpService;
}]);

app.factory('DrugPickUpItemService', function($resource, RESTURL) {
	return $resource(RESTURL.DRUG_PICK_UP_ITEM + '/:id', { id: '@id'}, {
		get: {
			method: 'GET' 
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_PICK_UP_ITEM + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DRUG_PICK_UP_ITEM + '/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('DrugDeliveryService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var DrugDeliveryService = $resource(RESTURL.DRUG_DELIVERY + '/:id', { id: '@id', complementaryId : '@complementaryId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_DELIVERY + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DRUG_DELIVERY + '/:id'
		},
		delete: {
			method: 'DELETE'
		},
		getWithDefaults: {
			method: 'GET',
			url: RESTURL.DRUG_DELIVERY + '/:complementaryId/with-defaults'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.DRUG_DELIVERY
		},
		getByUserComplementaryId: {
			method: 'GET',
			url: RESTURL.DRUG_PICK_UP + '/:complementaryId/by-user-complementary-id'
		}
	});
	DrugDeliveryService.getUrl = function() {
		return ''+RESTURL.DRUG_DELIVERY;
	}
	return DrugDeliveryService;
}]);

app.factory('DrugDeliveryItemService', function($resource, RESTURL) {
	return $resource(RESTURL.DRUG_DELIVERY_ITEM + '/:id', { id: '@id'}, {
		get: {
			method: 'GET' 
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_DELIVERY_ITEM + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DRUG_DELIVERY_ITEM + '/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('LogisticService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var LogisticService = $resource(RESTURL.LOGISTIC + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.LOGISTIC + '/search'
		},
		update: {
			method: 'PUT'
		},
		getAllUser: {
			method : 'GET',
			url: RESTURL.LOGISTIC + '/:id/user'
		},
		searchAllUser: {
			method: 'POST',
			url: RESTURL.LOGISTIC + '/:id/user/search'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.LOGISTIC
		}
	});
	LogisticService.getUrl = function() {
		return ''+RESTURL.LOGISTIC;
	}
	return LogisticService;
}]);

app.factory('LogisticUserService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var LogisticUserService = $resource(RESTURL.LOGISTIC_USER + '/:id', { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.LOGISTIC_USER + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.LOGISTIC_USER + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.LOGISTIC_USER
		},
		update: {
			method: 'PUT',
			url: RESTURL.LOGISTIC_USER + '/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.LOGISTIC_USER + '/:id'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.LOGISTIC_USER
		}
	});
	LogisticUserService.getUrl = function() {
		return ''+RESTURL.LOGISTIC_USER;
	}
	return LogisticUserService;
}]);

app.factory('DrugDisposalService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var DrugDisposalService = $resource(RESTURL.DRUG_DISPOSAL + '/:id', { id: '@id', complementaryId : '@complementaryId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_DISPOSAL + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DRUG_DISPOSAL + '/:id'
		},
		delete: {
			method: 'DELETE'
		},
		getWithDefaults: {
			method: 'GET',
			url: RESTURL.DRUG_DISPOSAL + '/:complementaryId/with-defaults'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.DRUG_DISPOSAL
		}
	});
	DrugDisposalService.getUrl = function() {
		return ''+RESTURL.DRUG_DISPOSAL;
	}
	return DrugDisposalService;
}]);

app.factory('DrugDisposalItemService', function($resource, RESTURL) {
	return $resource(RESTURL.DRUG_DISPOSAL_ITEM + '/:id', { id: '@id'}, {
		get: {
			method: 'GET' 
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_DISPOSAL_ITEM + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.DRUG_DISPOSAL_ITEM + '/:id'
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('PatientTemporaryHomeAddressService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var PatientTemporaryHomeAddressService = $resource(RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS + '/:id', { id: '@id', patientId: '@patientId'}, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS + '/:id'
		},
		delete: {
			method: 'DELETE'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS
		},
		confirm: {
			method: 'PUT',
			url: RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS + '/:id/confirm'
		}
	});
	PatientTemporaryHomeAddressService.getUrl = function() {
		return ''+RESTURL.PATIENT_TEMPORARY_HOME_ADDRESS;
	}
	return PatientTemporaryHomeAddressService;
}]);

app.factory('BloodSamplingService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var BloodSamplingService = $resource(RESTURL.BLOOD_SAMPLING + '/:id', { id: '@id', complementaryId : '@complementaryId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.BLOOD_SAMPLING + '/search'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.BLOOD_SAMPLING + '/:id'
		},
		delete: {
			method: 'DELETE'
		},
		getWithDefaults: {
			method: 'GET',
			url: RESTURL.BLOOD_SAMPLING + '/:complementaryId/with-defaults'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.BLOOD_SAMPLING
		}
	});
	BloodSamplingService.getUrl = function() {
		return ''+RESTURL.BLOOD_SAMPLING;
	}
	return BloodSamplingService;
}]);

app.factory('PatientWeightService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_WEIGHT + '/:id', { id: '@id', userComplementaryActivityId: '@userComplementaryActivityId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_WEIGHT + '/search'
		},
		countSearch: {
			method: 'POST',
			url: RESTURL.PATIENT_WEIGHT + '/search/count'
		},
		insert: {
			method: 'POST'
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_WEIGHT + '/:id'
		},
		delete: {
			method: 'DELETE'
		},
		getByUserComplementaryActivity: {
			method: 'GET',
			url: RESTURL.PATIENT_WEIGHT + '/user-complementary-activity/:userComplementaryActivityId'
		}
	});
});

app.factory('PharmacyTrainingSessionService', function($resource, RESTURL) {
	return $resource(RESTURL.PHARMACY_TRAINING_SESSION + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PHARMACY_TRAINING_SESSION + '/search'
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PHARMACY_TRAINING_SESSION
		},
		delete: {
			method: 'DELETE'
		}
	});
});

app.factory('DrugService', function($resource, RESTURL) {
	return $resource(RESTURL.DRUG + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG + '/search'
		},
	});
});

app.factory('PersonalEventTypeService', function($resource, RESTURL) {
	return $resource(RESTURL.PERSONAL_EVENT_TYPE + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PERSONAL_EVENT_TYPE + '/search'
		}
	});
});


app.factory('PersonalEventService', function($resource, RESTURL) {
	return $resource(RESTURL.PERSONAL_EVENT + '/:id', { id: '@id', userId: '@userId', operationType: '@operationType' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PERSONAL_EVENT + '/:userId/events/search'
		},
		update: {
			method: 'PUT',
			url: RESTURL.PERSONAL_EVENT +'/:id'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PERSONAL_EVENT 
		},
		delete: {
			method: 'PUT',
			url: RESTURL.PERSONAL_EVENT +'/:id/delete'
		}
	});
});

app.factory('AvatarService', function($resource, RESTURL) {
	return $resource(RESTURL.AVATAR + '/:id', { id: '@id' }, {
		search: {
			method: 'POST',
			url: RESTURL.AVATAR + '/search'
		}
	});
});

app.factory('UserEventService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_EVENT + '/:id', { id: '@id' }, {
		search: {
			method: 'POST',
			url: RESTURL.USER_EVENT + '/search'
		}
	});
});

app.factory('DrugPickUpDeliveryService', function($resource, RESTURL) {
	return $resource(RESTURL.DRUG_PICK_UP_DELIVERY + '/:complementaryId', { complementaryId: '@complementaryId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DRUG_PICK_UP_DELIVERY + '/search'
		}
	});
});

app.factory('DefaultActivityValueService', function($resource, RESTURL) {
	return $resource(RESTURL.DEFAULT_ACTIVITY_VALUE + '/:id', { id: '@id'}, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DEFAULT_ACTIVITY_VALUE + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.DEFAULT_ACTIVITY_VALUE
		},
		update: {
			method: 'PUT',
			url: RESTURL.DEFAULT_ACTIVITY_VALUE +'/:id'
		},
		delete: {
			method: 'DELETE'
		},
	});
});

app.factory('MasterActivityConfService', function($resource, RESTURL) {
	return $resource(RESTURL.MASTER_ACTIVITY_CONF, {}, {
		search: {
			method: 'POST',
			url: RESTURL.MASTER_ACTIVITY_CONF + '/search'
		},
	});
});

app.factory('PatientMaterialService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_MATERIAL + '/:id', { id: '@id'}, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_MATERIAL + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT_MATERIAL
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_MATERIAL +'/:id'
		},
		delete: {
			method: 'DELETE'
		},
	});
});

app.factory('PatientMaterialHandlingService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_MATERIAL_HANDLING + '/:id', { id: '@id'}, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_MATERIAL_HANDLING + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURL.PATIENT_MATERIAL_HANDLING
		},
		update: {
			method: 'PUT',
			url: RESTURL.PATIENT_MATERIAL_HANDLING +'/:id'
		},
		delete: {
			method: 'DELETE'
		},
	});
});

app.factory('UserComplementaryActivityService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_COMPLEMENTARY_ACTIVITY, { id : '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.USER_COMPLEMENTARY_ACTIVITY + '/:id'
		},
	});
});


app.factory('NotificationConfService', function($resource, RESTURL) {
	return $resource(RESTURL.NOTIFICATION_CONF, { id : '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.NOTIFICATION_CONF + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.NOTIFICATION_CONF + '/search'
		},
	});
});

app.factory('DriedBloodSpotTestService', function($resource, RESTURL) {
	var DriedBloodSpotTestService = $resource(RESTURL.DRIED_BLOOD_SPOT_TEST + '/:id', { id: '@id', patientId: '@patientId' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.DRIED_BLOOD_SPOT_TEST + '/search'
		},
		getDBSTestToAssignForPickUp: {
			method : 'GET',
			url: RESTURL.DRIED_BLOOD_SPOT_TEST + '/:patientId/for-pick-up'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.DRIED_BLOOD_SPOT_TEST
		}
	});
	DriedBloodSpotTestService.getUrl = function() {
		return ''+RESTURL.DRIED_BLOOD_SPOT_TEST;
	}
	return DriedBloodSpotTestService;
});

app.factory('VirtualMeetingService', function($resource, RESTURL) {
	return $resource(RESTURL.VIRTUAL_MEETING, { id : '@id' }, {
		get: {
			method: 'GET',
			url: RESTURL.VIRTUAL_MEETING + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.VIRTUAL_MEETING + '/search'
		},
		insert: { //richiedi incontro
			method: 'POST',
			url: RESTURL.VIRTUAL_MEETING
		},
		organize: {
			method: 'POST',
			url: RESTURL.VIRTUAL_MEETING + '/organize'
		},
		getQualifications: {
			method: 'GET',
			url: RESTURL.VIRTUAL_MEETING + '/:id/available-qualifications'
		},
		getAllQualifications: {
			method: 'GET',
			url: RESTURL.VIRTUAL_MEETING + '/available-qualifications'
		},
		update: {
			method: 'PUT',
			url: RESTURL.VIRTUAL_MEETING +'/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL.VIRTUAL_MEETING + '/:id'
		}
	});
});
app.factory('GatewayHcpService', function($resource, RESTURL) {
	return $resource(RESTURL.GATEWAY_HCP, { id : '@id' }, {
		search: {
			method: 'POST',
			url: RESTURL.GATEWAY_HCP + '/search'
		},
		activateNotification: {
			method: 'PUT',
			url: RESTURL.GATEWAY_HCP + '/:id/activate-notification'
		}
	});
});
app.factory('GatewayUserService', function($resource, RESTURL) {
	return $resource(RESTURL.GATEWAY_USER, { }, {
		bindUsers: {
			method: 'POST',
			url: RESTURL.GATEWAY_USER + '/bind-users'
		},
	});
});
app.factory('GatewayPatientService', function($resource, RESTURL) {
	return $resource(RESTURL.GATEWAY_PATIENT, { }, {
		search: {
			method: 'POST',
			url: RESTURL.GATEWAY_PATIENT + '/search'
		},
	});
});
app.factory('GatewayDrugService', function($resource, RESTURL) {
	return $resource(RESTURL.GATEWAY_DRUG, { }, {
		search: {
			method: 'POST',
			url: RESTURL.GATEWAY_DRUG + '/search'
		},
	});
});
app.factory('GatewayUserActivityService', function($resource, RESTURL) {
	return $resource(RESTURL.GATEWAY_ACTIVITY + '/:id', { id: '@id' }, {
		search: {
			method: 'POST',
			url: RESTURL.GATEWAY_ACTIVITY + '/search'
		},
		countSearch: {
			method: 'POST',
			url: RESTURL.GATEWAY_ACTIVITY + '/search/count'
		},
		searchMasterActivityTypesForRecipient: {
			method: 'POST',
			url: RESTURL.GATEWAY_ACTIVITY + '/search/recipient-type/activity-types'
		},
	});
});
app.factory('PatientDbsTestActivityService', function($resource, RESTURL) {
	return $resource(RESTURL.PATIENT_DBS_TEST_ACTIVITY, { }, {
		search: {
			method: 'POST',
			url: RESTURL.PATIENT_DBS_TEST_ACTIVITY + '/search'
		}
	});
});