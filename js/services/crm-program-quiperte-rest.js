'use strict';

app.constant("RESTURL_QUIPERTE", (function() {
	var BaseV1 = props['rest.v1'];
	return {
		PATIENT_ENROLLMENT: BaseV1 + '/patient-enrollment'
	}
})());

app.factory('PhuturemedService', function($resource, RESTURLQUIPERTE) {
	return $resource(RESTURLQUIPERTE.PHUTUREMED + '/:id', { id: '@id', patientId : '@patientId' }, {
		get: {
			method: 'GET',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURLQUIPERTE.PHUTUREMED + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURLQUIPERTE.PHUTUREMED
		},
		update: {
			method: 'PUT',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:id'
		},
		unpairing: {
			method: 'POST',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:id/unpairing'
		},
		generatePin: {
			method: 'POST',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:id/generate-pin'
		},
		state: {
			method: 'GET',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:id/state'
		},
		patientAdherences: {
			method: 'GET',
			url: RESTURLQUIPERTE.PHUTUREMED + '/:patientId/adherences'
		}
	});
});

app.factory('PhuturemedStateService', function($resource, RESTURLQUIPERTE) {
	return $resource(RESTURLQUIPERTE.PHUTUREMED_STATE + '/:id', { id: '@id' }, {
		get: {
			method: 'GET',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE + '/search'
		},
		insert: {
			method: 'POST',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE
		},
		update: {
			method: 'PUT',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE + '/:id'
		},
		delete: {
			method: 'DELETE',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE + '/:id'
		},
		ignore: {
			method: 'PUT',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE + '/:id/ignore'
		},
		activate: {
			method: 'PUT',
			url: RESTURLQUIPERTE.PHUTUREMED_STATE + '/:id/activate'
		}
	});
});

app.factory('PdfService', function($resource, RESTURLQUIPERTE) {
	return $resource(RESTURLQUIPERTE.PDF + '/:type', { type: '@type' }, {
		downloadPdf: {
			method: 'POST',
			url: RESTURLQUIPERTE.PDF + '/:type'
		}
	});
});

app.factory('UserServiceConfService', function($resource, RESTURL) {
	return $resource(RESTURL.USER_SERVICE_CONF, { id : '@id', userId : '@userId', masterActivityId: '@masterActivityId', uniqueKey: '@uniqueKey', serviceId: '@serviceId', selectectServiceConfId: '@selectectServiceConfId'}, {
		get: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/:id'
		},
		search: {
			method: 'POST',
			url: RESTURL.USER_SERVICE_CONF + '/search'
		},
		update: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id'
		},
		searchByUser: {
			method: 'POST', 
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/search'
		},
		activate: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/activate'
		},
		suspend: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/suspend'
		},
		terminate: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/terminate'
		},
		switchToAlternatives: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/switch-to-alternatives'
		},
		start: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/start'
		},
		getUserManualService: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/manual-services'
		},
		getUserManualMasterActivities: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/manual-activities'
		},
		startUserMasterActivityManually: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/activity/:masterActivityId/start-manually'
		},
		checkServices: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/check-services'
		},
		getDependingServices: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/:id/depending-services/:uniqueKey'
		},
		getUserDependingServicesGroups: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/depending-services-groups'
		},
		startUserServiceManually: {
			method: 'POST',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/service/:serviceId/start-manually'
		},
		updateDependingService: {
			method: 'PUT',
			url: RESTURL.USER_SERVICE_CONF + '/:id/depending-services'
		},
		getServiceActivityGroups: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/activity-groups'
		},
		getServiceActivityGroupsForUser: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/activity-groups/user/:userId'
		},
		getServiceConfMasterActivityName: {
			method: 'GET',
			url: RESTURL.USER_SERVICE_CONF + '/service-conf-master-activity-name'
		},
		startUserServiceManuallyCustom: {
			method: 'POST',
			url: RESTURL.USER_SERVICE_CONF + '/user/:userId/service/:serviceId/start-manually-custom'
		}
	});
});

app.factory('PatientEnrollmentService', function($resource, RESTURL_QUIPERTE) {
	return $resource(RESTURL_QUIPERTE.PATIENT_ENROLLMENT + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		insertAndUpload: {
			method: 'POST',
			url: RESTURL_QUIPERTE.PATIENT_ENROLLMENT+'/insert-upload'
		},
		insertAndUploadMultiple: {
			method: 'POST',
			url: RESTURL_QUIPERTE.PATIENT_ENROLLMENT+'/insert-upload-multiple'
		},
		search: {
			method: 'POST',
			url: RESTURL_QUIPERTE.PATIENT_ENROLLMENT+'/search'
		},
		update: {
			method: 'PUT'
		},
		delete: {
			method: 'DELETE',
			url: RESTURL_QUIPERTE.PATIENT_ENROLLMENT + '/:id'
		},
		downloadUrl: {
			method: 'GET',
			url: RESTURL_QUIPERTE.PATIENT_ENROLLMENT + '/:id/url'
		}
		
	});
});

app.factory('LinkService', function($resource, RESTURL) {
	return $resource(RESTURL.LINK + '/:id', { id: '@id' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.LINK+'/search'
		}
	});
});

app.factory('PatientService', ['$resource', 'RESTURL', function($resource, RESTURL) {
	var PatientService =  $resource(RESTURL.PATIENT + '/:id', { id: '@id', delegateIndex: '@delegateIndex' }, {
		get: {
			method: 'GET'
		},
		search: {
			method: 'POST',
			url: RESTURL.PATIENT + '/search'
		},
		exportXls: {
			method: 'POST',
			url: RESTURL.PATIENT
		},
		update: {
			method: 'PUT'
		},
		insert: {
			method: 'POST'
		},
		delete: {
			method: 'DELETE'
		},
		searchAllUser: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:id/user/search'
		},
		getDelegate: {
			method: 'GET',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		markDelegate: {
			method: 'POST',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		deleteDelegate: {
			method: 'DELETE',
			url : RESTURL.PATIENT + '/:id/delegate/:delegateIndex'
		},
		getCaregiver:{
			method: 'GET',
			url : RESTURL.PATIENT + '/:id/caregiver/'			
		},
		markCaregiver: {
			method: 'POST',
			url : RESTURL.PATIENT + '/:id/caregiver/'
		},
		deleteCaregiver: {
			method: 'DELETE',
			url : RESTURL.PATIENT + '/:id/caregiver/'
		},
		activate: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/activate'
		},
		suspend: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/suspend'
		},
		terminate: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/terminate'
		},
		updateAuthorizationKeys:{
			method: 'PUT',
			url: RESTURL.PATIENT + '/authorization-keys/:id'
		},
		activateNotification: {
			method: 'PUT',
			url : RESTURL.PATIENT + '/:id/activate-notification'
		},
		inviteActivationAll:{
			method: 'POST',
			url: RESTURL.PATIENT + '/search/invite-all'
		},
		countSearchInvite: {
			method: 'POST',
			url: RESTURL.PATIENT + '/search/count'
		},
		updateAvatar: {
			method: 'PUT',
			url: RESTURL.PATIENT + '/:id/avatar'
		},
		searchApprovals: {
			method: 'GET',
			url: RESTURL.PATIENT + '/approvals'
		},
		delegateHcpsActive: {
			method: 'PUT',
			url: RESTURL.PATIENT +'/delegate-hcp-active'
		},
		diaryDetails: {
			method: 'POST',
			url: RESTURL.PATIENT + '/:id/diary'
		}
	});
	PatientService.getUrl = function() {
		return ''+RESTURL.PATIENT;
	}
	return PatientService;
}]);