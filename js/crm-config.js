'use strict';

app.config(
	function($controllerProvider, $compileProvider, $filterProvider, $provide) {

		// lazy controller, directive and service
		app.controller = $controllerProvider.register;
		app.directive = $compileProvider.directive;
		app.filter = $filterProvider.register;
		app.factory = $provide.factory;
		app.service = $provide.service;
		app.constant = $provide.constant;
		app.value = $provide.value;
		//$compileProvider.debugInfoEnabled(false);
	}
);

app.config(
	function($translateProvider) {
		// Register a loader for the static files
		// So, the module will search missing translation tables under the specified urls.
		// Those urls are [prefix][langKey][suffix].
		/*
		$translateProvider.useStaticFilesLoader({
			prefix : 'l10n/',
			suffix : '.js'
		});
		*/
		// Tell the module what language to use by default
		$translateProvider.preferredLanguage('it');
		// Tell the module to store the language in the local storage
		$translateProvider.useLocalStorage();
	}
);

app.config(function($httpProvider) {
	
	if (!$httpProvider.defaults.headers.get) {
		$httpProvider.defaults.headers.get = {};
	}
	
	$httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
	$httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
	
	$httpProvider.defaults.useXDomain = true;
	$httpProvider.defaults.withCredentials = true;
	delete $httpProvider.defaults.headers.common['X-Requested-With'];
	
	/*
	 * Register an interceptor to manage error responses and
	 * set content-type and Auth Token for each request
	 */
	$httpProvider.interceptors.push(function($q, $rootScope, $location, $window, $injector, $timeout, $cookies) {
		return {
			'request' : function(config) {
				if(!angular.isDefined(config.data) || config.data == null){
					config.data = '';
				}
				
				if($cookies.getObject('italiassistenza_ua') !== undefined && (config.skipAuth === undefined || !config.skipAuth)) {
					config.headers['X-AUTH-TOKEN'] = $cookies.getObject('italiassistenza_ua')[1];
				}
				return config || $q.when(config);
			},
			'response': function(response) {
				var cookieNickName = $cookies.getObject('italiassistenza_ua') !== undefined ? $cookies.getObject('italiassistenza_ua')[0] : undefined;
				if($rootScope.loggedUser !== undefined && cookieNickName !== undefined && cookieNickName !== $rootScope.loggedUser.nickName) {
					//$location.path('/login'); 
				}
				return response;
			},
			'responseError': function(rejection) {
				var $state = $injector.get("$state");
				if(rejection.status === 401 && $state.current.name != 'access.login') {
					$state.go("access.login");
				}
				else if(rejection.status === 403) {
					if(angular.isDefined(rejection.data) && angular.isDefined(rejection.data.code) && rejection.data.code == "USER_AUTHORIZATION_KEY_NOT_ALLOWED"){
						$state.go('disclaimer');
					}
					else{
						if($cookies.getObject('italiassistenza_ua') !== undefined) {
							$state.go('access.denied');	
						}
						else {
							$state.go("access.login");
						}
					}
				
				}
//				else if(rejection.status === 404) {
//					$state.go("access.not-found");
//				}
				else {
					if(rejection.status == 0) {
						return $q.defer().promise;
					}
				}
				return $q.reject(rejection);
			}
		};
	});
});


app.constant("RESTURL", (function() {
	var BaseV1 = props['rest.v1'];
	return {
		BASE_V1: BaseV1,
		LOGIN: BaseV1 + '/login',
		LOGOUT: BaseV1 + '/logout',
		QUALIFICATION: BaseV1 + '/qualification',
		MEDICALCENTER: BaseV1 + '/medicalcenter',
		DEPARTMENT: BaseV1 + '/department',
		CONSTANT: BaseV1 + '/constant',
		PORTAL_AUTHORIZATION_KEY: BaseV1 + '/public/portal-authorization-key',
		REDIS_RUNNING_STATUS: BaseV1 + '/redis-restart',
		REDIS_STATUS: BaseV1 + '/redis-status',
		SUPPLIER: BaseV1 + '/supplier',
		SUPPLIER_USER: BaseV1 + '/supplier-user',
 		CUSTOMER: BaseV1 + '/customer',
 		PATIENT: BaseV1 + '/patient',
 		PATIENT_USER: BaseV1 + '/patient-user',
 		HCP: BaseV1 + '/hcp',
 		DISEASE : BaseV1 + '/disease',
 		PATIENT_THERAPY : BaseV1 + '/patient/therapy',
 		DOCUMENT: BaseV1 + '/document',
		DOCUMENT_CATEGORY: BaseV1 + '/document-category',
 		THERAPY: BaseV1 + '/therapy',
 		USER:  BaseV1 + '/user',
		INTERNALSTAFFUSER: BaseV1 + '/internal-staff-user',
		CUSTOMER_USER: BaseV1 + '/customer-user',
		PROGRAM_PROPERTY: BaseV1 + '/program-property',
		SERVICE_CONF: BaseV1 + '/service',
		MASTER_ACTIVITY_CONF: BaseV1 + '/master-activity',
		USER_SERVICE_CONF: BaseV1 + '/user-service',
		USER_MASTER_ACTIVITY_CONF: BaseV1 + '/user/master-activity',
		USER_ACTIVITY: BaseV1 + '/activity',
		TERRITORY: BaseV1 + '/territory',
		NATIONALITY: BaseV1 + '/nationality',
		PERMISSION: BaseV1 + '/permission',
		STATS: BaseV1 + '/stats',
		USER_MESSAGE: BaseV1 + '/message',
		DYNAMIC_PDF: BaseV1 + '/pdf',
		STORED_EXPORT: BaseV1 + '/export',
		WORKFLOW: BaseV1 + '/workflow',
		QUESTIONNAIRE: BaseV1 + '/questionnaire',
		USERQUESTIONNAIRE: BaseV1 + '/user-questionnaire',
		AVAILABILITIES: BaseV1 + '/availability',
		PHARMACOVIGILANCEEVENT: BaseV1 + '/pv-event',
		DEVICE_TYPE: BaseV1 + '/device-type',
		MATERIAL_DELIVERY: BaseV1 + '/material-delivery',
		MATERIAL_DAMAGE: BaseV1 + '/material-damage',
		MATERIAL: BaseV1 + '/material',
		ENTITY_CUSTOM_PROPERTY_CONF: BaseV1 + '/entity-custom-property-conf',
		LOGGED_SMS: BaseV1 + '/logged-sms',
		TRAINING_SESSION: BaseV1 + '/training-session',
		TRAINING_SUBJECT: BaseV1 + '/training-subject',
		BLOOD_DRAWING_CENTER: BaseV1 + '/blood-drawing-center',
		PATIENT_BLOOD_DRAWING_CENTER: BaseV1 + '/patient-blood-drawing-center',
		DRUG_PICK_UP: BaseV1 + '/drug-pick-up',
		DRUG_PICK_UP_ITEM: BaseV1 + '/drug-pick-up-item',
		DRUG_DISPOSAL: BaseV1 + '/drug-disposal',
		DRUG_DISPOSAL_ITEM: BaseV1 + '/drug-disposal-item',
		DRUG_DELIVERY: BaseV1 + '/drug-delivery',
		DRUG_DELIVERY_ITEM: BaseV1 + '/drug-delivery-item',
		PHARMACY: BaseV1 + '/pharmacy',
		LOGISTIC: BaseV1 + '/logistic',
		LOGISTIC_USER: BaseV1 + '/logistic-user',
		PATIENT_PHARMACY: BaseV1 + '/patient-pharmacy',
		PATIENT_TEMPORARY_HOME_ADDRESS: BaseV1 + '/patient-temporary-home-address',
		COMMUNICATION_MESSAGE: BaseV1 + '/communication-message',
		ALL_VIEW_PERMISSIONS: BaseV1 + '/permission/all-view',
		BLOOD_SAMPLING: BaseV1 + '/bloodsampling',
		PATIENT_WEIGHT: BaseV1 + '/patient-weight',
		PHARMACY_TRAINING_SESSION: BaseV1 + '/pharmacy-training-session',
		DRUG: BaseV1 + '/drug',
		PERSONAL_EVENT: BaseV1 + '/personal-event',
		PERSONAL_EVENT_TYPE: BaseV1 + '/personal-event-type',
		DRUG_PICK_UP_DELIVERY: BaseV1 + '/drug-pick-up-delivery',
		DEFAULT_ACTIVITY_VALUE: BaseV1 + '/default-activity-value',
		PATIENT_MATERIAL: BaseV1 + '/user-material',
		PATIENT_MATERIAL_HANDLING: BaseV1 + '/patient-material-handling',
		USER_EVENT: BaseV1 + '/user-event',
		AVATAR: BaseV1 + '/avatar',
		USER_COMPLEMENTARY_ACTIVITY: BaseV1 + '/complementary-activity',
		PATIENT_DIGITAL_ENROLLMENT: BaseV1 + '/patient-digital-enrollment',
		NOTIFICATION_CONF: BaseV1 + '/notification-conf',
		USER_DISCLAIMER_PUBLIC: BaseV1 + '/public/disclaimer',
		LINK: BaseV1 + '/link',
		DRIED_BLOOD_SPOT_TEST: BaseV1 + '/dried-blood-spot-test',
		PATIENT_DOSSIER: BaseV1 + '/patient-dossier',
		VIRTUAL_MEETING: BaseV1 + '/virtual-meeting',
		RELATED_PORTALS: BaseV1 + '/related-portals',
		GATEWAY_HCP: BaseV1 + '/gateway/hcp',
		GATEWAY_PATIENT: BaseV1 + '/gateway/patient',
		GATEWAY_USER: BaseV1 + '/gateway/user',
		GATEWAY_ACTIVITY: BaseV1 + '/gateway/activity',
		GATEWAY_DRUG: BaseV1 + '/gateway/drug',
		PATIENT_DBS_TEST_ACTIVITY: BaseV1 +  '/patient-dbstest-activity'
	}
})());

app.constant("DATE_TIME_FORMATS", {
	DATE : 'ddd DD MMM YYYY',
	DATETIME : 'ddd DD MMM YYYY HH:mm',
	TIME : 'HH:mm'
});


app.constant("REGEXP", {
	EMAIL : /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
	PHONE_NUMBER : /^\+39[0-9]{6,}$/,
	ZIP_CODE : /^[0-9]{5}$/,
	NICK_NAME : /^[A-Za-z\.\-_0-9]{6,}$/,
	PASSWORD : /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/,
	FISCAL_CODE: /^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z]$/i,
	VAT_NUMBER:/^[0-9]{11}$/,
	VAT_NUMBER_OR_FISCAL_CODE: /^([A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{3}[A-Z])|([0-9]{11})$/i
})


app.constant("SMART_TABLE_CONFIG", {
	TABLE_SIZE : 25,
}) 


